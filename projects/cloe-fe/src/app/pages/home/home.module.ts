import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { UIRouterModule } from '@uirouter/angular';
import { tabsComponents, tabsStates } from './home-tabs';
import { FeCoreModule } from '@fe-core/fe-core.module';
import { HOME_RESOLVERS } from './home.resolvers';
import { homeBoxesComponents } from './home-boxes';

const state = {
  name: 'home',
  parent: 'site-context-view',
  url: '',
  component: HomeComponent,
  resolve: [
    ...HOME_RESOLVERS
  ],
  data: {
    breadcrumbLabel: 'component.breadcrumbs.home'
  },
  redirectTo: 'tab-site-consumption'
};

const rootState = {
  name: 'root',
  url: '/',
  redirectTo: 'home'
};

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [state, rootState, ...tabsStates]}),
    FeCoreModule
  ],
  declarations: [HomeComponent, ...tabsComponents, ...homeBoxesComponents]
})
export class HomeModule {
}
