import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { TABLE_CONFIG } from './tab-cluster-consumption-table.config';
import {ConsumptionService} from '@cloe-core/@core/services/consumption-service/consumption-service';
import { find } from 'lodash';

@Component({
  selector: 'cloe-tab-site-clusters',
  templateUrl: './tab-site-clusters.component.html',
  // styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TabSiteClustersComponent implements OnInit {

  constructor(private consumptionService: ConsumptionService) {
  }

  @Input() clusterCategories;
  @Input() currentClusterUser;
  @Input() currentClusterConsumption;
  @Input() user;
  tableConfig = TABLE_CONFIG;
  tableTitleArgs: any = {};
  clusterCategoryName: any;

  ngOnInit() {
    this._initTableTitle();
  }

  private _initTableTitle() {
    this.clusterCategoryName = {
      clusterCategoryName : this.currentClusterUser.name
    };
  }

  onChangeClusterCategory(treeUuid) {
    this.currentClusterUser = find(this.clusterCategories, {uuid: treeUuid});
    this.consumptionService.getConsumptionByCluster(treeUuid, this.user).subscribe(resp => {
      this.currentClusterConsumption = resp;
      this._initTableTitle();
    });
  }
}
