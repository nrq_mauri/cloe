import { TabSiteDetailComponent } from './tab-site-detail/tab-site-detail.component';
import { Resolvable, Transition } from '@uirouter/angular';
import { HomeTabsComponent } from './home-tabs.component';
import { TabSiteConsumptionComponent } from './tab-site-consumption/tab-site-consumption.component';
import { TabSiteClustersComponent } from './tab-site-clusters/tab-site-clusters.component';
import {ConsumptionService} from '@cloe-core/@core/services/consumption-service/consumption-service';
import { TabSiteRangeComponent } from './tab-site-range/tab-site-range.component';
import { rangesResolver, yearlyMeasuresResolver } from './tab-site-range/tab-site-range.resolvers';

export const tabs = [
  {
    name: 'page.home.tabs.site-detail',
    ngState: 'tab-site-detail'
  },
  {
    name: 'page.home.tabs.site-consumption',
    ngState: 'tab-site-consumption'
  },
  {
    name: 'page.home.tabs.site-cluster',
    ngState: 'tab-site-clusters'
  },
  {
    name: 'page.home.tabs.site-ranges',
    ngState: 'tab-site-ranges'
  }
];

export const tabsComponents = [
  HomeTabsComponent,
  TabSiteDetailComponent,
  TabSiteConsumptionComponent,
  TabSiteClustersComponent,
  TabSiteRangeComponent
];

const siteConsumptionResolver = new Resolvable(
  'siteConsumptions',
  (consumptionService, site, user) => {
    if (site) {
      return consumptionService.getConsumptionsBySite(site.uuid, user).toPromise();
    }
    return null;
  },
  [ConsumptionService, 'site', 'user']
);

export const tabsStates = [
  {
    name: 'home-tabs',
    params: {
      site: null,
      item: null
    },
    url: '/site',
    parent: 'home',
    abstract: true,
    data: { breadcrumbLabel: undefined },
    component: HomeTabsComponent,
    resolve: [siteConsumptionResolver]
  },
  {
    name: 'tab-site-detail',
    parent: 'home-tabs',
    url: '/detail',
    data: {
      stateAs: 'home',
      breadcrumbLabel: 'component.breadcrumbs.site-detail'
    },
    component: TabSiteDetailComponent
  },
  {
    name: 'tab-site-consumption',
    parent: 'home-tabs',
    url: '/consumption',
    data: {
      stateAs: 'home',
      breadcrumbLabel: 'component.breadcrumbs.site-consumption'
    },
    component: TabSiteConsumptionComponent
  },
  {
    name: 'tab-site-clusters',
    parent: 'home-tabs',
    url: '/clusters',
    data: {
      stateAs: 'home',
      breadcrumbLabel: 'component.breadcrumbs.site-clusters'
    },
    component: TabSiteClustersComponent
  },
  {
    name: 'tab-site-ranges',
    parent: 'home-tabs',
    url: '/ranges',
    data: {
      stateAs: 'home',
      breadcrumbLabel: 'component.breadcrumbs.site-ranges'
    },
    component: TabSiteRangeComponent,
    resolve: [rangesResolver, yearlyMeasuresResolver]
  }
];
