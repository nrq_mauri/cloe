import { Component, Input, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { TABLE_CONFIG } from './tab-consumption-table.config';

@Component({
  selector: 'cloe-tab-site-consumption',
  templateUrl: './tab-site-consumption.component.html',
  encapsulation: ViewEncapsulation.None
})
export class TabSiteConsumptionComponent implements OnInit {

  @Input() siteConsumptions;
  @Input() site;
  tableData = [];
  tableConfig = TABLE_CONFIG;
  tableTitleArgs: any = {};

  constructor() {
  }

  ngOnInit() {
    if (this.site) {
      this.tableTitleArgs.siteName = this.site.name;
    }
    this.tableData = this.siteConsumptions;
  }
}
