import {ICloeTableStatic} from '@cloe-core/@core/@ui/components/cloe-table-static/cloe-table-static-interface/cloe-table-static-interface';

export const TABLE_CONFIG: ICloeTableStatic = {
  header: [
    {
      fieldName: 'nameWithSymbol',
      columnName: 'global.channel',
      rowHeader: true
    },
    {
      fieldName: 'totalMonth',
      columnName: 'global.totalMonth',
      pipe: 'engFormatWithUnit',
      pipeArgField: 'unitSymbol'
    },
    {
      fieldName: 'totalMonthLastYear',
      columnName: 'global.totalMonthLastYear',
      pipe: 'engFormatWithUnit',
      pipeArgField: 'unitSymbol'
    },
    {
      fieldName: 'yearToDate',
      columnName: 'global.yearToDate',
      pipe: 'engFormatWithUnit',
      pipeArgField: 'unitSymbol'
    },
    {
      fieldName: 'yearToDateLastYear',
      columnName: 'global.yearToDateLastYear',
      pipe: 'engFormatWithUnit',
      pipeArgField: 'unitSymbol'
    },
    {
      fieldName: 'totalLastYear',
      columnName: 'global.totalLastYear',
      pipe: 'engFormatWithUnit',
      pipeArgField: 'unitSymbol'
    },
    {
      fieldName: 'co2Month',
      columnName: 'global.co2Month',
      pipe: 'engFormat'
    },
    {
      fieldName: 'cosFiMonth',
      columnName: 'global.cosFiMonth',
      pipe: 'engFormat',
      pipeArgs: 'cosfi'
    }
  ],
  metadata: {
    tableTitle: 'page.home.table.site-consumption-title',
    tableTitleKey: 'page.home.table.site-consumption-title',
  },
  pagination: {
    'size': 10,
    'page': 1,
    'sizeOptions': [10, 30, 50],
    'maxSize': 5
  }
};
