import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import {SubjectsService} from '@cloe-core/@core/services/resize-service/subjects.service';


@Component({
  selector: 'cloe-home-tabs',
  template: '<ui-view></ui-view>',
  encapsulation: ViewEncapsulation.None
})
export class HomeTabsComponent implements OnInit {

  @Input() site;
  constructor(private subjectService: SubjectsService) {
  }

  ngOnInit() {
    this.subjectService.homeSiteLoadedInformer$.next(this.site);
  }
}
