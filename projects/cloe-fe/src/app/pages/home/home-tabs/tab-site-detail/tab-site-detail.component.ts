import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';


@Component({
  selector: 'cloe-site-detail',
  templateUrl: './tab-site-detail.component.html',
  // styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TabSiteDetailComponent implements OnInit {

  @Input() site;
  @Input() customer;
  constructor() {
  }

  ngOnInit() {
  }
}
