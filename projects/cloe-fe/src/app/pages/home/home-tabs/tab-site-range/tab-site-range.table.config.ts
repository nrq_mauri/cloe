import {ICloeTableStatic} from '@cloe-core/@core/@ui/components/cloe-table-static/cloe-table-static-interface/cloe-table-static-interface';

export const TAB_SITE_RANGE_TABLE_CONFIG: ICloeTableStatic = {
  header: [
    {
      fieldName: 'channelName',
      columnName: 'global.name',
      rowspan: 2
    }
  ],
  metadata: {
    tableTitle: 'component.tab-site-range.tableTitle',
    disablePagination: true,
    fixPageSize: 5
  }
};
