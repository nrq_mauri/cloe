import { Component, Input, OnInit } from '@angular/core';
import { TAB_SITE_RANGE_TABLE_CONFIG } from './tab-site-range.table.config';
import { ChannelRangeService } from '@cloe-core/@core/services/channel-range-service/channel-range-service';
import { values, cloneDeep, remove } from 'lodash';

@Component({
  selector: 'cloe-tab-site-range',
  templateUrl: './tab-site-range.component.html',
  styleUrls: ['./tab-site-range.component.scss']
})
export class TabSiteRangeComponent implements OnInit {

  @Input()
  channelRanges: any[];

  @Input()
  yearlyMeasures: any;

  data: any[] = [];

  currentRange;

  tableConfig = cloneDeep(TAB_SITE_RANGE_TABLE_CONFIG);
  constructor(private channelRangeService: ChannelRangeService) { }

  ngOnInit() {
  }

  onSelect(value) {
    this.currentRange = value;
    if (this.yearlyMeasures) {
      const channelRangeAlgorithm = this.channelRangeService.createAlgorithm(value);
      const rates = channelRangeAlgorithm.getRatesMap();
      this.yearlyMeasures[0].data.forEach(m => {
        const rateId = channelRangeAlgorithm.getRangeFromMeasure(m);
        const rate = rates[rateId];
        rate.value = (rate.value || 0) + m.value;
      });

      const channel = values(rates).reduce((acc, rate) => {
        acc[rate.name] = rate.value;
        return acc;
      }, {
        channelName: this.yearlyMeasures[0].channelName + ' (' + this.yearlyMeasures[0].integratedSymbol + ')',
        integratedSymbol: this.yearlyMeasures[0].integratedSymbol
      });

      this.data = [channel];

      remove(this.tableConfig.header, (h: any) => {
        return h.isRate;
      });

      this.channelRangeService.addRatesHeader(
        value,
        values(rates),
        this.tableConfig.header
      );
    }
  }

}
