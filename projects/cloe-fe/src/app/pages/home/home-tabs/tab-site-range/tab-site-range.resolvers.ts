import { ChannelRangeService } from '@cloe-core/@core/services/channel-range-service/channel-range-service';
import { Resolvable } from '@uirouter/angular';
import { HomePageService } from '@fe-core/services/homepage-service/home-page.service';

export const rangesResolver = new Resolvable(
  'channelRanges',
  channelRangeService => channelRangeService.getAllChannelRanges().toPromise(),
  [ChannelRangeService]
);


export const yearlyMeasuresResolver = new Resolvable(
  'yearlyMeasures',
  ( homePageService, userDefaults) => homePageService.getHomePageYearlyMeasures(userDefaults).toPromise(),
  [HomePageService, 'userDefaults']
);
