import { Component, Input, OnInit, QueryList, ViewChildren, ViewEncapsulation } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AgmMarker, LatLngBounds } from '@agm/core';
import { StateService, Transition, TransitionService } from '@uirouter/angular';
import { tabs } from './home-tabs';
import { SubjectsService } from '@cloe-core/@core/services/resize-service/subjects.service';
import { cloneDeep, remove } from 'lodash';

declare var google: any;

@Component({
  selector: 'cloe-page-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {
  @Input() user;
  @Input() siteTree;
  @Input() homeDetails;
  @Input() userDefaults;
  @Input() sitesLeaf;
  map: any;
  sites = [];

  bounds: LatLngBounds;
  @ViewChildren(AgmMarker)
  markers: QueryList<AgmMarker>;

  tabs = cloneDeep(tabs);

  constructor(
    public translate: TranslateService,
    private stateService: StateService,
    private subjectsService: SubjectsService,
    private transitionService: TransitionService
  ) {
  }

  ngOnInit() {
    if (this.sitesLeaf && this.sitesLeaf.length ===  1) {
      remove(this.tabs, t => t.ngState === 'tab-site-clusters');
      this.transitionService.onBefore({ to: 'tab-site-clusters' }, (trans: Transition) => {
        return trans.router.stateService.target('tab-site-ranges');
      });
    } else {
      remove(this.tabs, t => t.ngState === 'tab-site-ranges');
      this.transitionService.onBefore({ to: 'tab-site-ranges' }, (trans: Transition) => {
        return trans.router.stateService.target('tab-site-clusters');
      });
    }
    if (this.siteTree && this.siteTree.ok !== false) {
      this.sites = this.sitesLeaf.map(node => node.referObject);
      this._resetMarkersIcon();
    }
  }

  private _resetMarkersIcon() {
    this.sites.forEach(site => {
      site.iconUrl = '/assets/images/markers/marker-red.png';
      site.zIndex = 0;
    });
  }

  onMapReady(_map) {
    this.map = _map;
    this.bounds = new google.maps.LatLngBounds();
    this.sites.forEach((site) => {
      site.latitude = +site.latitude || +site.cityLatitude;
      site.longitude = +site.longitude || +site.cityLongitude;
      this.bounds.extend(new google.maps.LatLng(site.latitude, site.longitude));
    });

    this.subjectsService.homeSiteLoadedInformer$.subscribe(
      resp => {
        if (!resp) {
          return;
        }
        // We need this timeout because the map is already being center to its bounds
        setTimeout(() => {
          this._setMarkerAsSelectedByUuid(resp['uuid']);
          this._centerMapToMarker(resp);
        }, 250);
        this.subjectsService.homeSiteLoadedInformer$.complete();
      }
    );
  }

  private _setMarkerAsSelectedByUuid(uuid: string) {
    const selectedSite = this.sites.find(item => item.uuid === uuid);
    this._setMarkerAsSelected(selectedSite);
  }

  private _setMarkerAsSelected(marker) {
    this._resetMarkersIcon();
    marker.iconUrl = '/assets/images/markers/marker-blue.png';
    marker.zIndex = 1000;
  }

  private _centerMapToMarker(marker) {
    this.map.setCenter({
      lat: marker.latitude,
      lng: marker.longitude
    });
  }

  private _openSiteDetail(site) {
    this.stateService.go(
      'tab-site-detail',
      {
        item: site,
        site: site['uuid']
      }
    );
  }

  markerClick(marker: any) {
    this._centerMapToMarker(marker);
    this._setMarkerAsSelected(marker);
    this._openSiteDetail(marker);
  }
}
