import { AfterContentInit, Component, Input, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { flatMap, forEach, groupBy, map, mapValues, transform, values, get, set } from 'lodash';
import * as moment from 'moment';
import { LangChangeEvent, TranslateService } from '@ngx-translate/core';
import { HomePageService } from '@fe-core/services/homepage-service/home-page.service';
import { EngFormatService } from '@cloe-core/@core/services/engFormat-service/engFormat-service';
import { IRadarConfig, IRadarSerie } from '@fe-core/services/cloe-am4chart-radar-service/cloe-am4chart-radar-service';


const colors = [
  '#00c8fa',
  '#00a4dd',
  '#3366a9',
  '#dd4b35',
];

const VALUE_FIELD = 'integratedValue';

@Component({
  selector: 'cloe-yearly-detail-box',
  templateUrl: './yearly-detail-box.component.html',
  styleUrls: ['./yearly-detail-box.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class YearlyDetailBoxComponent implements OnInit, AfterContentInit, OnDestroy {

  yearlyDetail = [];
  langSubscription;
  @Input() userDefaults;
  radarConfig: IRadarConfig;

  constructor(
    private translateService: TranslateService,
    private homePageService: HomePageService,
    private engFormatService: EngFormatService
  ) {
  }

  ngOnInit(): void {
    this.homePageService.getHomePageYearlyDetail(this.userDefaults).subscribe((yearlyDetail: any) => {
      this.yearlyDetail = yearlyDetail;
      this._initGraphLabels(this.yearlyDetail);
    });
  }

  private _initGraphLabels(yearlyDetail) {
    this.langSubscription = this.translateService.onLangChange.subscribe((event: LangChangeEvent) => {
      this.radarConfig = this._generateRadarChartOption(
        yearlyDetail,
        event.translations.component['date-picker'].i18n.shortMonths
      );
    });
    // Call instant
    this.translateService.getTranslation(this.translateService.currentLang).subscribe(translations => {
      this.radarConfig = this._generateRadarChartOption(
        yearlyDetail,
        translations.component['date-picker'].i18n.shortMonths
      );
    });
  }

  ngAfterContentInit(): void {
  }

  private _generateRadarChartOption(yearlyDetail: Array<any>, months: any) {
    const allData = flatMap(yearlyDetail, 'data');
    const data = this._parseDataYearlyDate(allData);

    const option: IRadarConfig = {
      divId: 'home_radar',
      categoryAxis: 'category',
      valueAxis: 'value'
    };

    const newData = transform(months, (result, month: string) => result[month] = { category: month }, {});
    const unitSymbol = get(yearlyDetail[0], 'integratedSymbol');

    option.series = map(data, (measures, year) => {
      for (let i = 0; i < months.length; i++) {
        set(newData, '[' + months[i] + '].' + year, measures[i] || 0);
      }
      const serie: IRadarSerie = {};
      serie.name = year;
      serie.strokeWidth = 3;
      serie.dataFieldCategoryAxis = 'category';
      serie.dataFieldValueAxis = year;
      serie.tooltipFunction = (text, target, key) => {
        if ( target.tooltip.dataItem.dataContext ) {
          if ( target.tooltip.dataItem.dataContext[target.dataFields.valueY] ) {
            return target.tooltip.dataItem.dataContext[target.dataFields.categoryX] + ' ' +
              moment({ year: target.dataFields.valueY }).format('YYYY') + ': ' +
              this.engFormatService.engFormat(
                target.tooltip.dataItem.dataContext[target.dataFields.valueY],
                'default',
                unitSymbol
              );
          } else {
            return target.tooltip.dataItem.dataContext[target.dataFields.categoryX] + ' ' +
              moment({ year: target.dataFields.valueY }).format('YYYY') + ': N.A. ';
          }
        } else {
          return this.translateService.instant('page.home.boxes.null-value');
        }
      };
      return serie;
    });
    option.data = values(newData);
    option.legend = true;
    option.cursor = true;
    option.colorSet = colors;
    return option;
  }

  private _parseDataYearlyDate(allData: Array<any>) {
    const result = {};
    const byYear = groupBy(
      allData,
      o => moment(o.timeStamp).year()
    );
    forEach(byYear, (measures, k) => {

      const byMonth =
        mapValues(
          groupBy(
            measures,
            o => (moment(o.timeStamp).month() + 1)
          ),
          o => o[0][VALUE_FIELD]
        );
      result[k] = values(byMonth);
    });
    return result;
  }

  ngOnDestroy(): void {
    this.langSubscription.unsubscribe();
  }
}
