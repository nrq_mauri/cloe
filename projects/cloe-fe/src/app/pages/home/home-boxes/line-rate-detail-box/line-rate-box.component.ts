import { AfterContentInit, Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { flatMap, reverse, slice, sortBy, find, map, reduce } from 'lodash';
import * as _moment from 'moment';
import { colors } from '@fe-core/chart-colors';
import { StorageService } from '@cloe-core/@core/services/storage-service/storage.service';
import { HomePageService } from '@fe-core/services/homepage-service/home-page.service';

@Component({
  selector: 'cloe-line-rate-box',
  templateUrl: './line-rate-box.component.html',
  styleUrls: ['./line-rate-box.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LineRateBoxComponent implements OnInit, AfterContentInit {

  lineRateDetail = [];

  chartOptions = {};
  yesterday = _moment().subtract(1, 'days');
  @Input() userDefaults;

  constructor(private homePageService: HomePageService) {
  }

  ngOnInit(): void {
    this.homePageService.getHomePageLineRate(this.userDefaults).subscribe((lineRateDetail: any) => {
      this.lineRateDetail = lineRateDetail;
      this.chartOptions = this._generateSeriesForGuage(this.lineRateDetail);
    });
  }

  ngAfterContentInit(): void {
  }


  private _generateSeriesForGuage(measures: Array<any>) {
    if (!measures.length) {
      return {
        series: []
      };
    }
    if (measures.length > 10) {

    }
    const limitedData = slice(measures, 0, 10);
    const sortedData = reverse(sortBy(flatMap(limitedData, 'data'), ['value']));
    const channelNameMap = reduce(limitedData, (result, object) => {
      result[object.channelId] = object.channelName;
      return result;
    }, {});
    if (!sortedData.length) {
      return {
        series: []
      };
    }
    const maxValue = sortedData[0].value;
    const series = sortedData.map((item, i) => {
    const serieValue = Math.round(item.value * 1000 / maxValue) / 1000;

      return {
        name: channelNameMap[item.channelId],
        type: 'pie',
        clockWise: true,
        hoverAnimation: false,
        cursor: 'default',
        startAngle: 270,
        center: ['50%', '55%'],
        radius: [75 - (i * 5) + '%', 75 - (i * 5 + 2.5) + '%'],
        label: {
          normal: {
            show: false,
          }
        },
        data: [{
          value: serieValue,
          name: serieValue,
        }, {
          value: 1 - serieValue,
          tooltip: {
            trigger: 'none',
            formatter: ''
          },
          itemStyle: {
            normal: {
              color: 'transparent'
            }
          }
        }]
      };
    });

    return {
      tooltip: {
        trigger: 'item',
        formatter: '{a}({d}%)'
      },
      legend: {
        type: 'scroll',
        left: -5,
        itemGap: 5,
        itemWidth: 8,
        itemHeight: 8,
        icon: 'circle',
        data: map(sortedData, item => {
          return channelNameMap[item.channelId];
        }),
        textStyle: {
          fontFamily: 'OpenSans',
          fontSize: 14,
          color: 'rgba(0, 0, 0, 0.59)',
          padding: [0, 30, 0, 0]
        },
      },
      series: series,
      graph: {
        color: colors
      }
    };
  }
}
