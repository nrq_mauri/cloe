import { AfterContentInit, Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { colors } from '@fe-core/chart-colors';
import * as _moment from 'moment';
import { EngFormatService } from '@cloe-core/@core/services/engFormat-service/engFormat-service';
import { StorageService } from '@cloe-core/@core/services/storage-service/storage.service';
import { HomePageService } from '@fe-core/services/homepage-service/home-page.service';

@Component({
  selector: 'cloe-weekly-detail-box',
  templateUrl: './weekly-detail-box.component.html',
  styleUrls: ['./weekly-detail-box.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WeeklyDetailBoxComponent implements OnInit, AfterContentInit {

  lastWeekDetail = [];
  chartOptions = {};
  @Input() userDefaults;

  constructor(
    private engFormatService: EngFormatService,
    private homePageService: HomePageService
    ) {
  }

  ngOnInit(): void {
    this.homePageService.getHomePageLastWeekDetail(this.userDefaults).subscribe((lastWeekDetail: any) => {
      this.lastWeekDetail = lastWeekDetail;
      this.chartOptions = this._getBarChartData(this.lastWeekDetail);
    });
  }

  ngAfterContentInit(): void {
  }

  private _getRangeOfDates(start, end, key, arr = [start.startOf(key)]) {
    if (start.isAfter(end)) {
      throw new Error('start must precede end');
    }
    const next = _moment(start).add(1, key).startOf(key);
    if (next.isAfter(end, key)) {
      return arr;
    }
    return this._getRangeOfDates(next, end, key, arr.concat(next));
  }

  private _getCategoryAxisData() {
    const startDate = _moment().subtract(7, 'days');
    const endDate = _moment().subtract(1, 'days');
    return this._getRangeOfDates(startDate, endDate, 'day').map(d => d.date());
  }

  private _getBarChartData(charResponse) {
    const series = charResponse.map((channel, i) => {
      return {
        name: this.homePageService.formatChannelName(channel),
        type: 'bar',
        stack: channel.stackName || 'line',
        cursor: 'default',
        data: channel.data.map(m => (Math.round(m.value * 10)) / 10),
        itemStyle: {
          normal: {
            color: colors[i % 10]
          },
          emphasis: {
            color: colors[i % 10],
          }
        }
      };
    });
    return {
      legend: {
        type: 'scroll',
        left: -5,
        itemGap: 5,
        itemWidth: 8,
        itemHeight: 8,
        icon: 'circle',
        data: charResponse.map(channel => this.homePageService.formatChannelName(channel)),
        textStyle: {
          fontFamily: 'OpenSans',
          fontSize: 14,
          color: 'rgba(0, 0, 0, 0.59)',
          padding: [0, 30, 0, 0]
        },
      },
      grid: {
        top: 50,
        left: 'left',
        right: 0,
        bottom: 20,
        containLabel: true
      },
      xAxis: [
        {
          type: 'category',
          offset: 5,
          data: this._getCategoryAxisData(),
          axisTick: {
            show: true
          },
          axisLine: {
            show: true
          },
          axisLabel: {
            fontFamily: 'OpenSans',
            fontSize: 14,
            color: 'rgba(0, 0, 0, 0.59)'
          },
        }
      ],
      yAxis: [
        {
          type: 'value',
          offset: 20,
          axisLine: {
            show: false
          },
          axisTick: {
            show: false
          },
          axisLabel: {
            fontFamily: 'OpenSans',
            fontSize: 10,
            color: 'rgba(0, 0, 0, 0.59)',
            align: 'left',
            margin: 30,
            formatter: value => this.engFormatService.engFormat(value, 'value-axis')
          },
          splitLine: {
            lineStyle: {
              color: 'rgba(0, 0, 0, 0.11)'
            }
          }
        }
      ],
      tooltip: {
        trigger: 'item',
        formatter: (params, ticket, callback) => {
          const name = params.seriesName;
          const value = this.engFormatService.engFormat(params.value);
          return `${name}: ${value}`;
        }
      },
      series: series
    };
  }
}
