import { AfterContentInit, Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import * as _moment from 'moment';
import { maxBy } from 'lodash';
import { TranslateService } from '@ngx-translate/core';
import { HomePageService } from '@fe-core/services/homepage-service/home-page.service';
import { StorageService } from '@cloe-core/@core/services/storage-service/storage.service';


@Component({
  selector: 'cloe-channel-detail-box',
  templateUrl: './channel-detail-box.component.html',
  styleUrls: ['./channel-detail-box.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ChannelDetailBoxComponent implements OnInit, AfterContentInit {

  channelDetail;
  currentMonth = _moment().format('MMMM YYYY');
  details = [];
  peakDate;
  peakValue;
  @Input() userDefaults;

  constructor(
    private translateService: TranslateService,
    private homePageService: HomePageService
  ) {
  }

  ngOnInit(): void {
    const user = StorageService.getCurrentUser();
    this.homePageService.getHomePageDetail(this.userDefaults).subscribe((channelDetail: any) => {
      this.channelDetail = channelDetail;
      const maxMeasure = channelDetail.maxMeasure;
      this.peakDate = new Date(maxMeasure.timeStamp);
      this.peakValue = maxMeasure.value;
      this.translateService.onLangChange.subscribe(() => {
        this.currentMonth = _moment().format('MMMM YYYY');
      });
    });
  }

  ngAfterContentInit(): void {
  }

  getDeltaChevron(delta) {
    if (delta < 0) {
      return 'fa-caret-up';
    }

    return 'fa-caret-down';
  }
}
