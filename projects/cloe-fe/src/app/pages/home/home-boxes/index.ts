import { ChannelDetailBoxComponent } from './channel-detail-box/channel-detail-box.component';
import { WeeklyDetailBoxComponent } from './weekly-detail-box/weekly-detail-box.component';
import { LineRateBoxComponent } from './line-rate-detail-box/line-rate-box.component';
import { YearlyDetailBoxComponent } from './yearly-detail-box/yearly-detail-box.component';

export const homeBoxesComponents = [
  ChannelDetailBoxComponent,
  WeeklyDetailBoxComponent,
  LineRateBoxComponent,
  YearlyDetailBoxComponent
];
