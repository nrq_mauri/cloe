import { ConsumptionService } from '@cloe-core/@core/services/consumption-service/consumption-service';
import { Resolvable } from '@uirouter/angular';
import { ClusterService } from '@cloe-core/@core/services/cluster-service/cluster-service';
import { ChannelRangeService } from '@cloe-core/@core/services/channel-range-service/channel-range-service';
import { HomePageService } from '@fe-core/services/homepage-service/home-page.service';
import { CloeTreeNodeService } from '@cloe-core/@core/services/cloe-treeNode-service/cloe-tree-node.service';

const clusterCategoriesResolver = new Resolvable(
  'clusterCategories',
  (clusterService, user) => clusterService.getClusterForUser(user.cloeUser.uuid).toPromise(),
  [ClusterService, 'user']
);

const currentClusterUserResolver = new Resolvable(
  'currentClusterUser',
  (clusterCategories) => (clusterCategories && clusterCategories[0]) || {},
  ['clusterCategories']
);

const currentClusterConsumptionResolver = new Resolvable(
  'currentClusterConsumption',
  (consumptionService, currentClusterUser, user) => {
    if (currentClusterUser.uuid) {
      return consumptionService.getConsumptionByCluster(currentClusterUser.uuid, user).toPromise();
    }
    return [];
  },
  [ConsumptionService, 'currentClusterUser', 'user']
);

/*const homeDetailsResolver = new Resolvable(
  'homeDetails',
  (measureService, user, site) => measureService.getHomePageDetails(site, user.cloeUser).toPromise(),
  [MeasureService, 'user', 'site']
);*/



export const sitesLeafResolvers = new Resolvable(
  'sitesLeaf',
  ( siteTree, cloeTreeNodeService) => cloeTreeNodeService.getLeafsFromTree(siteTree),
  ['siteTree', CloeTreeNodeService]
);



export const HOME_RESOLVERS = [
  clusterCategoriesResolver,
  currentClusterUserResolver,
  currentClusterConsumptionResolver,
  sitesLeafResolvers
  // homeDetailsResolver
];
