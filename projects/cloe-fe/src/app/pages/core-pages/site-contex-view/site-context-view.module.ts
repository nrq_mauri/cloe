import { NgModule } from '@angular/core';
import { StatesModule, UIRouterModule } from '@uirouter/angular';
import { FeCoreModule } from '@fe-core/fe-core.module';
import { SiteContextViewComponent } from './site-context-view.component';
import { isSingleSiteTarget, SITE_CONTEXT_RESOLVERS } from './site-context-view.resolvers';
import { extend, last } from 'lodash';
import { StorageService } from '@cloe-core/@core/services/storage-service/storage.service';
import { siteContextSubject } from '@fe-core/components';

const siteContextTransitionHook = transition => {
  const $state = transition.router.stateService;
  const toState = transition.$to();
  const newParams = extend({}, transition.params() );

  if (isSingleSiteTarget(toState) && !newParams.singleSiteSelection) {
    newParams.singleSiteSelection = true;
    if (StorageService.getObject('singleSite')) {
      newParams.site = StorageService.getObject('singleSite');
    } else {
      if (StorageService.getObject('sitesMap')) {
        newParams.site = last(Object.keys(StorageService.getObject('sitesMap')));
      }
    }
    siteContextSubject.next({});
    return $state.target(toState.name, newParams);
  } else {
    if (!isSingleSiteTarget(toState) && newParams.singleSiteSelection) {
      newParams.singleSiteSelection = false;
      siteContextSubject.next({});
      return $state.target(toState.name, newParams);
    }
  }
};

const stateModule: StatesModule = {
  states: [
    {
      name: 'site-context-view',
      url: '?site',
      parent: 'logged-fe',
      data: {
        requiresAuth: true
      },
      params: {
        preselectSite: true,
        singleSiteSelection: false,
        singleSiteChanged: false
      },
      abstract: true,
      component: SiteContextViewComponent,
      resolve: [
        ...SITE_CONTEXT_RESOLVERS
      ],
      onRetain: siteContextTransitionHook,
      onEnter: siteContextTransitionHook
    }
  ]
};

@NgModule({
  imports: [
    UIRouterModule.forChild(stateModule),
    FeCoreModule,
  ],
  declarations: [SiteContextViewComponent],
  exports: [UIRouterModule]
})
export class SiteContextViewModule {
}
