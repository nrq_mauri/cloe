import { Resolvable, Transition } from '@uirouter/angular';
import { UserInitService } from '@cloe-core/@core/services/user-init-service/user-init-service';
import { CloeTreeNodeService } from '@cloe-core/@core/services/cloe-treeNode-service/cloe-tree-node.service';
import { StorageService } from '@cloe-core/@core/services/storage-service/storage.service';
import { CloeTreeService } from '@cloe-core/@core/services/cloe-tree-service/cloe-tree.service';


export function isSingleSiteTarget(toState) {
  const multiSiteStatesName = ['home'];
  const checkName = multiSiteStatesName.includes(toState.name);
  // check target state is inherit child of multi-site
  const checkStateAs = multiSiteStatesName.includes(toState.data.stateAs);
  // check is target state is direct child of multi-site
  const checkParent = multiSiteStatesName.includes(toState.parent.name);
  return checkName || checkStateAs || checkParent;
}

const siteTreeResolver = new Resolvable(
  'siteTree',
  (cloeTreeNodeService, user, sitesTree) => {
    return cloeTreeNodeService.getSitesTreeForUser(user, sitesTree[0]).toPromise();
  },
  [CloeTreeNodeService, 'user', 'sitesTree']
);

const sitesTreeResolver = new Resolvable(
  'sitesTree',
  (user, cloeTreeService) => cloeTreeService.searchTreeListForUser('SITE', user.cloeUser.uuid).toPromise(),
  ['user', CloeTreeService]
);

const siteResolver = new Resolvable(
  'site',
  (userDefaults: any) => {
    return userDefaults.defaultSite;
  },
  ['userDefaults']
);

const userDefaultsResolver = new Resolvable(
  'userDefaults',
  (trans, userInitService, siteTree, userDefaultSiteUuid, cloeTreeNodeService, userDefaultChannelUuid, user) => {
    let siteUuid;
    if ( !isSingleSiteTarget(trans.$to()) && StorageService.getObject('sitesMap')) {
      siteUuid = Object.keys(StorageService.getObject('sitesMap'))[0];
    } else {
      siteUuid = trans.params().site;
    }
    if (!siteUuid && trans.params().preselectSite) {
      siteUuid = siteUuid || userDefaultSiteUuid;
      if (!siteUuid && siteTree && siteTree.children) {
        siteUuid = cloeTreeNodeService.findFirstLeafUuid(siteTree.children);
      }
    }
    if (siteUuid) {
      return userInitService.getUserDefaults(siteUuid, userDefaultChannelUuid, user.cloeUser.uuid).toPromise();
    }
    return null;
  },
  [
    Transition,
    UserInitService,
    'siteTree',
    'userDefaultSiteUuid',
    CloeTreeNodeService,
    'userDefaultChannelUuid',
    'user'
  ]
);

const userDefaultChannelResolver = new Resolvable(
  'userDefaultChannelUuid',
  user => user.cloeUser.defaultChannelUuid,
  ['user']
);

const userDefaultSiteResolver = new Resolvable(
  'userDefaultSiteUuid',
  user => user.cloeUser.defaultSiteUuid,
  ['user']
);

const singleSiteSelectResolver = new Resolvable(
  'singleSiteSelection',
  trans => !!trans.params().singleSiteSelection,
  [Transition]
);

export const SITE_CONTEXT_RESOLVERS = [
  userDefaultsResolver,
  siteTreeResolver,
  siteResolver,
  userDefaultChannelResolver,
  userDefaultSiteResolver,
  singleSiteSelectResolver,
  sitesTreeResolver
];
