import { Component, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'cloe-page-site-context',
  templateUrl: './site-context-view.component.html',
  styleUrls: ['./site-context-view.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SiteContextViewComponent {

  @Input() site;
  @Input() siteTree;
  @Input() singleSiteSelection;

}
