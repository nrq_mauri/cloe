import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'cloe-logged-paged',
  template: '<ui-view></ui-view>',
  encapsulation: ViewEncapsulation.None
})
export class LoggedFeComponent implements OnInit {
  @Input() customer;
  styleSheetUrl: string;

  ngOnInit(): void {
    this._setThemeTemplate();
  }

  private _setThemeTemplate() {
    if (this.customer.themeTemplateSuffix) {
      this.styleSheetUrl = 'assets/customTemplates/styles_' + this.customer.themeTemplateSuffix + '.css';
      const link = document.createElement('link');
      link.rel = 'stylesheet';
      link.type = 'text/css';
      link.href = this.styleSheetUrl;
      link.media = 'all';
      document.head.appendChild(link);
    }
  }
}
