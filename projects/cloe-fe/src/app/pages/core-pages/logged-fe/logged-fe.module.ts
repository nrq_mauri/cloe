import { NgModule } from '@angular/core';
import { StatesModule, UIRouterModule } from '@uirouter/angular';
import { FeCoreModule } from '@fe-core/fe-core.module';
import { LoggedFeComponent } from './logged-fe.component';

const stateModule: StatesModule = {
  states: [
    {
      name: 'logged-fe',
      parent: 'logged',
      data: {
        requiresAuth: true
      },
      abstract: true,
      component: LoggedFeComponent
    }
  ]
};

@NgModule({
  imports: [
    UIRouterModule.forChild(stateModule),
    FeCoreModule,
  ],
  declarations: [LoggedFeComponent],
  exports: [UIRouterModule]
})
export class LoggedFeModule {
}
