import { FeCoreModule } from '@fe-core/fe-core.module';
import { StatesModule, UIRouterModule } from '@uirouter/angular';
import { NgModule } from '@angular/core';
import { LoginService } from '@cloe-core/@core/services/login-service/login.service';
import { of } from 'rxjs';
import { catchError, switchMap } from 'rxjs/operators';

const stateModule: StatesModule = {
  states: [
    {
      name: 'impersonate',
      url: '/impersonate/:tokenCode?site',
      parent: 'app',
      data: {
        requiresAuth: false
      },
      onEnter: (trans, state) => {
        const loginService: LoginService = trans.injector().get(LoginService);
        const tokenCode = trans.params().tokenCode;
        const site = trans.params().site;
        const $state = trans.router.stateService;
        return loginService.impersonate(tokenCode).pipe(
          switchMap(() => {
            return of($state.target('home', {site: site}, {location: true, reload: true}));
          }),
        ).toPromise();
      },
      abstract: false
    }
  ]
};

@NgModule({
  imports: [
    UIRouterModule.forChild(stateModule),
    FeCoreModule,
  ],
  exports: [UIRouterModule]
})
export class ImpersonatePageModule {
}
