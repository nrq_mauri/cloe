import { AfterContentInit, AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { remove, map, difference, uniqBy } from 'lodash';
import { SubjectsService } from '@cloe-core/@core/services/resize-service/subjects.service';
import { TREE_ACTIONS, TreeNode } from 'angular-tree-component';
import { Subject } from 'rxjs/internal/Subject';
import { TreeFiltersTypeEnum } from '@cloe-core/@core/services/cloe-tree-service/enums/TreeFiltersTypeEnum';
import { CloeTreeFiltersService } from '@cloe-core/@core/services/cloe-tree-service/cloe-tree-filters.service';
import { NodeCategoryEnum } from '@cloe-core/@core/enums/NodeCategoryEnum';
import { CloeTreeNodeService } from '@cloe-core/@core/services/cloe-treeNode-service/cloe-tree-node.service';
import { StorageService } from '@cloe-core/@core/services/storage-service/storage.service';
import { forkJoin } from 'rxjs';
import { CloeTreeComponent } from '@cloe-core/@core/@ui/components';
import { UserInitService } from '@cloe-core/@core/services/user-init-service/user-init-service';
import { CloeChartService } from '@fe-core/services/cloe-chart-service/cloe-chart-service';
import { SiteContextEnum, siteContextSubject } from '@fe-core/components/site-context-picker/site-context-picker.component';


export enum CHANNEL_VIEW_ACTIONS {
  NODE_ACTIVE = 'active',
  NODE_DEACTIVE = 'deactive',
  NODE_FORCE_ACTIVE = 'force-active', // active node from external component
  NODE_FORCE_DEACTIVE = 'force-active',
  TOGGLE_ACTIVE_TREE = 'toggle-active-tree',
  TREE_DEACTIVE_CLICK = 'tree-deactive-click',
  CLEAR_SELECTION = 'clear-selection',
  DRAG = 'grag'
}

export const channelTreeViewSubject$ = new Subject();

@Component({
  selector: 'cloe-channel-tree-view',
  templateUrl: './channel-tree-view.component.html',
  styleUrls: ['./channel-tree-view.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ChannelTreeViewComponent implements OnInit, AfterContentInit, OnDestroy {
  @ViewChild(CloeTreeComponent) tree: CloeTreeComponent;
  @Input() site;
  @Input() multiSelect;
  @Input() selectDefaultChannel;
  @Input() treeDynamicState;
  userDefaults;
  sidebarOpen;
  id = 1;
  activeChannelsUuidMap = {};
  channelStateSubscription;
  treeDisabled = false;
  filtersConfig;
  channelTreeNode = { name: 'ROOT', nodeCategory: NodeCategoryEnum.ROOT, children: [] };
  siteContextViewSubscription;

  treeConfig: any = {};

  options = {
    allowDrag: node => !node.isActive,
    useCheckbox: false,
    actionMapping: {
      mouse: {
        checkboxClick: (tree, node, $event) =>
          this.multiSelect ? TREE_ACTIONS.TOGGLE_ACTIVE_MULTI(tree, node, $event)
            : TREE_ACTIONS.TOGGLE_ACTIVE(tree, node, $event),
        click: (tree, node, $event) => {
          if (this.treeDisabled) {
            channelTreeViewSubject$.next({
              action: CHANNEL_VIEW_ACTIONS.TREE_DEACTIVE_CLICK
            });
            return;
          }
          if (node.data.nodeCategory !== 'LEAF') {
            return;
          }
          node.toggleSelected();
          if (this.multiSelect) {
            TREE_ACTIONS.TOGGLE_ACTIVE_MULTI(tree, node, $event);
          } else {
            TREE_ACTIONS.TOGGLE_ACTIVE(tree, node, $event);
          }
        },
        dragStart: () => {
          channelTreeViewSubject$.next({
            action: CHANNEL_VIEW_ACTIONS.DRAG,
            value: true
          });
        },
        drop: () => {
          channelTreeViewSubject$.next({
            action: CHANNEL_VIEW_ACTIONS.DRAG,
            value: false
          });
        },
        dragEnd: () => {
          channelTreeViewSubject$.next({
            action: CHANNEL_VIEW_ACTIONS.DRAG,
            value: false
          });
        }
      }
    }
  };

  constructor(
    private userInitService: UserInitService,
    private subjectsService: SubjectsService,
    private cloeChartService: CloeChartService,
    private cloeTreeNodeService: CloeTreeNodeService,
    private treeFiltersService: CloeTreeFiltersService
  ) {
    this.channelStateSubscription = channelTreeViewSubject$.asObservable().subscribe((e: any) => {
      switch (e.action) {
        case CHANNEL_VIEW_ACTIONS.NODE_FORCE_ACTIVE:
          const predicate = n => n.data.referUuid === e.referUuid;
          const node = this.tree.tree.treeModel.getNodeBy(predicate);
          this.activeChannelsUuidMap[e.referUuid] = !e.triggerActiveEvent;
          node.data.axis = e.axis;
          node.toggleSelected();
          node.toggleActivated(true);
          break;
        case CHANNEL_VIEW_ACTIONS.NODE_FORCE_DEACTIVE:
          const deactivePredicate = n => n.data.referUuid === e.referUuid;
          const deactiveNode = this.tree.tree.treeModel.getNodeBy(deactivePredicate);
          deactiveNode.toggleSelected();
          deactiveNode.toggleActivated(true);
          delete this.activeChannelsUuidMap[e.referUuid];
          break;
        case CHANNEL_VIEW_ACTIONS.TOGGLE_ACTIVE_TREE:
          this.treeDisabled = !this.treeDisabled;
          break;
        case CHANNEL_VIEW_ACTIONS.CLEAR_SELECTION:
          this.tree.tree.treeModel.setActiveNode(this.tree.tree.treeModel.getFirstRoot(), false, false);
          this.activeChannelsUuidMap = {};
          break;
      }
    });
  }

  ngOnInit() {
    this.treeConfig = {
      nodeLabelFunction: (node: any) => {
        if (node.isRoot) {
          return 'page.channel-tree-view.channels';
        }
        if (!node.data.referObject) {
          return node.data.name;
        }
        return `${node.data.referObject.name} (${node.data.referObject.unitSymbol})`;
      }
    };
    const sibarCollapseObservable$: Observable<any> = this.subjectsService.sibarCollapseInformer$.asObservable();
    sibarCollapseObservable$.subscribe(isClosed => {
      this.sidebarOpen = !isClosed;
    });
  }

  ngAfterContentInit() {
    this.siteContextViewSubscription = siteContextSubject.asObservable().subscribe((event: any) => {
      switch (event.action) {
        case SiteContextEnum.UPDATE_SITES:
          const sitesToRemove = difference(
            map(this.channelTreeNode.children, (child) => child.rootReferUuid),
            event.data.currentState
          );
          const sitesToAdd = difference(
            event.data.currentState,
            map(this.channelTreeNode.children, (child) => child.rootReferUuid),
          );
          if (sitesToRemove.length > 0) {
            this.tree.tree.treeModel.getActiveNodes().forEach((node) => node.setIsActive(false, true));
            remove(this.channelTreeNode.children, (child: any) => sitesToRemove.includes(child.rootReferUuid));
            this.tree.tree.treeModel.update();
            if (this.channelTreeNode.children.length > 0) {
              this.treeInit(event.data.currentState[0]);
            }
          }
          if (sitesToAdd.length > 0) {
            this.loadChannelsTree(sitesToAdd, event.data.currentState);
          }
          setTimeout(() => this.loadFilters());
          break;
      }
    });
  }

  loadFilters() {
    this.filtersConfig = this.treeFiltersService.addFiltersByType(
      TreeFiltersTypeEnum.CHANNEL_FILTERS,
      [this.channelTreeNode]
    );
  }

  private loadChannelsTree(sitesUuid: string[], allCurrentSites) {
    forkJoin(
      map(sitesUuid, (siteUuid) => {
        return this.cloeTreeNodeService.getTreeNodeByReferUuid(
          'CHANNEL',
          StorageService.getCurrentUser(),
          false,
          siteUuid
        );
      })
    ).subscribe((res) => {
      this.channelTreeNode.children.push(...res);
      this.treeConfig.rootReferUuids = allCurrentSites;
      this.tree.tree.treeModel.update();
      this.treeInit(allCurrentSites[0]);
    });
  }

  treeInit(siteUuid) {
    if (!this.selectDefaultChannel || this.tree.tree.treeModel.activeNodes.length > 0) {
      return;
    }
    this.userInitService.getUserDefaults(
      siteUuid,
      StorageService.getCurrentUser().cloeUser.defaultChannelUuid,
      StorageService.getCurrentUser().cloeUser.uuid
    ).subscribe((res) => {
      this.userDefaults = res;
      let predicate;
      if (this.userDefaults.defaultChannel) {
        predicate = node => {
          return  node.data.referObject && node.data.referObject.uuid === this.userDefaults.defaultChannel.uuid;
        };
      } else {
        predicate = node => node.isLeaf && node.data.referUuid;
      }
      const defaultNode = this.tree.tree.treeModel.getNodeBy(predicate);
      if (defaultNode) {
        defaultNode.toggleSelected();
        defaultNode.setActiveAndVisible(true);
      }
      this.tree.tree.treeModel.update();
    });
  }

  onNodeActive(e: any) {
    if (this.activeChannelsUuidMap[e.node.data.referUuid]) {
      return;
    }
    channelTreeViewSubject$.next({
      action: CHANNEL_VIEW_ACTIONS.NODE_ACTIVE,
      referUuid: e.node.data.referUuid,
      name: e.node.data.name,
      channelPath: this.getChannelNodePath(e.node.id),
      referObject: e.node.data.referObject,
      axis: e.node.data.axis
    });
    this.activeChannelsUuidMap[e.node.data.referUuid] = true;
  }

  onNodeDeactive(e: any) {
    channelTreeViewSubject$.next({
      action: CHANNEL_VIEW_ACTIONS.NODE_DEACTIVE,
      referUuid: e.node.data.referUuid
    });
    delete this.activeChannelsUuidMap[e.node.data.referUuid];
  }

  private getChannelNodePath(nodeId) {
    let path = '';
    const node = this.tree.tree.treeModel.getNodeById(nodeId);
    if (node) {
      node.path.slice(1, (node.path.length - 1)).forEach((ancestorID, index) => {
        const ancestorNode: TreeNode = this.tree.tree.treeModel.getNodeById(+ancestorID);
        path += index === 0 ? ancestorNode.data.name : '> ' + ancestorNode.data.name;
      });
    }
    return path;
  }

  ngOnDestroy(): void {
    this.channelStateSubscription.unsubscribe();
    this.siteContextViewSubscription.unsubscribe();
  }
}
