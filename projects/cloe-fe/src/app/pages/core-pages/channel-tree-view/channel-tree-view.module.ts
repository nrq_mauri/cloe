import { NgModule } from '@angular/core';
import { StateDeclaration, Transition, UIRouterModule } from '@uirouter/angular';
import { ChannelTreeViewComponent } from './channel-tree-view.component';
import { FeCoreModule } from '@fe-core/fe-core.module';
import { extend } from 'lodash';
import { CHANNEL_TREE_VIEW_RESOLVERS } from './channel-tree-view.resolver';

const stateHandler = (trans: Transition, s: StateDeclaration) => {
  const $state = trans.router.stateService;
  let newParams;
  switch (trans.to().name) {
    case 'visualizza-curve':
    case 'tab-view-chart':
    case 'tab-view-table':
      if (!trans.params().multiselect || !trans.params().selectDefaultChannel || !trans.params().treeDynamicState) {
        newParams = extend({}, trans.params(), {
          multiselect: true,
          selectDefaultChannel: true,
          treeDynamicState: true
        });
      }
      break;
    case 'confronta-periodi':
    case 'tab-view-serial-chart':
    case 'tab-view-serial-table':
      if (!trans.params().multiselect || !trans.params().selectDefaultChannel || !!trans.params().treeDynamicState) {
        newParams = extend({}, trans.params(), {
          multiselect: true,
          selectDefaultChannel: true,
          treeDynamicState: false
        });
        return $state.target(trans.to(), newParams);
      }
      break;
    case 'curva-durata':
    case 'tab-view-duration-curves-chart':
    case 'tab-view-duration-curves-table':
      if (!trans.params().multiselect || !trans.params().selectDefaultChannel || !!trans.params().treeDynamicState) {
        newParams = extend({}, trans.params(), {
          multiselect: true,
          selectDefaultChannel: true,
          treeDynamicState: false
        });
        return $state.target(trans.to(), newParams);
      }
      break;
    case 'tab-by-measure':
      if (!trans.params().multiselect || trans.params().selectDefaultChannel  || !!trans.params().treeDynamicState) {
        newParams = extend({}, trans.params(), {
          multiselect: true,
          selectDefaultChannel: false,
          treeDynamicState: false
        });
      }
      break;
    case 'tab-by-period':
      if (!!trans.params().multiselect || !!trans.params().selectDefaultChannel || !!trans.params().treeDynamicState) {
        newParams = extend({}, trans.params(), {
          multiselect: false,
          selectDefaultChannel: false,
          treeDynamicState: false
        });
      }
      break;
  }
  if (newParams) {
    return $state.target(trans.to().name, newParams);
  }
};

const state = {
  name: 'channel-tree-view',
  abstract: true,
  parent: 'site-context-view',
  params: {
    treeDynamicState: null,
    multiselect: null,
    selectDefaultChannel: null
  },
  resolve: [
    ...CHANNEL_TREE_VIEW_RESOLVERS
  ],
  onRetain: stateHandler,
  onEnter: stateHandler,
  component: ChannelTreeViewComponent
};

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [state]}),
    FeCoreModule
  ],
  declarations: [ChannelTreeViewComponent]
})
export class ChannelTreeViewModule {
}
