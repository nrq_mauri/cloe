import { Resolvable, Transition } from '@uirouter/angular';

const multiSelectResolver = new Resolvable(
  'multiSelect',
  trans => !!trans.params().multiselect,
  [Transition]
);

const selectDefaultChannel = new Resolvable(
  'selectDefaultChannel',
  trans => !!trans.params().selectDefaultChannel,
  [Transition]
);

const treeDynamicStateResolver = new Resolvable(
  'treeDynamicState',
  trans => !!trans.params().treeDynamicState,
  [Transition]
);

export const CHANNEL_TREE_VIEW_RESOLVERS = [
  multiSelectResolver,
  selectDefaultChannel,
  treeDynamicStateResolver
];
