import { NgModule } from '@angular/core';
import { ChannelTreeViewModule } from './channel-tree-view/channel-tree-view.module';
import { SiteContextViewModule } from './site-contex-view/site-context-view.module';
import { LoggedFeModule } from './logged-fe/logged-fe.module';
import { ImpersonatePageModule } from './impersonate-page/impersonate-page.module';

@NgModule({
  imports: [
    LoggedFeModule,
    ChannelTreeViewModule,
    SiteContextViewModule,
    ImpersonatePageModule
  ]
})
export class FeCorePagesModule {
}
