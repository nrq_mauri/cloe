import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { DiagnosisService } from '@cloe-core/@core/services/diagnosis-service/diagnosis.service';
import { StateService } from '@uirouter/core';
import { keys, remove } from 'lodash';
import { DiagStatusEnum } from '@cloe-core/@core/enums/DiagStatus';
import { DiagFileTypeEnum } from '@cloe-core/@core/enums/DiagFileTypeEnum';

@Component({
  selector: 'cloe-diag-detail',
  templateUrl: './diag-detail.component.html',
  styleUrls: ['./diag-detail.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DiagDetailComponent implements OnInit {
  diagFileList;
  @Input() diagCode;
  @Input() diag;
  @Input() fileDescriptions;
  jsonData = {};
  file;

  constructor(private diagService: DiagnosisService, private stateService: StateService) {
  }

  ngOnInit(): void {
    if (this.diag.jsonData) {
      this.jsonData = JSON.parse(this.diag.jsonData);
      this._parseDiagFileList(this.jsonData);
    } else {
      this.diagService.getDiagFileList(this.diagCode).subscribe((list: Array<string>) => {
        list.forEach(value => {
          this.jsonData[value] = {};
        });
        this.diagFileList = list;
      });
    }
  }

  private _parseDiagFileList(json) {
    this.diagFileList = keys(json);
  }

  save() {
    if (this.isReadOnly()) {
      return;
    }
    this.diag.jsonData = JSON.stringify(this.jsonData);
    this.diagService.saveDiag(this.diag).subscribe(() => {
      this.cancel();
    });
  }

  getFileDescription(fileName) {
    const descCode = fileName.split('_').shift();
    return this.fileDescriptions[descCode];
  }

  addNew() {
    if (this.isUploadDisable()) {
      return;
    }
    this.diagService.uploadNewFile(this.file, this.diagCode, DiagFileTypeEnum.IMAGE)
      .subscribe((resp: any) => {
        const partsName = resp.message.split('\.');
        const fileName = partsName[0];
        const ext = partsName[1];
        this.jsonData[fileName] = {
          type: ext,
          isImage: this.diagService.isImage(ext),
          fileName: resp.message,
          diagCode: this.diagCode,
          removable: true
        };
        this.diagFileList.push(fileName);
        this.file = undefined;
      });
  }

  cancel() {
    this.stateService.go('diagnosis');
  }

  setFile(files: FileList) {
    this.file = files.item(0);
  }

  isReadOnly() {
    return this.diag.status === DiagStatusEnum.CONFIRMED;
  }

  isUploadDisable() {
    return !this.file || this.isReadOnly();
  }

  onRemove(fileName) {
    const fileNameKey = fileName.split('\.')[0];
    delete this.jsonData[fileNameKey];
    remove(this.diagFileList, o => o === fileNameKey);
  }
}
