import { Resolvable, Transition } from '@uirouter/angular';
import { DiagnosisService } from '@cloe-core/@core/services/diagnosis-service/diagnosis.service';
import { groupBy, map } from 'rxjs/operators';
import { reduce } from 'lodash';


const diagResolver = new Resolvable(
  'diag',
  (diagnosisService, diagCode) => {
    return diagnosisService.getDiagByCode(diagCode).toPromise();
  },
  [DiagnosisService, 'diagCode']
);

const diagCodeResolver = new Resolvable(
  'diagCode',
  (trans) => {
    return trans.params().diagCode;
  },
  [Transition]
);

const diagFileDescriptionsResolver = new Resolvable(
  'fileDescriptions',
  diagService => {
    return diagService.getFileDescriptions()
      .pipe(
        map(r => reduce(r, (a, c: any) => {
          a[c.codeName] = c.description;
          return a;
        }, {}))
      ).toPromise();
  },
  [DiagnosisService]
);

export const DIAG_FILE_LIST_RESOLVERS = [
  diagCodeResolver,
  diagResolver,
  diagFileDescriptionsResolver
];
