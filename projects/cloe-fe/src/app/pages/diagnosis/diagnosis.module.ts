import { NgModule } from '@angular/core';
import { UIRouterModule } from '@uirouter/angular';
import { FeCoreModule } from '@fe-core/fe-core.module';
import { DiagnosisComponent } from './diagnosis.component';
import { DiagDetailComponent } from './diag-detail/diag-detail.component';
import { DIAG_FILE_LIST_RESOLVERS } from './diag-detail/diag-detail.resolvers';

const state = {
  name: 'diagnosis',
  parent: 'logged',
  url: '/diagnosis',
  component: DiagnosisComponent,
  data: {
    stateAs: 'home',
    breadcrumbLabel: 'component.breadcrumbs.diagnosis'
  },
};

const stateDiagDetail = {
  name: 'diag-detail',
  parent: 'logged',
  url: '/diagnosis-detail?diagCode',
  params: {
    diagCode: ''
  },
  component: DiagDetailComponent,
  resolve: DIAG_FILE_LIST_RESOLVERS,
  data: {
    stateAs: 'diagnosis',
    breadcrumbLabel: 'component.breadcrumbs.diagnosis-detail'
  },
};

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [state, stateDiagDetail]}),
    FeCoreModule
  ],
  declarations: [DiagnosisComponent, DiagDetailComponent]
})
export class DiagnosisModule {
}
