import { Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { DIAG_LIST_TABLE_CONFIG, diagnosisSubject$ } from './diagnosis-table.config';
import { StorageService } from '@cloe-core/@core/services/storage-service/storage.service';
import { DiagnosisActionsEnum } from './enums/DiagnosisActionsEnum';
import { DiagnosisService } from '@cloe-core/@core/services/diagnosis-service/diagnosis.service';
import { DiagStatusEnum } from '@cloe-core/@core/enums/DiagStatus';
import { StateService } from '@uirouter/core';
import { CloeTableComponent } from '@cloe-core/@core/@ui/components/cloe-table';
import { DomSanitizer } from '@angular/platform-browser';
import { DiagFileTypeEnum } from '@cloe-core/@core/enums/DiagFileTypeEnum';

@Component({
  selector: 'cloe-page-diagnosis',
  templateUrl: './diagnosis.component.html',
  styleUrls: ['./diagnosis.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DiagnosisComponent implements OnInit, OnDestroy {
  @ViewChild(CloeTableComponent) diagTable;
  tableConfig = DIAG_LIST_TABLE_CONFIG;
  filters = {
    userUuid: StorageService.getCurrentUser().cloeUser.uuid,
    status: [ 'LOADING', 'ACQUIRED', 'GENERATING', 'GENERATED', 'CONFIRMED', 'EXCEL_ERRORS', 'ERROR' ]
  };

  file;
  currentDiagCode;
  @ViewChild('fileInput')
  inputFile: ElementRef;

  diagnosisActionsSubscription;
  diagAutoReloadTimer;

  constructor(
    private diagnosisService: DiagnosisService,
    private stateService: StateService,
    private sanitizer: DomSanitizer
  ) {}

  ngOnInit(): void {
    this.diagnosisActionsSubscription = diagnosisSubject$.asObservable().subscribe( (event: any) => {
      switch (event.action) {
        case DiagnosisActionsEnum.DETAIL:
          this.stateService.go(
            'diag-detail',
            {
              diagCode: event.data.code
            }
          );
          break;
        case DiagnosisActionsEnum.GENERATE_DOC:
          this.diagnosisService.uploadJSONComment(event.data.jsonData, event.data.code).subscribe( (result: any) => {
            this.diagnosisService.updateDiag(DiagStatusEnum.GENERATING, event.data.code, result.message).subscribe( () => {
              this.diagTable.loadData();
            });
          });
          break;
        case DiagnosisActionsEnum.CONFIRM:
          this.diagnosisService.updateDiag(DiagStatusEnum.CONFIRMED, event.data.code).subscribe(() => {
            this.diagTable.loadData();
          });
          break;
        case DiagnosisActionsEnum.DOWNLOAD:
          const docFileName = 'Diagnosi-master.docx';
          this.diagnosisService.downloadDiagFile(event.data.code, docFileName, DiagFileTypeEnum.MASTER_DOCX)
            .subscribe((data: any) => {
              this._downloadFile(data, docFileName, 'vnd.openxmlformats-officedocument.wordprocessingml.document');
          });
          break;
        case DiagnosisActionsEnum.DELETE:
          this.diagnosisService.updateDiag(DiagStatusEnum.CANCELED, event.data.code).subscribe(() => {
            this.diagTable.loadData();
          });
          break;

        case DiagnosisActionsEnum.UPLOAD_PDF:
          this.currentDiagCode = event.data.code;
          this.inputFile.nativeElement.dispatchEvent(
            new MouseEvent('click', {bubbles: true})
          );
          break;

        case DiagnosisActionsEnum.DOWNLOAD_PDF:
          this.diagnosisService.downloadDiagFile(
            event.data.code,
            event.data.pdfFileName,
            DiagFileTypeEnum.MASTER_PDF
          ).subscribe((data: any) => {
            this._downloadFile(data, event.data.pdfFileName, 'pdf');
          });
          break;
      }
    });
    this.diagAutoReloadTimer = setInterval(() => {
      this.diagTable.loadData();
    }, 20 * 1000);
  }

  setFile(files: FileList) {
    this.file = files.item(0);
    this.diagnosisService.uploadNewFile(this.file, this.currentDiagCode, DiagFileTypeEnum.MASTER_PDF)
      .subscribe(value => {
        this.diagTable.loadData();
      });
  }

  private _downloadFile(data: any, fileName: string, ext) {
    const blob = new Blob([data]);
    const url = window.URL.createObjectURL(blob);

    const a = document.createElement('a');
    a.href = url;
    a.download = fileName;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);

    URL.revokeObjectURL(url);
    console.log('url', url);
  }

  ngOnDestroy(): void {
    this.diagnosisActionsSubscription.unsubscribe();
    clearInterval(this.diagAutoReloadTimer);
  }
}
