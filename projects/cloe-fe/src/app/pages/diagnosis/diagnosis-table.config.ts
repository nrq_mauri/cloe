import { FilterTypeEnum } from '@cloe-core/@core/enums/FilterTypeEnum';
import {
  DiagnosisExcelErrorsTableComponent,
  DiagnosisUploadNewFileComponent,
  NewDiagnosisComponent
} from '@fe-core/components/modals';
import { ACTIONS } from '@cloe-core/@core/enums/ActionsEnum';
import { DiagnosisActionsEnum } from './enums/DiagnosisActionsEnum';
import { Subject } from 'rxjs';
import { DiagStatusEnum } from '@cloe-core/@core/enums/DiagStatus';
import { DiagFileTypeEnum } from '@cloe-core/@core/enums/DiagFileTypeEnum';
import {ITableConfig} from '@cloe-core/@core/@ui/components/cloe-table/cloe-table.interface';

export const diagnosisSubject$ = new Subject();

export const DIAG_LIST_TABLE_CONFIG: ITableConfig = {
  header: [
    {
      fieldName: 'code',
      columnName: 'global.code'
    },
    {
      fieldName: 'name',
      columnName: 'global.name'
    },
    {
      fieldName: 'status',
      columnName: 'global.status',
      i18n: {
        prefix: 'global.diagnosis.',
        suffix: '',
      }
    },
    {
      fieldName: 'fileName',
      columnName: 'page.diagnosis.fileName'
    },
    {
      fieldName: 'creationDate',
      columnName: 'page.diagnosis.creation-date',
      pipe: 'cloeDate'
    }
  ],
  metadata: {
    tableTitle: 'page.diagnosis.tableTitle',
    sort: 'name',
    sortType: 'asc',
    reloadButton: true,
    addNewButton: {
      modalComponent: NewDiagnosisComponent,
      buttonLabel: 'global.addNew'
    }
  },
  actions: [
    {
      icon: 'file_upload',
      actionType: ACTIONS.OPEN_MODAL,
      metadata: {
        fileType: DiagFileTypeEnum.MASTER_EXCEL,
        modalComponent: DiagnosisUploadNewFileComponent,
        altMessage: 'page.diagnosis.re-upload',
        visible: (row) => {
          return row.status !== DiagStatusEnum.LOADING &&
            row.status !== DiagStatusEnum.GENERATING;
        }
      }
    },
    {
      icon: 'visibility',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: diagnosisSubject$,
        action: DiagnosisActionsEnum.DETAIL,
        altMessage: 'page.diagnosis.diagnosis-detail',
        visible: (row) => {
          return row.status === DiagStatusEnum.GENERATING ||
            row.status === DiagStatusEnum.GENERATED ||
            row.status === DiagStatusEnum.ACQUIRED ||
            row.status === DiagStatusEnum.CONFIRMED;
        }
      }
    },
    {
      icon: 'content_copy',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: diagnosisSubject$,
        action: DiagnosisActionsEnum.GENERATE_DOC,
        altMessage: 'page.diagnosis.generate-doc',
        visible: (row) => {
          return row.status === DiagStatusEnum.GENERATED ||
            row.status === DiagStatusEnum.ACQUIRED;
        }
      }
    },
    {
      icon: 'check',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: diagnosisSubject$,
        action: DiagnosisActionsEnum.CONFIRM,
        altMessage: 'page.diagnosis.confirm',
        visible: (row) => {
          return row.status === DiagStatusEnum.GENERATED;
        }
      }
    },
    {
      icon: 'fa-file-word-o',
      iconType: 'fa',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: diagnosisSubject$,
        action: DiagnosisActionsEnum.DOWNLOAD,
        altMessage: 'page.diagnosis.download',
        visible: (row) => {
          return row.status === DiagStatusEnum.CONFIRMED ||
            row.status === DiagStatusEnum.GENERATED;
        }
      }
    },
    {
      icon: 'delete',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: diagnosisSubject$,
        action: DiagnosisActionsEnum.DELETE,
        altMessage: 'global.delete',
        visible: (row) => {
          return row.status === DiagStatusEnum.LOADING;
        }
      }
    },
    {
      icon: 'fa-file-pdf-o',
      iconType: 'fa',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: diagnosisSubject$,
        action: DiagnosisActionsEnum.DOWNLOAD_PDF,
        altMessage: 'page.diagnosis.download-pdf',
        visible: (row: any) => {
          return row.status === DiagStatusEnum.CONFIRMED && !!row.pdfFileName;
        }
      }
    },
    {
      icon: 'fa-upload',
      iconType: 'fa',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: diagnosisSubject$,
        action: DiagnosisActionsEnum.UPLOAD_PDF,
        altMessage: 'page.diagnosis.upload-pdf',
        visible: (row: any) => {
          return row.status === DiagStatusEnum.CONFIRMED;
        }
      }
    },
    {
      icon: 'fa-file-excel-o',
      iconType: 'fa',
      actionType: ACTIONS.OPEN_MODAL,
      metadata: {
        modalComponent: DiagnosisExcelErrorsTableComponent,
        altMessage: 'page.diagnosis.view-excel-errors',
        visible: (row: any) => {
          return row.status === DiagStatusEnum.EXCEL_ERRORS;
        }
      }
    },
    {
      icon: 'fa-wrench',
      iconType: 'fa',
      actionType: ACTIONS.OPEN_MODAL,
      metadata: {
        fileType: DiagFileTypeEnum.INTERVENTION_EXCEL,
        modalComponent: DiagnosisUploadNewFileComponent,
        altMessage: 'page.diagnosis.upload-actions',
        visible: (row: any) => {
          return row.status === DiagStatusEnum.GENERATED;
        }
      }
    }
  ],
  filters: [
    {
      filterType: FilterTypeEnum.TEXT,
      filterField: 'code',
      fieldLabel: 'global.code'
    },
    {
      filterType: FilterTypeEnum.TEXT,
      filterField: 'name',
      fieldLabel: 'global.name'
    },
    {
      filterType: FilterTypeEnum.DROPDOWN,
      filterField: 'status',
      fieldLabel: 'global.status',
      i18nPrefix: 'global.diagnosis.',
      items: ['LOADING', 'ACQUIRED', 'GENERATING', 'GENERATED', 'CONFIRMED', 'EXCEL_ERRORS', 'ERROR']
    },
  ]
};
