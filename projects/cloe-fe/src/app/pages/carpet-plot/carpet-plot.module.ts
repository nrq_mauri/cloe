import { NgModule } from '@angular/core';
import { CarpetPlotComponent } from './carpet-plot.component';
import { UIRouterModule } from '@uirouter/angular';
import { FeCoreModule } from '@fe-core/fe-core.module';
import { tabsComponents, tabsStates } from './carpet-plots-tabs';

const state = {
  name: 'carpet-plot',
  parent: 'channel-tree-view',
  url: '/carpet-plot',
  data: { breadcrumbLabel: 'component.breadcrumbs.carpet-plot' },
  component: CarpetPlotComponent,
  redirectTo: {state: 'tab-by-measure', params: {selectDefaultChannel: false}}
};

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [state, ...tabsStates]}),
    FeCoreModule
  ],
  declarations: [CarpetPlotComponent, ...tabsComponents]
})
export class CarpetPlotModule {
}
