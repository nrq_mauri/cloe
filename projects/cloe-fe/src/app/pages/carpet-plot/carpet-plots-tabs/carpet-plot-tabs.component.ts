import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'cloe-carpet-plot-tabs',
  template: '<ui-view></ui-view>',
  encapsulation: ViewEncapsulation.None
})
export class CarpetPlotTabsComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {}
}
