import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {ChartTypeEnum} from '@fe-core/components/cloe-period-selector/enums/ChartTypeEnum';
import {CHANNEL_VIEW_ACTIONS, channelTreeViewSubject$} from '../../../core-pages/channel-tree-view/channel-tree-view.component';
import {isEqual, findIndex} from 'lodash';
import {Subscription} from 'rxjs';
import {CloeHeatMapService, HeatMap, HeatMapActionEnum} from '@fe-core/services/cloe-heatMap-service/cloe-heatMap-service';
import {CarpetPlotsTabEnum} from '../enums/CarpetPlotsTabsEnum';
import {MeasureService} from '@fe-core/services/measure-service/measure-service';
import {TABLE_CONFIG} from './tab-heatMap-byPeriod-table.config';
import { CloeChartService } from '@fe-core/services/cloe-chart-service/cloe-chart-service';

@Component({
  selector: 'cloe-heat-map-by-period',
  templateUrl: './tab-heatMap-by-period.component.html',
  styleUrls: ['./tab-heatMap-by-period.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TabHeatMapByPeriodComponent implements OnInit, OnDestroy {
  carpetPlotsTypes = CarpetPlotsTabEnum;
  heatMaps: HeatMap[];
  channelSelection;
  chartType = ChartTypeEnum;
  channelStateSubscription: Subscription;
  heatMapStateSubscription: Subscription;
  initialPeriodConfig = {rangeTypeDisabled: false, periodTypeDisabled: false};
  tableConfig = TABLE_CONFIG;

  constructor(
    private measureService: MeasureService,
    private cloeHeatMapService: CloeHeatMapService,
    private cloeChartService: CloeChartService
  ) {}

  ngOnInit() {
    this.heatMaps = this.cloeHeatMapService.heatMaps;
    this.heatMapStateSubscription = this.cloeHeatMapService.chartEventSubject$.asObservable().subscribe((e: any) => {
      if (e.action === HeatMapActionEnum.HEATMAPS_UPDATE) {
        this.heatMaps = e.heatMaps;
      }
    });
    this.channelStateSubscription = channelTreeViewSubject$.asObservable().subscribe((e: any) => {
      if (e.action === CHANNEL_VIEW_ACTIONS.NODE_ACTIVE) {
        this.channelSelection = e.referObject;
      }
    });
  }

  onPeriodSelect($event: any) {
    const periodIndex = findIndex(this.heatMaps, (heatMap) => {
      return isEqual($event, heatMap.periodSelection);
    });
    if (this.channelSelection && periodIndex === -1 && this.heatMaps.length < 9) {
      if (this.heatMaps.length === 0) {
        this.initialPeriodConfig.periodTypeDisabled = true;
        this.initialPeriodConfig.rangeTypeDisabled = true;
        channelTreeViewSubject$.next({action: CHANNEL_VIEW_ACTIONS.TOGGLE_ACTIVE_TREE});
      }
      let heatMap: HeatMap = {
        divId: this.channelSelection.uuid + '_HeatMapDivId' + this.heatMaps.length,
        periodSelection: $event,
        uuidChannel: this.channelSelection.uuid,
        integrable: this.cloeChartService.checkChannelIsIntegrable(this.channelSelection)
      };

      heatMap = this.cloeHeatMapService.createHeatMapFromChannelUuid(heatMap);
      this.cloeHeatMapService.addHeatMap(heatMap);
    }
  }

  clearSelections() {
    if (this.channelSelection) {
      this.cloeHeatMapService.clearHeatMaps();
      this.channelSelection = undefined;
      this.initialPeriodConfig.periodTypeDisabled = false;
      this.initialPeriodConfig.rangeTypeDisabled = false;
      channelTreeViewSubject$.next({action: CHANNEL_VIEW_ACTIONS.TOGGLE_ACTIVE_TREE});
      channelTreeViewSubject$.next({action: CHANNEL_VIEW_ACTIONS.CLEAR_SELECTION});
    }
  }

  ngOnDestroy(): void {
    this.channelStateSubscription.unsubscribe();
    this.heatMapStateSubscription.unsubscribe();
    this.cloeHeatMapService.clearHeatMaps();
  }
}
