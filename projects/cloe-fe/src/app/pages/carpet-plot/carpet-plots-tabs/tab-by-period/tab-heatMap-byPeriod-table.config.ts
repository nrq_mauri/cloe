import {
  ICloeTableStatic
} from '@cloe-core/@core/@ui/components/cloe-table-static/cloe-table-static-interface/cloe-table-static-interface';

export const TABLE_CONFIG: ICloeTableStatic = {
  header: [
    {
      fieldName: 'periodSelection.startDate',
      columnName: 'page.carpet-plot.startDate',
      pipe: 'cloeDate',
    },
    {
      fieldName: 'periodSelection.endDate',
      columnName: 'page.carpet-plot.endDate',
      pipe: 'cloeDate'
    },
    {
      fieldName: 'summaryData.avg',
      columnName: 'page.carpet-plot.average-value',
      pipe: 'engFormatWithUnit',
      pipeArgField: 'summaryData.dynamicSymbol',
    },
    {
      fieldName: 'summaryData.min',
      columnName: 'page.carpet-plot.min-value',
      pipe: 'engFormatWithUnit',
      pipeArgField: 'summaryData.dynamicSymbol',
    },
    {
      fieldName: 'summaryData.max',
      columnName: 'page.carpet-plot.max-value',
      pipe: 'engFormatWithUnit',
      pipeArgField: 'summaryData.dynamicSymbol',
    },
    {
      fieldName: 'summaryData.peek',
      columnName: 'page.carpet-plot.sampling-max',
      pipe: 'engFormatWithUnit',
      pipeArgField: 'summaryData.unitSymbol',
      immutableSymbol: true
    },
    {
      fieldName: 'summaryData.totalIntegrated',
      columnName: 'page.carpet-plot.total-consumption',
      pipe: 'engFormatWithUnit',
      pipeArgField: 'summaryData.integratedSymbol',
      immutableSymbol: true
    }
  ],
  metadata: {
    disablePagination: true,
    fixPageSize: 30
  }
};
