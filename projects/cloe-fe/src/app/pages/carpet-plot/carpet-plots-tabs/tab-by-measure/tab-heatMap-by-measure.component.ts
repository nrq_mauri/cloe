import {Component, OnDestroy, OnInit, ViewEncapsulation} from '@angular/core';
import {CHANNEL_VIEW_ACTIONS, channelTreeViewSubject$} from '../../../core-pages/channel-tree-view/channel-tree-view.component';
import {ChartTypeEnum} from '@fe-core/components/cloe-period-selector/enums/ChartTypeEnum';
import {Subscription} from 'rxjs';
import {CloeHeatMapService, HeatMap, HeatMapActionEnum} from '@fe-core/services/cloe-heatMap-service/cloe-heatMap-service';
import {MeasureService} from '@fe-core/services/measure-service/measure-service';
import {CarpetPlotsTabEnum} from '../enums/CarpetPlotsTabsEnum';
import {TABLE_CONFIG} from './tab-heatMap-byMeasure-table.config';
import { animate, keyframes, style, transition, trigger } from '@angular/animations';
import { CloeChartService } from '@fe-core/services/cloe-chart-service/cloe-chart-service';

@Component({
  selector: 'cloe-heat-map-by-measure',
  templateUrl: './tab-heatMap-by-measure.component.html',
  styleUrls: ['./tab-heatMap-by-measure.component.scss'],
  animations: [
    trigger('shake', [
      transition('false => true', animate(1000, keyframes([
        style({transform: 'translate3d(-1px, 0, 0)', offset: .10}),
        style({transform: 'translate3d(2px, 0, 0)', offset: .20}),
        style({transform: 'translate3d(-4px, 0, 0)', offset: .30}),
        style({transform: 'translate3d(4px, 0, 0)', offset: .40}),
        style({transform: 'translate3d(-4px, 0, 0)', offset: .50}),
        style({transform: 'translate3d(4px, 0, 0)', offset: .60}),
        style({transform: 'translate3d(-4px, 0, 0)', offset: .70}),
        style({transform: 'translate3d(2px, 0, 0)', offset: .80}),
        style({transform: 'translate3d(-1px, 0, 0)', offset: .90})
      ]))),
    ])
  ],
  encapsulation: ViewEncapsulation.None
})
export class TabHeatMapByMeasureComponent implements OnInit, OnDestroy {

  carpetPlotsTypes = CarpetPlotsTabEnum;
  heatMaps: HeatMap[];
  periodSelection: any;
  chartType = ChartTypeEnum;
  channelStateSubscription: Subscription;
  heatMapStateSubscription: Subscription;
  initialPeriodConfig = {
    rangeTypeDisabled: false,
    periodTypeDisabled: false,
    calendarDisabled: false
  };
  tableConfig = TABLE_CONFIG;
  shakeState = false;

  constructor(
    private measureService: MeasureService,
    private cloeHeatMapService: CloeHeatMapService,
    private cloeChartService: CloeChartService
  ) {}

  ngOnInit() {
    this.heatMaps = this.cloeHeatMapService.heatMaps;
    this.heatMapStateSubscription = this.cloeHeatMapService.chartEventSubject$.asObservable().subscribe((e: any) => {
      if (e.action === HeatMapActionEnum.HEATMAPS_UPDATE) {
        this.heatMaps = e.heatMaps;
      }
    });
    channelTreeViewSubject$.next({action: CHANNEL_VIEW_ACTIONS.TOGGLE_ACTIVE_TREE});
    this.channelStateSubscription = channelTreeViewSubject$.asObservable().subscribe((e: any) => {
      if (e.action === CHANNEL_VIEW_ACTIONS.NODE_ACTIVE && this.heatMaps.length < 9) {
        let heatMap: HeatMap = {
          divId: e.referUuid + '_HeatMapDivId',
          periodSelection: this.periodSelection,
          uuidChannel: e.referUuid,
          channelName: e.name,
          channelPath: e.channelPath,
          integrable: this.cloeChartService.checkChannelIsIntegrable(e.referObject)
        };

        heatMap = this.cloeHeatMapService.createHeatMapFromChannelUuid(heatMap);
        this.cloeHeatMapService.addHeatMap(heatMap);
      }
      if (e.action === CHANNEL_VIEW_ACTIONS.NODE_DEACTIVE) {
        this.cloeHeatMapService.removeHeatMapByReferUuid(e.referUuid);
      }
      if (e.action === CHANNEL_VIEW_ACTIONS.TREE_DEACTIVE_CLICK) {
        this.shakeState = true;
      }
    });
  }

  onPeriodSelect($event: any) {
    channelTreeViewSubject$.next({action: CHANNEL_VIEW_ACTIONS.TOGGLE_ACTIVE_TREE});
    this.periodSelection = $event;
    this.initialPeriodConfig.periodTypeDisabled = true;
    this.initialPeriodConfig.rangeTypeDisabled = true;
    this.initialPeriodConfig.calendarDisabled = true;
  }

  clearSelections() {
    if (this.periodSelection) {
      channelTreeViewSubject$.next({action: CHANNEL_VIEW_ACTIONS.CLEAR_SELECTION});
      channelTreeViewSubject$.next({action: CHANNEL_VIEW_ACTIONS.TOGGLE_ACTIVE_TREE});
      this.periodSelection = undefined;
      this.initialPeriodConfig.periodTypeDisabled = false;
      this.initialPeriodConfig.rangeTypeDisabled = false;
      this.initialPeriodConfig.calendarDisabled = false;
    }
  }

  ngOnDestroy(): void {
    this.channelStateSubscription.unsubscribe();
    this.heatMapStateSubscription.unsubscribe();
    this.cloeHeatMapService.clearHeatMaps();
  }

  onShakeEnd() {
    this.shakeState = false;
  }
}
