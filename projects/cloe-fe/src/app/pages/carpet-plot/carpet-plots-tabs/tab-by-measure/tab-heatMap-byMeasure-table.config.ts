import {
  ICloeTableStatic
} from '@cloe-core/@core/@ui/components/cloe-table-static/cloe-table-static-interface/cloe-table-static-interface';

export const TABLE_CONFIG: ICloeTableStatic = {
  header: [
    {
      fieldName: 'channelPath',
      columnName: 'global.path',
      pipe: 'ellipsis',
      pipeArgs: 4,
      skipPipeOnExport: true,
      conditionalPipe: true
    },
    {
      fieldName: 'channelNameWithSymbol',
      columnName: 'global.channel'
    },
    {
      fieldName: 'summaryData.avg',
      columnName: 'page.carpet-plot.average-value',
      pipe: 'engFormatWithUnit',
      pipeArgField: 'summaryData.dynamicSymbol',
    },
    {
      fieldName: 'summaryData.min',
      columnName: 'page.carpet-plot.min-value',
      pipe: 'engFormatWithUnit',
      pipeArgField: 'summaryData.dynamicSymbol',
    },
    {
      fieldName: 'summaryData.max',
      columnName: 'page.carpet-plot.max-value',
      pipe: 'engFormatWithUnit',
      pipeArgField: 'summaryData.dynamicSymbol',
    },
    {
      fieldName: 'summaryData.peek',
      columnName: 'page.carpet-plot.sampling-max',
      pipe: 'engFormatWithUnit',
      pipeArgField: 'summaryData.unitSymbol',
      immutableSymbol: true
    },
    {
      fieldName: 'summaryData.totalIntegrated',
      columnName: 'page.carpet-plot.total-consumption',
      pipe: 'engFormatWithUnit',
      pipeArgField: 'summaryData.integratedSymbol',
      immutableSymbol: true
    }
  ],
  metadata: {
    disablePagination: true,
    fixPageSize: 30,
    sort: 'channelName',
    sortType: 'asc',
  }
};
