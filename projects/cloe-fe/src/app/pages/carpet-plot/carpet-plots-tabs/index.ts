import {TabHeatMapByMeasureComponent} from './tab-by-measure/tab-heatMap-by-measure.component';
import {TabHeatMapByPeriodComponent} from './tab-by-period/tab-heatMap-by-period.component';
import {CarpetPlotTabsComponent} from './carpet-plot-tabs.component';

export const tabs = [
  {
    name: 'page.carpet-plot.carpet-plot-tabs.tab-by-measure',
    ngState: 'tab-by-measure'
  },
  {
    name: 'page.carpet-plot.carpet-plot-tabs.tab-by-period',
    ngState: 'tab-by-period'
  }
];

export const tabsComponents = [
  CarpetPlotTabsComponent,
  TabHeatMapByMeasureComponent,
  TabHeatMapByPeriodComponent
];

export const tabsStates = [
  {
    name: 'carpet-plot-tabs',
    url: '/carpet-plot-tabs',
    parent: 'carpet-plot',
    abstract: true,
    data: { breadcrumbLabel: undefined },
    component: CarpetPlotTabsComponent
  },
  {
    name: 'tab-by-measure',
    parent: 'carpet-plot-tabs',
    url: '/byMeasure',
    data: {
      stateAs: 'carpet-plot',
      breadcrumbLabel: 'component.breadcrumbs.carpet-plot-by-measure'
    },
    component: TabHeatMapByMeasureComponent
  },
  {
    name: 'tab-by-period',
    parent: 'carpet-plot-tabs',
    url: '/byPeriod',
    data: {
      stateAs: 'carpet-plot',
      breadcrumbLabel: 'component.breadcrumbs.carpet-plot-by-period'
    },
    component: TabHeatMapByPeriodComponent
  }
];
