import {AfterContentInit, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {tabs} from './carpet-plots-tabs';
import { CloeHeatMapService } from '@fe-core/services/cloe-heatMap-service/cloe-heatMap-service';


@Component({
  selector: 'cloe-carpet-plot',
  templateUrl: './carpet-plot.component.html',
  styleUrls: ['./carpet-plot.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CarpetPlotComponent implements OnInit, AfterContentInit {

  tabs = tabs;

  constructor(private cloeHeatMapService: CloeHeatMapService) {
  }

  ngOnInit(): void {
    this.cloeHeatMapService.initIntegrated();
  }

  ngAfterContentInit(): void {
  }
}
