import {ACTIONS} from '@cloe-core/@core/enums/ActionsEnum';
import { ClusterManagementComponent } from '@fe-core/components/modals';
import { Subject } from 'rxjs';
import {ICloeTableStatic} from '@cloe-core/@core/@ui/components/cloe-table-static/cloe-table-static-interface/cloe-table-static-interface';

export const clusterTreeChartSubject$ = new Subject();
export const clusterTableSubject$ = new Subject();

export const TABLE_CONFIG: ICloeTableStatic = {
  header: [
    {
      fieldName: 'name',
      columnName: 'global.name',
    },
    {
      fieldName: 'defaultChannelTypeId',
      columnName: 'global.channel',
    }
  ],
  actions: [
    {
      icon: 'visibility',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: clusterTreeChartSubject$,
        altMessage: 'component.cluster-tree-view.open-cluster-tree'
      }
    },
    {
      icon: 'edit',
      actionType: ACTIONS.OPEN_MODAL,
      metadata: {
        modalComponent: ClusterManagementComponent,
        altMessage: 'global.edit'
      }
    },
    {
      icon: 'delete',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        action: ACTIONS.DELETE,
        triggerSubject: clusterTableSubject$,
        altMessage: 'global.delete'
      }
    }
  ],
  metadata: {
    tableTitle: 'page.cluster-crud.tableTitle',
    sort: 'name',
    sortType: 'asc',
    addNewButton: {
      modalComponent: ClusterManagementComponent,
      buttonLabel: 'global.addNew'
    }
  }
};
