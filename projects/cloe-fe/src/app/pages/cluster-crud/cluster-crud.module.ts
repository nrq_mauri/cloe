import { NgModule } from '@angular/core';
import {ClusterCrudComponent} from './cluster-crud.component';
import {UIRouterModule } from '@uirouter/angular';
import {FeCoreModule} from '@fe-core/fe-core.module';
import { CLUSTER_RESOLVERS } from './cluster.resolvers';

const state = {
  name: 'cluster-crud',
  parent: 'logged',
  url: '/cluster/list',
  resolve: [
    ...CLUSTER_RESOLVERS
  ],
  data: { breadcrumbLabel: 'component.breadcrumbs.cluster-crud' },
  component: ClusterCrudComponent
};

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [state]}),
    FeCoreModule
  ],
  declarations: [ClusterCrudComponent]
})
export class ClusterCrudModule {}
