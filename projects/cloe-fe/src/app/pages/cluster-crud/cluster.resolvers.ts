import { Resolvable } from '@uirouter/angular';
import { ClusterService } from '@cloe-core/@core/services/cluster-service/cluster-service';

const clustersForUserResolver = new Resolvable(
  'clustersForUser',
  (clusterService, user) => clusterService.getClusterForUserAndSystem(
    user.cloeUser.uuid
  ).toPromise(),
  [ClusterService, 'user']
);

export const CLUSTER_RESOLVERS = [
  clustersForUserResolver
];
