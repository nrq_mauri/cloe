import { Component, Input, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { clusterTableSubject$, clusterTreeChartSubject$, TABLE_CONFIG } from './cluster-table.config';
import { CloeTreeChartService, TreeChart } from '@fe-core/services/cloe-tree-chart-service/cloe-tree-chart-service';
import { ACTIONS } from '@cloe-core/@core/enums/ActionsEnum';
import { ModalService } from '@cloe-core/@core/services/modal-service/modal-service';
import { ConfirmModalComponent } from '@cloe-core/@core/@ui/components/modals';
import { CloeTreeNodeService } from '@cloe-core/@core/services/cloe-treeNode-service/cloe-tree-node.service';
import { StorageService } from '@cloe-core/@core/services/storage-service/storage.service';
import { cloneDeep } from 'lodash';
import { ClusterService } from '@cloe-core/@core/services/cluster-service/cluster-service';
import { CloeSearchService } from '@cloe-core/@core/services/cloe-search-service/cloe.search.service';
import { PAGINATION } from '@cloe-core/@core/@ui/components/cloe-table/configuration/cloe-table-conf';
import { staticTableSubject$ } from '@cloe-core/@core/@ui/components/cloe-table-static';

@Component({
  selector: 'cloe-cluster-management',
  templateUrl: './cluster-crud.component.html',
  styleUrls: ['./cluster-crud.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ClusterCrudComponent implements OnInit, OnDestroy {

  @Input() clustersForUser;
  treeCharts: TreeChart[];
  staticTableSubscription;
  clusterTreeChartSubscription;
  clusterTableSubscription;
  tableConfig = TABLE_CONFIG;
  clusterTree;

  constructor(
    private treeChartService: CloeTreeChartService,
    private cloeTreeNodeService: CloeTreeNodeService,
    private cloeSearchService: CloeSearchService,
    private clusterService: ClusterService,
    private modalService: ModalService
  ) {}

  ngOnInit() {
    this.cloeSearchService.search(
      'TREE',
      {},
      cloneDeep(PAGINATION),
      { isCluster: true, nodeNature: 'SITE' }
    ).subscribe((res) => {
      if (res.content[0]) {
        this.clusterTree = res.content[0];
      }
    });
    this.treeCharts = this.treeChartService.treeCharts;
    this.staticTableSubscription = staticTableSubject$.asObservable().subscribe((event: any) => {
      switch (event.action) {
        case ACTIONS.RELOAD_DATA:
          this.loadData();
          break;
      }
    });
    this.clusterTreeChartSubscription = clusterTreeChartSubject$.asObservable().subscribe( (event: any) => {
      this.treeChartService.clearTreeCharts();
      this.treeChartService.addTreeChart({
        divId: event.data.uuid + '_TREE_CHART',
        treeUuid: event.data.uuid
      });
    });
    this.clusterTableSubscription = clusterTableSubject$.asObservable().subscribe((event: any) => {
      switch (event.action) {
        case ACTIONS.DELETE:
          this.modalService.open(ConfirmModalComponent, {keyboard: false}).then((res) => {
            if (res) {
              this.cloeTreeNodeService.getTreeNodeByReferUuid(
                'SITE',
                StorageService.getCurrentUser(),
                false,
                event.data.uuid,
                false,
                true,
                false
              ).subscribe((result) => {
                this.clusterService.deleteClusterByTreeNode(
                  result,
                  this.clusterTree,
                  event.data.uuid
                ).subscribe( () => this.loadData());
              });
            }
          }).catch(() => {
            // dummy catch, we don't use it for now
          });
          this.modalService.getModalInstance().componentInstance.confirmMessage = event.metadata.confirmMessage;
          break;
      }
    });
  }

  private loadData() {
    this.clusterService.getClusterForUserAndSystem(StorageService.getCurrentUser().cloeUser.uuid).subscribe((res) => {
      this.clustersForUser = res;
    });
  }

  ngOnDestroy(): void {
    this.clusterTreeChartSubscription.unsubscribe();
    this.clusterTableSubscription.unsubscribe();
    this.staticTableSubscription.unsubscribe();
  }
}
