import { AfterViewInit, Component, ElementRef, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { viewCurvesChartConfig } from '../../view-curves.am-chart';
import {
  CloeStockChartService,
  IViewCurvesState, viewCurvesGuidesSubject$, viewCurvesSubject$
} from '@fe-core/services/cloe-stock-chart-service/cloe-stock-chart-service';
import { values } from 'lodash';

@Component({
  selector: 'cloe-tab-view-chart',
  templateUrl: './tab-view-chart.component.html',
  styleUrls: ['./tab-view-chart.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TabViewChartComponent implements OnInit, OnDestroy, AfterViewInit {

  @ViewChild('cloeStockChart')
  cloeStockChart;

  stockChartConfig = viewCurvesChartConfig;
  viewCurvesSubscription;
  viewCurvesGuidesSubscription;

  constructor(private stockChartService: CloeStockChartService) {
  }

  ngOnInit() {
  }

  ngOnDestroy(): void {
    this.viewCurvesSubscription.unsubscribe();
    this.viewCurvesGuidesSubscription.unsubscribe();
  }

  private _updateChart(viewCurvesState: IViewCurvesState) {
    const recipes = values(viewCurvesState.graphRecipes);
    const chart = this.cloeStockChart.stockChart.chart;
    chart.dataSets[0].dataProvider = viewCurvesState.dataProvider;
    this.stockChartService.removeGraphFromChart(chart);
    recipes.forEach(r => {
      this.stockChartService.upsertGraphToChart(chart, r);
    });
    this.stockChartService.updateMinPeriod(chart, viewCurvesState.period.periodType);
    this.stockChartService.forceChartUpdate(chart);
  }

  ngAfterViewInit(): void {
    this.viewCurvesSubscription = viewCurvesSubject$.asObservable()
      .subscribe((viewCurvesState: IViewCurvesState) => {
        this._updateChart(viewCurvesState);
      });

    this.viewCurvesGuidesSubscription = viewCurvesGuidesSubject$.asObservable().subscribe(guides => {
      const chart = this.cloeStockChart.stockChart.chart;
      this.stockChartService.updateGuides(chart, guides);
      this.stockChartService.forceChartUpdate(chart);
    });
  }
}
