import { AfterViewInit, Component, Input, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { CURVES_TABLE_CONFIG } from './tab-view-table.config';
import { cloneDeep, map, remove, extend, values } from 'lodash';
import { CloeChartService } from '@fe-core/services/cloe-chart-service/cloe-chart-service';
import { CsvService } from '@cloe-core/@core/services/csv-service/csv.service';
import { RECAP_TABLE_CONFIG } from '../../view-curves-recap-table.config';
import {
  IGraphRecipe,
  IViewCurvesState, viewCurvesGuidesSubject$,
  viewCurvesRecapTableSubject$,
  viewCurvesSubject$
} from '@fe-core/services/cloe-stock-chart-service/cloe-stock-chart-service';
import {
  ChannelRangeAlgorithmService
} from '@cloe-core/@core/services/channel-range-service/channel-range-algorithm-service';
import { ChannelRangeService } from '@cloe-core/@core/services/channel-range-service/channel-range-service';

@Component({
  selector: 'cloe-tab-view-table',
  templateUrl: './tab-view-table.component.html',
  styleUrls: ['./tab-view-table.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TabViewTableComponent implements OnInit, AfterViewInit, OnDestroy {

  viewCurvesSubscription;
  viewCurvesGuidesSubscription;
  tableConfig = cloneDeep(CURVES_TABLE_CONFIG);
  data = [];
  headerTimeStamp = {
    fieldName: 'timeStamp',
    columnName: 'global.date',
    pipe: 'cloeDateTimeUTC',
    rowspan: 2
  };

  headerChannelRange = {
    fieldName: 'range'
  };

  guides: any = null;

  constructor(
    private cloeChartService: CloeChartService,
    private csvService: CsvService,
    private channelRangeService: ChannelRangeService
  ) {
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.viewCurvesSubscription = viewCurvesSubject$.asObservable()
      .subscribe((viewCurvesState: IViewCurvesState) => {
        this._updateTableConfig(viewCurvesState);
        setTimeout(() => {
          this.data = viewCurvesState.dataProvider;
          this._updateGuides(viewCurvesGuidesSubject$.value);
        }, 0);
      });

    this.viewCurvesGuidesSubscription = viewCurvesGuidesSubject$.asObservable().subscribe((guides: any) => {
      setTimeout(() => {
        this.guides = guides;
        if (!guides || guides.updateGuides) {
          this._updateGuides(guides);
        }
      }, 0);
    });
  }

  private _updateGuides(guides: any) {
    this._removeGuides();
    if (guides) {
      this._addGuides(guides);
    }
  }

  ngOnDestroy(): void {
    this.viewCurvesSubscription.unsubscribe();
    if (this.viewCurvesGuidesSubscription) {
      this.viewCurvesGuidesSubscription.unsubscribe();
    }
  }

  downloadCSV() {
    const dataArray = [];
    dataArray.push(
      this.csvService.generateDataToExportCSV(RECAP_TABLE_CONFIG.header, viewCurvesRecapTableSubject$.value.data),
      this.csvService.generateDataToExportCSV(this.tableConfig.header, this.data)
    );
    this.csvService.downloadArrayCSV(dataArray, {});
  }

  private _addGuides(guides: any) {
    const channelRangeAlgorithmService: ChannelRangeAlgorithmService =
      this.channelRangeService.createAlgorithm(guides.originalChannelRange);
    this.data = map(this.data, row => {
      const range = guides[channelRangeAlgorithmService.getRangeFromMeasure(row)];
      row.range = range ? range.name : undefined;
      return row;
    });
    this.tableConfig.header.splice(
      1,
      0,
      extend(
        {
          columnName: guides.originalChannelRange.name,
          rowspan: 2
        },
        this.headerChannelRange
      )
    );
  }

  private _removeGuides() {
    remove(this.tableConfig.header, header => header.fieldName === 'range');
  }

  private _updateTableConfig(viewCurvesState: IViewCurvesState) {
    const recipes = values(viewCurvesState.graphRecipes);
    const columnHeaders = map(recipes, (g: IGraphRecipe) => {
      const symbol = g.channel[this.cloeChartService.getSymbolField(g.integrated)];
      if (g.channelPath) {
        return {
          columnName: g.channelPath,
          colspan: 1,
          children: [
            {
              fieldName: this.cloeChartService._id(g.channelId, g.integrated),
              columnName: `${g.channel.name} (${symbol})`,
              pipe: 'engFormat',
              pipeArgs: symbol === '-' ? 'cosfi' : undefined,
              skipPipeOnExport: true,
              rowspan: 2
            }
          ]
        };
      } else {
        const symbolStr = symbol ? ' (' + symbol + ')' : '';
        return {
          fieldName: this.cloeChartService._id(g.channelId, g.integrated),
          columnName: g.channel.name + symbolStr,
          pipe: 'engFormat',
          pipeArgs: symbol === '-' ? 'cosfi' : undefined,
          skipPipeOnExport: true,
          rowspan: 2
        };
      }
    });
    remove(this.tableConfig.header, () => true);
    this.tableConfig.header.push(this.headerTimeStamp, ...columnHeaders);

    if (this.guides) {
      this._addGuides(this.guides);
    }
  }
}
