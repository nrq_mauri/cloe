import { TabViewChartComponent } from './tab-view-chart/tab-view-chart.component';
import { TabViewTableComponent } from './tab-view-table/tab-view-table.component';

export const tabsComponents = [
  TabViewChartComponent,
  TabViewTableComponent
];

export const tabsStates = [
  {
    name: 'tab-view-chart',
    parent: 'visualizza-curve',
    url: '/curves',
    data: {
      stateAs: 'visualizza-curve',
      breadcrumbLabel: 'component.breadcrumbs.chart'
    },
    component: TabViewChartComponent
  },
  {
    name: 'tab-view-table',
    parent: 'visualizza-curve',
    url: '/tables',
    data: {
      stateAs: 'visualizza-curve',
      breadcrumbLabel: 'component.breadcrumbs.table'
    },
    component: TabViewTableComponent
  }
];
