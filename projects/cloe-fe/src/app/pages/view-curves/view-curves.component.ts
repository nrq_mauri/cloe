import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { CHANNEL_VIEW_ACTIONS, channelTreeViewSubject$ } from '../core-pages/channel-tree-view/channel-tree-view.component';
import { ChartTypeEnum } from '@fe-core/components/cloe-period-selector/enums/ChartTypeEnum';
import { forkJoin, Subscription } from 'rxjs';
import * as moment from 'moment';
import { PeriodTypeEnum } from '@fe-core/components/cloe-period-selector/enums/PeriodTypeEnum';
import { ChartAxis, CLOE_CHARTS_ACTIONS, CloeChartService } from '@fe-core/services/cloe-chart-service/cloe-chart-service';
import { MeasureService } from '@fe-core/services/measure-service/measure-service';
import { findIndex, forEach, keyBy, remove, uniqBy, values, set, transform, difference, map, filter } from 'lodash';
import { RangeTypeEnum } from '@fe-core/components/cloe-period-selector/enums/RangeTypeEnum';
import { RECAP_TABLE_CONFIG } from './view-curves-recap-table.config';
import { StorageService } from '@cloe-core/@core/services/storage-service/storage.service';
import { chartLegendSubject$ } from '@fe-core/components/cloe-chart-legend';
import {
  CloeStockChartService,
  IGraphRecipe,
  IViewCurvesState,
  viewCurvesGuidesSubject$,
  viewCurvesRecapTableSubject$,
  viewCurvesSubject$
} from '@fe-core/services/cloe-stock-chart-service/cloe-stock-chart-service';
import { ChannelRangeService } from '@cloe-core/@core/services/channel-range-service/channel-range-service';
import { ChannelRangeTypeEnum } from '@cloe-core/@core/enums/ChannelRangeTypeEnum';
import { SiteContextEnum, siteContextSubject } from '@fe-core/components';

const initialRange = {
  skip: true,
  id: 'init-range',
  name: 'global.select-channel-range'
};

@Component({
  selector: 'cloe-view-curves',
  templateUrl: './view-curves.component.html',
  styleUrls: ['./view-curves.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ViewCurvesComponent implements OnInit, OnDestroy {

  chartType = ChartTypeEnum.STOCK_CHART;
  channelStateSubscription: Subscription;
  recapTableSubscription: Subscription;
  siteTreeViewSubscription: Subscription;
  currentPeriod;
  initialPeriodDates = {
    startDate: moment().subtract(1, 'days').startOf('day'),
    endDate: moment().subtract(1, 'days').endOf('day'),
    initialPeriodType: PeriodTypeEnum.QUARTER,
    initialRangeType: RangeTypeEnum.DAY
  };
  tableConfig: any = RECAP_TABLE_CONFIG;

  recapTableData: Array<any> = [];

  selectedChannels = [];
  channelRanges = [];
  selectedRanges = [];
  selectedRange: any = initialRange;
  channelRangesSelectSettings = {
    enableCheckAll: false,
    text: 'select-rates',
    badgeShowLimit: 1,
    groupBy: {
      field: 'rangeType',
      singleSelectionGroups: ['RATE']
    },
    selectGroup: false,
    labelKey: 'name'
  };
  integrated: boolean;

  tabs = [
    {
      name: 'page.view-curves.tab-view-chart',
      ngState: 'tab-view-chart'
    },
    {
      name: 'page.view-curves.tab-view-table',
      ngState: 'tab-view-table'
    }
  ];

  bulletActive = false;

  graphActions = [
    {
      action: CLOE_CHARTS_ACTIONS.INTEGRATE,
      label: 'global.integrated',
      icon: 'fa-info',
      toggleActive: false
    },
    {
      action: CLOE_CHARTS_ACTIONS.TOGGLE_BULLETS,
      label: 'chart.insert-bullets',
      icon: 'fa-circle',
      toggleActive: this.bulletActive
    }
  ];

  constructor(
    private cloeChartService: CloeChartService,
    private measureService: MeasureService,
    private channelRangeService: ChannelRangeService,
    private stockChartService: CloeStockChartService
  ) {
    this.integrated = !!StorageService.getCurrentUser().cloeUser.useIntegratedValues;
  }

  ngOnInit(): void {
    this.channelStateSubscription = channelTreeViewSubject$.asObservable().subscribe((e: any) => {
      const currentState: IViewCurvesState = viewCurvesSubject$.value;
      if (e.action === CHANNEL_VIEW_ACTIONS.NODE_ACTIVE) {
        const graphRecipe: IGraphRecipe = {
          axis: e.axis || ChartAxis.LEFT_RIGHT,
          channelId: e.referObject.id,
          channelUuid: e.referUuid,
          channelName: e.referObject.name,
          channelPath: e.channelPath,
          channel: e.referObject,
          unitSymbol: e.referObject.unitSymbol,
          integratedSymbol: e.referObject.integratedSymbol,
          integrated: this.integrated
        };
        this.selectedChannels.push(e.referObject);
        this.stockChartService.addGraphsByRecipes([graphRecipe], this.currentPeriod).subscribe();
      }

      if (e.action === CHANNEL_VIEW_ACTIONS.NODE_DEACTIVE) {
        this.removeGraphByReferUuidList([e.referUuid]);
      }
    });

    this.siteTreeViewSubscription = siteContextSubject.asObservable().subscribe((e: any) => {
      if (e.action === SiteContextEnum.UPDATE_SITES) {
        this._updateChannelRange(e.data.currentState);
      }
    });

    const storedDates: any = StorageService.getObject('initialViewCurvesDates');
    if (storedDates) {
      this.initialPeriodDates = storedDates;
    }

    this.recapTableSubscription = viewCurvesRecapTableSubject$.asObservable().subscribe(recapData => {
      this.recapTableData = [...recapData.data];
      if (recapData.updateRecapRates) {
        this.onSelectRange(this.selectedRange);
      }
    });
  }

  removeGraphByReferUuidList(referUuidList) {
    referUuidList.forEach((referUuid) => {
      this.stockChartService.removeGraph(referUuid);
      remove(this.selectedChannels, {uuid: referUuid});
    });
  }

  _updateChannelRange(uuidList) {
    this.channelRangeService.getChannelRangesBySiteList(uuidList).subscribe((res: any[]) => {
      this.channelRanges = res;
      const rangeToRemove = difference(
        map(
          filter(values(viewCurvesSubject$.value.graphRecipes), (recipe) => recipe.isRange),
          (range) => range.channelUuid
        ),
        map(this.channelRanges, (range) => range.uuid)
      );
      if (rangeToRemove.length > 0) {
        this.removeGraphByReferUuidList(rangeToRemove);
      }
    });
  }

  onChannelRangeSelected(channelRange) {
    if (channelRange.rangeType === ChannelRangeTypeEnum.RATE) {
      this.onSelectRange(channelRange);
    }
    if (channelRange.rangeType === ChannelRangeTypeEnum.PROFILE) {
      let axis = ChartAxis.LEFT_RIGHT;
      let checked = false;
      this.selectedChannels.forEach((selectedChannel) => {
        if (findIndex(selectedChannel.channelRanges, (range: any) => range.id === channelRange.id ) !== -1 && !checked) {
          axis = viewCurvesSubject$.value.graphRecipes[selectedChannel.uuid].axis;
          checked = true;
        }
      });
      const graphRecipe: IGraphRecipe = {
        axis: axis,
        channelId: channelRange.id,
        channelUuid: channelRange.uuid,
        channelName: channelRange.name,
        channel: channelRange,
        isRange: true
      };
      this.selectedChannels.push(channelRange);
      this.stockChartService.addGraphsByRecipes([graphRecipe], this.currentPeriod).subscribe();
    }
  }

  onChannelRangeDeselected(channelRange) {
    if (channelRange.rangeType === ChannelRangeTypeEnum.RATE) {
      this.onSelectRange(initialRange);
    }
    if (channelRange.rangeType === ChannelRangeTypeEnum.PROFILE) {
      this.stockChartService.removeGraph(channelRange.uuid);
      remove(this.selectedChannels, {uuid: channelRange.uuid});
    }
  }

  onSelectRange(channelRange) {
    this.selectedRange = channelRange;
    if (channelRange.skip) {
      this.removeRatesHeader();
      viewCurvesGuidesSubject$.next(null);
      return;
    }
    this._calculateAndDispatchGuides(channelRange, this.currentPeriod, true);
  }

  recapTableDataForRates(guides, channelRange) {
    this.removeRatesHeader();
    const headerRanges = [];
    const rates = keyBy(values(guides), 'name');
    const recapRateDataObservables = [];
    this.recapTableData.forEach((recapData) => {
      recapRateDataObservables.push(
        this.measureService.getRecapRatesMeasuresForChannelUuid(
          recapData.channelUuid,
          channelRange.uuid,
          this.currentPeriod
        )
      );
    });
    forkJoin(recapRateDataObservables).subscribe(resps => {
      resps.forEach((resp: any, index) => {
        this.recapTableData[index].recapRatesData = transform(resp, (result: any, v, k) => {
          set(result, k + '.totalRateValue', v);
          headerRanges.push(rates[k]);
          return result;
        }, {});
      });
      this.addRatesHeader(
        uniqBy(headerRanges, 'id').sort((e1, e2) => {
          return e1.id > e2.id ? 1 : -1;
        })
      );
    });
    viewCurvesRecapTableSubject$.next({ data: this.recapTableData });
  }

  private addRatesHeader(headerRanges) {
    const groupHeader = {
      columnName: this.selectedRange.name,
      colspan: headerRanges.length,
      isRate: true,
      children: []
    };
    const rangesList = [];
    forEach(headerRanges, headerRange => {
      const headerIndex = findIndex(this.tableConfig.header, (header: any) => {
        return header.columnName === headerRange.name;
      });
      if (headerIndex === -1) {
        const column: any = {
          fieldName: 'recapRatesData.' + headerRange.name + '.totalRateValue',
          columnName: headerRange.name,
          pipe: 'engFormatWithUnit',
          pipeArgField: 'integratedSymbol',
          skipPipeOnExport: true,
          backgroundColor: headerRange.color,
          nowrap: true
        };
        rangesList.push(column);
      }
    });
    groupHeader.children = rangesList;
    this.tableConfig.header.push(groupHeader);
  }

  private removeRatesHeader() {
    remove(this.tableConfig.header, (header: any) => {
      return header.isRate;
    });
  }

  private _calculateAndDispatchGuides(channel, period, updateGuides) {
    this.cloeChartService.getGuideFromChannelRange(channel, period, updateGuides).subscribe(r => {
      viewCurvesGuidesSubject$.next(this._isPeriodGuidesNotCompliance() ? null : r);
      this.recapTableDataForRates(r, channel);
    });
  }

  ngOnDestroy(): void {
    viewCurvesSubject$.next({
      graphRecipes: {},
      period: {},
      dataProvider: [],
      recapTableData: []
    });
    this.selectedRange = initialRange;
    viewCurvesRecapTableSubject$.next({ data: [], updateRecapRates: true });
    viewCurvesGuidesSubject$.next(null);
    this.channelStateSubscription.unsubscribe();
    this.siteTreeViewSubscription.unsubscribe();
    this.recapTableSubscription.unsubscribe();
  }

  compareRange(r1, r2): boolean {
    return r1 && r2 ? r1.id === r2.id : r1 === r2;
  }

  onPeriodSelected(e) {
    StorageService.saveObject('initialViewCurvesDates', {
      startDate: moment(e.startDate, 'YYYY-MM-DD').format('YYYY-MM-DD'),
      endDate: moment(e.endDate, 'YYYY-MM-DD').format('YYYY-MM-DD'),
      initialPeriodType: e.periodType,
      initialRangeType: e.rangeType,
      isFreeRange: e.isFreeRange,
      integrated: e.integrated
    });
    this.currentPeriod = e;
    const currentState: IViewCurvesState = viewCurvesSubject$.value;
    const recipes = values(currentState.graphRecipes);

    this.stockChartService.addGraphsByRecipes(recipes, this.currentPeriod, true).subscribe();
  }

  private _isPeriodGuidesNotCompliance() {
    // Guides doesn't exists for YEAR, MONTH, WEEK, DAY
    return this.currentPeriod.periodType === PeriodTypeEnum.YEAR ||
      this.currentPeriod.periodType === PeriodTypeEnum.MONTH ||
      this.currentPeriod.periodType === PeriodTypeEnum.WEEK ||
      this.currentPeriod.periodType === PeriodTypeEnum.DAY;
  }

  handleChartAction(e: any) {
    switch (e.action) {
      case CLOE_CHARTS_ACTIONS.TOGGLE_BULLETS:
        this.bulletActive = !this.bulletActive;
        e.toggleActive = this.bulletActive;
        chartLegendSubject$.next({
          action: CLOE_CHARTS_ACTIONS.TOGGLE_BULLETS
        });
        break;
      case CLOE_CHARTS_ACTIONS.INTEGRATE:
        e.toggleActive = !e.toggleActive;
        const currentState: IViewCurvesState = viewCurvesSubject$.value;
        const recipes = values(currentState.graphRecipes);
        const uuids = recipes
          .filter((r: IGraphRecipe) => this.cloeChartService.checkChannelIsIntegrable(r))
          .map((r: any) => r.channelUuid);
        this.stockChartService.updateGraphsByUuid(uuids, {integrated: e.toggleActive});
        this.onSelectRange(this.selectedRange);
        break;
    }
  }
}
