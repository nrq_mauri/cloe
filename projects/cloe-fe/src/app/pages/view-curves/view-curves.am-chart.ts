export const viewCurvesChartConfig = {
  type: 'stock',
  language: 'it',
  categoryAxesSettings: {
    // minPeriod: 'mm',
    groupToPeriods: []
  },
  // currentPeriod: 'mm',
  // synchronizeGrid: true,
  zoomOutOnDataSetChange: true,
  dataSets: [{
    fieldMappings: [
    ],
    dataProvider: [
    ],
    categoryField: 'timeStamp'
  }],
  panels: [{
    stockGraphs: [
    ],
    categoryAxis: {
      parseDates: true,
      // minPeriod: 'mm'
    },
    valueAxes: [
      {
        id: 'Y1',
        position: 'left',
        axisColor: '#373B3E',
        axisThickness: 3,
        axisAlpha: 1,
        offset: 50,
        color: '#373B3E',
        gridColor: '#373B3E',
        gridAlpha: 0,
        synchronizationMultiplier: 1
      },
      {
        id: 'Y2',
        position: 'left',
        axisColor: '#373B3E',
        axisThickness: 3,
        axisAlpha: 1,
        color: '#373B3E',
        gridColor: '#373B3E',
        gridAlpha: 0.7,
        synchronizationMultiplier: 1
      },
      {
        id: 'Y3',
        position: 'right',
        axisColor: '#373B3E',
        axisThickness: 3,
        axisAlpha: 1,
        color: '#373B3E',
        gridColor: '#373B3E',
        gridAlpha: 0,
        synchronizationMultiplier: 1
      },
      {
        id: 'Y4',
        position: 'right',
        axisColor: '#373B3E',
        offset: 50,
        axisThickness: 3,
        axisAlpha: 1,
        color: '#373B3E',
        gridColor: '#373B3E',
        gridAlpha: 0,
        synchronizationMultiplier: 1
      }
    ],
    stockLegend: {
      useGraphSettings: false,
      divId: 'legendContainer',
      enabled: false
    },
    legend: {
      enabled: false
    }
  }],
  panelsSettings: {
    marginLeft: 100, // inside: false requires that to gain some space
    marginRight: 100,
    marginTop: 10,
    svgIcons: false
  },
  valueAxesSettings: {
    axisColor: '#373B3E',
    axisThickness: 1,
    axisAlpha: 1,
    inside: false
  },
  chartScrollbarSettings: {
    userPeriod: 'WW',
    color: '#000',
    backgroundColor: '#FFF',
    graphFillColor: '#DDD',
    graphFillAlpha: 0.5,
    selectedGraphFillAlpha: 0.5
  },
  chartCursorSettings: {
    valueBalloonsEnabled: true
  },
  legendSettings: {
    enabled: false,
    labelText: '[[title]]',
    valueText: ''
  },
  export: {
    enabled: true,
    drawing: {
      'enabled': true,
      'arrow': 'end',
      'lineCap': 'butt',
      'mode': 'pencil',
      'modes': [
        'pencil',
        'line',
        'arrow',
        'pencil',
        'line',
        'arrow'
      ],
      'color': '#000000',
      'colors': [
        '#000000',
        '#FFFFFF',
        '#FF0000',
        '#00FF00',
        '#0000FF',
        '#000000',
        '#FFFFFF',
        '#FF0000',
        '#00FF00',
        '#0000FF'
      ],
      'shapes': [
        '11.svg',
        '14.svg',
        '16.svg',
        '17.svg',
        '20.svg',
        '27.svg',
        '11.svg',
        '14.svg',
        '16.svg',
        '17.svg',
        '20.svg',
        '27.svg'
      ],
      'width': 1,
      'fontSize': 11,
      'widths': [
        1,
        5,
        10,
        15,
        1,
        5,
        10,
        15
      ],
      'opacity': 1,
      'opacities': [
        1,
        0.8,
        0.6,
        0.4,
        0.2,
        1,
        0.8,
        0.6,
        0.4,
        0.2
      ],
      'menu': [
        {
          'class': 'export-drawing',
          'menu': [
            {
              'label': 'Add ...',
              'menu': [
                {
                  'label': 'Shape ...',
                  'action': 'draw.shapes'
                },
                {
                  'label': 'Text',
                  'action': 'text'
                }
              ]
            },
            {
              'label': 'Change ...',
              'menu': [
                {
                  'label': 'Mode ...',
                  'action': 'draw.modes'
                },
                {
                  'label': 'Color ...',
                  'action': 'draw.colors'
                },
                {
                  'label': 'Size ...',
                  'action': 'draw.widths'
                },
                {
                  'label': 'Opacity ...',
                  'action': 'draw.opacities'
                },
                'UNDO',
                'REDO'
              ]
            },
            {
              'label': 'Download as ...',
              'menu': [
                {
                  label: 'PNG',
                  click: function() {
                    this.toPNG( {}, function( data ) {
                      this.download( data, 'image/png', 'amCharts.png' );
                      this.capture({}, function() {});
                    });
                  }
                },
                {
                  label: 'JPG',
                  click: function() {
                    this.toJPG( {}, function( data ) {
                      this.download( data, 'image/jpg', 'amCharts.jpg' );
                      this.capture({}, function() {});
                    });
                  }
                },
                {
                  label: 'SVG',
                  click: function() {
                    this.toSVG( {}, function( data ) {
                      this.download( data, 'image/svg', 'amCharts.svg' );
                      this.capture({}, function() {});
                    });
                  }
                },
                {
                  label: 'PDF',
                  click: function() {
                    this.toPDF( {}, function( data ) {
                      this.download( data, 'application/pdf', 'amCharts.pdf' );
                      this.capture({}, function() {});
                    });
                  }
                }
              ],
            },
            {
              label: 'Print',
              click: function() {
                this.toPRINT({}, function () {
                  this.capture({}, function() {});
                });
              }
            },
            {
              label: 'Cancel',
              click: function() {
                this.capture({}, function() {});
              }
            }
          ]
        }
      ],
      'autoClose': true,
    },
    beforeCapture: function() {
      const chart = this.setup.chart;
      chart.legendSettings.enabled = true;
      chart.validateNow();
    },
    afterCapture: function(item) {
      if (item.action !== 'draw') {
        const chart = this.setup.chart;
        setTimeout(function() {
          chart.legendSettings.enabled = false;
          chart.validateNow();
        }, 10);
      }
    }
  }
};
