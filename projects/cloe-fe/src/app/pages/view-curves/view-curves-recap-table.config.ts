import {ICloeTableStatic} from '@cloe-core/@core/@ui/components/cloe-table-static/cloe-table-static-interface/cloe-table-static-interface';

export const RECAP_TABLE_CONFIG: ICloeTableStatic = {
  header: [
    {
      fieldName: 'channelPath',
      columnName: 'global.path',
      pipe: 'ellipsis',
      pipeArgs: 4,
      skipPipeOnExport: true,
      conditionalPipe: true,
      rowspan: 2
    },
    {
      fieldName: 'channelNameWithSymbol',
      columnName: 'global.channel',
      pipe: 'ellipsis',
      pipeArgs: 4,
      skipPipeOnExport: true,
      conditionalPipe: true,
      rowspan: 2
    },
    {
      fieldName: 'avg',
      columnName: 'page.carpet-plot.average-value',
      pipe: 'engFormatWithUnit',
      pipeArgField: 'dynamicSymbol',
      skipPipeOnExport: true,
      rowspan: 2
    },
    {
      fieldName: 'min',
      columnName: 'page.carpet-plot.min-value',
      pipe: 'engFormatWithUnit',
      pipeArgField: 'dynamicSymbol',
      skipPipeOnExport: true,
      rowspan: 2
    },
    {
      fieldName: 'max',
      columnName: 'page.carpet-plot.max-value',
      pipe: 'engFormatWithUnit',
      pipeArgField: 'dynamicSymbol',
      skipPipeOnExport: true,
      rowspan: 2
    },
    {
      fieldName: 'peek',
      columnName: 'page.carpet-plot.sampling-max',
      pipe: 'engFormatWithUnit',
      pipeArgField: 'unitSymbol',
      skipPipeOnExport: true,
      rowspan: 2
    },
    {
      fieldName: 'totalIntegrated',
      columnName: 'page.carpet-plot.total-consumption',
      pipe: 'engFormatWithUnit',
      pipeArgField: 'integratedSymbol',
      skipPipeOnExport: true,
      rowspan: 2
    }
  ],
  metadata: {
    disablePagination: true,
    fixPageSize: 30
  }
};
