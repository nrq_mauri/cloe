import { NgModule } from '@angular/core';
import { ViewCurvesComponent } from './view-curves.component';
import { UIRouterModule } from '@uirouter/angular';
import { FeCoreModule } from '@fe-core/fe-core.module';
import { tabsComponents, tabsStates } from './tabs';


const state = {
  name: 'visualizza-curve',
  parent: 'channel-tree-view',
  url: '/view-curves',
  data: { breadcrumbLabel: 'component.breadcrumbs.view-curves' },
  redirectTo: 'tab-view-chart',
  component: ViewCurvesComponent
};
@NgModule({
  imports: [
    UIRouterModule.forChild({states: [state, ...tabsStates]}),
    FeCoreModule
  ],
  declarations: [ViewCurvesComponent, ...tabsComponents]
})
export class ViewCurvesModule {
}
