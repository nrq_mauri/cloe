import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HomeModule } from './home/home.module';
import { ViewCurvesModule } from './view-curves/view-curves.module';
import { ClusterCrudModule } from './cluster-crud/cluster-crud.module';
import { CarpetPlotModule } from './carpet-plot/carpet-plot.module';
import { FeCorePagesModule } from './core-pages/fe-core-pages.module';
import { CloeCoreModule, ICorePagesConfig } from '@cloe-core/@core/cloe-core.module';
import { FeTypesEnum } from '@cloe-core/@core/enums/FeTypesEnum';
import { PeriodsCompareModule } from './periods-compare/periods-compare.module';
import { DurationCurvesModule } from './duration-curves/duration-curves.module';
import { DiagnosisModule } from './diagnosis/diagnosis.module';
import { LoginModule } from '@cloe-core/@core/core-pages/login/login.module';

const corePageConfig: ICorePagesConfig = {feType: FeTypesEnum.FE};

@NgModule({
  imports: [
    // CLoE Core Pages
    CloeCoreModule.forRoot(corePageConfig),
    FeCorePagesModule,
    // CloE Modules
    FormsModule,
    HomeModule,
    LoginModule,
    ViewCurvesModule,
    ClusterCrudModule,
    PeriodsCompareModule,
    CarpetPlotModule,
    DurationCurvesModule,
    DiagnosisModule
  ]
})
export class PagesModule {
}
