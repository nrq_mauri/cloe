import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import {
  CHANNEL_VIEW_ACTIONS,
  channelTreeViewSubject$
} from '../core-pages/channel-tree-view/channel-tree-view.component';
import { ChartTypeEnum } from '@fe-core/components/cloe-period-selector/enums/ChartTypeEnum';
import { Subscription } from 'rxjs';
import { CLOE_CHARTS_ACTIONS, ICloeChart } from '@fe-core/services/cloe-chart-service/cloe-chart-service';
import {
  CloeSerialChartService,
  cloeSerialChartSubject$
} from '@fe-core/services/cloe-serial-chart-service/cloe-serial-chart-service';
import { remove, findIndex, isEqual } from 'lodash';
import { durationCurvesChartConfig } from './duration-curves.am-chart';
import { CloeDurationCurvesService } from '@fe-core/services/cloe-duration-curves-service/cloe-duration-curves-service';
import * as moment_ from 'moment';
import { PeriodTypeEnum } from '@fe-core/components/cloe-period-selector/enums/PeriodTypeEnum';
import { RangeTypeEnum } from '@fe-core/components/cloe-period-selector/enums/RangeTypeEnum';
import { StorageService } from '@cloe-core/@core/services/storage-service/storage.service';
const moment = moment_;

@Component({
  selector: 'cloe-duration-curves',
  templateUrl: './duration-curves.component.html',
  styleUrls: ['./duration-curves.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DurationCurvesComponent implements OnInit, OnDestroy {

  chartType = ChartTypeEnum.DURATION_CURVES;
  channelTreeViewSubscription: Subscription;
  selectedPeriods = [];
  chartConfig = durationCurvesChartConfig;
  charts: ICloeChart[] = [];
  initialPeriodConfig = {rangeTypeDisabled: false, periodTypeDisabled: false};
  periodPickerConfig = {
    startDate: moment().startOf('month'),
    endDate: moment().endOf('month'),
    initialPeriodType: PeriodTypeEnum.HOUR,
    initialRangeType: RangeTypeEnum.MONTH,
    isFreeRange: false
  };

  tabs = [
    {
      name: 'page.period-compare.tab-view-serial-charts',
      ngState: 'tab-view-duration-curves-chart'
    },
    {
      name: 'page.period-compare.tab-view-serial-tables',
      ngState: 'tab-view-duration-curves-table'
    }
  ];

  constructor(
    private cloeSerialChartService: CloeSerialChartService,
    private cloeDurationCurvesChartService: CloeDurationCurvesService,
  ) {
    this.charts = this.cloeSerialChartService.charts;
    this.channelTreeViewSubscription = channelTreeViewSubject$.asObservable().subscribe((e: any) => {
      if (e.action === CHANNEL_VIEW_ACTIONS.NODE_ACTIVE) {
        const serialChart: ICloeChart = {
          divId: e.referUuid + 'divId',
          channelUuid: e.referUuid,
          channelName: e.name,
          channelPath: e.channelPath,
          configuration: this.chartConfig,
          graphsConfig: [],
          dataProvider: [],
          unitSymbol: e.referObject.unitSymbol,
          integratedSymbol: e.referObject.unitSymbol
        };
        this.cloeSerialChartService.addSerialChart(serialChart);
        this.selectedPeriods.forEach( (period) => {
          this.cloeDurationCurvesChartService.createDurationCurvesGraphFromPeriod(period, serialChart);
        });
      }
      if (e.action === CHANNEL_VIEW_ACTIONS.NODE_DEACTIVE) {
        this.cloeSerialChartService.removeSerialChartByChannelUuid(e.referUuid);
        cloeSerialChartSubject$.next({
          action: CLOE_CHARTS_ACTIONS.REMOVE_CHART
        });
      }
    });
  }

  ngOnInit(): void {
    this.cloeDurationCurvesChartService.setDurationCurvesGuidesListener(this.chartConfig);
    const storedDates: any = StorageService.getObject('initialDurationCurvesDates');
    if (storedDates) {
      this.periodPickerConfig = undefined;
      this.selectedPeriods = storedDates;
      this.initialPeriodConfig.periodTypeDisabled = true;
      this.initialPeriodConfig.rangeTypeDisabled = true;
      this.periodPickerConfig = {
        startDate: moment(storedDates[0].startDate),
        endDate: moment(storedDates[0].startDate),
        initialPeriodType: storedDates[0].periodType,
        initialRangeType: storedDates[0].rangeType,
        isFreeRange: storedDates[0].isFreeRange
      };
    }
  }

  onPeriodSelected(e) {
    const periodIndex = findIndex(this.selectedPeriods, (period) => {
      return isEqual(e, period);
    });
    if (periodIndex === -1 && this.selectedPeriods.length < 5) {
      if (!this.initialPeriodConfig.periodTypeDisabled) {
        this.initialPeriodConfig.periodTypeDisabled = true;
        this.initialPeriodConfig.rangeTypeDisabled = true;
      }
      this.selectedPeriods.push(e);
      StorageService.saveObject('initialDurationCurvesDates', this.selectedPeriods);
      this.charts.forEach( (serialChart) => {
        this.cloeDurationCurvesChartService.createDurationCurvesGraphFromPeriod(e, serialChart);
      });
    }
  }

  clearSelections() {
    channelTreeViewSubject$.next({action: CHANNEL_VIEW_ACTIONS.CLEAR_SELECTION});
    remove(this.selectedPeriods);
    this.initialPeriodConfig.periodTypeDisabled = false;
    this.initialPeriodConfig.rangeTypeDisabled = false;
    cloeSerialChartSubject$.next({
      action: CLOE_CHARTS_ACTIONS.CLEAR_ALL_THRESHOLDS
    });
  }

  ngOnDestroy(): void {
    this.cloeSerialChartService.clearSerialCharts();
    this.channelTreeViewSubscription.unsubscribe();
  }

}
