import { NgModule } from '@angular/core';
import { DurationCurvesComponent } from './duration-curves.component';
import { UIRouterModule } from '@uirouter/angular';
import { FeCoreModule } from '@fe-core/fe-core.module';
import { tabsComponents, tabsStates } from './tabs';


const state = {
  name: 'curva-durata',
  parent: 'channel-tree-view',
  url: '/duration-curves',
  data: { breadcrumbLabel: 'component.breadcrumbs.duration-curves' },
  redirectTo: 'tab-view-duration-curves-chart',
  component: DurationCurvesComponent
};

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [state, ...tabsStates]}),
    FeCoreModule
  ],
  declarations: [DurationCurvesComponent, ...tabsComponents]
})
export class DurationCurvesModule {
}
