import { cloeSerialChartSubject$ } from '@fe-core/services/cloe-serial-chart-service/cloe-serial-chart-service';
import { ChartAxis, CLOE_CHARTS_ACTIONS } from '@fe-core/services/cloe-chart-service/cloe-chart-service';

function handleLegendClick(graph) {
  const chart = graph.chart;
  for (let i = 0; i < chart.graphs.length; i++) {
    if (graph.id === chart.graphs[i].id) {
      if (graph.hidden) {
        chart.showGraph(chart.graphs[i]);
        cloeSerialChartSubject$.next({
          action: CLOE_CHARTS_ACTIONS.CLEAR_THRESHOLDS,
          data: graph.uuid
        });
        // chart.chartScrollbar.graph = chart.graphs[i];
      }
    } else {
      chart.hideGraph(chart.graphs[i]);
    }
  }
  // return false so that default action is canceled
  return false;
}

export const durationCurvesChartConfig = {
  'type': 'serial',
  'theme': 'light',
  'categoryField': 'formattedxAxis',
  'categoryAxis': {
    'axisAlpha': 0,
    'gridAlpha': 0.1
  },
  'categoryAxesSettings': {},
  'graphs': [],
  'singleGraphVisible': true,
  'valueAxes': [
    {
      'id': ChartAxis.LEFT_RIGHT,
      'minimum': 0
    }
  ],
  'legend': {
    'useGraphSettings': true,
    'valueText': '',
    'position': 'top',
    'clickMarker': handleLegendClick,
    'clickLabel': handleLegendClick
  },
  'dataProvider': [],
  'guides': [],
  'chartCursor': {
    'valueLineBalloonEnabled': false,
    'valueLineAlpha': 0.5,
    'cursorColor': '#FF0000',
    'categoryBalloonEnabled': true,
    'zoomable': false
  },
  'chartCursorSettings': {
    'valueBalloonsEnabled': true
  },
  'pathToImages': 'http://cdn.amcharts.com/lib/3/images/', // required for grips
  'chartScrollbar': {
    'scrollbarHeight': 40,
    'offset': 40,
    'updateOnReleaseOnly': true,
    'oppositeAxis': false
  },
  'export': {
    'enabled': true
  },
  'listeners': [{
    'event': 'changed',
    'method': (e) => {
      /**
       * Log cursor's last known position
       */
      e.chart.lastCursorPosition = e.index;
    }
  }]
};
