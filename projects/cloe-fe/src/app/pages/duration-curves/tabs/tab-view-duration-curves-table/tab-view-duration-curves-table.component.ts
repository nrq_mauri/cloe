import { Component, Input, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { CsvService } from '@cloe-core/@core/services/csv-service/csv.service';
import { cloneDeep, remove, last } from 'lodash';
import { DURATION_CURVES_TABLE_CONFIG } from './tab-duration-curves-table.config';
import {
  CloeSerialChartService, cloeSerialChartSubject$
} from '@fe-core/services/cloe-serial-chart-service/cloe-serial-chart-service';
import {
  CLOE_CHARTS_ACTIONS,
  CloeChartService, GraphConfig
} from '@fe-core/services/cloe-chart-service/cloe-chart-service';
import { CloeTableStaticComponent } from '@cloe-core/@core/@ui/components/cloe-table-static';
import { ArraySortPipe } from '@cloe-core/@core/@ui/pipes';

@Component({
  selector: 'cloe-tab-view-table',
  templateUrl: './tab-view-duration-curves-table.component.html',
  styleUrls: ['./tab-view-duration-curves-table.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TabViewDurationCurvesTableComponent implements OnInit, OnDestroy {

  @ViewChild(CloeTableStaticComponent) staticTable: CloeTableStaticComponent;
  tableSerialViewSubscription;
  tableConfig = cloneDeep(DURATION_CURVES_TABLE_CONFIG);
  headerDuration = {
    fieldName: 'formattedxAxis',
    columnName: 'page.duration-curves.duration',
    rowspan: 2
  };
  currentGraph: GraphConfig = { id: '', data: [] };
  graphsConfig = [];
  charts = [];

  @Input() site;

  constructor(
    private cloeSerialChartService: CloeSerialChartService,
    private cloeChartService: CloeChartService,
    private csvService: CsvService,
    private arraySortPipe: ArraySortPipe
  ) {
  }

  ngOnInit() {
    this.charts = this.cloeSerialChartService.charts;
    if (this.charts.length >= 1) {
      this.updateData();
    }
    this.tableSerialViewSubscription = cloeSerialChartSubject$.asObservable().subscribe((e: any) => {
      switch (e.action) {
        case CLOE_CHARTS_ACTIONS.UPDATE_DATA_PROVIDER:
          this.updateData();
          break;
        case CLOE_CHARTS_ACTIONS.REMOVE_CHART:
          this.charts.length > 0 ? this.updateData() : this.clearColumns();
          break;
        case CLOE_CHARTS_ACTIONS.CLEAR_CHART:
          this.clearColumns();
          break;
      }
    });
  }

  private clearColumns() {
    remove(this.tableConfig.header);
    this.currentGraph = { id: '', data: [] };
    this.graphsConfig = [];
  }

  private updateData() {
    this.graphsConfig = last(this.charts).graphsConfig;
    this.currentGraph = last(this.graphsConfig);
    this.updateColumns();
  }

  updateColumns() {
    const columnHeaders = [];
    this.charts.forEach((serialChart) => {
      columnHeaders.push(
        {
          columnName: serialChart.channelPath,
          colspan: 1,
          children: [
            {
              fieldName: this.cloeChartService._id(this.currentGraph.id),
              columnName: `${serialChart.channelName} (${serialChart.unitSymbol})`,
              pipe: 'engFormat',
              pipeArgs: serialChart.unitSymbol === '-' ? 'cosfi' : undefined,
              skipPipeOnExport: true
            }
          ]
        }
      );
    });
    remove(this.tableConfig.header, () => true);
    this.tableConfig.header.push(this.headerDuration, ...columnHeaders);
  }

  downloadCSV() {
    const dataArray = [];
    dataArray.push(
      this.csvService.generateDataToExportCSV(
        this.tableConfig.header,
        this.arraySortPipe.transform(this.currentGraph.data, this.staticTable.metadata)
      )
    );
    this.csvService.downloadArrayCSV(dataArray, {
      outputFileName: this.site.name + '_period_' + this.currentGraph.name + '_'
    });
  }

  ngOnDestroy(): void {
    this.tableSerialViewSubscription.unsubscribe();
  }
}
