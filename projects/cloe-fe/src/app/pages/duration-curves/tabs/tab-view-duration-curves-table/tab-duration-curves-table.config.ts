import {ICloeTableStatic} from '@cloe-core/@core/@ui/components/cloe-table-static/cloe-table-static-interface/cloe-table-static-interface';

export const DURATION_CURVES_TABLE_CONFIG: ICloeTableStatic = {
  header: [],
  metadata: {
    sort: 'formattedxAxis',
    sortType: 'asc'
  },
  pagination: {
    'size': 30,
    'page': 1,
    'sizeOptions': [30, 50, 70, 100],
    'maxSize': 5
  }
};
