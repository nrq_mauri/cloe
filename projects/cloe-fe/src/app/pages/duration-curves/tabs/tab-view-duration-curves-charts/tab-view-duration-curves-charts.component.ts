import { AfterViewInit, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ICloeChart } from '@fe-core/services/cloe-chart-service/cloe-chart-service';
import { CloeSerialChartService } from '@fe-core/services/cloe-serial-chart-service/cloe-serial-chart-service';

@Component({
  selector: 'cloe-tab-view-duration-curves-chart',
  templateUrl: './tab-view-duration-curves-charts.component.html',
  styleUrls: ['./tab-view-duration-curves-charts.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TabViewDurationCurvesChartsComponent implements OnInit, OnDestroy, AfterViewInit {

  charts: ICloeChart[];

  constructor(
    private cloeSerialChartService: CloeSerialChartService
  ) {}

  ngOnInit() {
    this.charts = this.cloeSerialChartService.charts;
  }

  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
  }
}
