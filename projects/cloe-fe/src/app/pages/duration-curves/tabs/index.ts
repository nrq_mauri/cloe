import {
  TabViewDurationCurvesTableComponent,
} from './tab-view-duration-curves-table/tab-view-duration-curves-table.component';
import {
  TabViewDurationCurvesChartsComponent
} from './tab-view-duration-curves-charts/tab-view-duration-curves-charts.component';

export const tabsComponents = [
  TabViewDurationCurvesChartsComponent,
  TabViewDurationCurvesTableComponent
];

export const tabsStates = [
  {
    name: 'tab-view-duration-curves-chart',
    parent: 'curva-durata',
    url: '/duration-curves-charts',
    data: {
      stateAs: 'curva-durata',
      breadcrumbLabel: 'component.breadcrumbs.chart'
    },
    component: TabViewDurationCurvesChartsComponent
  },
  {
    name: 'tab-view-duration-curves-table',
    parent: 'curva-durata',
    url: '/duration-curves-tables',
    data: {
      stateAs: 'curva-durata',
      breadcrumbLabel: 'component.breadcrumbs.table'
    },
    component: TabViewDurationCurvesTableComponent
  }
];
