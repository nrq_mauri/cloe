import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import {
  CHANNEL_VIEW_ACTIONS,
  channelTreeViewSubject$
} from '../core-pages/channel-tree-view/channel-tree-view.component';
import { ChartTypeEnum } from '@fe-core/components/cloe-period-selector/enums/ChartTypeEnum';
import { forkJoin, Subscription } from 'rxjs';
import {
  ChartAxis,
  CLOE_CHARTS_ACTIONS,
  CloeChartService,
  ICloeChart
} from '@fe-core/services/cloe-chart-service/cloe-chart-service';
import {
  CloeSerialChartService,
  cloeSerialChartSubject$
} from '@fe-core/services/cloe-serial-chart-service/cloe-serial-chart-service';
import { remove, findIndex, isEqual, last, forEach, map, cloneDeep } from 'lodash';
import { periodCompareChartConfig } from './period-compare.am-chart';
import { CloeComparePeriodService } from '@fe-core/services/cloe-compare-period-service/cloe-compare-period-service';
import { PeriodTypeEnum } from '@fe-core/components/cloe-period-selector/enums/PeriodTypeEnum';
import { RangeTypeEnum } from '@fe-core/components/cloe-period-selector/enums/RangeTypeEnum';
import * as moment_ from 'moment';
import { StorageService } from '@cloe-core/@core/services/storage-service/storage.service';
import { MeasureService } from '@fe-core/services/measure-service/measure-service';
import { IExpandableTableConfig } from '@fe-core/components/cloe-expandable-tables/cloe-expandable-tables.models';
import { COMPARE_TABLE_RECAP_BASE_CONFIG, COMPARE_TABLE_RECAP_CONFIG } from './periods-compare.tablet.config';

const moment = moment_;

@Component({
  selector: 'cloe-periods-compare',
  templateUrl: './periods-compare.component.html',
  styleUrls: ['./periods-compare.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PeriodsCompareComponent implements OnInit, OnDestroy {

  chartType = ChartTypeEnum.SERIAL_CHART;
  channelTreeViewSubscription: Subscription;
  selectedPeriods = [];
  chartConfig = periodCompareChartConfig;
  initialPeriodConfig = {rangeTypeDisabled: false, periodTypeDisabled: false};
  periodPickerConfig = {
    startDate: moment().startOf('month'),
    endDate: moment().endOf('month'),
    initialPeriodType: PeriodTypeEnum.HOUR,
    initialRangeType: RangeTypeEnum.MONTH,
    isFreeRange: false
  };
  charts: ICloeChart[] = [];
  integrated: boolean;

  compareData = {};
  currentPeriodIndex = 0;
  recapTableConfig: IExpandableTableConfig = cloneDeep(COMPARE_TABLE_RECAP_CONFIG);
  channelsInfoData = {};

  tabs = [
    {
      name: 'page.period-compare.tab-view-serial-charts',
      ngState: 'tab-view-serial-chart'
    },
    {
      name: 'page.period-compare.tab-view-serial-tables',
      ngState: 'tab-view-serial-table'
    }
  ];

  constructor(
    private cloeSerialChartService: CloeSerialChartService,
    private cloeComparePeriodChartService: CloeComparePeriodService,
    private measureService: MeasureService,
    private cloeChartService: CloeChartService
  ) {
    this.integrated = !!StorageService.getCurrentUser().cloeUser.useIntegratedValues;
    this.charts = this.cloeSerialChartService.charts;
    this.channelTreeViewSubscription = channelTreeViewSubject$.asObservable().subscribe((e: any) => {
      if (e.action === CHANNEL_VIEW_ACTIONS.NODE_ACTIVE) {
        const serialChart: ICloeChart = {
          divId: e.referUuid + 'divId',
          channelUuid: e.referUuid,
          channelName: e.name,
          channelPath: e.channelPath,
          configuration: this.chartConfig,
          graphsConfig: [],
          dataProvider: [],
          unitSymbol:  e.referObject.unitSymbol,
          integratedSymbol: e.referObject.integratedSymbol,
          integrated: this.integrated,
          integrable: this.cloeChartService.checkChannelIsIntegrable(e.referObject)
        };
        this.cloeSerialChartService.addSerialChart(serialChart);
        this._addPeriodsToChart(this.selectedPeriods, serialChart);
      }

      if (e.action === CHANNEL_VIEW_ACTIONS.NODE_DEACTIVE) {
        console.log('e', e);
        delete this.compareData[e.referUuid];
        console.log('delete', this.compareData);
        this._calculateRecapTableData();
        this.cloeSerialChartService.removeSerialChartByChannelUuid(e.referUuid);
      }
    });
  }

  refPeriodChange(e) {
    this._calculateRecapTableData();
  }

  private _addPeriodsToChart(periods: any[], chart: ICloeChart) {
    const measureObservables = [];
    periods.forEach( (period) => {
      measureObservables.push(this.measureService.getMeasuresForChannelUuid(chart.channelUuid, period));
    });
    forkJoin(
      measureObservables
    ).subscribe((resp: Array<any>) => {
      let index = 0;
      resp.forEach( (data) => {
        if (data.measures && data.measures.length > 0) {
          const period = periods[index++];
          if (!this.compareData[data.channelUuid]) {
            this.compareData[data.channelUuid] = [];
          }
          const periodIndex = findIndex(this.selectedPeriods, p => {
            return p.startDate === period.startDate && p.endDate === period.endDate;
          });
          this.compareData[data.channelUuid][periodIndex] = data.summary.totalIntegrated;
          this.channelsInfoData[data.channelUuid] = {
            channelId: data.channelId,
            channelUuid: data.channelUuid,
            channelName: data.channelName,
            channelPath: chart.channelPath,
            summary: data.summary
          };
          const graphConfig = this.cloeSerialChartService._createGraphConfigForPeriod(
            ChartAxis.LEFT_RIGHT,
            period,
            data
          );
          if (period.isFreeRange) {
            this.cloeComparePeriodChartService.freeRangeDataAdapter(data.measures, graphConfig);
          } else {
            this.cloeComparePeriodChartService.dataAdapter(data.measures, period, graphConfig);
          }
          chart.graphsConfig.push(graphConfig);
          const dataProvider = chart.dataProvider;
          chart.dataProvider = this.cloeChartService._mergeDataProvider(
            this.cloeChartService._id(graphConfig.id),
            dataProvider,
            data.measures,
            'formattedxAxis'
          );
          cloeSerialChartSubject$.next({
            action: CLOE_CHARTS_ACTIONS.UPDATE_DATA_PROVIDER
          });
        }
      });
      this._calculateRecapTableData();
    });
  }

  private _calculateRecapTableData() {
    const recapConfigData = cloneDeep(COMPARE_TABLE_RECAP_CONFIG);
    forEach(this.compareData, (v: any[], k) => {
      let index = 0;
      const data = map(v, maxValue => {
        const result: any = {
          period: this.selectedPeriods[index],
          total: maxValue,
          integratedSymbol: this.channelsInfoData[k].summary.integratedSymbol
        };
        if (v.length > 1 && index !== this.currentPeriodIndex) {
          const refValue = this.compareData[k][this.currentPeriodIndex];
          result.deltaRef = maxValue - refValue;
          result.deltaRefPercent = (maxValue - refValue) / refValue * 100;
        }
        index++;
        return result;
      });
      const tableConfig = cloneDeep(COMPARE_TABLE_RECAP_BASE_CONFIG);
      tableConfig.headerTitle =  this.channelsInfoData[k].channelName + ' (' + this.channelsInfoData[k].channelPath + ')';
      tableConfig.data = [...data];
      recapConfigData.tables.push(tableConfig);
    });
    this.recapTableConfig = { ...recapConfigData };
  }

  ngOnInit(): void {
    const storedDates: any = StorageService.getObject('initialPeriodCompareDates');
    if (storedDates) {
      this.periodPickerConfig = undefined;
      this.selectedPeriods = storedDates;
      this.initialPeriodConfig.periodTypeDisabled = true;
      this.initialPeriodConfig.rangeTypeDisabled = true;
      this.periodPickerConfig = {
        startDate: moment(storedDates[0].startDate),
        endDate: moment(storedDates[0].startDate),
        initialPeriodType: storedDates[0].periodType,
        initialRangeType: storedDates[0].rangeType,
        isFreeRange: storedDates[0].isFreeRange
      };
    }
  }

  onPeriodSelected(e) {
    const lastPeriod = last(this.selectedPeriods);
    if (lastPeriod && e.isFreeRange && e.rangeType !== lastPeriod.rangeType) {
      return; // TODO notificare il problema: coerenza rangetype in selezioni libere
    }
    const periodIndex = findIndex(this.selectedPeriods, (period) => {
      return isEqual(e, period);
    });
    if (periodIndex === -1 && this.selectedPeriods.length < 5) {
      if (!this.initialPeriodConfig.periodTypeDisabled) {
        this.initialPeriodConfig.periodTypeDisabled = true;
        this.initialPeriodConfig.rangeTypeDisabled = true;
      }
      this.selectedPeriods.push(e);
      StorageService.saveObject('initialPeriodCompareDates', this.selectedPeriods);
      this.charts.forEach( (serialChart) => {
        this._addPeriodsToChart([e], serialChart);
      });
    }
  }

  clearSelections() {
    channelTreeViewSubject$.next({action: CHANNEL_VIEW_ACTIONS.CLEAR_SELECTION});
    remove(this.selectedPeriods);
    this.initialPeriodConfig.periodTypeDisabled = false;
    this.initialPeriodConfig.rangeTypeDisabled = false;
    this.compareData = {};
    this._calculateRecapTableData();
  }

  ngOnDestroy(): void {
    this.cloeSerialChartService.clearSerialCharts();
    this.channelTreeViewSubscription.unsubscribe();
  }

}
