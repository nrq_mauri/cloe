import { NgModule } from '@angular/core';
import { PeriodsCompareComponent } from './periods-compare.component';
import { UIRouterModule } from '@uirouter/angular';
import { FeCoreModule } from '@fe-core/fe-core.module';
import { tabsComponents, tabsStates } from './tabs';


const state = {
  name: 'confronta-periodi',
  parent: 'channel-tree-view',
  url: '/periods-compare',
  data: { breadcrumbLabel: 'component.breadcrumbs.compare-periods' },
  redirectTo: 'tab-view-serial-chart',
  component: PeriodsCompareComponent
};

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [state, ...tabsStates]}),
    FeCoreModule
  ],
  declarations: [PeriodsCompareComponent, ...tabsComponents]
})
export class PeriodsCompareModule {
}
