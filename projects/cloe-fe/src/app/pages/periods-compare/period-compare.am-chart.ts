import * as moment from 'moment';

export const periodCompareChartConfig = {
  'type': 'serial',
  'theme': 'light',
  'categoryField': 'formattedxAxis',
  'categoryAxis': {
    'axisAlpha': 0,
    'gridAlpha': 0.1,
    'labelFunction': function(valueText, date, categoryAxis) {
      if (date.dataContext.labelFormat) {
        return moment(date.dataContext.timeStamp).format(date.dataContext.labelFormat);
      } else {
        return date.dataContext.formattedxAxis;
      }
    }
  },
  'categoryAxesSettings': {
  },
  'graphs': [
  ],
  'valueAxes': [
  ],
  'legend': {
    'useGraphSettings': true,
    'valueText': '',
    'position': 'top'
  },
  'dataProvider': [
  ],
  'chartCursor': {
    'valueLineBalloonEnabled': false,
    'valueLineAlpha': 0.5,
    'cursorColor': '#FF0000',
    'categoryBalloonEnabled': false
  },
  'chartCursorSettings': {
    'valueBalloonsEnabled': true
  },
  'pathToImages': 'http://cdn.amcharts.com/lib/3/images/', // required for grips
  'chartScrollbar': {
    'scrollbarHeight': 40,
    'offset': 40,
    'updateOnReleaseOnly': true,
    'oppositeAxis': false
  },
  'export': {
    'enabled': true
  }
};
