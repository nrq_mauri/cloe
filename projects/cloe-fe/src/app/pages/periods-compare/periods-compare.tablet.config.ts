const headerConfig = [
  {
    columnName: 'global.period',
    colspan: 2,
    children: [
      { fieldName: 'period.startDate', columnName: 'global.startDate', pipe: 'cloeDate'},
      { fieldName: 'period.endDate', columnName: 'global.endDate', pipe: 'cloeDate'},
    ]
  },
  {
    columnName: 'global.total',
    fieldName: 'total',
    rowspan: 2,
    pipe: 'engFormatWithUnit',
    pipeArgField: 'integratedSymbol',
  },
  {
    columnName: 'global.deltaRef',
    fieldName: 'deltaRef',
    rowspan: 2,
    pipe: 'engFormatWithUnit',
    pipeArgField: 'integratedSymbol',
  },
  {
    columnName: 'global.deltaRefPercent',
    fieldName: 'deltaRefPercent',
    rowspan: 2,
    pipe: 'percent'
  }
];

export const COMPARE_TABLE_RECAP_BASE_CONFIG = {
  headerTitle: '',
  data: [],
  config: {
    header: [ ...headerConfig ],
    metadata: {
      disablePagination: true,
      fixPageSize: 10
    }
  }
};

export const COMPARE_TABLE_RECAP_CONFIG = {
  multiExpand: true,
  tables: [
  ]
};

