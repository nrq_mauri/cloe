import { TabViewSerialTableComponent } from './tab-view-serial-table/tab-view-serial-table.component';
import { TabViewSerialChartsComponent } from './tab-view-serial-charts/tab-view-serial-charts.component';

export const tabsComponents = [
  TabViewSerialChartsComponent,
  TabViewSerialTableComponent
];

export const tabsStates = [
  {
    name: 'tab-view-serial-chart',
    parent: 'confronta-periodi',
    url: '/serial-charts',
    data: {
      stateAs: 'confronta-periodi',
      breadcrumbLabel: 'component.breadcrumbs.chart'
    },
    component: TabViewSerialChartsComponent
  },
  {
    name: 'tab-view-serial-table',
    parent: 'confronta-periodi',
    url: '/serial-tables',
    data: {
      stateAs: 'confronta-periodi',
      breadcrumbLabel: 'component.breadcrumbs.table'
    },
    component: TabViewSerialTableComponent
  }
];
