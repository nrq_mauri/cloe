import { Component, Input, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { CsvService } from '@cloe-core/@core/services/csv-service/csv.service';
import { cloneDeep, remove, concat, keyBy, values, merge, map } from 'lodash';
import { COMPARE_TABLE_CONFIG } from './tab-view-serial-table.config';
import {
  CloeSerialChartService,
  cloeSerialChartSubject$
} from '@fe-core/services/cloe-serial-chart-service/cloe-serial-chart-service';
import {
  CLOE_CHARTS_ACTIONS,
  CloeChartService, GraphConfig,
  ICloeChart
} from '@fe-core/services/cloe-chart-service/cloe-chart-service';

@Component({
  selector: 'cloe-tab-view-table',
  templateUrl: './tab-view-serial-table.component.html',
  styleUrls: ['./tab-view-serial-table.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TabViewSerialTableComponent implements OnInit, OnDestroy {

  tableSerialViewSubscription;
  tableConfig = cloneDeep(COMPARE_TABLE_CONFIG);
  headerTimeStamp = {
    fieldName: 'timeStamp',
    columnName: 'global.date',
    pipe: 'cloeDateTimeUTC',
    rowspan: 2
  };
  data = [];
  charts = [];

  @Input() site;

  constructor(
    private cloeSerialChartService: CloeSerialChartService,
    private cloeChartService: CloeChartService,
    private csvService: CsvService
  ) {
  }

  ngOnInit() {
    this.charts = this.cloeSerialChartService.charts;
    this.tableSerialViewSubscription = cloeSerialChartSubject$.asObservable().subscribe((e: any) => {
      switch (e.action) {
        case CLOE_CHARTS_ACTIONS.UPDATE_DATA_PROVIDER:
          this.updateData();
          break;
        case CLOE_CHARTS_ACTIONS.REMOVE_CHART:
          this.tableConfig.header.length === 2 ?
            this.clearColumns() :
            this.removeColumn(e.value.channelUuid);
          break;
        case CLOE_CHARTS_ACTIONS.CLEAR_CHART:
          this.clearColumns();
          break;
      }
    });
    this.updateData();
  }

  private clearColumns() {
    remove(this.tableConfig.header);
  }

  private removeColumn(channelUuid) {
    remove(this.tableConfig.header, (column) => {
      return column.fieldName === this.cloeChartService._id(channelUuid);
    });
  }

  private updateData() {
    const columnHeaders = [];
    this.charts.forEach((serialChart: ICloeChart) => {
      const unitSymbol = serialChart[this.cloeChartService.getSymbolField(serialChart.integrated)];
      columnHeaders.push({
        columnName: serialChart.channelPath,
        colspan: 1,
        children: [
          {
            fieldName: this.cloeChartService._id(serialChart.channelUuid),
            columnName: `${serialChart.channelName} (${unitSymbol})`,
            pipe: 'engFormat',
            pipeArgs: unitSymbol === '-' ? 'cosfi' : undefined,
            skipPipeOnExport: true
          }
        ]
      });
    });
    remove(this.tableConfig.header, () => true);
    this.tableConfig.header.push(this.headerTimeStamp, ...columnHeaders);
    this.updateDataProvider();
  }

  private updateDataProvider() {
    this.charts.forEach((serialChart: ICloeChart) => {
      const valueField = this.cloeChartService._id(serialChart.channelUuid);
      let tempData = [];
      serialChart.graphsConfig.forEach((graph: GraphConfig) => {
        const value = this.cloeChartService._id(graph.id, serialChart.integrated);
        tempData = concat(
          tempData,
          map(graph.data, (measure) => {
            measure[valueField] = measure[value];
            return measure;
          })
        );
      });
      this.data = values(
        merge(
          keyBy(this.data, 'timeStamp'),
          keyBy(tempData, 'timeStamp')
        )
      );
    });
  }

  downloadCSV() {
    const dataArray = [];
    dataArray.push(
      this.csvService.generateDataToExportCSV(this.tableConfig.header, this.data)
    );
    this.csvService.downloadArrayCSV(dataArray, {
      outputFileName: this.site.name
    });
  }

  ngOnDestroy(): void {
    this.tableSerialViewSubscription.unsubscribe();
  }
}
