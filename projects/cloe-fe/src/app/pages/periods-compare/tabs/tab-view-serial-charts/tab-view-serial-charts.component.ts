import { AfterViewInit, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { ICloeChart } from '@fe-core/services/cloe-chart-service/cloe-chart-service';
import { CloeSerialChartService } from '@fe-core/services/cloe-serial-chart-service/cloe-serial-chart-service';

@Component({
  selector: 'cloe-tab-view-serial-chart',
  templateUrl: './tab-view-serial-charts.component.html',
  styleUrls: ['./tab-view-serial-charts.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TabViewSerialChartsComponent implements OnInit, OnDestroy, AfterViewInit {

  charts: ICloeChart[];

  constructor(
    private cloeSerialChartService: CloeSerialChartService
  ) {}

  ngOnInit() {
    this.charts = this.cloeSerialChartService.charts;
  }

  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
  }
}
