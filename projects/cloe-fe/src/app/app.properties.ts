import {environment} from '../environments/environment';

export class AppProperties {
  constructor() {}

  static getFeType() {
    return environment.feType;
  }

  static getApiKey(service) {
    return environment.apiKeys[service];
  }
}
