import { ACTIONS } from '@cloe-core/@core/enums/ActionsEnum';
import { cloeSerialChartSubject$ } from '../../services/cloe-serial-chart-service/cloe-serial-chart-service';
import { CLOE_CHARTS_ACTIONS } from '../../services/cloe-chart-service/cloe-chart-service';

export const THRESHOLD_TABLE_CONFIG = {
  header: [
    {
      fieldName: 'thresholdName',
      columnName: 'component.threshold-table.threshold'
    },
    {
      fieldName: 'thresholdValue',
      columnName: 'global.value',
      pipe: 'engFormat'
    },
    {
      fieldName: 'operationHour',
      columnName: 'component.threshold-table.operation-hour'
    },
    {
      fieldName: 'operationHourPercent',
      columnName: 'component.threshold-table.percent-operation-hour'
    }
  ],
  actions: [
    {
      icon: 'delete',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: cloeSerialChartSubject$,
        action: CLOE_CHARTS_ACTIONS.REMOVE_THRESHOLD
      }
    }
  ],
  metadata: {
    sort: 'operationHourPercent',
    sortType: 'asc',
    disablePagination: true,
    fixPageSize: 30
  }
};

export const DURATION_CURVES_TABLE_RECAP_CONFIG = {
  multiExpand: true,
  tables: [
  ]
};
