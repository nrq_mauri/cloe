import { ChangeDetectorRef, Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import {
  DURATION_CURVES_TABLE_RECAP_CONFIG,
  THRESHOLD_TABLE_CONFIG
} from './cloe-threshold-table.config';
import { cloeSerialChartSubject$ } from '../../services/cloe-serial-chart-service/cloe-serial-chart-service';
import { CLOE_CHARTS_ACTIONS } from '../../services/cloe-chart-service/cloe-chart-service';
import { remove, round, flatMap, filter, cloneDeep, find } from 'lodash';
import { CloeDurationCurvesService } from '../../services/cloe-duration-curves-service/cloe-duration-curves-service';
import * as moment_ from 'moment';
import { PeriodType } from '../cloe-period-selector/enums/PeriodType';
import { IExpandableTable, IExpandableTableConfig } from '@fe-core/components/cloe-expandable-tables/cloe-expandable-tables.models';
import { CHANNEL_VIEW_ACTIONS, channelTreeViewSubject$ } from '../../../pages/core-pages/channel-tree-view/channel-tree-view.component';

const moment = moment_;

@Component({
  selector: 'cloe-threshold-table',
  templateUrl: './cloe-threshold-table.component.html',
  styleUrls: ['./cloe-threshold-table.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloeThresholdTableComponent implements OnInit, OnDestroy {

  serialChartSubscription;
  channelTreeViewSubscription;
  recapTableConfig: IExpandableTableConfig = cloneDeep(DURATION_CURVES_TABLE_RECAP_CONFIG);
  chartData;

  constructor(
    private changeDetector: ChangeDetectorRef,
    private durationCurvesService: CloeDurationCurvesService
  ) {
  }

  ngOnInit() {
    this.channelTreeViewSubscription = channelTreeViewSubject$.asObservable().subscribe((e: any) => {
      if (e.action === CHANNEL_VIEW_ACTIONS.NODE_ACTIVE) {
        const tableConfig: IExpandableTable = {
          tableId: e.referUuid,
          headerTitle: e.name + ' (' + e.channelPath + ')',
          data: [],
          config: THRESHOLD_TABLE_CONFIG
        };
        this.recapTableConfig.tables.push(tableConfig);
      }
      if (e.action === CHANNEL_VIEW_ACTIONS.NODE_DEACTIVE) {
        remove(this.recapTableConfig.tables, (table: any) => table.tableId === e.referUuid);
      }
    });
    this.serialChartSubscription = cloeSerialChartSubject$.asObservable().subscribe((e: any) => {
      switch (e.action) {
        case CLOE_CHARTS_ACTIONS.ADD_THRESHOLD:
          this.chartData =
            this.durationCurvesService.computeOperationHours(
              flatMap(e.value.graph.data, 'dataContext'),
              e.value.graph.period.periodType,
              e.value.graph.id
            );
          const newThresholdTable = find(this.recapTableConfig.tables, (table) => table.tableId === e.value.graph.uuid);
          newThresholdTable.data = [...newThresholdTable.data, {
            channelUuid: e.value.graph.uuid,
            thresholdName: e.value.thresholdName,
            thresholdValue: e.value.thresholdValue,
            operationHour: e.value.row.formattedxAxis,
            operationHourPercent: this.computePercentThresholdOperationHour(e.value)
          }];
          this.changeDetector.detectChanges();
          break;
        case CLOE_CHARTS_ACTIONS.REMOVE_THRESHOLD:
          const removeThresholdTable = find(this.recapTableConfig.tables, (table) => table.tableId === e.data.channelUuid);
          removeThresholdTable.data = remove(removeThresholdTable.data, (threshold: any) => {
            return threshold.thresholdValue !== e.data.thresholdValue;
          });
          this.changeDetector.detectChanges();
          break;
        case CLOE_CHARTS_ACTIONS.CLEAR_THRESHOLDS:
          this.recapTableConfig.tables.forEach((table) => {
            if (table.tableId === e.data) {
              table.data = [];
            }
          });
          this.changeDetector.detectChanges();
          break;
        case CLOE_CHARTS_ACTIONS.CLEAR_ALL_THRESHOLDS:
          this.recapTableConfig.tables.forEach((table) => {
            table.data = [];
          });
          this.changeDetector.detectChanges();
          break;
      }
    });
  }

  private computePercentThresholdOperationHour(threshold) {
    const currentGraphData = filter(threshold.graph.data, element => {
      return element.dataContext[threshold.graph.id];
    });
    const totalHours = ((PeriodType.periodTypeToSampling(threshold.graph.period.periodType) * currentGraphData.length) / 60);
    threshold.operationHourPercent = round(
      (
        (threshold.row.formattedxAxis / totalHours) * 100
      ),
        2
      );
    return threshold.operationHourPercent;
  }

  ngOnDestroy(): void {
    this.serialChartSubscription.unsubscribe();
    this.channelTreeViewSubscription.unsubscribe();
  }
}
