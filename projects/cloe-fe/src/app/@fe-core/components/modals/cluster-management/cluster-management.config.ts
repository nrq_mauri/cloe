import {NatureTypesEnum} from '@cloe-core/@core/enums/NatureTypesEnum';

export const CLUSTER_CONFIG = {
  search: {
    showedFieldName: 'name',
    fieldToSearch: {
      name: ''
    },
    nodeNature: NatureTypesEnum.CHANNEL_TYPE
  }
};

export const CLUSTER_FILTER = {
  nodeNature: NatureTypesEnum.SITE,
  isCluster: true
};
