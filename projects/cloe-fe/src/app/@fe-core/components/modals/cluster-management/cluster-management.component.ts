import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ITreeOptions, TREE_ACTIONS, TreeModel, TreeNode } from 'angular-tree-component';
import { cloneDeep } from 'lodash';
import { Tree } from '@cloe-core/@core/models/Tree';
import { Cluster } from '@cloe-core/@core/models/Cluster';
import { StorageService } from '@cloe-core/@core/services/storage-service/storage.service';
import { NatureTypesEnum } from '@cloe-core/@core/enums/NatureTypesEnum';
import { CloeModalComponentInterface } from '@cloe-core/@core/interfaces/cloe-modal-component.interface';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CloeSearchService } from '@cloe-core/@core/services/cloe-search-service/cloe.search.service';
import { NodeCategoryEnum } from '@cloe-core/@core/enums/NodeCategoryEnum';
import { forkJoin } from 'rxjs';
import { ChannelTypeService } from '@cloe-core/@core/services/channelType-service/channelType-service';
import { CloeTreeFiltersService } from '@cloe-core/@core/services/cloe-tree-service/cloe-tree-filters.service';
import { TreeFiltersTypeEnum } from '@cloe-core/@core/services/cloe-tree-service/enums/TreeFiltersTypeEnum';
import { ClusterService } from '@cloe-core/@core/services/cluster-service/cluster-service';
import { CloeTreeNodeService } from '@cloe-core/@core/services/cloe-treeNode-service/cloe-tree-node.service';
import { CloeTreeService } from '@cloe-core/@core/services/cloe-tree-service/cloe-tree.service';
import { PAGINATION } from '@cloe-core/@core/@ui/components/cloe-table/configuration/cloe-table-conf';
import { CloeTreeComponent } from '@cloe-core/@core/@ui/components';

@Component({
  selector: 'cloe-page-cluster-management',
  templateUrl: './cluster-management.component.html',
  styleUrls: ['./cluster-management.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ClusterManagementComponent implements OnInit, CloeModalComponentInterface {
  @ViewChild('currentTree') currentTreeComponent: CloeTreeComponent;
  sitesTree;
  modalTitle: string;
  clustersLimit = 5;
  upsertEnabled = false;
  currentClusterTreeRef;
  currentClusterNodes = [];
  optionsCurrentCluster: ITreeOptions = {
    allowDrag: (node: any) => node.data.nodeCategory !== 'ROOT',
    allowDrop: false,
    actionMapping: {
      mouse: {
        click: (tree, node, $event) => {
          if (node.data.nodeCategory === 'GROUP' || node.data.nodeCategory === 'ROOT') {
            if (!$event.ctrlKey) {
              tree.setActiveNode(node, false, false);
            }
            this.recursiveActiveLeaves(tree, node, $event);
          } else {
            $event.ctrlKey ? TREE_ACTIONS.TOGGLE_ACTIVE_MULTI(tree, node, $event)
              : TREE_ACTIONS.TOGGLE_ACTIVE(tree, node, $event);
          }
        },
        // drag single node without activating it first
        dragStart: (tree, node, $event) => {
          if (tree.activeNodes.length < 2) {
            tree.setActiveNode(node, false, false);
            if (node.data.nodeCategory === 'GROUP' || node.data.nodeCategory === 'ROOT') {
              this.recursiveActiveLeaves(tree, node, $event);
            } else {
              TREE_ACTIONS.ACTIVATE(tree, node, $event);
            }
          }
        }
      }
    }
  };
  newClusterTreeRef;
  newClusterNodes: any = [{name: 'ROOT', nodeCategory: 'GROUP', isNew: true, children: []}];
  optionsNewCluster: ITreeOptions = {
    allowDrag: (node: any) => node.data.nodeCategory !== 'ROOT',
    allowDrop: true,
    actionMapping: {
      mouse: {
        drop: (tree: TreeModel, node: TreeNode, $event: any, {from, to}: { from: any, to: any }) => {
          if (from.treeModel.virtualRoot.data.id === to.parent.treeModel.virtualRoot.data.id) {
            tree.moveNode(from, to);
          } else {
            this.currentClusterTreeRef.treeModel.activeNodes.forEach(n => {
              n.setIsHidden(true);
              tree.copyNode(n, to);
            });
          }
        }
      }
    },
    getNodeClone: (node: any) => {
      const newNode: any = cloneDeep(node.data);
      newNode.isNew = true;
      return newNode;
    }
  };
  newClusterNodesConfig = {
    removableNodes: [NodeCategoryEnum.LEAF],
    areFoldersAddable: true
  };
  clusterCategories;
  cluster;
  clusterTree: Tree;
  nodeIdsToDelete = [];
  domainPickerConfig = {
    labelSearch: 'global.channelType',
    itemAsObject: {
      identifyBy: 'id',
      displayBy: 'name',
      nodeNature: NatureTypesEnum.CHANNEL_TYPE,
      fieldToSearch: {
        name: ''
      },
      autocompleteItems: true
    },
    maxItems: 1
  };
  selectedChannelType: any;
  filtersConfig;

  constructor(
    private cloeTreeService: CloeTreeService,
    private clusterService: ClusterService,
    private cloeSearchService: CloeSearchService,
    private cloeTreeNodeService: CloeTreeNodeService,
    private channelTypeService: ChannelTypeService,
    private treeFiltersService: CloeTreeFiltersService,
    private modalRef: NgbActiveModal
  ) {
  }

  ngOnInit() {
    this.cluster = new Cluster();

    this.cloeSearchService.search(
      'TREE',
      {},
      cloneDeep(PAGINATION),
      { isCluster: true, nodeNature: 'SITE' }
    ).subscribe((res) => {
      if (res.content[0]) {
        this.clusterTree = res.content[0];
      }
    });

    this.clusterService.getClusterForUserAndSystem(StorageService.getCurrentUser().cloeUser.uuid).subscribe((result: any) => {
      if (result.length < this.clustersLimit) {
        this.upsertEnabled = true;
      }
    });

    this.clusterService.getClusterForUserAndSystem(StorageService.getCurrentUser().cloeUser.uuid, true).subscribe((result: any) => {
      this.clusterCategories = result;
    });
  }

  initData(data?: any) {
    this.cloeTreeService.searchTreeListForUser('SITE', StorageService.getCurrentUser().cloeUser.uuid).subscribe((res) => {
      this.sitesTree = res[0];
      if (data) {
        this.modalTitle = 'component.cluster-management.edit-cluster';
        forkJoin(
          this.cloeTreeNodeService.getSitesTreeForUser(
            StorageService.getCurrentUser(),
            this.sitesTree
          ),
          this.cloeTreeNodeService.getTreeNodeByReferUuid(
            'SITE',
            StorageService.getCurrentUser(),
            false,
            data.uuid,
            false,
            true
          )
        ).subscribe((results) => {
          this.cluster = cloneDeep(data);
          if (this.cluster.defaultChannelTypeId) {
            this.channelTypeService.getChannelTypeById(this.cluster.defaultChannelTypeId).subscribe((resp) => {
              this.selectedChannelType = [resp];
            });
          }
          this.currentClusterNodes = [results[0]];
          this.newClusterNodes = [results[1]];
          this.filtersConfig = this.addFilters(this.currentClusterNodes);
          setTimeout( () => {
              this.hidePresentSites(this.newClusterNodes[0]);
              this.currentTreeComponent.tree.sizeChanged();
            },
            500
          );
        });
      } else {
        this.modalTitle = 'component.cluster-management.new-cluster';
        this.cloeTreeNodeService.getSitesTreeForUser(
          StorageService.getCurrentUser(),
          this.sitesTree
        ).subscribe((resp: any) => {
          this.currentClusterNodes = [resp];
          this.filtersConfig = this.addFilters(this.currentClusterNodes);
          setTimeout( () => {
              this.currentTreeComponent.tree.sizeChanged();
            }
          );
        });
      }
    });
  }

  initCurrentCluster(tree) {
    this.currentClusterTreeRef = tree;
  }

  initNewCluster(tree) {
    this.newClusterTreeRef = tree;
  }

  private addFilters(nodes) {
    return this.treeFiltersService.addFiltersByType(TreeFiltersTypeEnum.SITE_FILTERS, nodes);
  }

  loadClusterNodes(treeUuid: string) {
    this.currentClusterTreeRef.treeModel.setHiddenNodeIds([]);
    this.cloeTreeNodeService.getTreeNodeByReferUuid(
      'SITE',
      StorageService.getCurrentUser(),
      false,
      treeUuid,
      false,
      true
    ).subscribe(result => {
      this.markNodesAsNew(result);
      this.newClusterNodes[0].rootElement = true;
      this.deleteAllNodes(this.newClusterNodes[0]);
      this.newClusterNodes[0].children = result.children;
    });
  }

  private deleteAllNodes(root: any) {
    if (!root.isNew && !root.rootElement) {
      this.nodeIdsToDelete.push(root.id);
    }
    if (root.children) {
      root.children.forEach(child => {
        this.deleteAllNodes(child);
      });
    }
  }

  private markNodesAsNew(root: any) {
    root.isNew = true;
    if (root.children) {
      root.children.forEach(child => {
        this.markNodesAsNew(child);
      });
    }
  }

  recursiveActiveLeaves(tree, node, $event) {
    if (node.data.nodeCategory === 'LEAF') {
      TREE_ACTIONS.TOGGLE_ACTIVE_MULTI(tree, node, $event);
    }
    node.children.forEach(child => {
      this.recursiveActiveLeaves(tree, child, $event);
    });
  }

  onFiltersChanged($event: any) {
    this.hidePresentSites(this.newClusterNodes[0]);
  }

  hidePresentSites(node) {
    if (node.nodeCategory === 'LEAF') {
      const findedNode = this.currentClusterTreeRef.treeModel.getNodeBy((currentNode) => {
        return !!currentNode.data.referObject &&
          currentNode.data.referObject.uuid === node.referObject.uuid;
      });
      if (findedNode) {
        findedNode.setIsHidden(true);
      }
    }
    if (node.children) {
      node.children.forEach(child => {
        this.hidePresentSites(child);
      });
    }
  }

  onNodeRemoved($event: any) {
    this.currentClusterTreeRef.treeModel.getNodeBy((node) => {
      return !!node.data.referObject && node.data.referObject.uuid === $event.data.referObject.uuid;
    }).setIsHidden(false);
    if (!$event.data.isNew) {
      this.nodeIdsToDelete.push($event.data.id);
    }
  }

  onSubmit() {
    this.cluster.defaultChannelTypeId = this.selectedChannelType ? this.selectedChannelType[0].id : undefined;
    this.clusterService.upsertCluster(this.cluster).subscribe((result: any) => {
      this.cloeTreeNodeService.saveTreeNodes(
        this.newClusterNodes,
        this.clusterTree,
        this.nodeIdsToDelete,
        result.uuid
      ).subscribe(() => {
        this.modalRef.close('RELOAD');
      });
    });
  }
}
