import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { CloeModalComponentInterface } from '@cloe-core/@core/interfaces/cloe-modal-component.interface';
import { CloeTreeChartService, TreeChart } from '../../../services/cloe-tree-chart-service/cloe-tree-chart-service';

@Component({
  selector: 'cloe-page-cluster-tree-view',
  templateUrl: './cluster-tree-view.component.html',
  styleUrls: ['./cluster-tree-view.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ClusterTreeViewComponent implements OnInit, OnDestroy, CloeModalComponentInterface {
  modalTitle;
  treeCharts: TreeChart[];

  constructor(
    private treeChartService: CloeTreeChartService
  ) {}

  ngOnInit() {
  }

  initData(data?: any) {
    this.modalTitle = 'component.cluster-tree-view.cluster-tree';
    this.treeCharts = this.treeChartService.treeCharts;
    if (data) {
      this.treeChartService.addTreeChart({
        divId: data.uuid + '_TREE_CHART',
        treeUuid: data.uuid
      });
    }
  }

  ngOnDestroy(): void {
    this.treeChartService.clearTreeCharts();
  }
}
