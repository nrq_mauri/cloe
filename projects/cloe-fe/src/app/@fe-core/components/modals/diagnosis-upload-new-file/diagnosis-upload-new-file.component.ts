import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { DiagnosisService } from '@cloe-core/@core/services/diagnosis-service/diagnosis.service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CloeModalComponentInterface } from '@cloe-core/@core/interfaces/cloe-modal-component.interface';
import { UPLOAD_EXCEL_ERRORS_TABLE_CONFIG } from './diagnosis-upload-new-file.table.config';
import { DiagFileTypeEnum } from '@cloe-core/@core/enums/DiagFileTypeEnum';
import { Observable } from 'rxjs/internal/Observable';

@Component({
  selector: 'cloe-diagnosis-upload-new-file',
  templateUrl: './diagnosis-upload-new-file.component.html',
  styleUrls: ['./diagnosis-upload-new-file.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DiagnosisUploadNewFileComponent implements OnInit, CloeModalComponentInterface {

  diagCode: string;
  errors: Array<any>;
  tableConfig = UPLOAD_EXCEL_ERRORS_TABLE_CONFIG;
  currentFile;
  fileType: DiagFileTypeEnum;

  constructor(
    private diagnosisService: DiagnosisService,
    public modalRef: NgbActiveModal
  ) {
  }

  ngOnInit() {
  }

  handleFileInput(files: FileList) {
    this.currentFile = files.item(0);
  }

  upload() {
    this.errors = [];
    let observable: Observable<any>;
    switch (this.fileType) {
      case DiagFileTypeEnum.MASTER_EXCEL:
        observable = this.diagnosisService.updateMasterExcel(this.currentFile, this.diagCode);
        break;
      case DiagFileTypeEnum.INTERVENTION_EXCEL:
        observable = this.diagnosisService.validateThenUpload(
          this.currentFile,
          this.diagCode,
          this.fileType
        );
        break;
    }
    observable.subscribe((resp: any) => {
      if (resp.statusCode === 200) {
        this.modalRef.close();
      } else {
        this.errors = resp.error;
      }
    });
  }

  initData(data?: any) {
    if (data) {
      this.diagCode = data.code;
      this.fileType = data.btnMetadata.fileType;
    }
  }

}
