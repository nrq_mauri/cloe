import {ICloeTableStatic} from '@cloe-core/@core/@ui/components/cloe-table-static/cloe-table-static-interface/cloe-table-static-interface';

export const UPLOAD_EXCEL_ERRORS_TABLE_CONFIG: ICloeTableStatic = {
  header: [
    {
      fieldName: 'code',
      columnName: 'global.code',
      i18n: {
        prefix: 'errors.codes.',
        suffix: '',
      }
    },
    {
      fieldName: 'sheetName',
      columnName: 'page.diagnosis.sheetName',
    },
    {
      fieldName: 'cell',
      columnName: 'page.diagnosis.cell'
    },
    {
      fieldName: 'descCode',
      columnName: 'global.description',
      i18n: {
        prefix: 'errors.descriptions.',
        suffix: '',
        conditionalField: 'disableI18n'
      }
    }
  ],
  metadata: {
    tableTitle: 'page.diagnosis.errors-table-title',
    sort: 'sheetName',
    secondarySort: 'cell',
    sortType: 'asc',
  },
  pagination: {
    size: 10,
    page: 1,
    sizeOptions: [10, 30, 50],
    maxSize: 5
  }
};
