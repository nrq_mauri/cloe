import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { CloeModalComponentInterface } from '@cloe-core/@core/interfaces/cloe-modal-component.interface';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { EXCEL_ERRORS_TABLE_CONFIG } from './diagnosis-excel-errors-table.table.config';

@Component({
  selector: 'cloe-page-diagnosis-excel-errors',
  templateUrl: './diagnosis-excel-errors-table.component.html',
  styleUrls: ['./diagnosis-excel-errors-table.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DiagnosisExcelErrorsTableComponent implements OnInit, CloeModalComponentInterface {
  modalTitle: string;
  errors = [];
  tableConfig = EXCEL_ERRORS_TABLE_CONFIG;

  constructor(
    public modalRef: NgbActiveModal
  ) {}

  ngOnInit() {
    this.modalTitle = 'global.errors';
  }

  initData(data?: any) {
    if (data && data.errors) {
      this.errors = JSON.parse(data.errors);
    }
  }
}
