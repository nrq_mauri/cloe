import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { CloeModalComponentInterface } from '@cloe-core/@core/interfaces/cloe-modal-component.interface';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { DiagnosisService } from '@cloe-core/@core/services/diagnosis-service/diagnosis.service';
import { StorageService } from '@cloe-core/@core/services/storage-service/storage.service';

@Component({
  selector: 'cloe-page-new-diagnosis',
  templateUrl: './new-diagnosis.component.html',
  styleUrls: ['./new-diagnosis.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NewDiagnosisComponent implements OnInit, CloeModalComponentInterface {
  modalTitle: string;
  diagnosisModel: any = {};
  selectedFile;

  constructor(
    private diagnosisService: DiagnosisService,
    public modalRef: NgbActiveModal
  ) {}

  ngOnInit() {
    this.modalTitle = 'page.diagnosis.new-diagnosis';
  }

  initData(data?: any) {
  }

  handleFileInput(files: FileList) {
    this.selectedFile = files.item(0);
  }

  onSubmit() {
    this.diagnosisService.createDiag(
      this.selectedFile,
      StorageService.getCurrentUser(),
      this.diagnosisModel.name
    ).subscribe( (resp: any) => {
      this.modalRef.close('RELOAD');
    });
  }
}
