import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';

export enum GraphActionsEnum {
  TO_LINE = 'line',
  TO_COLUMN = 'column',
  TO_AREA = 'area',
  TOGGLE = 'hide',
  REMOVE = 'remove',
  INTEGRATE = 'integrate'
}

@Component({
  selector: 'cloe-chart-legend',
  templateUrl: './chart-legend.component.html',
  styleUrls: ['./chart-legend.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ChartLegendComponent implements OnInit{
  @Input() graph;
  @Output() actionClick = new EventEmitter<any>();

  graphActions = [];

  getGraphIcon() {
    if (this.graph.type === GraphActionsEnum.TO_LINE ) {
      if (this.graph.fillAlphas > 0) {
        return 'fa-area-chart';
      } else {
        return 'fa-line-chart';
      }
    }
    return 'fa-bar-chart';
  }

  handleAction(e: any) {

    if (e.group) {
      e.toggleActive = true;
      this.graphActions.forEach(event => {
        if (event.group === e.group && event.action !== e.action) {
          event.toggleActive = false;
        }
      });
    } else {
      e.toggleActive = !e.toggleActive;
    }

    this.actionClick.emit({
      action: e.action,
      graphId: this.graph.id,
      uuid: this.graph.uuid,
      toggleActive: e.toggleActive
    });
  }

  ngOnInit(): void {
    this.graphActions = [
      {
        action: GraphActionsEnum.TO_LINE,
        label: 'chart.line',
        icon: 'fa-line-chart',
        toggleActive: this.graph.type === 'line',
        group: 'type'
      },
      {
        action: GraphActionsEnum.TO_COLUMN,
        label: 'chart.column',
        icon: 'fa-bar-chart',
        toggleActive: this.graph.type === 'column',
        group: 'type'
      },
      {
        action: GraphActionsEnum.TO_AREA,
        label: 'chart.area',
        icon: 'fa-area-chart',
        toggleActive: this.graph.type === 'line' && this.graph.fillAlphas > 0,
        group: 'type'
      },
      {
        action: GraphActionsEnum.TOGGLE,
        label: 'chart.hide',
        icon: 'fa-eye-slash',
        toggleActive: false
      },
      {
        action: GraphActionsEnum.REMOVE,
        label: 'chart.remove',
        icon: 'fa-trash-o',
        toggleActive: false
      },
      {
        action: GraphActionsEnum.INTEGRATE,
        label: 'global.integrated',
        icon: 'fa-info',
        toggleActive: this.graph.integrated,
        disabled: !this.graph.integrable
      }
    ];
  }
}
