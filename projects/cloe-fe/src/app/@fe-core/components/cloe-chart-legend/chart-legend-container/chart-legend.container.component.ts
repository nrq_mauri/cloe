import { Component, Input, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs/internal/Subject';
import { GraphActionsEnum } from '../chart-legend/chart-legend.component';

export const chartLegendSubject$ = new Subject();

export enum ChartLegendActions {
  REMOVE_GRAPH = 'remove-graph',
  CHANGE_GRAPH_TYPE = 'change-graph-type',
  TOGGLE_GRAPH = 'hide-graph',
  INTEGRATE_GRAPH = 'integrate-graph'
}

@Component({
  selector: 'cloe-chart-legend-container',
  templateUrl: './chart-legend.container.component.html',
  styleUrls: ['./chart-legend.container.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ChartLegendContainerComponent {
  @Input() graphs;

  onActionClick(e: any) {
    let action;
    switch (e.action) {
      case GraphActionsEnum.REMOVE:
        action = ChartLegendActions.REMOVE_GRAPH;
        break;
      case GraphActionsEnum.TOGGLE:
        action = ChartLegendActions.TOGGLE_GRAPH;
        break;
      case GraphActionsEnum.INTEGRATE:
        action = ChartLegendActions.INTEGRATE_GRAPH;
        break;
      default:
        action = ChartLegendActions.CHANGE_GRAPH_TYPE;
        break;
    }
    chartLegendSubject$.next(
      {
        action: action,
        value: {
          graphId: e.graphId,
          type: e.action,
          uuid: e.uuid,
          toggleActive: e.toggleActive
        }
      }
    );
  }
}
