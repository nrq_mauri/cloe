import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { DiagnosisService } from '@cloe-core/@core/services/diagnosis-service/diagnosis.service';
import { keys, map, extend } from 'lodash';
import {ICloeTableStatic} from '@cloe-core/@core/@ui/components/cloe-table-static/cloe-table-static-interface/cloe-table-static-interface';

@Component({
  selector: 'cloe-diag-file-parser',
  templateUrl: './diag-file-parser.component.html',
  styleUrls: ['./diag-file-parser.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DiagFileParserComponent implements OnInit {
  @Input() diagCode;
  @Input() fileName;
  @Input() fileMeta;
  @Input() description;
  @Input() readOnly;
  @Output() remove = new EventEmitter();

  file;
  tableConfig: ICloeTableStatic = {
    header: [],
    metadata: {
      disablePagination: true,
      fixPageSize: 30
    }
  };

  imgUrl;

  constructor(private diagnosisService: DiagnosisService) {
  }

  ngOnInit(): void {
    if (!this.fileMeta.type) {
      this.diagnosisService.getFileMeta(this.diagCode, this.fileName).subscribe(
        r => {
          extend(this.fileMeta, r);
          this._getFileFromCloe(r);
        }
      );
    } else {
      this._getFileFromCloe(this.fileMeta);
    }
  }

  private _getFileFromCloe(fileMeta) {
    if (this.fileMeta.isImage) {
      this.imgUrl = this.diagnosisService.getDiagFileUrl(fileMeta.diagCode, fileMeta.fileName);
    } else {
      this.diagnosisService.downloadDiagJSONFile(fileMeta.diagCode, fileMeta.fileName)
        .subscribe((data: any) => {
            this._parseTableDate(data);
        }, error => {
          console.log(error);
        });
    }
  }

  private _parseTableDate(data) {
    if (!data) {
      return;
    }
    const fieldNames = keys(data[0]);
    this.tableConfig.metadata['sort'] = fieldNames[0];
    this.tableConfig.metadata['sortType'] = 'asc';
    this.tableConfig.header.push(...map(fieldNames, col => ({
      fieldName: col,
      columnName: col
    })));
    this.file = data;
  }

  removeIt(data) {
    this.remove.emit(data.fileName);
  }
}
