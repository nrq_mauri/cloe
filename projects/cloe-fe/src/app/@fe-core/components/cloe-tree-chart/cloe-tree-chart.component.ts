import { Component, OnInit, ViewEncapsulation, AfterViewInit, OnDestroy, Input} from '@angular/core';
import { CloeTreeChartService, TreeChart } from '../../services/cloe-tree-chart-service/cloe-tree-chart-service';
import * as echarts from 'echarts';

@Component({
  selector: 'cloe-tree-chart',
  templateUrl: './cloe-tree-chart.component.html',
  styleUrls: ['./cloe-tree-chart.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloeTreeChartComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() treeChart: TreeChart;

  constructor(
    private treeChartService: CloeTreeChartService
  ) {}

  ngOnInit() {}

  ngAfterViewInit(): void {
    this.treeChart.chart = echarts.init(<HTMLDivElement>document.getElementById(this.treeChart.divId));
    this.treeChart = this.treeChartService.createTreeChartByTreeUuid(this.treeChart);
  }

  ngOnDestroy(): void {
    this.treeChartService.destroyChart(this.treeChart.chart);
  }
}
