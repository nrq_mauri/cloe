import { AfterViewInit, Component, Input, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { CLOE_CHARTS_ACTIONS, CloeChartService, ICloeChart } from '../../services/cloe-chart-service/cloe-chart-service';
import {
  CHANNEL_VIEW_ACTIONS,
  channelTreeViewSubject$
} from '../../../pages/core-pages/channel-tree-view/channel-tree-view.component';
import { Subject } from 'rxjs/internal/Subject';
import { ChartLegendActions, chartLegendSubject$ } from '../cloe-chart-legend/chart-legend-container/chart-legend.container.component';
import { CloeStockChartService } from '../../services/cloe-stock-chart-service/cloe-stock-chart-service';
import { find } from 'lodash';
import { AxesActionsEnum, cloeStockChartAxesSubject$ } from './axis-action/axis-action.component';


// export const cloeStockChartSubject$ = new Subject();

@Component({
  selector: 'cloe-stock-chart',
  templateUrl: './cloe-stock-chart.component.html',
  styleUrls: ['./cloe-stock-chart.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloeStockChartComponent implements OnInit, AfterViewInit, OnDestroy {

  dragging = false;
  channelTreeViewSubscription;
  cloeStockChartAxesSubscription;
  chartLegendSubscription;
  stockChart: ICloeChart;
  @Input() configuration;
  graphs = [];
  toggleStackableAxis = {
    'Y1': false,
    'Y2': false,
    'Y3': false,
    'Y4': false,
  };

  constructor(
    private cloeChartService: CloeChartService,
    private cloeStockChartService: CloeStockChartService
  ) {
    this.channelTreeViewSubscription = channelTreeViewSubject$.asObservable().subscribe((e: any) => {
      if (e.action === CHANNEL_VIEW_ACTIONS.DRAG) {
        this.dragging = e.value;
      }
    });
  }

  ngOnInit() {
    this.stockChart = this.cloeChartService.createChart('cloe-stock-chart-container', this.configuration);

    this.chartLegendSubscription = chartLegendSubject$.asObservable().subscribe((e: any) => {
      const value = e.value;
      switch (e.action) {
        case ChartLegendActions.REMOVE_GRAPH:
          channelTreeViewSubject$.next({
            action: CHANNEL_VIEW_ACTIONS.NODE_FORCE_ACTIVE,
            referUuid: value.uuid
          });
          break;
        case ChartLegendActions.TOGGLE_GRAPH:
          const stockGraph: any = find(this.stockChart.chart.panels[0].stockGraphs, {id: value.graphId});
          if (stockGraph.hidden) {
            this.stockChart.chart.panels[0].showGraph(stockGraph);
          } else {
            this.stockChart.chart.panels[0].hideGraph(stockGraph);
          }
          break;
        case ChartLegendActions.CHANGE_GRAPH_TYPE:
          this.cloeStockChartService.updateGraph(this.stockChart.chart, value.uuid, {
            // there is no type area
            type: value.type === 'area' ? 'line' : value.type,
            fillAlphas: value.type === 'area' || value.type === 'column' ? 0.4 : 0,
          });
          break;
        case CLOE_CHARTS_ACTIONS.TOGGLE_BULLETS:
          this.cloeStockChartService.toggleBullets(this.stockChart.chart);
          break;
        case ChartLegendActions.INTEGRATE_GRAPH:
          this.cloeStockChartService.updateGraphsByUuid([value.uuid], {integrated: value.toggleActive});
          break;
      }
    });
    this.cloeStockChartAxesSubscription = cloeStockChartAxesSubject$.asObservable().subscribe( (e: any) => {
      switch (e.action) {
        case AxesActionsEnum.CUMULATE:
          this.onAxisStackableChange(e.data);
          break;
        case AxesActionsEnum.TOGGLE_ZERO_ORIGIN:
          this.cloeStockChartService.toggleZeroOriginAxes(this.stockChart.chart, e.data.id, e.data.value);
          break;
        case AxesActionsEnum.FORCE_SYNCHRONIZATION:
          this.cloeStockChartService.toggleAxesSynchronization(this.stockChart.chart, e.data.id, e.data.value);
          break;
      }
    });
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.graphs = this.stockChart.chart.panels[0].stockGraphs;
      this.cloeStockChartService.setValueAxisLabelFunction(this.stockChart.chart);
    }, 0);
  }

  ngOnDestroy() {
    this.cloeStockChartService.destroyChart(this.stockChart.chart);
    this.channelTreeViewSubscription.unsubscribe();
    this.chartLegendSubscription.unsubscribe();
  }

  onDrop(e: any) {
    const axisName = e.event.target.id;
    const channelUuid = e.element.data.referUuid;
    const referObject = e.element.data.referObject;

    this._resetAxisStackable(axisName);

    // call action to update channelView tree node
    channelTreeViewSubject$.next({
      action: CHANNEL_VIEW_ACTIONS.NODE_FORCE_ACTIVE,
      referUuid: channelUuid,
      axis: axisName,
      triggerActiveEvent: true // IMO this should be always like this
    });
  }

  onAxisStackableChange(e: any) {
    this.toggleStackableAxis[e.id] = e.value;
    // need this timeout for blocking the toggle to be ON
    setTimeout(() => {
      this.toggleStackableAxis[e.id] = this._toggleAxisStackable(e.id, e.value);
    }, 100);
  }

  private _resetAxisStackable(axisId) {
    if (this.toggleStackableAxis[axisId]) {
      this.toggleStackableAxis[axisId] = this._toggleAxisStackable(axisId, false);
    }
  }

  private _toggleAxisStackable(axisId, stackable) {
    if (stackable && !this.cloeStockChartService.isAxisValidForStack(this.stockChart.chart, axisId)) {
      return false;
    }

    this.cloeStockChartService.toggleAxisStackable(this.stockChart.chart, axisId, stackable);
    return stackable;
  }
}
