import { Component, EventEmitter, Input, OnDestroy, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { Subject } from 'rxjs';
import { find } from 'lodash';
import { SiteContextEnum, siteContextSubject } from '../../site-context-picker/site-context-picker.component';

export const cloeStockChartAxesSubject$ = new Subject();

@Component({
  selector: 'cloe-axis-action',
  templateUrl: './axis-action.component.html',
  styleUrls: ['./axis-action.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class AxisActionComponent implements OnInit, OnDestroy {
  private _toggleValue: boolean;
  toggleZeroOrigin = false;
  toggleSync = false;
  @Input() positionConfig: {
    positionClass,
    dropdownPosition?
  };
  @Output() actionClick = new EventEmitter<any>();

  cloeStockChartAxesSubscription;
  siteContextSubscription;

  axesActions = [
    {
      action: AxesActionsEnum.TOGGLE_ZERO_ORIGIN,
      label: 'axes.toggle-zero-origin',
      icon: 'fa-line-chart',
      toggleActive: this.toggleZeroOrigin
    },
    {
      action: AxesActionsEnum.CUMULATE,
      label: 'axes.cumulate',
      icon: 'fa-stack-overflow',
      toggleActive: this.toggleValue
    },
    {
      action: AxesActionsEnum.FORCE_SYNCHRONIZATION,
      label: 'axes.force-synchronization',
      icon: 'fa-refresh',
      toggleActive: this.toggleSync
    }
  ];

  ngOnInit() {
    this.cloeStockChartAxesSubscription = cloeStockChartAxesSubject$.asObservable().subscribe((event: any) => {
      switch (event.action) {
        case AxesActionsEnum.FORCE_SYNCHRONIZATION:
          if (event.data.id !== this.positionConfig.positionClass && this.toggleSync) {
            this.toggleSync = !this.toggleSync;
            const syncAction = find(this.axesActions, (axisAction) => {
              return axisAction.action === AxesActionsEnum.FORCE_SYNCHRONIZATION;
            });
            syncAction.toggleActive = this.toggleSync;
          }
          break;
      }
    });

    this.siteContextSubscription = siteContextSubject.asObservable().subscribe((event: any) => {
      switch (event.action) {
        case SiteContextEnum.UPDATE_SITES:
          this.resetAxesActions();
          break;
      }
    });

    if (this.positionConfig && !this.positionConfig.dropdownPosition) {
      this.positionConfig.dropdownPosition = 'right-top';
    }
  }

  @Input('toggleValue')
  set toggleValue(checked: boolean) {
    if (this._toggleValue === checked) {
      return;
    }
    this._toggleValue = checked;
    this.toggleCumulateEvent();
  }

  get toggleValue() {
    return this._toggleValue;
  }

  handleAction(e: any) {
    switch (e.action) {
      case AxesActionsEnum.CUMULATE:
        this.toggleValue = !this._toggleValue;
        e.toggleActive = this.toggleValue;
        break;
      case AxesActionsEnum.TOGGLE_ZERO_ORIGIN:
        this.toggleZeroOrigin = !this.toggleZeroOrigin;
        e.toggleActive = this.toggleZeroOrigin;
        this.toggleZeroOriginEvent();
        break;
      case AxesActionsEnum.FORCE_SYNCHRONIZATION:
        this.toggleSync = !this.toggleSync;
        e.toggleActive = this.toggleSync;
        this.toggleSyncEvent();
        break;
    }
  }
  private toggleCumulateEvent() {
    cloeStockChartAxesSubject$.next({
      action: AxesActionsEnum.CUMULATE,
      data: {
        id: this.positionConfig.positionClass,
        value: this._toggleValue
      }
    });
  }

  private toggleZeroOriginEvent() {
    cloeStockChartAxesSubject$.next({
      action: AxesActionsEnum.TOGGLE_ZERO_ORIGIN,
      data: {
        id: this.positionConfig.positionClass,
        value: this.toggleZeroOrigin
      }
    });
  }

  private toggleSyncEvent() {
    cloeStockChartAxesSubject$.next({
      action: AxesActionsEnum.FORCE_SYNCHRONIZATION,
      data: {
        id: this.positionConfig.positionClass,
        value: this.toggleSync
      }
    });
  }

  private resetAxesActions() {
    if (this.toggleValue) {
      this.toggleValue = !this.toggleValue;
      this.toggleCumulateEvent();
    }
    if (this.toggleZeroOrigin) {
      this.toggleZeroOrigin = !this.toggleZeroOrigin;
      this.toggleZeroOriginEvent();
    }
    if (this.toggleSync) {
      this.toggleSync = !this.toggleSync;
      this.toggleSyncEvent();
    }
  }

  ngOnDestroy(): void {
    this.cloeStockChartAxesSubscription.unsubscribe();
    this.siteContextSubscription.unsubscribe();
  }
}

export enum AxesActionsEnum {
  CUMULATE, TOGGLE_ZERO_ORIGIN, FORCE_SYNCHRONIZATION
}
