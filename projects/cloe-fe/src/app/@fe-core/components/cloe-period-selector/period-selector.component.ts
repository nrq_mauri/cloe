import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import * as moment from 'moment';
import { Moment } from 'moment';
import { MAT_MOMENT_DATE_FORMATS } from '@angular/material-moment-adapter';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from 'saturn-datepicker';
import { CloeDateAdapter } from './CloeDateAdapter';
import { RangeTypeEnum } from './enums/RangeTypeEnum';
import { PeriodType } from './enums/PeriodType';
import { TranslateService } from '@ngx-translate/core';
import { CloeDatePipe } from '@cloe-core/@core/@ui/pipes';
import { ChartTypeEnum } from './enums/ChartTypeEnum';

@Component({
  selector: 'cloe-core-period-selector',
  templateUrl: './period-selector.component.html',
  styleUrls: ['./period-selector.component.scss'],
  providers: [
    {provide: DateAdapter, useClass: CloeDateAdapter, deps: [MAT_DATE_LOCALE, TranslateService]},
    {provide: MAT_DATE_FORMATS, useValue: MAT_MOMENT_DATE_FORMATS},
    CloeDateAdapter
  ],
  encapsulation: ViewEncapsulation.None
})

export class CloePeriodSelectorComponent implements OnInit {
  rangeTypeEnum = RangeTypeEnum;
  rangeTypes = Object.keys(RangeTypeEnum);
  rangeType;
  freeRangeType;
  periodTypes;
  periodType;
  momentFromDate: Moment;
  momentToDate: Moment;
  maxDate = moment();

  @Input() submitLabel = 'global.apply';
  @Input() clearLabel = 'global.reset';
  @Input() miniMode = false;
  @Input() chartType;
  @Input() initialDates: {
    startDate?,
    endDate?,
    initialPeriodType?,
    initialRangeType?,
    isFreeRange?
  };
  @Input() config: {
    periodTypeDisabled?,
    rangeTypeDisabled?,
    calendarDisabled?,
    showAccordion?
  };

  @Output() periodSelected = new EventEmitter();
  @Output() clearSelected = new EventEmitter();

  constructor(
    private cloeDateAdapter: CloeDateAdapter,
    private cloeDatePipe: CloeDatePipe
  ) {
  }

  ngOnInit() {
    this.checkInit();
  }
  checkInit() {
    if (this.initialDates) {
      this.checkInitialDates();
    } else {
      this.rangeType = this.rangeTypes[0];
      this.initPeriod();
    }
  }

  checkInitialDates() {
    if (this.initialDates.initialPeriodType && this.initialDates.startDate && this.initialDates.endDate) {
      if (!this.initialDates.isFreeRange && this.initialDates.initialRangeType) {
        this.rangeType = this.initialDates.initialRangeType;
        this.periodTypes = PeriodType.getPeriodForChart(this.chartType, this.rangeType);
        this.momentFromDate = this.checkMaxDate(this.initialDates.startDate);
        this.momentToDate = this.checkMaxDate(this.initialDates.endDate);
      } else {
        this.rangeType = RangeTypeEnum.FREE;
        this.onRangeSelected({
          value: {
            begin: this.checkMaxDate(this.initialDates.startDate),
            end: this.checkMaxDate(this.initialDates.endDate)
          }
        });
      }
      this.periodType = this.initialDates.initialPeriodType;
      this.onSubmit();
    }
  }

  initPeriod() {
    let freeMode;
    switch (this.rangeType) {
      case RangeTypeEnum.YEAR:
      case RangeTypeEnum.MONTH:
        this.momentFromDate = moment();
        break;
      case RangeTypeEnum.DAY:
        this.momentFromDate = moment().startOf('day');
        this.momentToDate = moment(this.momentFromDate).endOf('day');
        break;
      case RangeTypeEnum.WEEK:
        this.momentFromDate = this.getFirstDayOfWeekFromDate(moment());
        this.momentToDate = this.checkMaxDate(moment(this.momentFromDate).add(6, 'day'));
        break;
      case RangeTypeEnum.FREE:
        freeMode = RangeTypeEnum.WEEK;
        this.momentFromDate = this.getFirstDayOfWeekFromDate(moment());
        this.momentToDate = this.checkMaxDate(moment(this.momentFromDate).add(6, 'day'));
        break;
    }
    this.periodTypes = PeriodType.getPeriodForChart(
      this.chartType,
      freeMode ? freeMode : this.rangeType
    );
    this.periodType = this.periodTypes[0];
    this.checkMiniMode();
  }

  get formattedRange(): string {
    return this.cloeDatePipe.transform(this.momentFromDate.format('YYYY-MM-DD'), 'YYYY-MM-DD') +
      ' - ' + this.cloeDatePipe.transform(this.momentToDate.format('YYYY-MM-DD'), 'YYYY-MM-DD');
  }

  get formattedYear(): string {
    return moment(this.momentFromDate).year() + '';
  }

  get formattedMonth(): string {
    return moment(this.momentFromDate).month() + 1 + '/' + moment(this.momentFromDate).year();
  }

  get formattedDay(): string {
    return this.cloeDatePipe.transform(this.momentFromDate.format('YYYY-MM-DD'), 'YYYY-MM-DD');
  }

  onYearOrMonthSelected($event, datePicker?) {
    this.momentFromDate = this.checkMaxDate($event);
    if (datePicker) {
      datePicker.close();
    }
    this.checkMiniMode();
  }

  onWeekSelected(date) {
    this.momentFromDate = this.getFirstDayOfWeekFromDate(date.value);
    this.momentToDate = this.checkMaxDate(moment(this.momentFromDate).add(6, 'day'));
    this.checkMiniMode();
  }

  onDaySelected(date) {
    this.momentFromDate = moment(date.value).startOf('day');
    this.momentToDate = moment(date.value).endOf('day');
    this.checkMiniMode();
  }

  onRangeSelected(date) {
    this.momentFromDate = moment(date.value.begin);
    this.momentToDate = moment(date.value.end);
    if (Math.abs(this.momentToDate.diff(this.momentFromDate, 'month', true)) >= 12) {
      this.momentToDate = moment(this.momentFromDate).add(12, 'month');
      this.periodTypes = PeriodType.getPeriodForChart(this.chartType, RangeTypeEnum.YEAR);
      this.periodType = this.periodTypes[0];
    } else {
      this.periodTypes = this.checkFreePeriodGranularity();
      this.periodType = this.periodTypes[0];
    }
    this.checkMiniMode();
  }

  private checkRoundUp() {
    switch (this.chartType) {
      case ChartTypeEnum.HEAT_MAP:
      case ChartTypeEnum.STOCK_CHART:
        return true;
      case ChartTypeEnum.SERIAL_CHART:
      case ChartTypeEnum.DURATION_CURVES:
        return false;
    }
  }

  checkFreePeriodGranularity() {
    const roundedUp = this.checkRoundUp();
    const monthDifference = Math.abs(this.momentToDate.diff(this.momentFromDate, 'month', true));
    if (monthDifference === 1) {
      this.freeRangeType = RangeTypeEnum.MONTH;
      return PeriodType.getPeriodForChart(this.chartType, this.freeRangeType);
    }
    if (monthDifference > 1) {
      this.freeRangeType = roundedUp ? RangeTypeEnum.YEAR : RangeTypeEnum.MONTH;
      return PeriodType.getPeriodForChart(this.chartType, this.freeRangeType);
    }
    const weekDifference = Math.abs(this.momentToDate.diff(this.momentFromDate, 'week', true));
    if (weekDifference === 1) {
      this.freeRangeType = RangeTypeEnum.WEEK;
      return PeriodType.getPeriodForChart(this.chartType, this.freeRangeType);
    }
    if (weekDifference > 1) {
      this.freeRangeType = roundedUp ? RangeTypeEnum.MONTH : RangeTypeEnum.WEEK;
      return PeriodType.getPeriodForChart(this.chartType, this.freeRangeType);
    }
    const dayDifference = Math.abs(this.momentToDate.diff(this.momentFromDate, 'day', true));
    if (dayDifference > 1) {
      this.freeRangeType = roundedUp ? RangeTypeEnum.WEEK : RangeTypeEnum.DAY;
      return PeriodType.getPeriodForChart(this.chartType, this.freeRangeType);
    }
    if (dayDifference <= 1) {
      this.freeRangeType = RangeTypeEnum.DAY;
      return PeriodType.getPeriodForChart(this.chartType, RangeTypeEnum.DAY);
    }
  }

  getFirstDayOfWeekFromDate(date) {
    const firstDayOfWeek = this.cloeDateAdapter.getFirstDayOfWeek();
    if (moment(date).isoWeekday() < firstDayOfWeek) {
      return moment(date).subtract(7, 'day').startOf('isoWeek').isoWeekday(firstDayOfWeek);
    } else {
      return moment(date).startOf('isoWeek').isoWeekday(firstDayOfWeek);
    }
  }

  checkMaxDate(date) {
    if (moment(date).isAfter(this.maxDate)) {
      return moment();
    } else {
      return moment(date);
    }
  }

  onSubmit() {
    let startDate;
    let endDate;
    let freeMode = false;
    switch (this.rangeType) {
      case RangeTypeEnum.YEAR:
        startDate =  moment(this.momentFromDate).startOf('year');
        endDate = moment(startDate).endOf('year');
        break;
      case RangeTypeEnum.MONTH:
        startDate = moment(this.momentFromDate).startOf('month');
        endDate = moment(startDate).endOf('month');
        break;
      case RangeTypeEnum.WEEK:
      case RangeTypeEnum.DAY:
        startDate = this.momentFromDate;
        endDate = this.momentToDate;
        break;
      case RangeTypeEnum.FREE:
        freeMode = true;
        startDate = this.momentFromDate;
        endDate = this.momentToDate;
        break;
    }
    this.periodSelected.emit({
      startDate: startDate.format('YYYY-MM-DD'),
      endDate: endDate.format('YYYY-MM-DD'),
      periodType: this.periodType,
      rangeType: freeMode ? this.freeRangeType : this.rangeType,
      isFreeRange: freeMode
    });
  }

  checkMiniMode() {
    if (this.miniMode) {
      this.onSubmit();
    }
  }

  onClear() {
    this.checkInit();
    this.clearSelected.emit();
  }

  nextPeriod(arrowType) {
    if (this.config && this.config.calendarDisabled) {
      return;
    }
    const rangeType = this.rangeType === RangeTypeEnum.FREE ? RangeTypeEnum.DAY : this.rangeType;
    let fromDate;
    let toDate;
    if (arrowType === 'left') {
      fromDate =  moment(this.momentFromDate).subtract(1, rangeType);
      toDate = moment(this.momentToDate).subtract(1, rangeType);
    }
    if (arrowType === 'right') {
      fromDate =  this.checkMaxDate(moment(this.momentFromDate).add(1, rangeType));
      toDate =  moment(this.momentToDate).add(1, rangeType);

    }
    switch (this.rangeType) {
      case RangeTypeEnum.YEAR:
      case RangeTypeEnum.MONTH:
        this.onYearOrMonthSelected(fromDate);
        break;
      case RangeTypeEnum.WEEK:
        this.onWeekSelected({value: fromDate});
        break;
      case RangeTypeEnum.DAY:
        this.onDaySelected({value: fromDate});
        break;
      case RangeTypeEnum.FREE:
        this.onRangeSelected({
          value: {
            begin: fromDate,
            end: toDate
          }
        });
        break;
    }
    this.onSubmit();
  }
}
