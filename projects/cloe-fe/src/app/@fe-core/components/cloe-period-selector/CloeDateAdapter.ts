import {Inject, Injectable, Optional} from '@angular/core';
import {MomentDateAdapter} from '@angular/material-moment-adapter';
import {StorageService} from '@cloe-core/@core/services/storage-service/storage.service';
import {TranslateService} from '@ngx-translate/core';
import {MAT_DATE_LOCALE} from 'saturn-datepicker';
import {findIndex, toLower} from 'lodash';
import {WeekDaysEnum} from './enums/WeekDaysEnum';

@Injectable()
export class CloeDateAdapter extends MomentDateAdapter {
  constructor(
    @Optional() @Inject(MAT_DATE_LOCALE) dateLocale: string,
    private translateService: TranslateService
  ) {
    super(translateService.currentLang);
    this.translateService.onLangChange.subscribe(() => {
      this.setLocale(this.translateService.currentLang);
    });
  }

  getFirstDayOfWeek(): number {
    if (StorageService.getCurrentUser().cloeUser.startWeek) {
      const index = findIndex(Object.keys(WeekDaysEnum), (weekDay) => {
        return toLower(weekDay) === toLower(StorageService.getCurrentUser().cloeUser.startWeek);
      });
      return index !== -1 ? index : 1;
    }
    return 1;
  }

}
