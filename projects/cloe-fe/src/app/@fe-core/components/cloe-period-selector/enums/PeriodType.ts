import {ChartTypeEnum} from './ChartTypeEnum';
import {RangeTypeEnum} from './RangeTypeEnum';
import {
  HEAT_MAP_DAY_GRANULARITY,
  HEAT_MAP_MONTH_GRANULARITY,
  HEAT_MAP_WEEK_GRANULARITY,
  HEAT_MAP_YEAR_GRANULARITY
} from './HeatMap_granularity';
import {
  STOCK_DAY_GRANULARITY,
  STOCK_MONTH_GRANULARITY,
  STOCK_WEEK_GRANULARITY,
  STOCK_YEAR_GRANULARITY
} from './Stock_granularity';
import {
  SERIAL_DAY_GRANULARITY,
  SERIAL_MONTH_GRANULARITY,
  SERIAL_WEEK_GRANULARITY,
  SERIAL_YEAR_GRANULARITY
} from './Serial_granularity';

import {
  DURATION_CURVES_DAY_GRANULARITY,
  DURATION_CURVES_WEEK_GRANULARITY,
  DURATION_CURVES_MONTH_GRANULARITY,
  DURATION_CURVES_YEAR_GRANULARITY
} from './Duration_curves_granularity';

import { PeriodTypeEnum } from './PeriodTypeEnum';

export class PeriodType {
  static getPeriodForChart(chartType, rangeType) {
    switch (chartType) {
      case ChartTypeEnum.HEAT_MAP:
        return PeriodType.getHeatMapPeriod(rangeType);
      case ChartTypeEnum.STOCK_CHART:
        return PeriodType.getStockPeriod(rangeType);
      case ChartTypeEnum.SERIAL_CHART:
        return PeriodType.getSerialPeriod(rangeType);
      case ChartTypeEnum.DURATION_CURVES:
        return PeriodType.getDurationCurvesPeriod(rangeType);
    }
  }
  private static getHeatMapPeriod(rangeType) {
    switch (rangeType) {
      case RangeTypeEnum.DAY:
        return HEAT_MAP_DAY_GRANULARITY;
      case RangeTypeEnum.WEEK:
        return HEAT_MAP_WEEK_GRANULARITY;
      case RangeTypeEnum.MONTH:
        return HEAT_MAP_MONTH_GRANULARITY;
      case RangeTypeEnum.YEAR:
      case RangeTypeEnum.FREE:
        return HEAT_MAP_YEAR_GRANULARITY;
    }
  }

  private static getStockPeriod(rangeType) {
    switch (rangeType) {
      case RangeTypeEnum.DAY:
        return STOCK_DAY_GRANULARITY;
      case RangeTypeEnum.WEEK:
        return STOCK_WEEK_GRANULARITY;
      case RangeTypeEnum.MONTH:
        return STOCK_MONTH_GRANULARITY;
      case RangeTypeEnum.YEAR:
      case RangeTypeEnum.FREE:
        return STOCK_YEAR_GRANULARITY;
    }
  }

  private static getSerialPeriod(rangeType) {
    switch (rangeType) {
      case RangeTypeEnum.DAY:
        return SERIAL_DAY_GRANULARITY;
      case RangeTypeEnum.WEEK:
        return SERIAL_WEEK_GRANULARITY;
      case RangeTypeEnum.MONTH:
        return SERIAL_MONTH_GRANULARITY;
      case RangeTypeEnum.YEAR:
      case RangeTypeEnum.FREE:
        return SERIAL_YEAR_GRANULARITY;
    }
  }

  private static getDurationCurvesPeriod(rangeType) {
    switch (rangeType) {
      case RangeTypeEnum.DAY:
        return DURATION_CURVES_DAY_GRANULARITY;
      case RangeTypeEnum.WEEK:
        return DURATION_CURVES_WEEK_GRANULARITY;
      case RangeTypeEnum.MONTH:
        return DURATION_CURVES_MONTH_GRANULARITY;
      case RangeTypeEnum.YEAR:
      case RangeTypeEnum.FREE:
        return DURATION_CURVES_YEAR_GRANULARITY;
    }
  }

  static samplingToPeriodType(n: string) {
    switch (n) {
      case '1':
        return PeriodTypeEnum.MIN;
      case '5':
        return PeriodTypeEnum.PENTA_MIN;
      case '10':
        return PeriodTypeEnum.DECA_MIN;
      case '15':
        return PeriodTypeEnum.QUARTER;
      case '30':
        return PeriodTypeEnum.HALF_HOUR;
      case '60':
        return PeriodTypeEnum.HOUR;
    }
  }

  static periodTypeToSampling(periodType: PeriodTypeEnum) {
    switch (periodType) {
      case PeriodTypeEnum.MIN:
        return 1;
      case PeriodTypeEnum.PENTA_MIN:
        return 5;
      case PeriodTypeEnum.DECA_MIN:
        return 10;
      case PeriodTypeEnum.QUARTER:
        return 15;
      case PeriodTypeEnum.HALF_HOUR:
        return 30;
      case PeriodTypeEnum.HOUR:
        return 60;
    }
  }
}
