import { PeriodTypeEnum } from './PeriodTypeEnum';

export const SERIAL_DAY_GRANULARITY = [
  PeriodTypeEnum.MIN,
  PeriodTypeEnum.PENTA_MIN,
  PeriodTypeEnum.DECA_MIN,
  PeriodTypeEnum.QUARTER,
  PeriodTypeEnum.HALF_HOUR,
  PeriodTypeEnum.HOUR
];

export const SERIAL_WEEK_GRANULARITY = [
  PeriodTypeEnum.MIN,
  PeriodTypeEnum.PENTA_MIN,
  PeriodTypeEnum.DECA_MIN,
  PeriodTypeEnum.QUARTER,
  PeriodTypeEnum.HALF_HOUR,
  PeriodTypeEnum.HOUR,
  PeriodTypeEnum.DAY
];

export const SERIAL_MONTH_GRANULARITY = [
  PeriodTypeEnum.HOUR,
  PeriodTypeEnum.DAY,
  PeriodTypeEnum.WEEK
];

export const SERIAL_YEAR_GRANULARITY = [
  PeriodTypeEnum.DAY,
  PeriodTypeEnum.WEEK,
  PeriodTypeEnum.MONTH
];
