export enum RangeTypeEnum {
  YEAR = 'YEAR',
  MONTH = 'MONTH',
  WEEK = 'WEEK',
  DAY = 'DAY',
  FREE = 'FREE'
}
