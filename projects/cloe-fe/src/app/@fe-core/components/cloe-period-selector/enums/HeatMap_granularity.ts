import { PeriodTypeEnum } from './PeriodTypeEnum';

export const HEAT_MAP_DAY_GRANULARITY = [
  PeriodTypeEnum.DECA_MIN,
  PeriodTypeEnum.QUARTER
];

export const HEAT_MAP_WEEK_GRANULARITY = [
  PeriodTypeEnum.DECA_MIN,
  PeriodTypeEnum.QUARTER,
  PeriodTypeEnum.HOUR
];

export const HEAT_MAP_MONTH_GRANULARITY = [
  PeriodTypeEnum.DECA_MIN,
  PeriodTypeEnum.QUARTER,
  PeriodTypeEnum.HOUR,
  PeriodTypeEnum.DAY
];

export const HEAT_MAP_YEAR_GRANULARITY = [
  PeriodTypeEnum.HOUR,
  PeriodTypeEnum.DAY
];
