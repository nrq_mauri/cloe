import { PeriodTypeEnum } from './PeriodTypeEnum';

export const STOCK_DAY_GRANULARITY = [
  PeriodTypeEnum.MIN,
  PeriodTypeEnum.PENTA_MIN,
  PeriodTypeEnum.DECA_MIN,
  PeriodTypeEnum.QUARTER,
  PeriodTypeEnum.HALF_HOUR,
  PeriodTypeEnum.HOUR
];

export const STOCK_WEEK_GRANULARITY = [
  PeriodTypeEnum.PENTA_MIN,
  PeriodTypeEnum.DECA_MIN,
  PeriodTypeEnum.QUARTER,
  PeriodTypeEnum.HALF_HOUR,
  PeriodTypeEnum.HOUR,
  PeriodTypeEnum.DAY
];

export const STOCK_MONTH_GRANULARITY = [
  PeriodTypeEnum.DECA_MIN,
  PeriodTypeEnum.QUARTER,
  PeriodTypeEnum.HALF_HOUR,
  PeriodTypeEnum.HOUR,
  PeriodTypeEnum.DAY,
  PeriodTypeEnum.WEEK
];

export const STOCK_YEAR_GRANULARITY = [
  PeriodTypeEnum.HOUR,
  PeriodTypeEnum.DAY,
  PeriodTypeEnum.WEEK,
  PeriodTypeEnum.MONTH
];
