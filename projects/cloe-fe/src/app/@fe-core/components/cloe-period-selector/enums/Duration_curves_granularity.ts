import { PeriodTypeEnum } from './PeriodTypeEnum';

export const DURATION_CURVES_DAY_GRANULARITY = [
  PeriodTypeEnum.DECA_MIN,
  PeriodTypeEnum.QUARTER,
  PeriodTypeEnum.HALF_HOUR,
  PeriodTypeEnum.HOUR
];

export const DURATION_CURVES_WEEK_GRANULARITY = [
  PeriodTypeEnum.DECA_MIN,
  PeriodTypeEnum.QUARTER,
  PeriodTypeEnum.HALF_HOUR,
  PeriodTypeEnum.HOUR,
];

export const DURATION_CURVES_MONTH_GRANULARITY = [
  PeriodTypeEnum.DECA_MIN,
  PeriodTypeEnum.QUARTER,
  PeriodTypeEnum.HALF_HOUR,
  PeriodTypeEnum.HOUR,
  PeriodTypeEnum.DAY
];

export const DURATION_CURVES_YEAR_GRANULARITY = [
  PeriodTypeEnum.HOUR,
  PeriodTypeEnum.DAY,
  PeriodTypeEnum.WEEK,
  PeriodTypeEnum.YEAR
];
