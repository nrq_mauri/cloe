import { Component, OnInit, ViewEncapsulation, AfterViewInit, OnDestroy, Input } from '@angular/core';
import { CloeHeatMapService, HeatMap } from '../../services/cloe-heatMap-service/cloe-heatMap-service';
import { CarpetPlotsTabEnum } from '../../../pages/carpet-plot/carpet-plots-tabs/enums/CarpetPlotsTabsEnum';
import {
  CHANNEL_VIEW_ACTIONS,
  channelTreeViewSubject$
} from '../../../pages/core-pages/channel-tree-view/channel-tree-view.component';

import * as echarts from 'echarts';
import { CLOE_CHARTS_ACTIONS, CloeChartService } from '../../services/cloe-chart-service/cloe-chart-service';

@Component({
  selector: 'cloe-heat-map',
  templateUrl: './cloe-heat-map.component.html',
  styleUrls: ['./cloe-heat-map.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloeHeatMapComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() heatMap: HeatMap;
  @Input() carpetPlotType: CarpetPlotsTabEnum;
  carpetPlotsTypes = CarpetPlotsTabEnum;

  graphActions: Array<any> = [
    {
      action: CLOE_CHARTS_ACTIONS.REMOVE_CHART,
      label: 'global.delete',
      icon: 'fa-trash'
    }
  ];

  constructor(
    private heatMapService: CloeHeatMapService,
    private cloeChartService: CloeChartService
  ) {
  }

  ngOnInit() {
    this.graphActions.unshift(
      {
        action: CLOE_CHARTS_ACTIONS.INTEGRATE,
        label: 'global.integrated',
        icon: 'fa-info',
        toggleActive: this.heatMap.integrated,
        disabled: !this.heatMap.integrable
      }
    );
  }

  ngAfterViewInit(): void {
    this.heatMap.chart = echarts.init(<HTMLDivElement>document.getElementById(this.heatMap.divId));
  }

  ngOnDestroy(): void {
    this.heatMapService.destroyChart(this.heatMap.chart);
  }

  handleAction(e) {
    switch (e.action) {
      case CLOE_CHARTS_ACTIONS.INTEGRATE:
        e.toggleActive = !e.toggleActive;
        this.heatMapService.integrateHeat(this.heatMap.divId, e.toggleActive);
        break;
      case CLOE_CHARTS_ACTIONS.REMOVE_CHART:
        this.closeHeatMap(this.heatMap);
        break;
    }
  }

  integrateSymbol() {
    return this.heatMap.integrated ? '[I] ' : '';
  }

  getSymbol() {
    if (this.heatMap.summaryData) {
      return  this.heatMap.summaryData.dynamicSymbol;
    }
  }

  closeHeatMap(heatMapItem) {
    if (this.carpetPlotType === this.carpetPlotsTypes.BY_MEASURE) {
      channelTreeViewSubject$.next({
        action: CHANNEL_VIEW_ACTIONS.NODE_FORCE_DEACTIVE,
        referUuid: heatMapItem.uuidChannel
      });
    }
    this.heatMapService.removeHeatMapByDivId(heatMapItem.divId);
  }
}
