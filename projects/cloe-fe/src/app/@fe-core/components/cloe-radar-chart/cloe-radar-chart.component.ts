import { Component, OnInit, ViewEncapsulation, AfterViewInit, OnDestroy, Input, NgZone } from '@angular/core';
import { CloeAm4chartRadarService, IRadarConfig } from '../../services/cloe-am4chart-radar-service/cloe-am4chart-radar-service';
import * as am4charts from '@amcharts/amcharts4/charts';

@Component({
  selector: 'cloe-radar-chart',
  templateUrl: './cloe-radar-chart.component.html',
  styleUrls: ['./cloe-radar-chart.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloeRadarChartComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() radarChartConfig: IRadarConfig;
  chart: am4charts.RadarChart;

  constructor(
    private cloeAm4ChartService: CloeAm4chartRadarService,
    private zone: NgZone
  ) {
  }

  ngOnInit() {
  }

  ngAfterViewInit(): void {
    this.zone.runOutsideAngular(() => {
      this.chart = this.cloeAm4ChartService.createChart(this.radarChartConfig);
    });
  }

  ngOnDestroy(): void {
    this.zone.runOutsideAngular(() => {
      this.cloeAm4ChartService.destroyChart(this.chart);
    });
  }
}
