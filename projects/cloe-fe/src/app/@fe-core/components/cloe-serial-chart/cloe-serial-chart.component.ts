import { AfterViewInit, Component, Input, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import {
  CLOE_CHARTS_ACTIONS,
  CloeChartService,
  ICloeChart
} from '../../services/cloe-chart-service/cloe-chart-service';
import { CloeSerialChartService, cloeSerialChartSubject$ } from '../../services/cloe-serial-chart-service/cloe-serial-chart-service';
import { CHANNEL_VIEW_ACTIONS, channelTreeViewSubject$ } from '../../../pages/core-pages/channel-tree-view/channel-tree-view.component';
import { remove, find } from 'lodash';

@Component({
  selector: 'cloe-serial-chart',
  templateUrl: './cloe-serial-chart.component.html',
  styleUrls: ['./cloe-serial-chart.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloeSerialChartComponent implements OnInit, AfterViewInit, OnDestroy {
  @Input() serialChart: ICloeChart;
  cloeSerialChartSubscription;

  bulletActive = false;
  @Input() integrable;

  graphActions: Array<any> = [
    {
      action: CLOE_CHARTS_ACTIONS.TOGGLE_BULLETS,
      label: 'chart.insert-bullets',
      icon: 'fa-circle',
      toggleActive: this.bulletActive
    },
    {
      action: CLOE_CHARTS_ACTIONS.REMOVE_CHART,
      label: 'global.delete',
      icon: 'fa-trash'
    }
  ];

  constructor(
    private cloeSerialChartService: CloeSerialChartService,
    private cloeChartService: CloeChartService
  ) {
  }

  ngOnInit() {
    this.cloeSerialChartSubscription = cloeSerialChartSubject$.asObservable().subscribe((e: any) => {
      switch (e.action) {
        case CLOE_CHARTS_ACTIONS.UPDATE_DATA_PROVIDER:
          this.cloeSerialChartService.addGraphsFromModel(this.serialChart);
          break;
        case CLOE_CHARTS_ACTIONS.ADD_THRESHOLD:
          if (e.value.graph.uuid === this.serialChart.channelUuid) {
            this.serialChart.thresholds ?
              this.serialChart.thresholds.push(e.value) :
              this.serialChart.thresholds = [e.value];
            this.cloeSerialChartService.addGuide(
              this.serialChart.chart,
              {
                'labelIndex': e.value.labelIndex,
                'value': e.value.thresholdValue,
                'lineAlpha': 1,
                'label': e.value.thresholdName,
                'lineThickness': 2,
                'lineColor': '#c44',
                'inside': true
              });
            this.cloeSerialChartService.addGuide(
              this.serialChart.chart,
              {
                'labelIndex': e.value.labelIndex,
                'category': e.value.row.formattedxAxis,
                'lineAlpha': 1,
                'label': e.value.thresholdName,
                'labelRotation': 90,
                'lineThickness': 1,
                'lineColor': '#c44',
                'inside': true
              }
            );
          }
          break;
        case CLOE_CHARTS_ACTIONS.REMOVE_THRESHOLD:
          if (e.data.channelUuid === this.serialChart.channelUuid) {
            remove(this.serialChart.thresholds, threshold => e.data.thresholdValue === threshold.thresholdValue);
            this.cloeSerialChartService.removeGuide(this.serialChart.chart, e.data);
          }
          break;
        case CLOE_CHARTS_ACTIONS.CLEAR_THRESHOLDS:
          if (e.data === this.serialChart.channelUuid) {
            remove(this.serialChart.thresholds);
            this.cloeSerialChartService.clearGuides(this.serialChart.chart);
          }
          break;
        case CLOE_CHARTS_ACTIONS.CLEAR_ALL_THRESHOLDS:
          remove(this.serialChart.thresholds);
          this.cloeSerialChartService.clearGuides(this.serialChart.chart);
          break;
      }
    });
    // Update Action Integrate
    if (this.integrable) {
      this.graphActions.unshift(
        {
          action: CLOE_CHARTS_ACTIONS.INTEGRATE,
          label: 'global.integrated',
          icon: 'fa-info',
          toggleActive: this.serialChart.integrated,
          disabled: !this.serialChart.integrable
        }
      );
    }
  }

  ngAfterViewInit(): void {
    this.serialChart = this.cloeSerialChartService.createChart(this.serialChart);
    if (this.serialChart.graphsConfig && this.serialChart.dataProvider) {
      this.cloeSerialChartService.addGraphsFromModel(this.serialChart);
    }
    if (this.serialChart.thresholds) {
      const guides = [];
      this.serialChart.thresholds.forEach((threshold) => {
        guides.push(
          {
            'value': threshold.thresholdValue,
            'lineAlpha': 1,
            'label': threshold.thresholdName,
            'lineThickness': 2,
            'lineColor': '#c44',
            'inside': true
          },
          {
            'category': threshold.row.formattedxAxis,
            'lineAlpha': 1,
            'label': threshold.thresholdName,
            'labelRotation': 90,
            'lineThickness': 1,
            'lineColor': '#c44',
            'inside': true
          }
        );
      });
      this.cloeSerialChartService.addGuideList(this.serialChart.chart, guides);
    }
  }

  closeSerialChart() {
    channelTreeViewSubject$.next({
      action: CHANNEL_VIEW_ACTIONS.NODE_FORCE_DEACTIVE,
      referUuid: this.serialChart.channelUuid
    });
  }

  handleAction(e: any) {
    switch (e.action) {
      case CLOE_CHARTS_ACTIONS.TOGGLE_BULLETS:
        this.bulletActive = !this.bulletActive;
        e.toggleActive = this.bulletActive;
        this.cloeSerialChartService.toggleBullets(this.serialChart.chart);
        break;
      case CLOE_CHARTS_ACTIONS.INTEGRATE:
        e.toggleActive = !e.toggleActive;
        this.serialChart.integrated = e.toggleActive;
        this.cloeSerialChartService.addGraphsFromModel(this.serialChart);
        break;
      case CLOE_CHARTS_ACTIONS.REMOVE_CHART:
        this.closeSerialChart();
        break;
    }
  }

  getUnitSymbol() {
    return this.serialChart[this.cloeChartService.getSymbolField(this.serialChart.integrated)];
  }

  getIntegragleSymbol() {
    return this.serialChart.integrated ? '[I] ' : '';
  }

  ngOnDestroy() {
    this.cloeSerialChartService.destroyChart(this.serialChart);
    this.cloeSerialChartSubscription.unsubscribe();
  }
}
