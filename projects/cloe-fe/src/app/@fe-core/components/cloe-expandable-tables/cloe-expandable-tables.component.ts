import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { IExpandableTableConfig } from './cloe-expandable-tables.models';
import { expandCollapseAnimation } from '@cloe-core/@core/@ui/animations';

@Component({
  selector: 'cloe-expendable-tables',
  templateUrl: './cloe-expandable-tables.component.html',
  styleUrls: ['./cloe-expandable-tables.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    expandCollapseAnimation
  ]
})
export class CloeExpandableTablesComponent implements OnInit {

  tableStatus = [true];

  @Input() config: IExpandableTableConfig;

  constructor() { }


  toggleTable(index) {
    const isOpen = !this.tableStatus[index];
    if (!this.config.multiExpand) {
      this.tableStatus = [];
    }
    this.tableStatus[index] = isOpen;
  }

  ngOnInit() {
  }

}
