import {ICloeTableStatic} from '@cloe-core/@core/@ui/components/cloe-table-static/cloe-table-static-interface/cloe-table-static-interface';

export interface IExpandableTableConfig {
  multiExpand: boolean;
  tables: Array<IExpandableTable>;
}

export interface IExpandableTable {
  tableId?: string;
  headerTitle: string;
  data: Array<any>;
  config: ICloeTableStatic;
}
