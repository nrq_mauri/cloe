import { AfterContentInit, AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { StateService } from '@uirouter/angular';
import { cloneDeep, extend, last } from 'lodash';
import { CloeTreeFiltersService } from '@cloe-core/@core/services/cloe-tree-service/cloe-tree-filters.service';
import { TreeFiltersTypeEnum } from '@cloe-core/@core/services/cloe-tree-service/enums/TreeFiltersTypeEnum';
import { BehaviorSubject, Subject } from 'rxjs';
import { ITreeOptions, TREE_ACTIONS, TreeNode } from 'angular-tree-component';
import { NodeCategoryEnum } from '@cloe-core/@core/enums/NodeCategoryEnum';
import { TranslateService } from '@ngx-translate/core';
import { TREE_EVENTS } from 'angular-tree-component/dist/constants/events';
import { StorageService } from '@cloe-core/@core/services/storage-service/storage.service';
import { CloeTreeComponent } from '@cloe-core/@core/@ui/components';

export const siteContextSubject = new BehaviorSubject({});

@Component({
  selector: 'cloe-site-context-picker',
  templateUrl: './site-context-picker.component.html',
  styleUrls: ['./site-context-picker.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SiteContextPickerComponent implements OnInit, AfterViewInit, OnDestroy {

  @Input() site;
  @Input() siteTree;
  sitesMap;
  isPanelClosed = true;
  sitesData = [];
  @ViewChild(CloeTreeComponent) cloeTree: CloeTreeComponent;
  pageLoaded = false;
  currentSiteName = '';
  currentSitesTooltip = [];
  filtersConfig;
  @Input() singleSiteSelection;
  siteTreeOptions: ITreeOptions = {
    actionMapping: {
      mouse: {
        click: (tree, node, $event) => {
          if (node.data.nodeCategory === NodeCategoryEnum.LEAF) {
            if (!this.singleSiteSelection) {
              TREE_ACTIONS.TOGGLE_ACTIVE_MULTI(tree, node, $event);
            } else {
              TREE_ACTIONS.TOGGLE_ACTIVE(tree, node, $event);
            }
          }
        }
      }
    }
  };
  siteTreeInfo = {
    nodeLabelFunction: (node) => {
      if (node.isRoot) {
        return 'component.site-picker.sites';
      } else {
        if (node.data.referObject) {
          return node.data.referObject.name;
        }
        return node.data.name;
      }
    }
  };

  constructor(
    private stateService: StateService,
    private treeFiltersService: CloeTreeFiltersService,
    private translateService: TranslateService
  ) {}

  ngOnInit() {
    this.sitesData = [this.siteTree];
    this.filtersConfig = this.treeFiltersService.addFiltersByType(TreeFiltersTypeEnum.SITE_FILTERS, this.sitesData);
  }

  ngAfterViewInit(): void {
    const firstRoot = this.cloeTree.tree.treeModel.roots[0];
    if (firstRoot) {
      firstRoot.expand();
    }
    let node;
    this.sitesMap = StorageService.getObject('sitesMap');
    if (this.singleSiteSelection && this.site) {
      node = this.cloeTree.tree.treeModel.getNodeBy(n => n.data.referUuid === this.site.uuid );
      node.setActiveAndVisible(true);
    } else {
      if (this.sitesMap && Object.keys(this.sitesMap).length > 0) {
        Object.keys(this.sitesMap).forEach((siteUuid) => {
          node = this.cloeTree.tree.treeModel.getNodeBy(n => n.data.referUuid === siteUuid);
          node.setActiveAndVisible(true);
        });
      } else {
        node = this.cloeTree.tree.treeModel.getNodeBy(n => n.data.referUuid === this.site.uuid);
        node.setActiveAndVisible(true);
        this.sitesMap = {};
        this.sitesMap[this.site.uuid] = this.site;
        StorageService.saveObject('sitesMap', this.sitesMap);
      }
      siteContextSubject.next({
        action: SiteContextEnum.UPDATE_SITES,
        data: {
          currentState: Object.keys(this.sitesMap)
        }
      });
    }
    this.cloeTree.tree.treeModel.update();
    setTimeout(() => {
      this.pageLoaded = true;
      this.currentSiteName = this.getCurrentSiteName();
    }, 500);
  }

  onClickNode(event) {
    if (!this.pageLoaded) {
      return;
    }

    const node = event.node.data;
    const nodeData = node.referObject;

    // move to the marker only if were clicking leafs of the tree
    if (node.nodeCategory === NodeCategoryEnum.LEAF) {
      if (this.singleSiteSelection) {
        if ( event.eventName === TREE_EVENTS.nodeActivate ) {
          StorageService.saveObject('singleSite', nodeData.uuid);
          const currentStateName = this.stateService.$current.name;
          const params = this.stateService.$current.params;
          this.stateService.go(
            currentStateName,
            extend(cloneDeep(params), {
              site: nodeData.uuid,
              singleSiteSelection: true
            }),
            {
              reload: true,
              inherit: false,
              notify: true
            });
        }
        return;
      } else {
        StorageService.removeObject('singleSite');
        if (event.eventName === TREE_EVENTS.nodeActivate) {
          this.sitesMap[nodeData.uuid] = nodeData;
        } else {
          delete this.sitesMap[nodeData.uuid];
        }
        siteContextSubject.next({
          action: SiteContextEnum.UPDATE_SITES,
          data: {
            currentState: Object.keys(this.sitesMap)
          }
        });
        StorageService.saveObject('sitesMap', this.sitesMap);
      }
      this.currentSiteName = this.getCurrentSiteName();
    }
  }

  private getCurrentSiteName() {
    if (this.singleSiteSelection) {
      this.currentSitesTooltip = this.buildNameTooltip([this.site.uuid]);
      return this.site.name;
    } else {
      const sitesNumber = Object.keys(this.sitesMap).length;
      if (sitesNumber === 1) {
        this.currentSitesTooltip = this.buildNameTooltip([last(Object.keys(this.sitesMap))]);
        return this.sitesMap[last(Object.keys(this.sitesMap))].name;
      }
      if (sitesNumber > 1) {
        this.currentSitesTooltip = this.buildNameTooltip(Object.keys(this.sitesMap));
        return this.translateService.instant('component.site-picker.sites-selected') + sitesNumber;
      }
      if (sitesNumber === 0) {
        const name = [this.translateService.instant('component.site-picker.no-sites-selected')];
        this.currentSitesTooltip = name;
        return name;
      }
    }
  }

  buildNameTooltip(sitesUuid: string[]) {
    const finalTooltip = [];
    sitesUuid.forEach((uuid) => {
      let tooltip = '';
      const siteNode: TreeNode = this.cloeTree.tree.treeModel.getNodeBy((node) => node.data.referUuid === uuid);
      if (siteNode) {
        const siteNodePath = siteNode.path.slice(1);
        siteNodePath.forEach((id, index) => {
          const currentSiteNode = this.cloeTree.tree.treeModel.getNodeById(+id);
          switch (index) {
            case 0:
              tooltip +=  '- ' + currentSiteNode.data.name + '\n';
              break;
            case (siteNodePath.length - 1):
              tooltip += ' > <b>' + currentSiteNode.data.name + '</b> \n';
              break;
            default:
              tooltip += ' > ' + currentSiteNode.data.name + '\n';
          }
        });
      }
      finalTooltip.push(tooltip);
    });
    return finalTooltip;
  }

  openPanel() {
    this.isPanelClosed = !this.isPanelClosed;
    setTimeout(() => this.cloeTree.tree.sizeChanged());
  }

  ngOnDestroy(): void {
    siteContextSubject.next({});
  }
}

export enum SiteContextEnum {
  UPDATE_SITES = 'UPDATE_SITES'
}
