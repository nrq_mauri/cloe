import {
  ClusterManagementComponent,
  ClusterTreeViewComponent,
  NewDiagnosisComponent,
  DiagnosisExcelErrorsTableComponent,
  DiagnosisUploadNewFileComponent
} from './modals';


export const MODAL_COMPONENTS = [
  // Components from MODAL_COMPONENTS_MAP
  ClusterManagementComponent,
  ClusterTreeViewComponent,
  NewDiagnosisComponent,
  DiagnosisExcelErrorsTableComponent,
  DiagnosisUploadNewFileComponent
];
