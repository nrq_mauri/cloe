export const colors = [
  '#0be3df',
  '#0abfbc',
  '#00c8fa',
  '#5e788c',
  '#3e4f5c',
  '#3a438c',
  '#84428c',
  '#8c576c',
  '#8c301f',
  '#8c8536',
];
