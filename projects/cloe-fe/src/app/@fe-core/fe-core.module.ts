import { NgModule } from '@angular/core';

import {
  CloeSerialChartComponent,
  CloeHeatMapComponent,
  CloePeriodSelectorComponent,
  CloeStockChartComponent,
  SiteContextPickerComponent,
  ChartLegendComponent,
  ChartLegendContainerComponent,
  AxisActionComponent,
  CloeThresholdTableComponent,
  CloeTreeChartComponent,
  DiagFileParserComponent, CloeRadarChartComponent,
  CloeExpandableTablesComponent
} from './components';
import { CloeCoreModule } from '@cloe-core/@core/cloe-core.module';
import { MODAL_COMPONENTS } from './components/modal-components.config';
import { MatDatepickerModule, MatFormFieldModule } from '@angular/material';
import { SatDatepickerModule } from 'saturn-datepicker';
import { AmChartsModule } from '@amcharts/amcharts3-angular';
import { NgxEchartsModule } from 'ngx-echarts';
import {
  CloeChartService,
  MeasureService,
  CloeHeatMapService,
  CloeSerialChartService,
  CloeStockChartService,
  CloeComparePeriodService,
  CloeDurationCurvesService,
  HomePageService,
  CloeTreeChartService,
  CloeAm4chartRadarService,
} from './services';

const BASE_MODULES = [
  CloeCoreModule,
  MatDatepickerModule,
  MatFormFieldModule,
  SatDatepickerModule,
  NgxEchartsModule
];

const COMPONENTS = [
  ...MODAL_COMPONENTS,
  SiteContextPickerComponent,
  CloePeriodSelectorComponent,
  CloeStockChartComponent,
  AxisActionComponent,
  CloeHeatMapComponent,
  CloeRadarChartComponent,
  ChartLegendComponent,
  ChartLegendContainerComponent,
  CloeSerialChartComponent,
  CloeThresholdTableComponent,
  CloeTreeChartComponent,
  DiagFileParserComponent,
  CloeExpandableTablesComponent
];

const PIPES = [];

const SERVICES = [
  CloeChartService,
  MeasureService,
  CloeHeatMapService,
  CloeSerialChartService,
  CloeStockChartService,
  CloeComparePeriodService,
  CloeDurationCurvesService,
  HomePageService,
  CloeTreeChartService,
  CloeAm4chartRadarService,
];

@NgModule({
  imports: [CloeCoreModule, AmChartsModule, ...BASE_MODULES],
  exports: [CloeCoreModule, AmChartsModule, ...COMPONENTS, ...PIPES, ...BASE_MODULES],
  declarations: [...COMPONENTS, ...PIPES],
  entryComponents: [...MODAL_COMPONENTS],
  providers: [
    ...PIPES,
    ...SERVICES
  ]
})
export class FeCoreModule {

}
