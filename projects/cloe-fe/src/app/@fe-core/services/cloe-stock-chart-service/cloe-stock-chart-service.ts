import { Injectable } from '@angular/core';
import { AmChart, AmChartsService } from '@amcharts/amcharts3-angular';
import { extend, filter, findIndex, map, remove, uniq, values, keys, difference } from 'lodash';
import { MeasureService, MeasuresResponse } from '../measure-service/measure-service';
import { ChartAxis, CloeChartService } from '../cloe-chart-service/cloe-chart-service';
import { EngFormatService } from '@cloe-core/@core/services/engFormat-service/engFormat-service';
import { PeriodTypeEnum } from '../../components/cloe-period-selector/enums/PeriodTypeEnum';
import { forkJoin } from 'rxjs/internal/observable/forkJoin';
import { Observable } from 'rxjs/internal/Observable';
import { tap } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';

export const viewCurvesRecapTableSubject$ = new BehaviorSubject<any>({ data: [], updateRecapRates: true });
export const viewCurvesGuidesSubject$ = new BehaviorSubject<any>(null);
export const viewCurvesSubject$ = new BehaviorSubject<IViewCurvesState>({
  graphRecipes: {},
  period: {},
  dataProvider: [],
  recapTableData: []
});

export interface IViewCurvesState {
  graphRecipes: { [key: string]: IGraphRecipe };
  period: any;
  dataProvider: Array<any>;
  recapTableData: Array<any>;
}

export interface IGraphRecipe {
  channelId: number;
  channelUuid: string;
  channelName: string;
  unitSymbol?: string;
  integratedSymbol?: string;
  channel: any;
  channelPath?: string;
  axis: ChartAxis;
  integrated?: boolean;
  summary?: any;
  integratedSummary?: any;
  type?: string;
  fillAlphas?: number;
  isRange?: boolean;
}

@Injectable()
export class CloeStockChartService {

  chart: AmChart;

  constructor(
    private AmCharts: AmChartsService,
    private measureService: MeasureService,
    private chartService: CloeChartService,
    private engFormatService: EngFormatService
  ) {
  }

  addGraphsByRecipes(recipes: Array<IGraphRecipe>, period: any, truncateDataProvider?: boolean) {
    const currentState: IViewCurvesState = viewCurvesSubject$.value;
    const recapTableData = currentState.recapTableData;
    const recipesMap = currentState.graphRecipes;
    let newDataProvider = truncateDataProvider ? [] : currentState.dataProvider;
    const measuresObservables = map(
      recipes,
      (graphRecipe: IGraphRecipe) => {
        recipesMap[graphRecipe.channelUuid] = graphRecipe;
        return this.measureService.getMeasuresForChannelUuid(
          graphRecipe.channelUuid,
          period
        );
      }
    );
    return new Observable(observer => {
      forkJoin(...measuresObservables).subscribe((resp: Array<MeasuresResponse>) => {
        resp.forEach(r => {
          const recipe = recipesMap[r.channelUuid];
          newDataProvider = this.chartService._mergeDataProvider(
            this.chartService._id(r.channelId),
            newDataProvider,
            r.measures,
            'timeStamp'
          );
          recipe.summary = r.summary;
          recipe.integratedSummary = r.integratedSummary;
          this.updateRecapTableData(recipe, recapTableData);
        });
        currentState.dataProvider = newDataProvider;
        currentState.period = period;
        observer.next(recipes);
      });
    }).pipe(
      tap(() => viewCurvesSubject$.next(currentState)),
      tap(() => viewCurvesRecapTableSubject$.next({ data: recapTableData, updateRecapRates: true }))
    );
  }

  updateGraphsByUuid(uuids: Array<string>, newConfig: any) {
    const currentState: IViewCurvesState = viewCurvesSubject$.value;
    const recipesMap = currentState.graphRecipes;
    const recipesToUpdate = map(uuids, uuid => recipesMap[uuid])
      .filter(recipe => !!recipe)
      .map(recipe => extend(recipe, newConfig));
    this.updateGraphsByRecipes(recipesToUpdate);
  }

  updateGraphsByRecipes(recipes: Array<IGraphRecipe>) {
    const currentState: IViewCurvesState = viewCurvesSubject$.value;
    const recapTableData = currentState.recapTableData;
    const recipesMap = currentState.graphRecipes;

    recipes.forEach((recipe: IGraphRecipe) => {
      this.updateRecapTableData(recipe, recapTableData);
      recipesMap[recipe.channelUuid] = recipe;
    });
    viewCurvesSubject$.next(currentState);
    viewCurvesRecapTableSubject$.next({ data: recapTableData });
  }

  updateRecapTableData(recipe: IGraphRecipe, recapTableData: Array<any>) {
    if (recipe.isRange) {
      return;
    }
    const summaryField = this.chartService.getSummaryField(recipe.integrated);
    const symbolField = this.chartService.getSymbolField(recipe.integrated);
    const graphRecapIndex = findIndex(recapTableData, (t: any) => t.channelId === recipe.channelId);
    const summary = recipe[summaryField];
    summary.dynamicSymbol = summary[symbolField];
    summary.channelNameWithSymbol = `${recipe.channelName} (${summary.dynamicSymbol}) `;
    summary.channelPath = recipe.channelPath;
    if (graphRecapIndex > -1) {
      recapTableData.splice(graphRecapIndex, 1, summary);
    } else {
      recapTableData.push(summary);
    }
  }

  removeGraph(channelUuid: string) {
    const currentState: IViewCurvesState = viewCurvesSubject$.value;
    remove(currentState.recapTableData, (t: any) => t.channelUuid === channelUuid);
    delete currentState.graphRecipes[channelUuid];
    viewCurvesSubject$.next(currentState);
    viewCurvesRecapTableSubject$.next({ data: currentState.recapTableData, updateRecapRates: true });
  }

  removeGraphFromChart(chart: AmChart) {
    const currentState: IViewCurvesState = viewCurvesSubject$.value;
    const recipes = keys(currentState.graphRecipes);
    const chartRecipes = chart.panels[0].stockGraphs.map((t: any) => t.uuid);
    const graphsUUIDToRemove = difference(chartRecipes, recipes);
    graphsUUIDToRemove.forEach(channelUuid => {
      remove(chart.panels[0].stockGraphs, (t: any) => t.uuid === channelUuid);
    });
  }

  setValueAxisLabelFunction(chart: AmChart) {
    this.AmCharts.updateChart(chart, () => {
      chart.panels[0].valueAxes.forEach((valueAxis) => {
        valueAxis.labelFunction = (item) => this.engFormatService.engFormat(item, 'value-axis');
      });
    });
  }

  forceChartUpdate(chart: AmChart) {
    this.AmCharts.updateChart(chart, () => {
    });
  }

  upsertGraphToChart(chart: AmChart, recipe: IGraphRecipe) {
    const graphId = this.chartService._id(recipe.channelId);
    const valueField = this.chartService._id(recipe.channelId, recipe.integrated);
    const dataSet = chart.dataSets[0];

    // upsert its fieldMap if doesn't exits
    const fieldMappings = dataSet.fieldMappings;
    const fieldMappingIndex = findIndex(fieldMappings, (t: any) => t.name === graphId);
    const fieldMapping = {
      name: graphId,
      fromField: valueField,
      toField: valueField
    };
    if (fieldMappingIndex > -1) {
      fieldMappings.splice(fieldMappingIndex, 1, fieldMapping);
    } else {
      fieldMappings.push(fieldMapping);
    }

    const symbolField = this.chartService.getSymbolField(recipe.integrated);
    const stockGraphs = chart.panels[0].stockGraphs;
    const stockGraphIndex = findIndex(stockGraphs, (t: any) => t.id === graphId);
    // create graph
    const stockGraph = {
      id: graphId,
      uuid: recipe.channelUuid,
      title:
      (recipe.integrated ? '[I] ' : '') +
      (recipe.channelName || graphId) +
        (recipe[symbolField] ? ' (' + recipe[symbolField] + ') ' : '') +
      ' [' + recipe.axis.toString() + ']',
      channelPath: recipe.channelPath,
      valueField: valueField,
      lineThickness: 2,
      valueAxis: recipe.axis,
      useDataSetColors: false,
      referObject: recipe.channel,
      connect: false,
      type: recipe.type || 'line',
      fillAlphas: recipe.fillAlphas || 0,
      integrated: recipe.integrated,
      integrable: this.chartService.checkChannelIsIntegrable(recipe),
      isRange: !!recipe.isRange,
      balloonFunction: (item) => this.engFormatService.engFormat(item.values.value, 'default')
    };

    if (stockGraphIndex > -1) {
      stockGraphs.splice(stockGraphIndex, 1, stockGraph);
    } else {
      stockGraphs.push(stockGraph);
    }

    // set scrollbar graph if not set
    const scrollBarSettings = chart.chartScrollbarSettings;
    const scrollGraph = findIndex(stockGraphs, (t: any) => t.id === scrollBarSettings.graph);

    if (scrollGraph === -1) {
      scrollBarSettings.graph = graphId;
    }
  }

  updateMinPeriod(chart: AmChart, periodType: PeriodTypeEnum) {
    let minPeriod = '';
    switch (periodType) {
      case PeriodTypeEnum.MIN:
        minPeriod = 'mm';
        break;
      case PeriodTypeEnum.PENTA_MIN:
        minPeriod = '5mm';
        break;
      case PeriodTypeEnum.DECA_MIN:
        minPeriod = '10mm';
        break;
      case PeriodTypeEnum.QUARTER:
        minPeriod = '15mm';
        break;
      case PeriodTypeEnum.HALF_HOUR:
        minPeriod = '30mm';
        break;
      case PeriodTypeEnum.HOUR:
        minPeriod = 'hh';
        break;
      case PeriodTypeEnum.DAY:
        minPeriod = 'DD';
        break;
      case PeriodTypeEnum.WEEK:
        minPeriod = '7DD';
        break;
      case PeriodTypeEnum.MONTH:
        minPeriod = 'MM';
        break;
      default:
        minPeriod = 'mm';
        break;
    }
    chart.panels[0].categoryAxis.minPeriod = minPeriod;
    chart.categoryAxesSettings.minPeriod = minPeriod;
    chart.categoryAxesSettings.groupToPeriods = [];
  }

  updateGraph(chart: AmChart, channelUuid, newGraphConfig) {
    const currentState: IViewCurvesState = viewCurvesSubject$.value;
    let graph = currentState.graphRecipes[channelUuid];
    graph = extend(graph, newGraphConfig);
    currentState.graphRecipes[channelUuid] = graph;
    viewCurvesSubject$.next(currentState);
    // if (graph) {
    //   this.AmCharts.updateChart(chart, () => {
    //     graph = extend(graph, newGraphConfig);
    //   });
    // }
  }

  destroyChart(chart: AmChart) {
    if (chart) {
      // this.AmCharts.destroyChart(chart);
      chart.dataSets[0].fieldMappings = [];
      chart.dataSets[0].dataProvider = [];
      chart.panels[0].stockGraphs = [];
      chart.panels[0].categoryAxis.guides = [];
      chart.validateNow(true);
    }
  }

  toggleAxisStackable(chart: AmChart, axisId, stackable) {
    const valueAxis = chart.panels[0].getValueAxisById(axisId);
    if (valueAxis) {
      this.AmCharts.updateChart(chart, () => {
        valueAxis.stackType = stackable ? 'regular' : 'none';
      });
    }
  }

  toggleZeroOriginAxes(chart: AmChart, axisId, toggle) {
    const valueAxis = chart.panels[0].getValueAxisById(axisId);
    this.AmCharts.updateChart(chart, () => {
      valueAxis.minimum = toggle ? 0 : undefined;
      valueAxis.toggleZeroOrigin = toggle;
    });
  }

  toggleAxesSynchronization(chart: AmChart, axisId, toggle) {
    const valueAxis = chart.panels[0].getValueAxisById(axisId);
    this.AmCharts.updateChart(chart, () => {
      chart.panels[0].graphs.forEach((graph) => {
        graph.valueAxis.minimum = graph.valueAxis.toggleZeroOrigin ? 0 : undefined;
        graph.valueAxis.maximum = undefined;
      });
    });

    this.AmCharts.updateChart(chart, () => {
      if (toggle) {
        chart.panels[0].graphs.forEach((graph) => {
          if (graph.valueAxis.id !== valueAxis.id) {
            graph.valueAxis.minimum = graph.valueAxis.toggleZeroOrigin ? 0 : valueAxis.min;
            graph.valueAxis.maximum = valueAxis.max;
          }
        });
      }
    });
  }

  toggleBullets(chart: AmChart) {
    this.AmCharts.updateChart(chart, () => {
      if (!chart.panels[0].graphs[0].bullet || chart.panels[0].graphs[0].bullet === 'none') {
        chart.panels[0].graphs.forEach((graph) => {
          graph.bullet = 'circle';
        });
      } else {
        chart.panels[0].graphs.forEach((graph) => {
          graph.bullet = 'none';
        });
      }
    });
  }

  isAxisValidForStack(chart: AmChart, axisId): boolean {
    // All the channels inside this axis must be with the same unit symbol!
    const stockGraphs = chart.panels[0].stockGraphs;
    const axisGraph = filter(stockGraphs, (graph: any) => graph.valueAxis.id === axisId && graph.uuid);
    return uniq(map(axisGraph, 'referObject.unitSymbol')).length === 1;
  }

  updateGuides(chart: AmChart, guidesToFormat) {
    const categoryAxis = chart.panels[0].categoryAxis;
    categoryAxis.guides = [];
    if (guidesToFormat) {
      values(guidesToFormat).forEach(range => {
        if (!range.periods || !range.periods.length) {
          return;
        }
        range.periods.forEach(period => {
          categoryAxis.addGuide({
            date: period.startDate,
            toDate: period.endDate,
            fillAlpha: 0.2,
            fillColor: range.color,
            inside: true,
            labelRotation: 90,
            fontSize: 14,
            boldLabel: true
          });
        });
      });
    } else {
      categoryAxis.guides = [];
    }
  }
}
