import { Injectable } from '@angular/core';
import { map } from 'lodash';
// @ts-ignore
import * as am4core from '@amcharts/amcharts4/core';
// @ts-ignore
import * as am4charts from '@amcharts/amcharts4/charts';
import am4themes_animated from '@amcharts/amcharts4/themes/animated';
import am4themes_kelly from '@amcharts/amcharts4/themes/kelly';

am4core.useTheme(am4themes_animated);
am4core.useTheme(am4themes_kelly);

@Injectable()
export class CloeAm4chartRadarService {

  constructor() {}

  createChart(config: IRadarConfig) {
    const chart = am4core.create(config.divId, am4charts.RadarChart);
    this.initChart(chart, config);
    return chart;
  }

  initChart(chart: am4charts.RadarChart, config: IRadarConfig) {
    const categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis<am4charts.AxisRendererCircular>());
    categoryAxis.dataFields.category = config.categoryAxis;
    categoryAxis.tooltip.disabled = true;
    const valueAxis = chart.yAxes.push(new am4charts.ValueAxis<am4charts.AxisRendererRadial>());
    valueAxis.tooltip.disabled = true;
    valueAxis.renderer.labels.template.disabled = true;

    chart.mouseWheelBehavior = 'none';

    chart.logo.disabled = true;

    if (config.colorSet) {
      this.addColorSet(chart, config.colorSet);
    }
    if (config.series) {
      this.addSeriesToChart(chart, config);
    }
    if (config.cursor) {
      this.addCursorToChart(chart, config.cursor);
    }
    if (config.legend) {
      this.addLegendToChart(chart, config.legend);
    }
    if (config.data) {
      this.addDataToChart(chart, config.data);
    }
  }

  addSeriesToChart(chart: am4charts.RadarChart, config: IRadarConfig) {
    const series = config.series;
    series.forEach((serie) => {
      const newSerie = chart.series.push(new am4charts.RadarSeries());
      newSerie.dataFields.valueY = config.valueAxis;
      newSerie.dataFields.categoryX = config.categoryAxis;
      newSerie.name = serie.name;
      newSerie.strokeWidth = 3;
      newSerie.fillOpacity = 0.5;
      newSerie.strokeOpacity = 0.5;
      newSerie.sequencedInterpolation = true;
      newSerie.sequencedInterpolationDelay = 100;
      newSerie.dataFields.categoryX = serie.dataFieldCategoryAxis;
      newSerie.dataFields.valueY = serie.dataFieldValueAxis;
      serie.tooltipFunction ?
        newSerie.adapter.add('tooltipText', serie.tooltipFunction) : newSerie.tooltipText = '[bold]{valueY}[/]';
    });
  }

  addCursorToChart(chart: am4charts.RadarChart, cursorConfig) {
    const cursor = chart.cursor = new am4charts.RadarCursor();
    cursor.behavior = 'none';
  }

  addLegendToChart(chart: am4charts.RadarChart, legendConfig) {
    const legend = chart.legend = new am4charts.Legend();
    legend.markers.template.height = 2;
  }

  addDataToChart(chart: am4charts.RadarChart, data: any[]) {
    chart.data = data;
  }

  addColorSet(chart: am4charts.RadarChart, colorSet: any[]) {
    chart.colors.list = map(colorSet, (color: string) => am4core.color(color));
  }

  destroyChart(chart: am4charts.RadarChart) {
    chart.dispose();
  }
}

export interface IRadarConfig {
  divId: string;
  valueAxis: string;
  categoryAxis: string;
  series?: IRadarSerie[];
  cursor?: any;
  legend?: any;
  data?: any[];
  colorSet?: any[];
}

export interface IRadarSerie {
  name?: string;
  strokeWidth?: number;
  tooltipFunction?: any;
  dataFieldValueAxis?: string;
  dataFieldCategoryAxis?: string;
}
