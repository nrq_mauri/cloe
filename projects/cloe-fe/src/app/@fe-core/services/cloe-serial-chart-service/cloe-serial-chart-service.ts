import { Injectable } from '@angular/core';
import { AmChart, AmChartsService } from '@amcharts/amcharts3-angular';
import { remove, cloneDeep, findIndex, find } from 'lodash';
import { MeasureService } from '../measure-service/measure-service';
import { CloeDatePipe } from '@cloe-core/@core/@ui/pipes';
import {
  CLOE_CHARTS_ACTIONS,
  CloeChartService,
  GraphConfig,
  ICloeChart
} from '../cloe-chart-service/cloe-chart-service';
import { Subject } from 'rxjs';
import { EngFormatService } from '@cloe-core/@core/services/engFormat-service/engFormat-service';
import { TranslateService } from '@ngx-translate/core';

export const cloeSerialChartSubject$ = new Subject();

@Injectable()
export class CloeSerialChartService {
  charts: ICloeChart[] = [];

  constructor(
    private AmCharts: AmChartsService,
    private measureService: MeasureService,
    private cloeDatePipe: CloeDatePipe,
    private cloeChartService: CloeChartService,
    private engFormatService: EngFormatService
  ) {
  }


  createChart(serialChart: ICloeChart): ICloeChart {
    const serialConf = cloneDeep(serialChart.configuration);
    serialChart.chart = this.AmCharts.makeChart(serialChart.divId, serialConf);
    serialChart.chart.language = 'it'; // TODO
    return serialChart;
  }

  _createGraphConfigForPeriod(axisNAme, period, {channelId, channelUuid, channelName, measures}): GraphConfig {
    return {
      id: channelId + this._formatPeriodName(period),
      uuid: channelUuid,
      period: period,
      name: this._formatPeriodName(period),
      axis: axisNAme,
      data: measures,
      baloonTextField: channelId + this._formatPeriodName(period) + 'baloonText'
    };
  }

  private _formatPeriodName(period) {
    return this.cloeDatePipe.transform(period.startDate, 'YYYY-MM-DD') +
      ' - ' + this.cloeDatePipe.transform(period.endDate, 'YYYY-MM-DD');
  }

  updateDataProvider(dataProvider, chart: AmChart) {
    this.AmCharts.updateChart(chart, () => {
      chart.dataProvider = dataProvider;
    });
  }

  addGuide(chart: AmChart, guide: any) {
    this.AmCharts.updateChart(chart, () => {
      chart.guides.push(guide);
    });
  }

  addGuideList(chart: AmChart, guides: any[]) {
    this.AmCharts.updateChart(chart, () => {
      chart.guides.push(...guides);
    });
  }

  removeGuide(chart: AmChart, guide: any) {
    this.AmCharts.updateChart(chart, () => {
      remove(chart.guides, (guideElement) => {
        return guideElement['value'] === guide.thresholdValue;
      });
      remove(chart.guides, (guideElement) => {
        return guideElement['category'] === guide.operationHour;
      });
      remove(chart.valueAxes[0].guides, (guideElement) => {
        return guideElement['value'] === guide.thresholdValue;
      });
      remove(chart.categoryAxis.guides, (guideElement) => {
        return guideElement['category'] === guide.operationHour;
      });
    });
  }

  clearGuides(chart: AmChart) {
    this.AmCharts.updateChart(chart, () => {
      remove(chart.guides);
      remove(chart.valueAxes[0].guides);
      remove(chart.categoryAxis.guides);
    });
  }

  private _updateValueField(graphConfig, integrated) {
    if (integrated) {
      graphConfig.valueField = graphConfig.id + this.cloeChartService.idIntegratedSuffix();
    } else {
      graphConfig.valueField = graphConfig.id;
    }
  }

  addGraphsFromModel(serialChart: ICloeChart) {
    serialChart.graphsConfig.forEach((graphConfig) => {
      this._addGraphToChart(graphConfig, serialChart.chart, serialChart.integrated, serialChart.thresholds);
    });
    this.updateDataProvider(serialChart.dataProvider, serialChart.chart);
  }

  _addGraphToChart(graphConfig: GraphConfig, chart: AmChart, integrated?, thresholds?) {
    this.AmCharts.updateChart(chart, () => {
      const graphId = this.cloeChartService._id(graphConfig.id);
      // create graph if it not exists
      const series = chart.graphs;
      const serialGraph = find(series, {id: graphId});
      let hidden = series.length !== 0 && chart.singleGraphVisible;
      if (thresholds) {
        hidden = thresholds[0].graph.id !== graphId;
      }
      if (!serialGraph) {
        const newGraph = {
          id: graphId,
          uuid: graphConfig.uuid,
          period: graphConfig.period,
          title: graphConfig.name || graphId,
          valueField: graphId,
          hidden: hidden,
          lineThickness: 2,
          valueAxis: graphConfig.axis,
          useDataSetColors: false,
          balloonFunction: (item, graph) => {
            let result = '';
            result +=
              item.dataContext[graphConfig.baloonTextField] ?
                'date: ' + item.dataContext[graphConfig.baloonTextField] + ' ' :
                '';
            result += 'value: ' + this.engFormatService.engFormat(item.values.value, 'default');
            return result;
          }
        };
        this._updateValueField(newGraph, integrated);
        series.push(newGraph);
      } else {
        this._updateValueField(serialGraph, integrated);
      }
    });
  }

  addSerialChart(serialChart: ICloeChart) {
    this.charts.push(serialChart);
  }

  toggleBullets(chart: AmChart) {
    this.AmCharts.updateChart(chart, () => {
      if (!chart.graphs[0].bullet || chart.graphs[0].bullet === 'none') {
        chart.graphs.forEach( (graph) => {
          graph.bullet = 'circle';
        });
      } else {
        chart.graphs.forEach( (graph) => {
          graph.bullet = 'none';
        });
      }
    });
  }

  removeSerialChartByChannelUuid(channelUuid: string) {
    remove(this.charts, (serialChart) => {
      return serialChart.channelUuid === channelUuid;
    });
    cloeSerialChartSubject$.next({
      action: CLOE_CHARTS_ACTIONS.REMOVE_CHART,
      value: {
        channelUuid: channelUuid
      }
    });
  }

  clearSerialCharts() {
    remove(this.charts);
    cloeSerialChartSubject$.next({
      action: CLOE_CHARTS_ACTIONS.CLEAR_CHART
    });
  }

  destroyChart(serialChart: ICloeChart) {
    this.AmCharts.updateChart(serialChart.chart, () => {
      serialChart.chart.graphs = [];
      serialChart.chart.dataProvider = [];
    });
  }
}
