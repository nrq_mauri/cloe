import { Injectable } from '@angular/core';
import { TREE_CHART_CONFIG } from './cloe-tree-chart-config';
import { remove, cloneDeep } from 'lodash';

import ECharts = echarts.ECharts;
import { CloeTreeNodeService } from '@cloe-core/@core/services/cloe-treeNode-service/cloe-tree-node.service';
import { StorageService } from '@cloe-core/@core/services/storage-service/storage.service';

@Injectable()
export class CloeTreeChartService {
  treeCharts: TreeChart[] = [];

  constructor(
    private cloeTreeNodeService: CloeTreeNodeService
  ) {}

  createTreeChartByTreeUuid(treeChart: TreeChart) {
    const treeChartOptions: any = cloneDeep(TREE_CHART_CONFIG);
    this.cloeTreeNodeService.getTreeNodeByReferUuid(
      'SITE',
      StorageService.getCurrentUser(),
      false,
      treeChart.treeUuid,
      false,
      true,
      false
    ).subscribe( (result) => {
      treeChartOptions.series[0].data.push(result);
      treeChart.chart.setOption(treeChartOptions);
    });
    return treeChart;
  }

  destroyChart(chart: ECharts) {
    chart.dispose();
  }

  addTreeChart(treeChart: TreeChart) {
    this.treeCharts.push(treeChart);
  }

  removeTreeChartByDivId(divId: string) {
    remove(this.treeCharts, (treeChart) => {
      return treeChart.divId === divId;
    });
  }

  clearTreeCharts() {
    remove(this.treeCharts);
  }
}

export interface TreeChart {
  divId: string;
  treeUuid: string;
  chart?: ECharts;
}
