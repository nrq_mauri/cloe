import { Injectable } from '@angular/core';
import { AmChart, AmChartsService } from '@amcharts/amcharts3-angular';
import { keyBy, map, merge, values, cloneDeep, extend, remove } from 'lodash';
import { MeasureService } from '../measure-service/measure-service';
import { Observable } from 'rxjs/internal/Observable';
import { PeriodType } from '../../components/cloe-period-selector/enums/PeriodType';
import { of } from 'rxjs/internal/observable/of';
import { IGraphRecipe } from '../cloe-stock-chart-service/cloe-stock-chart-service';
import { ChannelRangeService } from '@cloe-core/@core/services/channel-range-service/channel-range-service';
import {
  ChannelRangeAlgorithmService
} from '@cloe-core/@core/services/channel-range-service/channel-range-algorithm-service';

const ID_PREFIX = 'C-';
const ID_INTEGRATED_SUFFIX = '_I';

export enum CLOE_CHARTS_ACTIONS {
  ADD_GRAPH = 'add-graph',
  REMOVE_GRAPH = 'remove-graph',
  UPDATE_PERIOD = 'update-period',
  UPDATE_DATA_PROVIDER = 'update_data_provider',
  REMOVE_CHART = 'remove_chart',
  CLEAR_CHART = 'clear_chart',
  ADD_THRESHOLD = 'add_threshold',
  REMOVE_THRESHOLD = 'remove_threshold',
  CLEAR_THRESHOLDS = 'clear_thresholds',
  CLEAR_ALL_THRESHOLDS = 'clear_all_thresholds',
  TOGGLE_BULLETS = 'toggle_bullets',
  INTEGRATE = 'integrate'
}

export interface ICloeChart {
  divId: string;
  channelUuid?: string;
  channelName?: string;
  channelPath?: string;
  unitSymbol?: string;
  integratedSymbol?: string;
  chart?: AmChart;
  configuration?: any;
  graphsConfig?: any[];
  thresholds?: any[];
  dataProvider?: Array<any>;
  integrable?: boolean;
  integrated?: boolean;
}

@Injectable()
export class CloeChartService {

  chart: AmChart;

  constructor(
    public AmCharts: AmChartsService,
    private measureService: MeasureService,
    private channelRangeService: ChannelRangeService
  ) {}

  createChart(divId: string, chartConfig: any): ICloeChart {
    const chart = this.AmCharts.makeChart(divId, chartConfig);
    chart.language = 'it'; // TODO

    return {
      divId: divId,
      chart: chart
    };
  }

  _mergeDataProvider(
    graphId: string,
    dataProvider: any,
    data: Array<any>,
    categoryField: string
  ) {
    return values(
      merge(
        keyBy(dataProvider, categoryField),
        keyBy(
          map(data, measure => {
            measure[graphId] = measure['value'];
            measure[(graphId + ID_INTEGRATED_SUFFIX)] = measure[this.getMeasureField(true)];
            // delete measure[measureField];
            return measure;
          }),
          categoryField
        )
      )
    );
  }

  getSummaryField(integrated) {
    return integrated ? 'integratedSummary' : 'summary';
  }

  getMeasureField(integrated: boolean) {
    return integrated ? 'integratedValue' : 'value';
  }

  getSymbolField(integrated) {
    return integrated ? 'integratedSymbol' : 'unitSymbol';
  }

  _id(id: any, integrated = false) {
    return ID_PREFIX + id + (integrated ? ID_INTEGRATED_SUFFIX : '');
  }

  idIntegratedSuffix() {
    return ID_INTEGRATED_SUFFIX;
  }

  checkChannelIsIntegrable(r: any) {
    return !!r.integratedSymbol;
  }

  getGuideFromChannelRange(channel, period, updateGuides) {
    const channelRangeUuid = channel.uuid;
    if (!channelRangeUuid) {
      return of({});
    }
    const periodSelected = extend({}, period, {
      periodType: PeriodType.samplingToPeriodType(channel.sampling)
    });
    const channelRangeAlgorithmService: ChannelRangeAlgorithmService = this.channelRangeService.createAlgorithm(channel);
    return new Observable(observer => {
      observer.next(
        channelRangeAlgorithmService.getRangesFromPeriod(channel, periodSelected)
      );
    });
  }
}

export enum ChartAxis {
  LEFT_LEFT = 'Y1',
  LEFT_RIGHT = 'Y2',
  RIGHT_LEFT = 'Y3',
  RIGHT_RIGHT = 'Y4',
}

export interface IMeasure {
  channelId: number;
  timeStamp: string;
  value: number;
}

export interface GraphConfig {
  id?: string;
  uuid?: string;
  name?: string;
  axis?: ChartAxis;
  data?: Array<IMeasure>;
  hideLegend?: boolean;
  hidden?: boolean;
  referObject?: any;
  baloonTextField?: string;
  period?: any;
  type?: string;
  fillAlphas?: number;
}
