import { Injectable } from '@angular/core';
import { AmChartsService } from '@amcharts/amcharts3-angular';
import { concat, countBy, map, orderBy, transform, range, findIndex, find, difference, isNil, filter } from 'lodash';
import { MeasureService } from '../measure-service/measure-service';
import { CloeDatePipe } from '@cloe-core/@core/@ui/pipes';
import { ChartAxis, CLOE_CHARTS_ACTIONS, CloeChartService, ICloeChart } from '../cloe-chart-service/cloe-chart-service';
import { EngFormatService } from '@cloe-core/@core/services/engFormat-service/engFormat-service';
import { CloeSerialChartService, cloeSerialChartSubject$ } from '../cloe-serial-chart-service/cloe-serial-chart-service';
import { PeriodType } from '../../components/cloe-period-selector/enums/PeriodType';
import { PeriodTypeEnum } from '../../components/cloe-period-selector/enums/PeriodTypeEnum';
import { TranslateService } from '@ngx-translate/core';

@Injectable()
export class CloeDurationCurvesService {
  THRESHOLD_LIMIT = 4;

  constructor(
    private AmCharts: AmChartsService,
    private measureService: MeasureService,
    private cloeDatePipe: CloeDatePipe,
    private cloeChartService: CloeChartService,
    private engFormatService: EngFormatService,
    private serialChartService: CloeSerialChartService,
    private translateService: TranslateService
  ) {
  }

  setDurationCurvesGuidesListener(config) {
    config.listeners.push({
      'event': 'init',
      'method': (e) => {
        e.chart.chartDiv.addEventListener('click', () => {
          if (e.chart.lastCursorPosition !== undefined) {
            // get value of the last known cursor position
            const value = e.chart.dataProvider[e.chart.lastCursorPosition]['value'];
            const row = e.chart.dataProvider[e.chart.lastCursorPosition];

            const index = findIndex(e.chart.guides, (chartGuide) => {
              return chartGuide['value'] === value;
            });

            const visibleGraph = this.getVisibleGraph(e.chart);

            if (index === -1 && e.chart.guides.length < (this.THRESHOLD_LIMIT * 2) && row[visibleGraph.id]) {
              const avaiableIDs = filter(
                difference(range(1, e.chart.guides.length + 2), map(e.chart.guides, (r) => r.labelIndex)),
                (idElement) => !isNil(idElement)
              );
              const text = this.translateService.instant('page.duration-curves.threshold') + (' ' + avaiableIDs[0]);
              cloeSerialChartSubject$.next({
                action: CLOE_CHARTS_ACTIONS.ADD_THRESHOLD,
                value: {
                  labelIndex: avaiableIDs[0],
                  thresholdName: text,
                  thresholdValue: value,
                  row: row,
                  graph: visibleGraph
                }
              });
            }
            e.chart.validateData();
          }
        });
      }
    });
  }

  private getVisibleGraph(chart) {
    return find(chart.graphs, (graph) => {
      return !graph.hidden;
    });
  }

  computeOperationHours(data, periodType, graphID) {
    const groupedData = countBy(data, graphID);
    return transform(groupedData, (result, value, key) => {
        const measure: any = {};
        measure['operationHours'] =
          (value * PeriodType.periodTypeToSampling(periodType)) / PeriodType.periodTypeToSampling(PeriodTypeEnum.HOUR);
        measure['value'] = key;
        result.push(measure);
      }, []
    );
  }

  createDurationCurvesGraphFromPeriod(period, serialChart: ICloeChart) {
    this.measureService.getMeasuresForChannelUuid(serialChart.channelUuid, period)
      .subscribe((data: any) => {
        if (data.measures && data.measures.length > 0) {
          const graphConfig = this.serialChartService._createGraphConfigForPeriod(
            ChartAxis.LEFT_RIGHT,
            period,
            data
          );
          serialChart.graphsConfig.push(graphConfig);
          const dataProvider = serialChart.dataProvider;
          serialChart.dataProvider = this.concatDataProvider(
            this.cloeChartService._id(graphConfig.id),
            dataProvider,
            data.measures
          );
          cloeSerialChartSubject$.next({
            action: CLOE_CHARTS_ACTIONS.UPDATE_DATA_PROVIDER,
            value: {
              newGraph: graphConfig
            }
          });
        }
      });
  }

  private concatDataProvider(graphId: string, dataProvider: any, data: Array<any>) {
    let index = 1;
    data = map(data, measure => {
      measure[graphId] = +measure.value;
      return measure;
    });
    data = map(orderBy(data, graphId, 'desc'), measure => {
        measure['formattedxAxis'] = index++;
        return measure;
      }
    );
    return orderBy(concat(dataProvider, data), 'formattedxAxis', 'asc');
  }
}
