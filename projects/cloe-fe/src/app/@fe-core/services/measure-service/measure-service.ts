import { Injectable } from '@angular/core';
import { of } from 'rxjs/internal/observable/of';
import { HttpClient } from '@angular/common/http';
import { CloeSettings } from '@cloe-core/@core/services/cloe.settings';
import { MsContextEnum } from '@cloe-core/@core/enums/MsContextEnum';
import { Observable } from 'rxjs/internal/Observable';

const CHANNEL_MEASURE_API = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + '/measures';
const RATES_RECAP_MEASURE_API = CHANNEL_MEASURE_API + '/rates-data';
const CHANNEL_TABLE_MEASURE_API = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + '/measures/measure_table';
const HOME_DETAILS_API = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + '/home-page-data';

export interface MeasuresResponse {
  channelId: number;
  channelUuid: string;
  summary: any;
  integratedSummary: any;
  measures: Array<any>;
}

@Injectable()
export class MeasureService {
  constructor(private http: HttpClient) {

  }

  getMeasuresForChannelUuid(channelUuid: string, periodSelected: any) {
    return this.http.post(
      CHANNEL_MEASURE_API + '/' + channelUuid,
      periodSelected
    );
  }

  getRecapRatesMeasuresForChannelUuid(channelUuid: string, channelRangeUuid, periodSelected: any) {
    return this.http.post(
      RATES_RECAP_MEASURE_API + '/' + channelUuid + '/' + channelRangeUuid,
      periodSelected
    );
  }

  getTableMeasureForChannelUuid(channelUuid: string, periodSelected: any) {
    return this.http.post(
      CHANNEL_TABLE_MEASURE_API + '/' + channelUuid,
      periodSelected
    );
  }

  getHomePageDetails(site: any, cloeUser: any) {
    const dto = {
      defaultSiteUuid: site.uuid,
      defaultChannelUuid: cloeUser.defaultChannelUuid
    };
    return this.http.post(
      HOME_DETAILS_API,
      dto
    );
  }

  private _generateChartData() {
    const data = [];
    const firstDate = new Date();
    firstDate.setDate(firstDate.getDate() - 500);
    firstDate.setHours(0, 0, 0, 0);

    for (let i = 0; i < 30; i++) {
      const newDate = new Date(firstDate);
      newDate.setDate(newDate.getDate() + i);
      newDate.setHours(i % 24);

      const a1 = Math.round(Math.random() * (40 + i)) + 100 + i;

      data.push({
        timeStamp: newDate.toISOString(),
        value: a1,
      });
    }
    return data;
  }
}
