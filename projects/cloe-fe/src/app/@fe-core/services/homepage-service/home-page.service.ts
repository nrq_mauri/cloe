import { Injectable } from '@angular/core';
import { CloeSettings } from '@cloe-core/@core/services/cloe.settings';
import { MsContextEnum } from '@cloe-core/@core/enums/MsContextEnum';
import { HttpClient } from '@angular/common/http';

const HOME_PAGE_DATA = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + '/home-page-data';
const HOME_PAGE_DETAIL = HOME_PAGE_DATA + '/details';
const HOME_PAGE_LINE_RATE = HOME_PAGE_DATA + '/line-rate';
const HOME_PAGE_LAST_WEEK_DETAIL = HOME_PAGE_DATA + '/last-week';
const HOME_PAGE_YEARLY_DETAIL = HOME_PAGE_DATA + '/yearly-detail';
const HOME_PAGE_YEARLY_MEASURES = HOME_PAGE_DATA + '/yearly-measures';

@Injectable()
export class HomePageService {
  constructor(private http: HttpClient) {}

  private _createPayloadFromUserDefaults(userDefaults) {
    return {
      defaultSiteUuid: userDefaults.defaultSite.uuid,
      defaultChannelUuid: userDefaults.defaultChannel.uuid,
      uuid: userDefaults.userUuid
    };
  }

  formatChannelName(channel: any) {
    return `${channel.channelName} (${channel.integratedSymbol})`;
  }

  getHomePageDetail(userDefaults: any) {
    return this.http.post(
      HOME_PAGE_DETAIL,
      this._createPayloadFromUserDefaults(userDefaults)
    );
  }

  getHomePageLineRate(userDefaults: any) {
    return this.http.post(
      HOME_PAGE_LINE_RATE,
      this._createPayloadFromUserDefaults(userDefaults)
    );
  }

  getHomePageLastWeekDetail(userDefaults: any) {
    return this.http.post(
      HOME_PAGE_LAST_WEEK_DETAIL,
      this._createPayloadFromUserDefaults(userDefaults)
    );
  }

  getHomePageYearlyDetail(userDefaults: any) {
    return this.http.post(
      HOME_PAGE_YEARLY_DETAIL,
      this._createPayloadFromUserDefaults(userDefaults)
    );
  }

  getHomePageYearlyMeasures(userDefaults: any) {
    return this.http.post(
      HOME_PAGE_YEARLY_MEASURES,
      this._createPayloadFromUserDefaults(userDefaults)
    );
  }
}
