import { Injectable } from '@angular/core';
import { AmChart, AmChartsService } from '@amcharts/amcharts3-angular';
import { MeasureService } from '../measure-service/measure-service';
import { CloeDatePipe } from '@cloe-core/@core/@ui/pipes';
import { ChartAxis, CLOE_CHARTS_ACTIONS, CloeChartService, ICloeChart } from '../cloe-chart-service/cloe-chart-service';
import { EngFormatService } from '@cloe-core/@core/services/engFormat-service/engFormat-service';
import { CloeSerialChartService, cloeSerialChartSubject$ } from '../cloe-serial-chart-service/cloe-serial-chart-service';
import { findIndex, find } from 'lodash';
import { RangeTypeEnum } from '../../components/cloe-period-selector/enums/RangeTypeEnum';
import * as moment from 'moment';

@Injectable()
export class CloeComparePeriodService {

  constructor(private AmCharts: AmChartsService,
              private measureService: MeasureService,
              private cloeDatePipe: CloeDatePipe,
              private cloeChartService: CloeChartService,
              private engFormatService: EngFormatService,
              private serialChartService: CloeSerialChartService
  ) {}

  freeRangeDataAdapter(data, graphConfig) {
    let index = 1;
    data.map((element: any) => {
      element.formattedxAxis = '' + index++;
      element[graphConfig.baloonTextField] = moment(element.timeStamp).format('lll');
    });
  }

  dataAdapter(data, periodSelected, graphConfig) {
    const formats = {xFormat: '', xLabelFormat: ''};
    this._checkPeriod(periodSelected, formats);
    data.map((element: any) => {
      element.formattedxAxis = moment(element.timeStamp).format(formats.xFormat);
      element[graphConfig.baloonTextField] = moment(element.timeStamp).format('lll');
      element.labelFormat = formats.xLabelFormat;
    });
  }

  private _checkPeriod(periodSelection, formats) {
    switch (periodSelection.rangeType) {
      case RangeTypeEnum.DAY:
        formats.xFormat = 'HH:mm';
        formats.xLabelFormat = 'HH:mm';
        break;
      case RangeTypeEnum.WEEK:
        formats.xFormat = 'E/HH:mm';
        formats.xLabelFormat = 'E';
        break;
      case RangeTypeEnum.MONTH:
        formats.xFormat = 'D/HH:mm';
        formats.xLabelFormat = 'D';
        break;
      case RangeTypeEnum.YEAR:
        formats.xFormat = 'MM/D';
        formats.xLabelFormat = 'MM';
        break;
    }
  }

  createComparePeriodGraphFromPeriod(period, serialChart: ICloeChart) {
    this.measureService.getMeasuresForChannelUuid(serialChart.channelUuid, period)
      .subscribe((data: any) => {
        if (data.measures && data.measures.length > 0) {
          const graphConfig = this.serialChartService._createGraphConfigForPeriod(
            ChartAxis.LEFT_RIGHT,
            period,
            data
          );
          if (period.isFreeRange) {
            this.freeRangeDataAdapter(data.measures, graphConfig);
          } else {
            this.dataAdapter(data.measures, period, graphConfig);
          }
          serialChart.graphsConfig.push(graphConfig);
          const dataProvider = serialChart.dataProvider;
          serialChart.dataProvider = this.cloeChartService._mergeDataProvider(
            this.cloeChartService._id(graphConfig.id),
            dataProvider,
            data.measures,
            'formattedxAxis'
          );
          cloeSerialChartSubject$.next({
            action: CLOE_CHARTS_ACTIONS.UPDATE_DATA_PROVIDER
          });
        }
      });
  }
}
