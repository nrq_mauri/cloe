import { Injectable } from '@angular/core';
import { MeasureService } from '../measure-service/measure-service';
import { RangeTypeEnum } from '../../components/cloe-period-selector/enums/RangeTypeEnum';
import { remove, cloneDeep, slice, reverse, find } from 'lodash';
import { Subject } from 'rxjs';
import { EngFormatService } from '@cloe-core/@core/services/engFormat-service/engFormat-service';
import { PeriodTypeEnum } from '../../components/cloe-period-selector/enums/PeriodTypeEnum';
import { HEATMAP_CONFIG } from './cloe-heatMap-config';
import ECharts = echarts.ECharts;

import * as moment_ from 'moment';
import { CloeDateTimeUtils } from '@cloe-core/@core/@ui/components/cloe-date-time-picker/utils/CloeDateTimeUtils';
import { CloeChartService } from '../cloe-chart-service/cloe-chart-service';
import { StorageService } from '@cloe-core/@core/services/storage-service/storage.service';
const moment = moment_;

@Injectable()
export class CloeHeatMapService {
  heatMaps: HeatMap[] = [];
  chartEventSubject$ = new Subject();
  integrated: boolean;

  constructor(
    private measureService: MeasureService,
    private cloeChartService: CloeChartService,
    private engFormatService: EngFormatService
  ) {
  }

  createHeatMapFromChannelUuid(heatMap: HeatMap) {
    const heatMapOptions: any = cloneDeep(HEATMAP_CONFIG);
    heatMap.xAxisFormat = 'DD/MM/YYYY';
    heatMap.yAxisFormat = 'HH:mm';
    this.measureService.getMeasuresForChannelUuid(heatMap.uuidChannel, heatMap.periodSelection)
      .subscribe((data: any) => {
        heatMap.originalData = data;
        heatMap.summaryData = data.summary;
        heatMap.integrated = this.integrated;
        this._updateHeatMeasureField(heatMap);
      });
    return heatMap;
  }

  private _updateHeatMeasureField(heatMap: HeatMap) {
    const heatMapOptions: any = cloneDeep(HEATMAP_CONFIG);
    const data = heatMap.originalData;
    this.checkPeriod(heatMap, heatMapOptions);
    const summaryField = this.cloeChartService.getSummaryField(heatMap.integrated);
    const measureField = this.cloeChartService.getMeasureField(heatMap.integrated);
    const symbolField = this.cloeChartService.getSymbolField(heatMap.integrated);

    if (data.measures && data.measures.length > 0) {
      heatMap.summaryData = data[summaryField];
      heatMap.summaryData.dynamicSymbol = heatMap.summaryData[symbolField];
      heatMap.channelNameWithSymbol = `${heatMap.channelName} (${heatMap.summaryData.dynamicSymbol})`;
      data.measures.forEach((element: any) => {
        const xLabel = moment.utc(element.timeStamp).format(heatMap.xAxisFormat);
        const yLabel = moment.utc(element.timeStamp).format(heatMap.yAxisFormat);
        heatMapOptions.series[0].data.push([xLabel, yLabel, element[measureField]]);
      });
      heatMapOptions.visualMap.min = heatMap.summaryData.min;
      heatMapOptions.visualMap.max = heatMap.summaryData.max;
      heatMapOptions.visualMap.formatter = this.getFormatterVisualMapFunction(heatMap.summaryData.dynamicSymbol);
      heatMapOptions.tooltip.formatter = this.getFormatterTooltipFunction(heatMap.summaryData.dynamicSymbol);
      heatMap.chart.setOption(heatMapOptions);
    }
  }

  getIntegrated() {
    return this.integrated;
  }

  initIntegrated() {
    this.integrated = !!StorageService.getCurrentUser().cloeUser.useIntegratedValues;
  }

  integrateHeat(divId: string, integrated) {
    const heatMap = find(this.heatMaps, { divId });
    heatMap.integrated = integrated;
    this._updateHeatMeasureField(heatMap);
    this.fireHeatMapsUpdate();
  }

  private getFormatterTooltipFunction(symbol) {
    return (params, ticket, callback) => {
      return `${params.value[0]}, ${params.value[1]} - [${this.engFormatService.engFormat(params.value[2], 'default', symbol)} ]`;
    };
  }

  private getFormatterVisualMapFunction(symbol) {
    return (value) => {
      return this.engFormatService.engFormat(value, 'default', symbol);
    };
  }

  private checkPeriod(heatMap: HeatMap, options) {
    switch (heatMap.periodSelection.rangeType) {
      case RangeTypeEnum.DAY:
        heatMap.xAxisFormat = 'HH';
        heatMap.yAxisFormat = 'mm';
        break;
      case RangeTypeEnum.MONTH:
        if (heatMap.periodSelection.periodType === PeriodTypeEnum.DAY) {
          heatMap.xAxisFormat = 'WW';
          heatMap.yAxisFormat = 'dddd';
          const firstDayOfWeek = CloeDateTimeUtils.getFirstDayOfWeek();
          options.yAxis.data = reverse([
            ...slice(moment.weekdays(), firstDayOfWeek),
            ...slice(moment.weekdays(), 0, firstDayOfWeek)
          ]);
        }
        break;
      case RangeTypeEnum.YEAR:
        if (heatMap.periodSelection.periodType === PeriodTypeEnum.DAY) {
          heatMap.xAxisFormat = 'MM/YYYY';
          heatMap.yAxisFormat = 'DD';
        }
        break;
      default:
        heatMap.xAxisFormat = 'DD/MM/YYYY';
        heatMap.yAxisFormat = 'HH:mm';
    }
  }

  destroyChart(chart: ECharts) {
    chart.dispose();
  }

  addHeatMap(heatMap: HeatMap) {
    this.heatMaps = [...this.heatMaps, heatMap];
    this.fireHeatMapsUpdate();
  }

  removeHeatMapByDivId(divId: string) {
    remove(this.heatMaps, (heatMap) => {
      return heatMap.divId === divId;
    });
    this.heatMaps = [...this.heatMaps];
    this.fireHeatMapsUpdate();
  }

  removeHeatMapByReferUuid(uuidChannel: string) {
    remove(this.heatMaps, (heatMap) => {
      return heatMap.uuidChannel === uuidChannel;
    });
    this.heatMaps = [...this.heatMaps];
    this.fireHeatMapsUpdate();
  }

  private fireHeatMapsUpdate() {
    this.chartEventSubject$.next({
      action: HeatMapActionEnum.HEATMAPS_UPDATE,
      heatMaps: this.heatMaps
    });
  }

  clearHeatMaps() {
    remove(this.heatMaps);
  }
}

export interface HeatMap {
  divId: string;
  periodSelection: any;
  uuidChannel: string;
  channelName?: string;
  channelPath?: string;
  chart?: ECharts;
  xAxisFormat?: string;
  yAxisFormat?: string;
  summaryData?: any;
  originalData?: any;
  channelNameWithSymbol?: string;
  integrated?: boolean;
  integrable?: boolean;
}

export enum HeatMapActionEnum {
  HEATMAPS_UPDATE
}
