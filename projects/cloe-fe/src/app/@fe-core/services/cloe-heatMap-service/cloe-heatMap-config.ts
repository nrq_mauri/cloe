export const HEATMAP_CONFIG = {
  tooltip: {
    position: 'top'
  },
  xAxis: {
    type: 'category',
    splitArea: {
      show: true
    }
  },
  yAxis: {
    type: 'category',
    splitArea: {
      show: true
    }
  },
  visualMap: {
    min: 0,
    max: 1,
    calculable: true,
    orient: 'vertical',
    left: 'right',
    top: '8%',
    itemHeight: '420%',
    inRange: {
      color: ['#90EE90', '#00CC00', '#006400', '#FFFF00', '#FFB200', '#FF6600', '#FF0000', '#B20000']
    }
  },
  series: [{
    type: 'heatmap',
    data: [],
    itemStyle: {
      emphasis: {
        borderColor: '#333',
        borderWidth: 1
      }
    },
    progressive: 1000,
    animation: false
  }],
  useUTC: true
};
