import { NgModule } from '@angular/core';
import { UIRouterModule } from '@uirouter/angular';
import { PagesModule } from './pages/pages.module';
import { HttpClient } from '@angular/common/http';
import { TranslateLoader, TranslateModule } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';


import { AppComponent } from './app.component';
import { AgmCoreModule } from '@agm/core';
import { AppProperties } from './app.properties';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    UIRouterModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    AgmCoreModule.forRoot({
      apiKey: AppProperties.getApiKey('googleMaps')
    }),
    // Custom
    PagesModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
