import {FeTypesEnum} from '@cloe-core/@core/enums/FeTypesEnum';

export const environment = {
  production: true,
  envName: 'prod',
  feType: FeTypesEnum.FE,
  apiKeys: {
    googleMaps: 'AIzaSyBwFKA4VtbFxkM8Fo-FPio00p18W5C47LA'
  }
};
