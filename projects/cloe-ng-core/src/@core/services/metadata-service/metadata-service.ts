import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { of, Observable } from 'rxjs';
import { CloeSettings } from '../cloe.settings';

const NEW_METADATA_PATH = '/metadata';
const NEW_METADATA_API = CloeSettings.baseApiPath() + NEW_METADATA_PATH;
const NEW_METADATA_LIST_API = CloeSettings.baseApiPath() + NEW_METADATA_PATH + '/upsert/fromList';

const METADATA_TYPE_PATH = '/metadata-type';
const METADATA_TYPE_API = CloeSettings.baseApiPath() + METADATA_TYPE_PATH;

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class MetadataService {

  constructor(private http: HttpClient) {
  }

  upsertMetadataList(metadataToUpdate, metadataToDelete) {
    return this.http.post(
      NEW_METADATA_LIST_API,
      {
        metadataToUpdateList: metadataToUpdate,
        metadataToDeleteList: metadataToDelete
      }
    );
  }

  upsertMetadata(metadata) {
    if (!metadata.id) {
      return this.http.post(NEW_METADATA_API, metadata, httpOptions);
    }
    return this.http.put(NEW_METADATA_API, metadata, httpOptions);
  }

  getMetadataTypeEnum() {
    return this.http.get(METADATA_TYPE_API);
  }

  deleteMetadata(metadataId) {
    return this.http.delete(NEW_METADATA_API + '/' + metadataId)
      .pipe(
        catchError((err) => {
          return of(err);
        })
      );
  }

}
