import { Injectable } from '@angular/core';

export const TOKEN_STORAGE_KEY = 'access_token';
export const CURRENT_USER = 'current_user';
export const PERMISSIONS_KEY = 'permissions';
export const CURRENT_LANG = 'lang';
export const USER_BASE_INFO = 'user-base-info';

export const tokenGetter = () => {
  return localStorage.getItem(TOKEN_STORAGE_KEY);
};

@Injectable()
export class StorageService {
  constructor() {}

  static saveToken(resp) {
    localStorage.setItem(TOKEN_STORAGE_KEY, resp.id_token);
  }

  static getToken() {
    return tokenGetter();
  }

  static deleteToken() {
    localStorage.removeItem(TOKEN_STORAGE_KEY);
  }

  static savePermissions(permissions) {
    localStorage.setItem(PERMISSIONS_KEY, permissions);
  }

  static getPermissions() {
    const permissionsArrayString = localStorage.getItem(PERMISSIONS_KEY);
    if (permissionsArrayString) {
      return permissionsArrayString.split(',');
    }
    return [];
  }

  static deletePermissions() {
    localStorage.removeItem(PERMISSIONS_KEY);
  }

  static getCurrentUser() {
    const userJson = localStorage.getItem(CURRENT_USER);
    if (userJson) {
      return JSON.parse(userJson);
    }
    return undefined;
  }

  static setCurrentUser(user) {
    localStorage.setItem(CURRENT_USER, JSON.stringify(user));
  }

  static deleteCurrentUser() {
    localStorage.removeItem(CURRENT_USER);
  }

  static getCurrentLang() {
    return localStorage.getItem(CURRENT_LANG);
  }

  static setCurrentLang(lang) {
    localStorage.setItem(CURRENT_LANG, lang);
  }

  static deleteCurrentLang() {
    localStorage.removeItem(CURRENT_LANG);
  }

  static save(key, value) {
    localStorage.setItem(key, value);
  }

  static get(key) {
    localStorage.getItem(key);
  }

  static remove(key) {
    localStorage.removeItem(key);
  }

  static clear() {
    localStorage.clear();
  }

  static saveObject(key, object) {
    localStorage.setItem(key, JSON.stringify(object));
  }

  static getObject(key) {
    return JSON.parse(localStorage.getItem(key));
  }

  static removeObject(key) {
    localStorage.removeItem(key);
  }


}
