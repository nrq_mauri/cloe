import { CloeSearchService } from './cloe-search-service/cloe.search.service';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { JwtModule } from '@auth0/angular-jwt';
import { CloeSettings } from './cloe.settings';
import { StorageService } from './storage-service/storage.service';
import { AccountService } from './account-service/account.service';
import { CloeDeleteService } from './cloe-delete-service/cloe-delete.service';
import { CommonService } from './common-service/common-service';
import { CustomerService } from './customer-service/customer-service';
import { FileService } from './file-service/file-service';
import { LoginService } from './login-service/login.service';
import { MenuService } from './menu-service/menu.service';
import { ModalService } from './modal-service/modal-service';
import { SiteService } from './site-service/site.service';
import { ChannelService } from './channel-service/channel-service';
import { CloeItemsByNatureService } from './cloe-itemsByNatureExceptUUID-service/cloe-itemsByNature.service';
import { CloeThemeTemplateService } from './cloe-themeTemplate-service/cloe-themeTemplate.service';
import { DeviceService } from './device-service/device-service';
import { MetadataService } from './metadata-service/metadata-service';
import { SubjectsService } from './resize-service/subjects.service';
import { UnitService } from './unit-service/unit.service';
import { UserService } from './user-service/user-service';
import { ConsumptionService } from './consumption-service/consumption-service';
import { EngFormatService } from './engFormat-service/engFormat-service';
import { UserInitService } from './user-init-service/user-init-service';
import { CsvService } from './csv-service/csv.service';
import { JobService } from './job-service/job-service';
import { DynamicFormService } from './dynamic-form-service/dynamic-form-service';
import { CloeTreeFiltersService } from './cloe-tree-service/cloe-tree-filters.service';
import { CloeTreeChannelFiltersService } from './cloe-tree-service/cloe-tree-channel-filters.service';
import { CloeTreeSitesFiltersService } from './cloe-tree-service/cloe-tree-sites-filters.service';
import { ChannelTypeService } from './channelType-service/channelType-service';
import { DiagnosisService } from './diagnosis-service/diagnosis.service';
import { ClusterService } from './cluster-service/cluster-service';
import { CloeTreeNodeService } from './cloe-treeNode-service/cloe-tree-node.service';
import { CloeTreeService } from './cloe-tree-service/cloe-tree.service';
import { TagService } from './tag-service/tag-service';
import { NodeService } from './node-service/node.service';
import { ChannelRangeService } from './channel-range-service/channel-range-service';
import { CloeMathService } from './cloe-math-service/cloe.math.service';
import { PageHelpService } from './page-help-service/page-help-service';
import { UserTreeRootService } from './user-tree-root-service/user-tree-root-service';

const SERVICES = [
  AccountService,
  ChannelService,
  ChannelTypeService,
  CloeDeleteService,
  CloeItemsByNatureService,
  CloeSearchService,
  CloeThemeTemplateService,
  CommonService,
  ConsumptionService,
  CustomerService,
  DeviceService,
  FileService,
  LoginService,
  MenuService,
  MetadataService,
  ModalService,
  SubjectsService,
  SiteService,
  StorageService,
  UnitService,
  UserService,
  EngFormatService,
  UserInitService,
  CsvService,
  JobService,
  DynamicFormService,
  CloeTreeFiltersService,
  CloeTreeChannelFiltersService,
  CloeTreeSitesFiltersService,
  DiagnosisService,
  ClusterService,
  CloeTreeNodeService,
  CloeTreeService,
  TagService,
  NodeService,
  ChannelRangeService,
  CloeMathService,
  PageHelpService,
  UserTreeRootService,
];

@NgModule({
  imports: [
    HttpClientModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: StorageService.getToken,
        whitelistedDomains: [CloeSettings.getHostName()],
        blacklistedRoutes: ['']
      }
    })
  ],
  providers: [
    ...SERVICES,
  ],
})
export class CloeCoreServicesModule {
}
