import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CloeSettings } from '../cloe.settings';
import { Observable } from 'rxjs';
import {Customer} from '../../models/Customer';

const NEW_CUSTOMER_PATH = '/customers';
const NEW_CUSTOMER_API = CloeSettings.baseApiPath() + NEW_CUSTOMER_PATH;
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class CustomerService {
  constructor(private http: HttpClient) {
  }

  getCustomerByID(id): Observable<Customer> {
    return this.http.get<Customer>(NEW_CUSTOMER_API + '/' + id);
  }

  upsertCustomer(customer) {
    if (!customer.id) {
      return this.http.post(NEW_CUSTOMER_API, customer, httpOptions);
    }
    return this.http.put(NEW_CUSTOMER_API, customer, httpOptions);
  }

  getAllCustomers() {
    return this.http.get<Customer>(NEW_CUSTOMER_API);
  }
}
