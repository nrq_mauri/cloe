import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {CloeSettings} from '../cloe.settings';
import {MsContextEnum} from '../../enums/MsContextEnum';
import {Job} from '../../models/Job';
import {JobConfig} from '../../models/JobConfig';
import { Observable, Subject } from 'rxjs';

export const jobSubject$ = new Subject();

const NEW_JOB_PATH = '/schedule-jobs';
const NEW_JOB_API = CloeSettings.baseApiPath(MsContextEnum.JOBSCHEDULER) + NEW_JOB_PATH;
const EXECUTE_JOB_PATH = '/execute-job';
const EXECUTE_JOB_API = CloeSettings.baseApiPath(MsContextEnum.JOBSCHEDULER) + EXECUTE_JOB_PATH;
const DELETE_JOB_PATH = '/delete-job';
const DELETE_JOB_API = CloeSettings.baseApiPath(MsContextEnum.JOBSCHEDULER) + DELETE_JOB_PATH;
const NEW_JOB_CONFIG_PATH = '/job-configs';
const NEW_JOB_CONFIG_API = CloeSettings.baseApiPath(MsContextEnum.JOBSCHEDULER) + NEW_JOB_CONFIG_PATH;

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class JobService {

  constructor(private http: HttpClient) {
  }

  upsertJob(job) {
    console.log(job);
    if (!job.id) {
      return this.http.post(NEW_JOB_API, job, httpOptions);
    }
    return this.http.put(NEW_JOB_API, job, httpOptions);
  }

  deleteJob(job) {
    return this.http.post(DELETE_JOB_API, job);
  }

  executeJobByID(id) {
    return this.http.get(EXECUTE_JOB_API + '/' + id);
  }

  getAllJobs() {
    return this.http.get<Job>(NEW_JOB_API);
  }

  getAllJobConfigs() {
    return this.http.get<JobConfig[]>(NEW_JOB_CONFIG_API);
  }

}

export enum JobActionsEnum {
  EXECUTE_JOB = 'execute-job',
  DELETE_JOB = 'delete-job'
}
