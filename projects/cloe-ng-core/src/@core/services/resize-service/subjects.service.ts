import {Injectable} from '@angular/core';
import {ReplaySubject, Subject} from 'rxjs';
import {BehaviorSubject} from "rxjs/internal/BehaviorSubject";

/**
 * Service for informing about resizing of layout
 */
@Injectable()
export class SubjectsService {

  resizeInformer$ = new Subject();

  httpResquestPendingInformer$ = new Subject();

  homeSiteLoadedInformer$ = new ReplaySubject();

  sibarCollapseInformer$ = new BehaviorSubject(null);

  constructor() {
  }

}
