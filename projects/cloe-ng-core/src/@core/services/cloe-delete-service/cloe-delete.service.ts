import {Injectable} from '@angular/core';
import {CloeSettings} from '../cloe.settings';
import {HttpClient} from '@angular/common/http';
import {of, Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';

const DELETE_PATH = '/_delete';

@Injectable()
export class CloeDeleteService {
  constructor(private http: HttpClient) {
  }

  deleteByUuid(natureType: String, uuid) {
    return this.http.delete(
      CloeSettings.getBaseApiPath(natureType, DELETE_PATH) +
      '/' + natureType +
      '/' + uuid
    )
      .pipe(
        catchError((err) => {
          console.log(err);
          return of(err);
        })
      );
  }
}
