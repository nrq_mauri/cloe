import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { CloeSettings } from '../cloe.settings';
import {StorageService} from '../storage-service/storage.service';

const ACCOUNT_PATH = '/account';
const ACCOUNT_API = CloeSettings.baseApiPath() + ACCOUNT_PATH;
const CHANGE_PW__API = CloeSettings.baseApiPath() + ACCOUNT_PATH + '/change-password';

@Injectable()
export class AccountService {
  constructor(private http: HttpClient) {}

  getAccount() {
    const currentUser = StorageService.getCurrentUser();
    return new Observable(observer => {
      if (currentUser) {
        observer.next(currentUser);
        observer.complete();
      } else {
        this.http.get(ACCOUNT_API).subscribe(account => {
          observer.next(account);
          StorageService.setCurrentUser(account);
          observer.complete();
        });
      }
    });
  }

  changePassword (changePWDTO) {
    return this.http.post(CHANGE_PW__API, changePWDTO);
  }
}
