import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CloeSettings} from '../cloe.settings';
import {catchError} from 'rxjs/operators';
// noinspection ES6UnusedImports
import {of, Observable, Subscription} from 'rxjs';

const NODE_PATH = '/nodes';

@Injectable()
export class NodeService {
  constructor(private http: HttpClient) {
  }

  getNodesByReferUuidForUser(
    natureType,
    userUuid,
    referUuid: string,
    isCluster?: boolean,
    isSystemTree?: boolean,
    onlyAllowed?: boolean
  ) {
    return this.http.get(
      CloeSettings.getBaseApiPath(natureType, NODE_PATH) +
      '/' + natureType +
      '/' + userUuid +
      '/' + referUuid,
      {
        params: {
          isSystemTree: (!!isSystemTree).toString(),
          isCluster: (!!isCluster).toString(),
          onlyAllowed: (!!onlyAllowed).toString()
        }
      })
      .pipe(
        catchError((err) => {
          console.log(err);
          return of(err);
        })
      );
  }
}
