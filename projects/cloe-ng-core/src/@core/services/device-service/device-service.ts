import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {CloeSettings} from '../cloe.settings';
import {Observable} from 'rxjs';
import {MsContextEnum} from '../../enums/MsContextEnum';

const NEW_DEVICE_PATH = '/devices';
const NEW_DEVICE_API = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + NEW_DEVICE_PATH;
const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class DeviceService {
  constructor(private http: HttpClient) {
  }

  upsertDevice(device) {
    if (!device.id) {
      return this.http.post(NEW_DEVICE_API, device, httpOptions);
    }
    return this.http.put(NEW_DEVICE_API, device, httpOptions);
  }
}
