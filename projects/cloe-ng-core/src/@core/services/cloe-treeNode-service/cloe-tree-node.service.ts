import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CloeSettings } from '../cloe.settings';
import { catchError } from 'rxjs/operators';
// noinspection ES6UnusedImports
import { of, Observable, Subscription } from 'rxjs';
import { cloneDeep, sortBy, forEach, map } from 'lodash';
import { MsContextEnum } from '../../enums/MsContextEnum';
import { CloeTreeService } from '../cloe-tree-service/cloe-tree.service';

const TREE_NODE_PATH = '/cloeTreeNode';
const ALL_TREE_NODE_API = TREE_NODE_PATH + '/AllTree';
const TREE_NODE_FROM_ROOT = TREE_NODE_PATH + '/byRoot';
const TREE_NODE_PATH_FULL = TREE_NODE_PATH + '/full';
const TREE_NODE_PATH_RESTRICTED = TREE_NODE_PATH + '/restricted';
const UPSERT_CLOE_TREE_NODE_LIST_API = TREE_NODE_PATH + '/list/upsertCloeTreeNode';
const UPSERT_CLOE_TREE_NODE_API = TREE_NODE_PATH + '/upsertCloeTreeNode';
const DELETE_CLOE_TREE_NODE_API = TREE_NODE_PATH + '/deleteCloeTreeNode';

const USER_NODE_PATH = '/user-objects';
const SAVE_ALL_USER_NODE_API = USER_NODE_PATH + '/saveAll';

@Injectable()
export class CloeTreeNodeService {
  constructor(private http: HttpClient) {
  }

  getSitesTreeForUser(user, sitesTree) {
    if (user.authorities.includes('ROLE_SUPER_USER')) {
      return this.getAllTreeNodeFromTree(
        'SITE',
        sitesTree.tree.uuid,
        user.cloeUser.uuid,
        true,
        true,
        true
      );
    } else {
      return this.getTreeNodeByReferUuid(
        'SITE',
        user,
        true,
        'SITE',
        true,
        false,
        true,
        true
      );
    }
  }

  getAllTreeNodeFromTree(
    natureType,
    treeUuid: any,
    userUuid,
    metadataIncluded?: boolean,
    emptyFolderExcluded?: boolean,
    liteNodes?: boolean
  ) {
    return this.http.get(
      CloeSettings.getBaseApiPath(natureType, ALL_TREE_NODE_API) +
      '/' + natureType +
      '/' + userUuid,
      {
        params: {
          metadataIncluded: (!!metadataIncluded).toString(),
          emptyFolderIncluded: (!!emptyFolderExcluded).toString(),
          liteNodes: (!!liteNodes).toString(),
          treeUuid: treeUuid
        }
      })
      .pipe(
        catchError((err) => {
          console.log(err);
          return of(err);
        })
      );
  }

  getTreeNodeFromRoot(
    natureType,
    root: any,
    userUuid,
    resitricted?: boolean,
    metadataIncluded?: boolean,
    emptyFolderExcluded?: boolean
  ) {
    return this.http.post(
      CloeSettings.getBaseApiPath(natureType, TREE_NODE_FROM_ROOT) +
      '/' + userUuid,
      root,
      {
        params: {
          metadataIncluded: (!!metadataIncluded).toString(),
          onlyAllowed: (!!resitricted).toString(),
          emptyFolderIncluded: (!!emptyFolderExcluded).toString()
        }
      })
      .pipe(
        catchError((err) => {
          console.log(err);
          return of(err);
        })
      );
  }

  saveAllUserNodes(userNodeList: any[]) {
    return this.http.post(
      CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + SAVE_ALL_USER_NODE_API,
      userNodeList
    );
  }

  saveUserNode(userUuid, userNode) {
    return this.http.post(
      CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT)
      + USER_NODE_PATH,
      this.buildUserNodeDTO(userUuid, userNode))
      .subscribe();
  }

  updateUserNode(userUuid, selectedNode) {
    return this.http.put(
      CloeSettings.getBaseApiPath(selectedNode.nodeNature, USER_NODE_PATH),
      this.buildUserNodeDTO(userUuid, selectedNode.userNode))
      .subscribe();
  }

  buildUserNodeDTO(userUuid, userNode) {
    const userNodeDTO: { [k: string]: any } = {};
    userNodeDTO.id = userNode.id;
    userNodeDTO.objectUuid = userNode.objectUuid;
    userNodeDTO.userUuid = userUuid;
    userNodeDTO.status = userNode.status;
    userNodeDTO.validEndDate = userNode.validEndDate;
    userNodeDTO.validStartDate = userNode.validStartDate;
    return userNodeDTO;
  }

  createUserNode(referUuid, userUuid, status, validStartDate, validEndDate) {
    const userNode: { [k: string]: any } = {};
    userNode.objectUuid = referUuid;
    userNode.userUuid = userUuid;
    userNode.status = status;
    userNode.validStartDate = validStartDate;
    userNode.validEndDate = validEndDate;
    this.saveUserNode(userUuid, userNode);
  }

  getTreeNodeByReferUuid(
    natureType,
    user,
    resitricted: boolean,
    referUuid: string,
    metadataIncluded?: boolean,
    isCluster?: boolean,
    emptyFolderExcluded?: boolean,
    liteNodes?: boolean
  ) {
    return this.http.get(
      CloeSettings.getBaseApiPath(natureType, resitricted ? TREE_NODE_PATH_RESTRICTED : TREE_NODE_PATH_FULL) +
      '/' + natureType +
      '/' + user.cloeUser.uuid +
      '/' + referUuid,
      {
        params: {
          metadataIncluded: (!!metadataIncluded).toString(),
          isCluster: (!!isCluster).toString(),
          emptyFolderExcluded: (!!emptyFolderExcluded).toString(),
          liteNodes: (!!liteNodes).toString()
        }
      })
      .pipe(
        catchError((err) => {
          console.log(err);
          return of(err);
        })
      );
  }

  saveTreeNodesList(updateTreeNodeDTOList, deleteTreeNodeDTOList, tree) {
    return this.http.post(
      CloeSettings.getBaseApiPath(tree.nodeNature, UPSERT_CLOE_TREE_NODE_LIST_API),
      this.buildMultiTreeNodeDTO(updateTreeNodeDTOList, deleteTreeNodeDTOList, tree)
    )
      .pipe(
        catchError((err) => {
          console.log(err);
          return of(err);
        })
      );
  }

  saveTreeNodes(treeNodes, tree, nodeIdsToDelete, referUuid) {
    return this.http.post(
      CloeSettings.getBaseApiPath(tree.nodeNature, UPSERT_CLOE_TREE_NODE_API),
      this.buildTreeNodeDTO(treeNodes, tree, nodeIdsToDelete, referUuid)
    )
      .pipe(
        catchError((err) => {
          console.log(err);
          return of(err);
        })
      );
  }

  deleteAllSubTree(root, tree, referUuid) {
    return this.http.post(
      CloeSettings.getBaseApiPath(tree.nodeNature, DELETE_CLOE_TREE_NODE_API),
      this.buildDeleteTreeNodeDTO(root, tree, referUuid)
    )
      .pipe(
        catchError((err) => {
          console.log(err);
          return of(err);
        })
      );
  }

  buildMultiTreeNodeDTO(updateTreeNodeDTOList, deleteTreeNodeDTOList, tree) {
    const deleteTreeNodeList = [];
    const upsertTreeNodeList = [];
    const completeTreeNodeList = sortBy(
      [
        ...updateTreeNodeDTOList,
        ...map(deleteTreeNodeDTOList, (deleteTreeNode) => {
          const treeNode = deleteTreeNode;
          treeNode.toDelete = true;
          return treeNode;
        })
      ],
      (treeNode) => treeNode.treeNodes[0].lft,
      ['asc']
    );
    forEach(completeTreeNodeList, (treeNodeDTO: any, index) => {
      const currentTreeNodeDTO = treeNodeDTO.toDelete ?
        this.buildDeleteTreeNodeDTO(treeNodeDTO.treeNodes[0], tree, treeNodeDTO.referUuid) :
        this.buildTreeNodeDTO(treeNodeDTO.treeNodes, tree, treeNodeDTO.nodeIdsToDelete, treeNodeDTO.referUuid);
      const succTreeNodeDTO: any = completeTreeNodeList[ index + 1 ];
      if (succTreeNodeDTO) {
        succTreeNodeDTO.treeNodes[0].lft =
          succTreeNodeDTO.treeNodes[0].lft + currentTreeNodeDTO.rgtDelta;
        succTreeNodeDTO.treeNodes[0].rgt =
          succTreeNodeDTO.treeNodes[0].rgt + currentTreeNodeDTO.rgtDelta;
      }
      treeNodeDTO.toDelete ? deleteTreeNodeList.push(currentTreeNodeDTO) : upsertTreeNodeList.push(currentTreeNodeDTO);
    });
    return { updateTreeNodeDTO: upsertTreeNodeList, deleteTreeNodeDTO: deleteTreeNodeList };
  }

  buildDeleteTreeNodeDTO(root, tree, referUuid) {
    const treeNodeDTO: { [k: string]: any } = {};
    const oldRoot = cloneDeep(root);
    treeNodeDTO.oldRgt = oldRoot.rgt;
    treeNodeDTO.rgtDelta = oldRoot.lft - (treeNodeDTO.oldRgt + 1);
    treeNodeDTO.treeNode = root;
    treeNodeDTO.treeNode.rootReferUuid = referUuid;
    treeNodeDTO.tree = tree;
    const nodeIdsToDelete = [];
    this.getAllIds(root, nodeIdsToDelete);
    treeNodeDTO.nodeToDeleteList = nodeIdsToDelete;
    return treeNodeDTO;
  }

  private getAllIds(element, result) {
    result.push(element.id);
    if (element.children) {
      element.children.forEach(child => {
        this.getAllIds(child, result);
      });
    }
  }

  buildTreeNodeDTO(treeNodes, tree, nodeIdsToDelete, referUuid) {
    const treeNodeDTO: { [k: string]: any } = {};
    const nodes = cloneDeep(treeNodes);
    const oldRoot = cloneDeep(nodes[0]);
    this.buildNestedNode(nodes[0], oldRoot.lft ? nodes[0].lft - 1 : 1);
    treeNodeDTO.oldRgt = oldRoot.rgt ? oldRoot.rgt : 1;
    treeNodeDTO.rgtDelta = nodes[0].rgt - treeNodeDTO.oldRgt;
    treeNodeDTO.treeNode = nodes[0];
    treeNodeDTO.treeNode.rootReferUuid = referUuid;
    treeNodeDTO.tree = tree;
    treeNodeDTO.nodeToDeleteList = nodeIdsToDelete;
    return treeNodeDTO;
  }

  buildNestedNode(node, previousLeft) {
    delete node.referObject;
    if (node.isNew) {
      delete node.id;
      delete node.uuid;
    }
    node.lft = previousLeft + 1;

    let lastRight = node.lft;
    if (node.children) {
      node.children.forEach((element) => {
        lastRight = this.buildNestedNode(element, lastRight);
      });
    }
    node.rgt = lastRight + 1;
    return node.rgt;
  }

  findFirstLeafUuid(children) {
    if (children && children.length > 0) {
      for (let i = 0; i < children.length; i++) {
        const node = children[i];
        if (node.nodeCategory === 'LEAF') {
          return node.referUuid;
        } else {
          return this.findFirstLeafUuid(node.children);
        }
      }
    }
    return null;
  }

  getLeafsFromTree(tree, result = []) {
    tree.children.forEach(node => {
      if (node.children && node.children.length > 0) {
        result = this.getLeafsFromTree(node, result);
      } else if (node.nodeCategory === 'LEAF') {
        result.push(node);
      }
    });
    return result;
  }
}
