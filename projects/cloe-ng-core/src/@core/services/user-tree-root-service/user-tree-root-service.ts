import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CloeSettings } from '../cloe.settings';
import { MsContextEnum } from '../../enums/MsContextEnum';
import { Observable } from 'rxjs';

const USER_TREE_ROOT = '/user-tree-roots';
const UPSERT_USER_TREE_ROOT_API = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + USER_TREE_ROOT + '/upsert';


@Injectable()
export class UserTreeRootService {

  constructor(
    private http: HttpClient) {
  }

  upsertUserTreeRoot(userTreeRootUpsert) {
    return this.http.post(UPSERT_USER_TREE_ROOT_API, userTreeRootUpsert);
  }
}
