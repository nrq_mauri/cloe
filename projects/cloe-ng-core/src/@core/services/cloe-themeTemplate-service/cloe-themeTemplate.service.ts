import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {of, Observable} from 'rxjs';
import {CloeSettings} from '../cloe.settings';
const UPSERT_TEMPLATE_PATH = '/theme-templates';

@Injectable()
export class CloeThemeTemplateService {
  constructor(private http: HttpClient) {
  }

  saveThemeTemplate(themeTemplate) {
    if (!themeTemplate.id) {
      return this.http.post(
        CloeSettings.getBaseApiPath('THEME_TEMPLATE', UPSERT_TEMPLATE_PATH),
        themeTemplate)
        .pipe(
          catchError( (err) => {
            console.log(err);
            return of(err);
          })
        );
    } else {
      return this.http.put(
        CloeSettings.getBaseApiPath('THEME_TEMPLATE', UPSERT_TEMPLATE_PATH),
        themeTemplate)
        .pipe(
          catchError( (err) => {
            console.log(err);
            return of(err);
          })
        );
    }
  }

  getThemeTemplates() {
    return this.http.get(CloeSettings.getBaseApiPath('THEME_TEMPLATE', UPSERT_TEMPLATE_PATH))
      .pipe(
        catchError( (err) => {
          console.log(err);
          return of(err);
        })
      );
  }
}
