import { Injectable } from '@angular/core';
import { CloeSettings } from '../cloe.settings';
import { HttpClient } from '@angular/common/http';
import { of, Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { Subject } from 'rxjs/internal/Subject';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { formulasGroup } from './build-in-formulas';
import { constantsGroup } from './build-in-constants';
import { IFormulaEvent, IFormulaPickerState, MathArgumentTypeEnum } from './math.interfaces';
import { MsContextEnum } from '../../enums/MsContextEnum';

export const formulaButtonSubject$ = new Subject<IFormulaEvent>();

export const formulaBehaviorSubject$ = new BehaviorSubject<IFormulaPickerState>({
  fetched: false,
  groups: {
    [formulasGroup.name]: formulasGroup,
    [constantsGroup.name]: constantsGroup
  }
});

@Injectable()
export class CloeMathService {
  constructor(private http: HttpClient) {
  }

  private BASE_PATH = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT);
  private USER_DEFINED_FUNCTION = this.BASE_PATH + '/math-functions';
  private USER_DEFINED_ARGUMENT = this.BASE_PATH + '/math-arguments';
  private CHECK_FUNCTION = this.BASE_PATH + '/math/check-function';
  private CHECK_EXPRESSION = this.BASE_PATH + '/math/check-expression';
  private formModelMap = {
    [MathArgumentTypeEnum.FUNCTION]: 'form-formula-insert',
    [MathArgumentTypeEnum.CONSTANT]: 'form-constant-insert',
    [MathArgumentTypeEnum.CHANNEL]: 'form-channel-insert'
  };

  public getFunctions() {
    return this.http.get(this.USER_DEFINED_FUNCTION);
  }

  public getArguments() {
    return this.http.get(this.USER_DEFINED_ARGUMENT);
  }

  public getFormModel(argumentType: MathArgumentTypeEnum) {
    return this.formModelMap[argumentType];
  }

  public checkFunction(functionDTO) {
    return this.http.post(this.CHECK_FUNCTION, functionDTO);
  }

  public checkExpression(expressionDTO) {
    return this.http.post(this.CHECK_EXPRESSION, expressionDTO);
  }


}
