import { formulasGroup } from './build-in-formulas';
import { BehaviorSubject } from 'rxjs/internal/BehaviorSubject';
import { constantsGroup } from './build-in-constants';

export enum FormulaButtonAction {
  INSERT = 'INSERT',
  EDIT = 'EDIT',
  ADD_NEW = 'ADD_NEW',
  REMOVE = 'REMOVE'
}

export enum MathArgumentTypeEnum {
  CONSTANT = 'CONSTANT',
  CHANNEL = 'CHANNEL',
  FUNCTION = 'FUNCTION'
}

export interface IFormulaEvent {
  action: FormulaButtonAction;
  origin?: string;
  formModel?: string;
  data: any;
}

export interface IFormulaPickerGroup {
  name: string;
  editable?: boolean;
  addButton?: boolean;
  argumentType?: MathArgumentTypeEnum;
  items: Array<any>;
}

export interface IFormulaPickerState {
  fetched: boolean;
  groups: {
    [groupName: string]: IFormulaPickerGroup
  };
}

