import { IFormulaPickerGroup, MathArgumentTypeEnum } from './math.interfaces';

export const formulasGroup: IFormulaPickerGroup = {
  name: 'formulas',
  items: [
    {
      name: '+',
      description: 'Addition',
      example: 'a + b'
    },
    {
      name: '-',
      description: 'Subtraction',
      example: 'a - b'
    },
    {
      name: '*',
      description: 'Multiplication',
      example: 'a * b'
    },
    {
      name: '/',
      description: 'Division',
      example: 'a / b'
    },
    {
      name: '^',
      description: 'Exponentiation',
      example: 'a ^ b'
    },
    {
      name: '!',
      description: 'Factorial',
      example: 'n!'
    },
    {
      name: '#',
      description: 'Modulo function',
      example: 'a # b'
    },
    {
      name: '&',
      description: 'Logical conjunction (AND)',
      example: 'p & q'
    },
    {
      name: '&&',
      description: 'Logical conjunction (AND)',
      example: 'p && q'
    },
    {
      name: '/\\',
      description: 'Logical conjunction (AND)',
      example: 'p /\\ q'
    },
    {
      name: '~&',
      description: 'NAND - Sheffer stroke',
      example: 'p ~& q'
    },
    {
      name: '~&&',
      description: 'NAND - Sheffer stroke',
      example: 'p ~&& q'
    },
    {
      name: '~/\\',
      description: 'NAND - Sheffer stroke',
      example: 'p ~/\\ q'
    },
    {
      name: '|',
      description: 'Logical disjunction (OR)',
      example: 'p | q'
    },
    {
      name: '||',
      description: 'Logical disjunction (OR)',
      example: 'p || q'
    },
    {
      name: '\\/',
      description: 'Logical disjunction (OR)',
      example: 'p \\/ q'
    },
    {
      name: '~|',
      description: 'Logical NOR',
      example: 'p ~| q'
    },
    {
      name: '~||',
      description: 'Logical NOR',
      example: 'p ~|| q'
    },
    {
      name: '~\\/',
      description: 'Logical NOR',
      example: 'p ~\\/ q'
    },
    {
      name: '(+)',
      description: 'Exclusive or (XOR)',
      example: 'p (+) q'
    },
    {
      name: '-->',
      description: 'Implication (IMP)',
      example: 'p --> q'
    },
    {
      name: '<--',
      description: 'Converse implication (CIMP)',
      example: 'p <-- q'
    },
    {
      name: '-/>',
      description: 'Material nonimplication (NIMP)',
      example: 'p -/> q'
    },
    {
      name: '</-',
      description: 'Converse nonimplication (CNIMP)',
      example: 'p </- q'
    },
    {
      name: '<->',
      description: 'Logical biconditional (EQV)',
      example: 'p <-> q'
    },
    {
      name: '~',
      description: 'Negation',
      example: '~p'
    },
    {
      name: '=',
      description: 'Equality',
      example: 'a = b'
    },
    {
      name: 'Err:520',
      description: 'Equality',
      example: 'a == b'
    },
    {
      name: '<>',
      description: 'Inequation',
      example: 'a <> b'
    },
    {
      name: '~=',
      description: 'Inequation',
      example: 'a ~= b'
    },
    {
      name: '!=',
      description: 'Inequation',
      example: 'a != b'
    },
    {
      name: '<',
      description: 'Lower than',
      example: 'a < b'
    },
    {
      name: '>',
      description: 'Greater than',
      example: 'a > b'
    },
    {
      name: '<=',
      description: 'Lower or equal',
      example: 'a <= b'
    },
    {
      name: '>=',
      description: 'Greater or equal',
      example: 'a >= b'
    },
    {
      name: 'sin',
      description: 'Trigonometric sine function',
      example: 'sin(x)'
    },
    {
      name: 'cos',
      description: 'Trigonometric cosine function',
      example: 'cos(x)'
    },
    {
      name: 'tan',
      description: 'Trigonometric tangent function',
      example: 'tan(x)'
    },
    {
      name: 'tg',
      description: 'Trigonometric tangent function',
      example: 'tg(x)'
    },
    {
      name: 'ctan',
      description: 'Trigonometric cotangent function',
      example: 'ctan(x)'
    },
    {
      name: 'ctg',
      description: 'Trigonometric cotangent function',
      example: 'ctg(x)'
    },
    {
      name: 'cot',
      description: 'Trigonometric cotangent function',
      example: 'cot(x)'
    },
    {
      name: 'sec',
      description: 'Trigonometric secant function',
      example: 'sec(x)'
    },
    {
      name: 'cosec',
      description: 'Trigonometric cosecant function',
      example: 'cosec(x)'
    },
    {
      name: 'csc',
      description: 'Trigonometric cosecant function',
      example: 'csc(x)'
    },
    {
      name: 'asin',
      description: 'Inverse trigonometric sine function',
      example: 'asin(x)'
    },
    {
      name: 'arsin',
      description: 'Inverse trigonometric sine function',
      example: 'arsin(x)'
    },
    {
      name: 'arcsin',
      description: 'Inverse trigonometric sine function',
      example: 'arcsin(x)'
    },
    {
      name: 'acos',
      description: 'Inverse trigonometric cosine function',
      example: 'acos(x)'
    },
    {
      name: 'arcos',
      description: 'Inverse trigonometric cosine function',
      example: 'arcos(x)'
    },
    {
      name: 'arccos',
      description: 'Inverse trigonometric cosine function',
      example: 'arccos(x)'
    },
    {
      name: 'atan',
      description: 'Inverse trigonometric tangent function',
      example: 'atan(x)'
    },
    {
      name: 'arctan',
      description: 'Inverse trigonometric tangent function',
      example: 'arctan(x)'
    },
    {
      name: 'atg',
      description: 'Inverse trigonometric tangent function',
      example: 'atg(x)'
    },
    {
      name: 'arctg',
      description: 'Inverse trigonometric tangent function',
      example: 'arctg(x)'
    },
    {
      name: 'actan',
      description: 'Inverse trigonometric cotangent function',
      example: 'actan(x)'
    },
    {
      name: 'arcctan',
      description: 'Inverse trigonometric cotangent function',
      example: 'arcctan(x)'
    },
    {
      name: 'actg',
      description: 'Inverse trigonometric cotangent function',
      example: 'actg(x)'
    },
    {
      name: 'arcctg',
      description: 'Inverse trigonometric cotangent function',
      example: 'arcctg(x)'
    },
    {
      name: 'acot',
      description: 'Inverse trigonometric cotangent function',
      example: 'acot(x)'
    },
    {
      name: 'arccot',
      description: 'Inverse trigonometric cotangent function',
      example: 'arccot(x)'
    },
    {
      name: 'ln',
      description: 'Natural logarithm function (base e)',
      example: 'ln(x)'
    },
    {
      name: 'log2',
      description: 'Binary logarithm function (base 2)',
      example: 'log2(x)'
    },
    {
      name: 'log10',
      description: 'Common logarithm function (base 10)',
      example: 'log10(x)'
    },
    {
      name: 'rad',
      description: 'Degrees to radians function',
      example: 'rad(x)'
    },
    {
      name: 'exp',
      description: 'Exponential function',
      example: 'exp(x)'
    },
    {
      name: 'sqrt',
      description: 'Squre root function',
      example: 'sqrt(x)'
    },
    {
      name: 'sinh',
      description: 'Hyperbolic sine function',
      example: 'sinh(x)'
    },
    {
      name: 'cosh',
      description: 'Hyperbolic cosine function',
      example: 'cosh(x)'
    },
    {
      name: 'tanh',
      description: 'Hyperbolic tangent function',
      example: 'tanh(x)'
    },
    {
      name: 'tgh',
      description: 'Hyperbolic tangent function',
      example: 'tgh(x)'
    },
    {
      name: 'ctanh',
      description: 'Hyperbolic cotangent function',
      example: 'ctanh(x)'
    },
    {
      name: 'coth',
      description: 'Hyperbolic cotangent function',
      example: 'coth(x)'
    },
    {
      name: 'ctgh',
      description: 'Hyperbolic cotangent function',
      example: 'ctgh(x)'
    },
    {
      name: 'sech',
      description: 'Hyperbolic secant function',
      example: 'sech(x)'
    },
    {
      name: 'csch',
      description: 'Hyperbolic cosecant function',
      example: 'csch(x)'
    },
    {
      name: 'cosech',
      description: 'Hyperbolic cosecant function',
      example: 'cosech(x)'
    },
    {
      name: 'deg',
      description: 'Radians to degrees function',
      example: 'deg(x)'
    },
    {
      name: 'abs',
      description: 'Absolut value function',
      example: 'abs(x)'
    },
    {
      name: 'sgn',
      description: 'Signum function',
      example: 'sgn(x)'
    },
    {
      name: 'floor',
      description: 'Floor function',
      example: 'floor(x)'
    },
    {
      name: 'ceil',
      description: 'Ceiling function',
      example: 'ceil(x)'
    },
    {
      name: 'not',
      description: 'Negation function',
      example: 'not(x)'
    },
    {
      name: 'asinh',
      description: 'Inverse hyperbolic sine function',
      example: 'asinh(x)'
    },
    {
      name: 'arsinh',
      description: 'Inverse hyperbolic sine function',
      example: 'arsinh(x)'
    },
    {
      name: 'arcsinh',
      description: 'Inverse hyperbolic sine function',
      example: 'arcsinh(x)'
    },
    {
      name: 'acosh',
      description: 'Inverse hyperbolic cosine function',
      example: 'acosh(x)'
    },
    {
      name: 'arcosh',
      description: 'Inverse hyperbolic cosine function',
      example: 'arcosh(x)'
    },
    {
      name: 'arccosh',
      description: 'Inverse hyperbolic cosine function',
      example: 'arccosh(x)'
    },
    {
      name: 'atanh',
      description: 'Inverse hyperbolic tangent function',
      example: 'atanh(x)'
    },
    {
      name: 'arctanh',
      description: 'Inverse hyperbolic tangent function',
      example: 'arctanh(x)'
    },
    {
      name: 'atgh',
      description: 'Inverse hyperbolic tangent function',
      example: 'atgh(x)'
    },
    {
      name: 'arctgh',
      description: 'Inverse hyperbolic tangent function',
      example: 'arctgh(x)'
    },
    {
      name: 'actanh',
      description: 'Inverse hyperbolic cotangent function',
      example: 'actanh(x)'
    },
    {
      name: 'arcctanh',
      description: 'Inverse hyperbolic cotangent function',
      example: 'arcctanh(x)'
    },
    {
      name: 'acoth',
      description: 'Inverse hyperbolic cotangent function',
      example: 'acoth(x)'
    },
    {
      name: 'arcoth',
      description: 'Inverse hyperbolic cotangent function',
      example: 'arcoth(x)'
    },
    {
      name: 'arccoth',
      description: 'Inverse hyperbolic cotangent function',
      example: 'arccoth(x)'
    },
    {
      name: 'actgh',
      description: 'Inverse hyperbolic cotangent function',
      example: 'actgh(x)'
    },
    {
      name: 'arcctgh',
      description: 'Inverse hyperbolic cotangent function',
      example: 'arcctgh(x)'
    },
    {
      name: 'asech',
      description: 'Inverse hyperbolic secant function',
      example: 'asech(x)'
    },
    {
      name: 'arsech',
      description: 'Inverse hyperbolic secant function',
      example: 'arsech(x)'
    },
    {
      name: 'arcsech',
      description: 'Inverse hyperbolic secant function',
      example: 'arcsech(x)'
    },
    {
      name: 'acsch',
      description: 'Inverse hyperbolic cosecant function',
      example: 'acsch(x)'
    },
    {
      name: 'arcsch',
      description: 'Inverse hyperbolic cosecant function',
      example: 'arcsch(x)'
    },
    {
      name: 'arccsch',
      description: 'Inverse hyperbolic cosecant function',
      example: 'arccsch(x)'
    },
    {
      name: 'acosech',
      description: 'Inverse hyperbolic cosecant function',
      example: 'acosech(x)'
    },
    {
      name: 'arcosech',
      description: 'Inverse hyperbolic cosecant function',
      example: 'arcosech(x)'
    },
    {
      name: 'arccosech',
      description: 'Inverse hyperbolic cosecant function',
      example: 'arccosech(x)'
    },
    {
      name: 'sinc',
      description: 'Sinc function (normalized)',
      example: 'sinc(x)'
    }
  ]
};
