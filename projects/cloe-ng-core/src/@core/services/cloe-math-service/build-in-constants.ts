import { IFormulaPickerGroup, MathArgumentTypeEnum } from './math.interfaces';

export const constantsGroup: IFormulaPickerGroup = {
  name: `constants`,
  items: [
    {
      name: `pi`,
      description: `Pi, Archimedes' constant or Ludolph's number`,
      example: `2*pi`
    },
    {
      name: `e`,
      description: `Napier's constant, or Euler's number, base of Natural logarithm`,
      example: `e*3`
    },
    {
      name: `[gam]`,
      description: `Euler-Mascheroni constant`,
      example: `2*[gam]`
    },
    {
      name: `[phi]`,
      description: `Golden ratio`,
      example: `[phi]*3`
    },
    {
      name: `[PN]`,
      description: `Plastic constant`,
      example: `2*[PN]`
    },
    {
      name: `[B*]`,
      description: `Embree-Trefethen constant`,
      example: `[B*]*3`
    },
    {
      name: `[F'd]`,
      description: `Feigenbaum constant alfa`,
      example: `2*[F'd]`
    },
    {
      name: `[F'a]`,
      description: `Feigenbaum constant delta`,
      example: `[F'a]*3`
    },
    {
      name: `[C2]`,
      description: `Twin prime constant`,
      example: `2*[C2]`
    },
    {
      name: `[M1]`,
      description: `Meissel-Mertens constant`,
      example: `[M1]*3`
    },
    {
      name: `[B2]`,
      description: `Brun's constant for twin primes`,
      example: `2*[B2]`
    },
    {
      name: `[B4]`,
      description: `Brun's constant for prime quadruplets`,
      example: `[B4]*3`
    },
    {
      name: `[BN'L]`,
      description: `De Bruijn-Newman constant`,
      example: `2*[BN'L]`
    },
    {
      name: `[Kat]`,
      description: `Catalan's constant`,
      example: `[Kat]*3`
    },
    {
      name: `[K*]`,
      description: `Landau-Ramanujan constant`,
      example: `2*[K*]`
    },
    {
      name: `[K.]`,
      description: `Viswanath's constant`,
      example: `[K.]*3`
    },
    {
      name: `[B'L]`,
      description: `Legendre's constant`,
      example: `2*[B'L]`
    },
    {
      name: `[RS'm]`,
      description: `Ramanujan-Soldner constant`,
      example: `[RS'm]*3`
    },
    {
      name: `[EB'e]`,
      description: `Erdos-Borwein constant`,
      example: `2*[EB'e]`
    },
    {
      name: `[Bern]`,
      description: `Bernstein's constant`,
      example: `[Bern]*3`
    },
    {
      name: `[GKW'l]`,
      description: `Gauss-Kuzmin-Wirsing constant`,
      example: `2*[GKW'l]`
    },
    {
      name: `[HSM's]`,
      description: `Hafner-Sarnak-McCurley constant`,
      example: `[HSM's]*3`
    },
    {
      name: `[lm]`,
      description: `Golomb-Dickman constant`,
      example: `2*[lm]`
    },
    {
      name: `[Cah]`,
      description: `Cahen's constant`,
      example: `[Cah]*3`
    },
    {
      name: `[Ll]`,
      description: `Laplace limit`,
      example: `2*[Ll]`
    },
    {
      name: `[AG]`,
      description: `Alladi-Grinstead constant`,
      example: `[AG]*3`
    },
    {
      name: `[L*]`,
      description: `Lengyel's constant`,
      example: `2*[L*]`
    },
    {
      name: `[L.]`,
      description: `Levy's constant`,
      example: `[L.]*3`
    },
    {
      name: `[Dz3]`,
      description: `Apery's constant`,
      example: `2*[Dz3]`
    },
    {
      name: `[A3n]`,
      description: `Mills' constant`,
      example: `[A3n]*3`
    },
    {
      name: `[Bh]`,
      description: `Backhouse's constant`,
      example: `2*[Bh]`
    },
    {
      name: `[Pt]`,
      description: `Porter's constant`,
      example: `[Pt]*3`
    },
    {
      name: `[L2]`,
      description: `Lieb's square ice constant`,
      example: `2*[L2]`
    },
    {
      name: `[Nv]`,
      description: `Niven's constant`,
      example: `[Nv]*3`
    },
    {
      name: `[Ks]`,
      description: `Sierpinski's constant`,
      example: `2*[Ks]`
    },
    {
      name: `[Kh]`,
      description: `Khinchin's constant`,
      example: `[Kh]*3`
    },
    {
      name: `[FR]`,
      description: `Fransen-Robinson constant`,
      example: `2*[FR]`
    },
    {
      name: `[La]`,
      description: `Landau's constant`,
      example: `[La]*3`
    },
    {
      name: `[P2]`,
      description: `Parabolic constant`,
      example: `2*[P2]`
    },
    {
      name: `[Om]`,
      description: `Omega constant`,
      example: `[Om]*3`
    },
    {
      name: `[MRB]`,
      description: `MRB constant`,
      example: `2*[MRB]`
    },
    {
      name: `[li2]`,
      description: `li(2) - logarithmic integral function at x=2`,
      example: `[li2]*3`
    },
    {
      name: `[EG]`,
      description: `Gompertz constant`,
      example: `2*[EG]`
    },
    {
      name: `[c]`,
      description: `Light speed in vacuum [m/s] (m=1, s=1)`,
      example: `[c]*3`
    },
    {
      name: `[G.]`,
      description: `Gravitational constant (m=1, kg=1, s=1)]`,
      example: `2*[G.]`
    },
    {
      name: `[g]`,
      description: `Gravitational acceleration on Earth [m/s^2] (m=1, s=1)`,
      example: `[g]*3`
    },
    {
      name: `[hP]`,
      description: `Planck constant (m=1, kg=1, s=1)`,
      example: `2*[hP]`
    },
    {
      name: `[h-]`,
      description: `Reduced Planck constant / Dirac constant (m=1, kg=1, s=1)]`,
      example: `[h-]*3`
    },
    {
      name: `[lP]`,
      description: `Planck length [m] (m=1)`,
      example: `2*[lP]`
    },
    {
      name: `[mP]`,
      description: `Planck mass [kg] (kg=1)`,
      example: `[mP]*3`
    },
    {
      name: `[tP]`,
      description: `Planck time [s] (s=1)`,
      example: `2*[tP]`
    },
    {
      name: `[ly]`,
      description: `Light year [m] (m=1)`,
      example: `[ly]*3`
    },
    {
      name: `[au]`,
      description: `Astronomical unit [m] (m=1)`,
      example: `2*[au]`
    },
    {
      name: `[pc]`,
      description: `Parsec [m] (m=1)`,
      example: `[pc]*3`
    },
    {
      name: `[kpc]`,
      description: `Kiloparsec [m] (m=1)`,
      example: `2*[kpc]`
    },
    {
      name: `[Earth-R-eq]`,
      description: `Earth equatorial radius [m] (m=1)`,
      example: `[Earth-R-eq]*3`
    },
    {
      name: `[Earth-R-po]`,
      description: `Earth polar radius [m] (m=1)`,
      example: `2*[Earth-R-po]`
    },
    {
      name: `[Earth-R]`,
      description: `Earth mean radius (m=1)`,
      example: `[Earth-R]*3`
    },
    {
      name: `[Earth-M]`,
      description: `Earth mass [kg] (kg=1)`,
      example: `2*[Earth-M]`
    },
    {
      name: `[Earth-D]`,
      description: `Earth-Sun distance - semi major axis [m] (m=1)`,
      example: `[Earth-D]*3`
    },
    {
      name: `[Moon-R]`,
      description: `Moon mean radius [m] (m=1)`,
      example: `2*[Moon-R]`
    },
    {
      name: `[Moon-M]`,
      description: `Moon mass [kg] (kg=1)`,
      example: `[Moon-M]*3`
    },
    {
      name: `[Moon-D]`,
      description: `Moon-Earth distance - semi major axis [m] (m=1)`,
      example: `2*[Moon-D]`
    },
    {
      name: `[Solar-R]`,
      description: `Solar mean radius [m] (m=1)`,
      example: `[Solar-R]*3`
    },
    {
      name: `[Solar-M]`,
      description: `Solar mass [kg] (kg=1)`,
      example: `2*[Solar-M]`
    },
    {
      name: `[Mercury-R]`,
      description: `Mercury mean radius [m] (m=1)`,
      example: `[Mercury-R]*3`
    },
    {
      name: `[Mercury-M]`,
      description: `Mercury mass [kg] (kg=1)`,
      example: `2*[Mercury-M]`
    },
    {
      name: `[Mercury-D]`,
      description: `Mercury-Sun distance - semi major axis [m] (m=1)`,
      example: `[Mercury-D]*3`
    },
    {
      name: `[Venus-R]`,
      description: `Venus mean radius [m] (m=1)`,
      example: `2*[Venus-R]`
    },
    {
      name: `[Venus-M]`,
      description: `Venus mass [kg] (kg=1)`,
      example: `[Venus-M]*3`
    },
    {
      name: `[Venus-D]`,
      description: `Venus-Sun distance - semi major axis [m] (m=1)`,
      example: `2*[Venus-D]`
    },
    {
      name: `[Mars-R]`,
      description: `Mars mean radius [m] (m=1)`,
      example: `[Mars-R]*3`
    },
    {
      name: `[Mars-M]`,
      description: `Mars mass [kg] (kg=1)`,
      example: `2*[Mars-M]`
    },
    {
      name: `[Mars-D]`,
      description: `Mars-Sun distance - semi major axis [m] (m=1)`,
      example: `[Mars-D]*3`
    },
    {
      name: `[Jupiter-R]`,
      description: `Jupiter mean radius [m] (m=1)`,
      example: `2*[Jupiter-R]`
    },
    {
      name: `[Jupiter-M]`,
      description: `Jupiter mass [kg] (kg=1)`,
      example: `[Jupiter-M]*3`
    },
    {
      name: `[Jupiter-D]`,
      description: `Jupiter-Sun distance - semi major axis [m] (m=1)`,
      example: `2*[Jupiter-D]`
    },
    {
      name: `[Saturn-R]`,
      description: `Saturn mean radius [m] (m=1)`,
      example: `[Saturn-R]*3`
    },
    {
      name: `[Saturn-M]`,
      description: `Saturn mass [kg] (kg=1)`,
      example: `2*[Saturn-M]`
    },
    {
      name: `[Saturn-D]`,
      description: `Saturn-Sun distance - semi major axis [m] (m=1)`,
      example: `[Saturn-D]*3`
    },
    {
      name: `[Uranus-R]`,
      description: `Uranus mean radius [m] (m=1)`,
      example: `2*[Uranus-R]`
    },
    {
      name: `[Uranus-M]`,
      description: `Uranus mass [kg] (kg=1)`,
      example: `[Uranus-M]*3`
    },
    {
      name: `[Uranus-D]`,
      description: `Uranus-Sun distance - semi major axis [m] (m=1)`,
      example: `2*[Uranus-D]`
    },
    {
      name: `[Neptune-R]`,
      description: `Neptune mean radius [m] (m=1)`,
      example: `[Neptune-R]*3`
    },
    {
      name: `[Neptune-M]`,
      description: `Neptune mass [kg] (kg=1)`,
      example: `2*[Neptune-M]`
    },
    {
      name: `[Neptune-D]`,
      description: `Neptune-Sun distance - semi major axis [m] (m=1)`,
      example: `[Neptune-D]*3`
    },
    {
      name: `[Uni]`,
      description: `Random variable - Uniform continuous distribution U(0,1), usage example: 2*[Uni]`,
      example: `2*[Uni]`
    },
    {
      name: `[Int]`,
      description: `Random variable - random integer - usage example sin( 3*[Int] )`,
      example: `[Int]*3`
    },
    {
      name: `[Int1]`,
      description: `Random variable - random integer -
      Uniform discrete distribution U{-10^1, 10^1} - usage example sin( 3*[Int1] )`,
      example: `2*[Int1]`
    },
    {
      name: `[Int2]`,
      description: `Random variable - random integer -
      Uniform discrete distribution U{-10^2, 10^2} - usage example sin( 3*[Int2] )`,
      example: `[Int2]*3`
    },
    {
      name: `[Int3]`,
      description: `Random variable - random integer -
      Uniform discrete distribution U{-10^3, 10^3} - usage example sin( 3*[Int3] )`,
      example: `2*[Int3]`
    },
    {
      name: `[Int4]`,
      description: `Random variable - random integer -
      Uniform discrete distribution U{-10^4, 10^4} - usage example sin( 3*[Int4] )`,
      example: `[Int4]*3`
    },
    {
      name: `[Int5]`,
      description: `Random variable - random integer -
      Uniform discrete distribution U{-10^5, 10^5} - usage example sin( 3*[Int5] )`,
      example: `2*[Int5]`
    },
    {
      name: `[Int6]`,
      description: `Random variable - random integer -
      Uniform discrete distribution U{-10^6, 10^6} - usage example sin( 3*[Int6] )`,
      example: `[Int6]*3`
    },
    {
      name: `[Int7]`,
      description: `Random variable - random integer -
      Uniform discrete distribution U{-10^7, 10^7} - usage example sin( 3*[Int7] )`,
      example: `2*[Int7]`
    },
    {
      name: `[Int8]`,
      description: `Random variable - random integer -
      Uniform discrete distribution U{-10^8, 10^8} - usage example sin( 3*[Int8] )`,
      example: `[Int8]*3`
    },
    {
      name: `[Int9]`,
      description: `Random variable - random integer -
      Uniform discrete distribution U{-10^9, 10^9} - usage example sin( 3*[Int9] )`,
      example: `2*[Int9]`
    },
    {
      name: `[nat]`,
      description: `Random variable - random natural number including 0 - usage example sin( 3*[nat] )`,
      example: `[nat]*3`
    },
    {
      name: `[nat1]`,
      description: `Random variable - random natural number including 0 -
        Uniform discrete distribution U{0, 10^1} - usage example sin( 3*[nat1] )`,
      example: `2*[nat1]`
    },
    {
      name: `[nat2]`,
      description: `Random variable - random natural number including 0 -
        Uniform discrete distribution U{0, 10^2} - usage example sin( 3*[nat2] )`,
      example: `[nat2]*3`
    },
    {
      name: `[nat3]`,
      description: `Random variable - random natural number including 0 -
        Uniform discrete distribution U{0, 10^3} - usage example sin( 3*[nat3] )`,
      example: `2*[nat3]`
    },
    {
      name: `[nat4]`,
      description: `Random variable - random natural number including 0 -
        Uniform discrete distribution U{0, 10^4} - usage example sin( 3*[nat4] )`,
      example: `[nat4]*3`
    },
    {
      name: `[nat5]`,
      description: `Random variable - random natural number including 0 -
      Uniform discrete distribution U{0, 10^5} - usage example sin( 3*[nat5] )`,
      example: `2*[nat5]`
    },
    {
      name: `[nat6]`,
      description: `Random variable - random natural number including 0 -
      Uniform discrete distribution U{0, 10^6} - usage example sin( 3*[nat6] )`,
      example: `[nat6]*3`
    },
    {
      name: `[nat7]`,
      description: `Random variable - random natural number including 0 -
      Uniform discrete distribution U{0, 10^7} - usage example sin( 3*[nat7] )`,
      example: `2*[nat7]`
    },
    {
      name: `[nat8]`,
      description: `Random variable - random natural number including 0 -
      Uniform discrete distribution U{0, 10^8} - usage example sin( 3*[nat8] )`,
      example: `[nat8]*3`
    },
    {
      name: `[nat9]`,
      description: `Random variable - random natural number including 0 -
      Uniform discrete distribution U{0, 10^9} - usage example sin( 3*[nat9] )`,
      example: `2*[nat9]`
    },
    {
      name: `[Nat]`,
      description: `Random variable - random natural number - usage example sin( 3*[Nat] )`,
      example: `[Nat]*3`
    },
    {
      name: `[Nat1]`,
      description: `Random variable - random natural number -
      Uniform discrete distribution U{1, 10^1} - usage example sin( 3*[Nat1] )`,
      example: `2*[Nat1]`
    },
    {
      name: `[Nat2]`,
      description: `Random variable - random natural number -
      Uniform discrete distribution U{1, 10^2} - usage example sin( 3*[Nat2] )`,
      example: `[Nat2]*3`
    },
    {
      name: `[Nat3]`,
      description: `Random variable - random natural number -
      Uniform discrete distribution U{1, 10^3} - usage example sin( 3*[Nat3] )`,
      example: `2*[Nat3]`
    },
    {
      name: `[Nat4]`,
      description: `Random variable - random natural number -
      Uniform discrete distribution U{1, 10^4} - usage example sin( 3*[Nat4] )`,
      example: `[Nat4]*3`
    },
    {
      name: `[Nat5]`,
      description: `Random variable - random natural number -
      Uniform discrete distribution U{1, 10^5} - usage example sin( 3*[Nat5] )`,
      example: `2*[Nat5]`
    },
    {
      name: `[Nat6]`,
      description: `Random variable - random natural number -
      Uniform discrete distribution U{1, 10^6} - usage example sin( 3*[Nat6] )`,
      example: `[Nat6]*3`
    },
    {
      name: `[Nat7]`,
      description: `Random variable - random natural number -
      Uniform discrete distribution U{1, 10^7} - usage example sin( 3*[Nat7] )`,
      example: `2*[Nat7]`
    },
    {
      name: `[Nat8]`,
      description: `Random variable - random natural number -
      Uniform discrete distribution U{1, 10^8} - usage example sin( 3*[Nat8] )`,
      example: `[Nat8]*3`
    },
    {
      name: `[Nat9]`,
      description: `Random variable - random natural number -
      Uniform discrete distribution U{1, 10^9} - usage example sin( 3*[Nat9] )`,
      example: `2*[Nat9]`
    },
    {
      name: `[Nor]`,
      description: `Random variable - Normal distribution N(0,1) - usage example cos( 3*[Nor]+1 )`,
      example: `[Nor]*3`
    }
  ]
};
