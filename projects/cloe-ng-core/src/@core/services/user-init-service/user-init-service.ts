import { Inject, Injectable, InjectionToken } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CloeSettings } from '../cloe.settings';
import { MsContextEnum } from '../../enums/MsContextEnum';
import { tap } from 'rxjs/operators';
import { StorageService, USER_BASE_INFO } from '../storage-service/storage.service';
import { Observable, of } from 'rxjs';
import { FeTypesEnum } from '../../enums/FeTypesEnum';

const USER_INIT_INFO = '/user-init-info';
const USER_INIT_API = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + USER_INIT_INFO;
const USER_DEFAULTS_API = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + USER_INIT_INFO + '/defaults';

interface ICorePagesConfig {
  feType: FeTypesEnum;
}

@Injectable()
export class UserInitService {

  constructor(private http: HttpClient, @Inject('CorePagesConfig') private corePageConfig) {
  }

  getUserInitialInfo(fe?: FeTypesEnum) {
    const feType = fe || this.corePageConfig.feType;
    const userBaseInfo = StorageService.getObject(USER_BASE_INFO);
    if (userBaseInfo) {
      return of(userBaseInfo);
    }
    return this.http.get(USER_INIT_API + '/' + feType).pipe(
      tap((resp: any) => {
        StorageService.setCurrentUser(resp.user);
        StorageService.saveObject(USER_BASE_INFO, resp);
      })
    );
  }

  getUserDefaults(siteUuid, channelUuid, userUuid) {
    const payload = {
      defaultSiteUuid: siteUuid,
      defaultChannelUuid: channelUuid,
      uuid: userUuid
    };
    return this.http.post(USER_DEFAULTS_API, payload);
  }
}
