import { AbstractControl, ValidatorFn } from '@angular/forms';
import * as moment_ from 'moment';
const moment = moment_;

export function dateRangeValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const dateValidation = moment(control.value.fromDate).isValid() && moment(control.value.toDate);
    return dateValidation ? null : {'dateRange': true};
  };
}
