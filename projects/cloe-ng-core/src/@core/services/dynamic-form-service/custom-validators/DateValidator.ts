import { AbstractControl, ValidatorFn } from '@angular/forms';
import * as moment_ from 'moment';
const moment = moment_;

export function dateValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const dateValidation = moment(control.value).isValid();
    return dateValidation ? null : {'date': true};
  };
}
