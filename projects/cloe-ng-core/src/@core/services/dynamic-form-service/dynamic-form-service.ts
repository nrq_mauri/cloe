import { FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';
import { QuestionBase } from '../../@ui/components/cloe-dynamic-form/models/QuestionBase';
import { Injectable } from '@angular/core';
import { ValidatorsEnum } from '../../@ui/components/cloe-dynamic-form/models/enums/ValidatorsEnum';
import { CloeValidator } from '../../@ui/components/cloe-dynamic-form/models/validators/CloeValidator';
import { dateValidator } from './custom-validators/DateValidator';
import { dateRangeValidator } from './custom-validators/DateRangeValidator';

@Injectable()
export class DynamicFormService {
  constructor() { }

  toFormGroup(questions: QuestionBase<any>[] ) {
    const group: any = {};
    questions.forEach(question => {
      group[question.key] = new FormControl(
        question.value || '',
        Validators.compose(this.getValidators(question.validators))
      );
    });
    return new FormGroup(group);
  }

  private getValidators(validators: CloeValidator[]): ValidatorFn[] {
    const validatorsArray: ValidatorFn[] = [];
    validators.forEach( (validator) => {
      switch (validator.validatorType) {
        case ValidatorsEnum.EMAIL:
          validatorsArray.push(Validators.email);
          break;
        case ValidatorsEnum.REQUIRED:
          validatorsArray.push(Validators.required);
          break;
        case ValidatorsEnum.NUMBER:
          validatorsArray.push(Validators.pattern('^-?[0-9]*$'));
          break;
        case ValidatorsEnum.PATTERN:
          validatorsArray.push(Validators.pattern(validator.params));
          break;
        case ValidatorsEnum.DATE:
          validatorsArray.push(dateValidator());
          break;
        case ValidatorsEnum.DATE_RANGE:
          validatorsArray.push(dateRangeValidator());
          break;
      }
    });
    return validatorsArray;
  }
}
