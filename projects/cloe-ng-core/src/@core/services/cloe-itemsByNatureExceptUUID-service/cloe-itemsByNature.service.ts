import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {of, Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {CloeSettings} from '../cloe.settings';
import {MsContextEnum} from '../../enums/MsContextEnum';

const SEARCH_PATH = '/_items';
const BY_NATURE = '/byNature';
const BY_UUID = '/byUuid';
const EXCEPT_UUID_LIST = '/exceptUUID';
const BY_NATURE_WITH_FILTERS = '/withFilters';

@Injectable()
export class CloeItemsByNatureService {
  constructor(private http: HttpClient) {
  }

  searchByNature(natureType) {
    return this.http.get(
      CloeSettings.getBaseApiPath(natureType, SEARCH_PATH) +
      BY_NATURE +
      '/' + natureType
    )
      .pipe(
        catchError((err) => {
          console.log(err);
          return of(err);
        })
      );
  }

  getItemByNatureAndUuid(natureType, uuid) {
    return this.http.get(
      CloeSettings.getBaseApiPath(natureType, SEARCH_PATH) +
      BY_UUID +
      `/${natureType}/${uuid}`
      );
  }

  searchExceptUUID(natureType, uuidList) {
    return this.http.post(
      CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT)
      + SEARCH_PATH
      + EXCEPT_UUID_LIST +
      '/' + natureType,
      uuidList
    )
      .pipe(
        catchError((err) => {
          console.log(err);
          return of(err);
        })
      );
  }

  multiFieldSearchByNature(config, term) {
    const filters: any = {
      search: config.fieldToSearch
    };
    this.buildSearchTerm(filters.search, term);
    return this.http.post(
      CloeSettings.getBaseApiPath(config.nodeNature, SEARCH_PATH) +
      BY_NATURE_WITH_FILTERS +
      '/' + config.nodeNature,
      filters
    )
      .pipe(
        catchError((err) => {
          console.log(err);
          return of(err);
        })
      );
  }
  searchByNatureWithFilters(natureType, filters) {
    return this.http.post(
      CloeSettings.getBaseApiPath(natureType, SEARCH_PATH) +
      BY_NATURE_WITH_FILTERS +
      '/' + natureType,
      filters
    )
      .pipe(
        catchError((err) => {
          console.log(err);
          return of(err);
        })
      );
  }
  private buildSearchTerm(searchField, term) {
    Object.keys(searchField).forEach(key => {
      if (typeof searchField[key] === 'string') {
        searchField[key] = term;
      } else {
        this.buildSearchTerm(searchField[key], term);
      }
    });
  }
}
