import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import {CloeSettings} from '../cloe.settings';
import {MsContextEnum} from '../../enums/MsContextEnum';
import { Cluster } from '../../models/Cluster';
import { catchError } from 'rxjs/operators';
import { CloeTreeNodeService } from '../cloe-treeNode-service/cloe-tree-node.service';

const CLUSTERS_PATH = '/clusters';
const CLUSTERS_API = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + CLUSTERS_PATH;

const CLUSTER_PATH = '/_cluster';
const ALL_CLUSTERS_FOR_USER_API = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + CLUSTER_PATH + '/all';
const ALL_CLUSTERS_FOR_USER_AND_SYSTEM_API = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + CLUSTER_PATH + '/systemTree';
const DELETE_CLUSTERS_BY_TREE_NODE_API = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + CLUSTER_PATH + '/delete';

@Injectable()
export class ClusterService {

  constructor(
    private http: HttpClient,
    private cloeTreeNodeService: CloeTreeNodeService
  ) {}

  getClusterForUser(userUuid: string) {
    return this.http.get(ALL_CLUSTERS_FOR_USER_API + '/' + userUuid);
  }

  getClusterForUserAndSystem(userUuid: string, systemTree?: boolean) {
    return this.http.get(
      ALL_CLUSTERS_FOR_USER_AND_SYSTEM_API + '/' + userUuid,
      {
        params: {
          systemTree: (!!systemTree).toString()
        }
      });
  }

  upsertCluster(cluster: Cluster) {
    return cluster.id ? this.http.put(CLUSTERS_API, cluster) : this.http.post(CLUSTERS_API, cluster);
  }

  deleteClusterByTreeNode(root, tree, referUuid) {
    return this.http.post(
      DELETE_CLUSTERS_BY_TREE_NODE_API,
      this.cloeTreeNodeService.buildDeleteTreeNodeDTO(root, tree, referUuid)
    )
      .pipe(
        catchError((err) => {
          console.log(err);
          return of(err);
        })
      );
  }

}
