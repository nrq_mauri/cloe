import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { of, Observable } from 'rxjs';
import { CloeSettings } from '../cloe.settings';
import { MsContextEnum } from '../../enums/MsContextEnum';

const PAGE_HELP_PATH = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + '/page-helps';
const PAGE_HELP_BY_STATE_LIST_API = PAGE_HELP_PATH + '/by-state-list';


@Injectable()
export class PageHelpService {

  constructor(private http: HttpClient) {
  }

  getPageHelpByStateList(stateList: string[]) {
    return this.http.get(PAGE_HELP_BY_STATE_LIST_API, {
        params: {
          stateList: stateList
        }
      }
    );
  }

  pageHelpUpsert(pageHelp) {
    return pageHelp.id ? this.http.put(PAGE_HELP_PATH, pageHelp) : this.http.post(PAGE_HELP_PATH, pageHelp);
  }
}
