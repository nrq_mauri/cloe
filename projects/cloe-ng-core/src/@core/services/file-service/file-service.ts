import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CloeSettings} from '../cloe.settings';
import {Observable, Subscription} from 'rxjs';
import {MsContextEnum} from '../../enums/MsContextEnum';


const ENUM_UPLOAD_TYPES_PATH = '/enum/uploadTypes';
const UPLOAD_TYPES_API = CloeSettings.baseApiPath(MsContextEnum.DOCUMENT) + ENUM_UPLOAD_TYPES_PATH;

const UPLOAD_PATH = '/upload';
const UPLOAD_API = CloeSettings.baseApiPath(MsContextEnum.DOCUMENT) + UPLOAD_PATH;

const ADD_METADATA_TO_FILE_PATH = '/addMetadataToFile';
const ADD_METADATA_TO_FILE_API = CloeSettings.baseApiPath(MsContextEnum.DOCUMENT) + ADD_METADATA_TO_FILE_PATH;

const DOWNLOAD_PATH = '/download';
const DOWNLOAD_API = CloeSettings.baseApiPath(MsContextEnum.DOCUMENT) + DOWNLOAD_PATH;

const DELETE_PATH = '/uploads';
const DELETE_API = CloeSettings.baseApiPath(MsContextEnum.DOCUMENT) + DELETE_PATH;

@Injectable()
export class FileService {

  constructor(private http: HttpClient) {
  }

  getEnumUploadTypes() {
    return this.http.get(UPLOAD_TYPES_API);
  }

  uploadFile(fileToUpload, owner, uploadType) {
    const formData: FormData = new FormData();
    formData.append('uploadedFile', fileToUpload);
    const UPLOAD_API_OWNER_TYPE = UPLOAD_API + '?ownerUuid=' + owner.cloeUser.uuid + '&type=' + uploadType;
    return this.http.post(UPLOAD_API_OWNER_TYPE, formData);
  }

  uploadMetaDataToFile(metaData, file) {
    const headers = {'Content-Type': 'application/json;charset=utf-8'};
    const ADD_METADATA_TO_FILE = ADD_METADATA_TO_FILE_API + '?fileUuid=' + file.uuid;
    return this.http.post(ADD_METADATA_TO_FILE, metaData, {headers: headers}).subscribe();
  }

  downloadOrViewFile(file, op) {
    const DOWNLOAD_UUID_OP = DOWNLOAD_API + '/' + file.uuid + '?op=' + op;
    let headers = {};
    if (op === 'VIEW') {
      headers = {responseType: 'blob'};
    }
    return this.http.get(DOWNLOAD_UUID_OP, headers);
  }

  deleteFile(file) {
    const DELETE_ID = DELETE_API + '/' + file.id;
    return this.http.delete(DELETE_ID);
  }

}
