import { environment } from '../environments/environment.current';
import {MsContextEnum} from '../enums/MsContextEnum';

export class CloeSettings {
  constructor() {}

  static getHostName() {
    return environment.api.hostname;
  }

  static baseApiPath(context?) {
    return `${this.basePath(context)}${environment.api.basePath}`;
  }

  static basePath(context?) {
    return `${environment.api.protocol}://${environment.api.hostname}${this.getMsContext(context)}`;
  }

  static getApiPrefix() {
    return environment.api.basePath;
  }

  static getMsContext(context?: MsContextEnum) {
    switch (context) {
      case MsContextEnum.DIAGNOSIS:
        return environment.api.msContext.diagnosis;
      case MsContextEnum.MANAGEMENT:
        return environment.api.msContext.management;
      case MsContextEnum.DATA:
        return environment.api.msContext.data;
      case MsContextEnum.DOCUMENT:
        return environment.api.msContext.document;
      case MsContextEnum.JOBSCHEDULER:
        return environment.api.msContext.jobscheduler;
      case MsContextEnum.GATEWAY:
      default:
        return '';
    }
  }

  static getBaseApiPath(nature, path) {
    return this.baseApiPath(this.getContextByNature(nature)) + path;
  }

  static getContextByNature(nature) {
    switch (nature) {
      case 'CLOEUSER':
      case 'CUSTOMER':
      case 'METADATA':
      case 'THEME_TEMPLATE':
        return MsContextEnum.GATEWAY;
      case 'MENU':
      case 'CHANNEL':
      case 'CHANNEL_TYPE':
      case 'CHANNEL_RANGE':
      case 'DEVICE':
      case 'TREE':
      case 'SITE':
      case 'UNIT':
      case 'PAGE_HELP_ONLINE':
        return MsContextEnum.MANAGEMENT;
      case 'JOB':
        return MsContextEnum.JOBSCHEDULER;
      case 'DIAGNOSIS':
        return MsContextEnum.DIAGNOSIS;
      default:
        return MsContextEnum.GATEWAY;
    }
  }
}
