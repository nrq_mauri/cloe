import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { of, Observable } from 'rxjs';
import {CloeSettings} from '../cloe.settings';
import {MsContextEnum} from '../../enums/MsContextEnum';
const NEW_UNIT_PATH = '/units';
const NEW_UNIT_API = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + NEW_UNIT_PATH;

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class UnitService {

  constructor(private http: HttpClient) {
  }

  upsertUnit(unit) {
    if (!unit.id) {
      return this.http.post(NEW_UNIT_API, unit, httpOptions);
    }
    return this.http.put(NEW_UNIT_API, unit, httpOptions);
  }

  deleteUnit(unitId) {
    return this.http.delete(NEW_UNIT_API + '/' + unitId)
      .pipe(
        catchError((err) => {
          return of(err);
        })
      );
  }

}
