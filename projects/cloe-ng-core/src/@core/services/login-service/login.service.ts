import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { StorageService, USER_BASE_INFO } from '../storage-service/storage.service';
import { Observable } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { CloeSettings } from '../cloe.settings';
import { StateService } from '@uirouter/core';
import { MsContextEnum } from '../../enums/MsContextEnum';
import { FeTypesEnum } from '../../enums/FeTypesEnum';
import { UserInitService } from '../user-init-service/user-init-service';
import { catchError, switchMap } from 'rxjs/operators';
import { of } from 'rxjs';

const AUTHENTICATE_PATH = '/authenticate';
const IMPERSONATE = '/impersonate';
const USER_PERMISSIONS = '/user-permissions';
const ACCOUNT_PATH = '/account';
const AUTHENTICATE_API = CloeSettings.baseApiPath() + AUTHENTICATE_PATH;
const IMPERSONATE_PUBLIC_API = CloeSettings.baseApiPath() + '/public' + IMPERSONATE;
const IMPERSONATE_API = CloeSettings.baseApiPath() + IMPERSONATE;
const USER_PERMISSIONS_API = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) +
  ACCOUNT_PATH +
  USER_PERMISSIONS;


@Injectable()
export class LoginService {

  constructor(private http: HttpClient, private stateService: StateService, private userInitService: UserInitService) {}

  private handleError(e) {
    console.log(e);
  }

  isAuthenticated() {
    const helper = new JwtHelperService();
    const token = StorageService.getToken();
    return !!helper && !helper.isTokenExpired(token);
  }

  doLogin(payload, feType: FeTypesEnum) {
    return this.http.post(AUTHENTICATE_API, payload).pipe(
      switchMap((resp: any) => {
        const jwt = resp.data || resp;
        StorageService.saveToken(jwt);
        return this.userInitService.getUserInitialInfo(feType).pipe(
          switchMap((userInfo: any) => {
            StorageService.saveObject(USER_BASE_INFO, userInfo);
            StorageService.savePermissions(userInfo.permissions);
            return of(true);
          })
        );
      }),
      catchError(() => of(false))
    );
  }

  impersonate(tokenCode: string) {
    const url = IMPERSONATE_PUBLIC_API + '/' + tokenCode;
    return this.http.get(url).pipe(
      switchMap((resp: any) => {
        if (!resp.success) {
          return of(false);
        }
        this._cleanStorage();
        const jwt = resp.data;
        StorageService.saveToken(jwt);
        return this.userInitService.getUserInitialInfo(FeTypesEnum.FE).pipe(
          switchMap((userInfo: any) => {
            StorageService.saveObject(USER_BASE_INFO, userInfo);
            StorageService.savePermissions(userInfo.permissions);
            return of(true);
          })
        );
      })
    );
  }

  generateTempToken(userUuid) {
    return this.http.post(IMPERSONATE_API + '/' + userUuid, {});
  }

  doLogout() {
    this._cleanStorage();
    this.stateService.go('login', null, {reload: true, location: true});
  }

  private _cleanStorage() {
    StorageService.clear();
  }
}
