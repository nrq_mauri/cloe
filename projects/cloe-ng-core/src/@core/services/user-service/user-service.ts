import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {catchError} from 'rxjs/operators';
import {of, Observable} from 'rxjs';
import {CloeSettings} from '../cloe.settings';

const NEW_USER_PATH = '/userCloeUsers';
const NEW_USER_API = CloeSettings.baseApiPath() + NEW_USER_PATH;

const DELETE_USER_PATH = '/userCloeUsers';
const DELETE_USER_API = CloeSettings.baseApiPath() + DELETE_USER_PATH;

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class UserService {

  constructor(private http: HttpClient) {
  }

  upsertUser(user) {
    if (!user.id) {
      return this.http.post(NEW_USER_API, user, httpOptions);
    }
    return this.http.put(NEW_USER_API, user, httpOptions);
  }

  deleteUserCloeUser(cloeUserId) {
    return this.http.delete(DELETE_USER_API + '/' + cloeUserId)
      .pipe(
        catchError((err) => {
          return of(err);
        })
      );
  }

}
