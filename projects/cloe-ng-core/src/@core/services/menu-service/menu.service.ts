import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {CloeSettings} from '../cloe.settings';
import {Observable} from 'rxjs';
import {MsContextEnum} from '../../enums/MsContextEnum';

const TREE_NODE_PATH = '/menus';
const TREE_NODE_API = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + TREE_NODE_PATH;

const MENU_PATH = '/menus';
const MENU_API = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + MENU_PATH;

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class MenuService {
  constructor(private http: HttpClient) {
  }

  getMenuForCurrentUser(user, feType) {
    return new Observable(observer => {
      this.http.get(`${TREE_NODE_API}/${feType.toString()}/${user.cloeUser.uuid}`)
        .subscribe((resp: any) => {
          observer.next(resp.children);
          observer.complete();
        });
    });
  }

  upsertMenu(menu) {
    if (!menu.id) {
      return this.http.post(MENU_API, menu, httpOptions);
    }
    return this.http.put(MENU_API, menu, httpOptions);
  }

}
