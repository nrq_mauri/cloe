import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CloeSettings} from '../cloe.settings';
import {Observable} from 'rxjs';
import {MsContextEnum} from '../../enums/MsContextEnum';

const CUSTOMER_STATUS_PATH = '/common-status';
const CUSTOMER_STATUS_API = CloeSettings.baseApiPath() + CUSTOMER_STATUS_PATH;

const WEEK_DAYS_PATH = '/week-days';
const WEEK_DAYS_API = CloeSettings.baseApiPath() + WEEK_DAYS_PATH;

const USER_AUTHORITIES_PATH = '/users/authorities';
const USER_AUTHORITIES_API = CloeSettings.baseApiPath() + USER_AUTHORITIES_PATH;

const CITY_PATH = '/cities/prefix';
const CITY_API = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + CITY_PATH;

const NATURE_TYPE_PATH = '/nature-type';
const NATURE_TYPE_API = CloeSettings.baseApiPath() + NATURE_TYPE_PATH;

const NODE_TYPE_PATH = '/node-status/NODE';
const NODE_TYPE_API = CloeSettings.baseApiPath() + NODE_TYPE_PATH;

const NODE_NATURE_PATH = '/node-nature';
const NODE_NATURE_API = CloeSettings.baseApiPath() + NODE_NATURE_PATH;

const UNIT_PATH = '/units';
const UNIT_API = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + UNIT_PATH;

const DEVICE_TYPES_PATH = '/device-types';
const DEVICE_TYPES_API = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + DEVICE_TYPES_PATH;

const CHANNEL_TYPES_PATH = '/channel-types';
const CHANNEL_TYPES_API = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + CHANNEL_TYPES_PATH;

const TIME_ZONE_PATH = '/time-zones';
const TIME_ZONE_API = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + TIME_ZONE_PATH;

const DEVICES_PATH = '/devices';
const DEVICES_API = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + DEVICES_PATH;

const CHANNEL_VALIDITY_PATH = '/channel-validities';
const CHANNEL_VALIDITY_API = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + CHANNEL_VALIDITY_PATH;

@Injectable()
export class CommonService {

  constructor(private http: HttpClient) {
  }

  getCommonStatus() {
    return this.http.get(CUSTOMER_STATUS_API);
  }

  getAuthorities() {
    return this.http.get(USER_AUTHORITIES_API);
  }

  getWeekDays() {
    return this.http.get(WEEK_DAYS_API);
  }

  getNatureTypes(natureGroup) {
    return this.http.get(NATURE_TYPE_API + '/' + natureGroup);
  }

  getNodeStatus() {
    return this.http.get(NODE_TYPE_API);
  }

  getNodeNatureEnum() {
    return this.http.get(NODE_NATURE_API);
  }

  getCityList(prefix): Observable<any> {
    return this.http.get(CITY_API + '/' + prefix);
  }

  getUnitList() {
    return this.http.get(UNIT_API);
  }

  getDeviceTypes() {
    return this.http.get(DEVICE_TYPES_API);
  }

  getChannelTypes() {
    return this.http.get(CHANNEL_TYPES_API);
  }

  getChannelValidities() {
    return this.http.get(CHANNEL_VALIDITY_API);
  }

  getTimeZone() {
    return this.http.get(TIME_ZONE_API);
  }

  getDevices() {
    return this.http.get(DEVICES_API);
  }

}



