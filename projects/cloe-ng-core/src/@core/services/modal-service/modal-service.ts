import {Injectable} from '@angular/core';
import {NgbModal, NgbModalOptions, NgbModalRef} from '@ng-bootstrap/ng-bootstrap';
import {TransitionService} from '@uirouter/angular';

@Injectable()
export class ModalService {

  modalInstance: NgbModalRef;

  constructor(private ngbModal: NgbModal, private transitionService: TransitionService) {
    this.transitionService.onSuccess({}, () => {
      if (this.modalInstance) {
        this.close();
      }
    });
  }

  open(content: any, options: NgbModalOptions) {
    if (this.modalInstance) {
      throw Error('Only one instance of modal can be opened at the same time');
    }
    this.modalInstance = this.ngbModal.open(content, options);
    return new Promise((resolve, reject) => {
      this.modalInstance.result.then(result => {
        this.invalidateModale();
        resolve(result);
      }).catch(result => {
        this.invalidateModale();
        reject(result);
      });
    });
  }

  getModalInstance(): NgbModalRef {
    return this.modalInstance;
  }

  private invalidateModale() {
    this.modalInstance = undefined;
  }

  close(result?: any) {
    this.modalInstance.close(result);
    this.invalidateModale();
  }
}
