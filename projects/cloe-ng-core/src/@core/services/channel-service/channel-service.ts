import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { of, Observable } from 'rxjs';
import { CloeSettings } from '../cloe.settings';
import { MsContextEnum } from '../../enums/MsContextEnum';

const CHANNEL_PATH = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + '/channels';
const CHANNEL_LIST_UPSERT = CHANNEL_PATH + '/upsertList';
const CHANNELS_NOT_IN_NODE_API = CHANNEL_PATH + '/notInNode';
const ALL_EXTERNAL_CHANNELS_API = CHANNEL_PATH + '/all/extChannels';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class ChannelService {

  constructor(private http: HttpClient) {
  }

  getChannelsNotInNode() {
    return this.http.get(CHANNELS_NOT_IN_NODE_API);
  }

  getAllExternalChannels() {
    return this.http.get(ALL_EXTERNAL_CHANNELS_API);
  }

  upsertChannel(channel) {
    if (!channel.id) {
      return this.http.post(CHANNEL_PATH, channel, httpOptions);
    }
    return this.http.put(CHANNEL_PATH, channel, httpOptions);
  }

  upsertChannelList(channels) {
    return this.http.post(CHANNEL_LIST_UPSERT, channels);
  }

  deleteChannel(channelId) {
    return this.http.delete(CHANNEL_PATH + '/' + channelId)
      .pipe(
        catchError((err) => {
          return of(err);
        })
      );
  }

}
