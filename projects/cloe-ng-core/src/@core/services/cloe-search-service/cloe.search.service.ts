import { Injectable } from '@angular/core';
import {CloeSettings} from '../cloe.settings';
import {HttpClient} from '@angular/common/http';
import {of, Observable} from 'rxjs';
import {catchError} from 'rxjs/operators';

const SEARCH_PATH =  '/_search';

@Injectable()
export class CloeSearchService {
  constructor(private http: HttpClient) {}

  search(natureType: String, metadata, pagination, filters) {
    return this.http.post(
      CloeSettings.getBaseApiPath(natureType, SEARCH_PATH) +
      '/' + natureType +
      this.buildPaginationPath(natureType, metadata, pagination),
      filters
    )
      .pipe(
        catchError( (err) => {
          console.log(err);
          return of(err);
        })
      );
  }
  buildPaginationPath(natureType, metadata, pagination) {
    let parameters = '?';
    Object.entries(metadata).forEach(
      ([key, value]) => {
        if (key !== 'sort' && key !== 'sortType') {
          parameters += key + '=' + value + '&';
        }
      }
    );
    Object.entries(pagination).forEach(
      ([key, value]) => {
        if (key !== 'sizeOptions') {
          parameters += key + '=' + value + '&';
        }
      }
    );
    if (metadata.sort && metadata.sortType) {
      parameters += 'sort=' + metadata.sort + ',' + metadata.sortType;
    }
    return parameters;
  }
}
