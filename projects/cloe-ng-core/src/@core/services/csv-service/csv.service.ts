import { Injectable } from '@angular/core';
import { cloneDeep, extend, get, join, map, transform } from 'lodash';
import * as moment_ from 'moment';
import { DynamicPipe } from '../../@ui/pipes';
import { TranslateService } from '@ngx-translate/core';
import { StorageService } from '../storage-service/storage.service';

const moment = moment_;

export interface IDataToExportCSV {
  headers: Array<ICSVHeader>;
  content: Array<any>;
}

export interface CSVOptions {
  fieldSeparator?: string;
  quoteStrings?: string;
  cr?: string;
  skipHeader?: boolean;
  outputFileName?: string;
}

export interface ICSVHeader {
  fieldName: string;
  columnName: string;
  pipe?: string;
  pipeArgs?: any;
  skipPipeOnExport?: boolean;
}

const defaultCsvOptions: CSVOptions = {
  fieldSeparator: ';',
  quoteStrings: '\'',
  cr: '\r\n',
  skipHeader: false
};

declare const URL: any;
declare const document: any;

@Injectable()
export class CsvService {

  constructor (private dynamicPipe: DynamicPipe, private translateService: TranslateService) {}


  exportArrayObjectToCSV(data: IDataToExportCSV, options: CSVOptions = defaultCsvOptions) {
    let csv = '';
    if (!options.skipHeader) {
      csv += join(
        map(
          map(data.headers, 'columnName'),
            label => this.translateService.instant(label)
        ),
        options.fieldSeparator
      );
      csv += options.cr;
    }

    data.content.forEach((row: any) => {
      csv += join(
        this._getFieldsValues(row, data.headers),
        options.fieldSeparator
      );
      csv += options.cr;
    });

    return csv;
  }

  generateDataToExportCSV(headers: any, data: Array<any>): IDataToExportCSV {
    const flattedHeaders = transform(headers, (result, header) => {
      const headersLeafResult = [];
      this.getLeafHeader(header, headersLeafResult);
      result.push(...headersLeafResult);
    }, []);
    return {
      headers: flattedHeaders,
      content: data
    };
  }

  getLeafHeader(root, result) {
    if (!root.children) {
      result.push(root);
    } else {
      root.children.forEach((child) => {
        const extendedChild = cloneDeep(child);
        extendedChild.columnName = root.columnName + ' > ' + extendedChild.columnName;
        this.getLeafHeader(extendedChild, result);
      });
    }
  }

  downloadCSV(data: IDataToExportCSV, options: CSVOptions = {}) {
    const csvOptions = extend(defaultCsvOptions, options);
    this.downloadArrayCSV([data], csvOptions);
  }

  downloadArrayCSV(data: Array<IDataToExportCSV>, options: CSVOptions = {}) {
    const csvOptions = extend(defaultCsvOptions, options);
    let csvOutput = '';
    data.forEach((dataToExport: IDataToExportCSV) => {
      csvOutput += this.exportArrayObjectToCSV(dataToExport, csvOptions);
      csvOutput += csvOptions.cr;
    });
    const blob = new Blob([csvOutput], { type: 'text/csv' });
    const url = URL.createObjectURL(blob);

    const a = document.createElement('a');
    a.href = url;
    a.download = (csvOptions.outputFileName || '' ) + moment().format('YYYYMMDDHHmm') + '_export.csv';
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);

    URL.revokeObjectURL(url);
  }

  private _getFieldsValues(row: any, headers: Array<ICSVHeader>) {
    const rowValues = [];
    headers.forEach((header: ICSVHeader) => {
      let value = get(row, header.fieldName);
      if (header.pipe) {
        if (header.pipe === 'translate') {
          value = this.translateService.instant(get(row, header.fieldName));
        } else if (header.pipe && !header.skipPipeOnExport) {
          value = this.dynamicPipe.transform(get(row, header.fieldName), header.pipe, header.pipeArgs);
        }
      }
      const currentUserCsvSeparator = StorageService.getCurrentUser().cloeUser.csvSeparator;
      if (currentUserCsvSeparator) {
        value = this.dynamicPipe.transform(value, 'cloeDecimalFormatter', currentUserCsvSeparator);
      }
      rowValues.push(value);
    });
    return rowValues;
  }
}
