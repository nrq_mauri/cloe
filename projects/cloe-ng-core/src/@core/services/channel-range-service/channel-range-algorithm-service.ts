import { Injectable } from '@angular/core';
import { ChannelRange } from './models/ChannelRange';
import { keyBy, cloneDeep } from 'lodash';
import * as moment_ from 'moment';

const moment = moment_;

export class ChannelRangeAlgorithmService {
  jsonAlgorithm;
  specialProfiles;
  weeklyProfiles;
  dailyProfiles;

  constructor(specialProfiles, weeklyProfiles, dailyProfiles, jsonAlgorithm) {
    this.specialProfiles = specialProfiles;
    this.weeklyProfiles = weeklyProfiles;
    this.dailyProfiles = dailyProfiles;
    this.jsonAlgorithm = jsonAlgorithm;
  }

  getRatesMap(): any {
    return cloneDeep(keyBy(this.jsonAlgorithm.rates, 'id'));
  }

  getRangesFromPeriod(channelRange: ChannelRange, period) {
    const result: any = cloneDeep(keyBy(this.jsonAlgorithm.rates, 'id'));
    result.originalChannelRange = channelRange;
    result.updateGuides = true;
    const dailyProfiles = cloneDeep(keyBy(this.jsonAlgorithm.dailyProfiles, 'id'));
    let currentDate = moment.utc(period.startDate);
    const endDate = moment.utc(period.endDate).add(1, 'day');
    while (endDate.diff(currentDate) >= 0) {
      const currentDailyProfile = dailyProfiles[+(this.getDailyProfileIndexByDay(currentDate))];
      currentDailyProfile.periods.forEach((dailyPeriod) => {
        const startPeriod = moment.utc(dailyPeriod.startPeriod, 'HH:mm');
        const endPeriod = moment.utc(dailyPeriod.endPeriod, 'HH:mm');
        result[dailyPeriod.rateId].periods ?
          result[dailyPeriod.rateId].periods.push({
            startDate: currentDate.set({
              hour: startPeriod.get('hour'),
              minute: startPeriod.get('minute')
            }).toISOString(true),
            endDate: currentDate.set({
              hour: endPeriod.get('hour'),
              minute: endPeriod.get('minute')
            }).toISOString(true)
          }) :
          result[dailyPeriod.rateId].periods = [{
            startDate: currentDate.set({
              hour: startPeriod.get('hour'),
              minute: startPeriod.get('minute')
            }).toISOString(true),
            endDate: currentDate.set({
              hour: endPeriod.get('hour'),
              minute: endPeriod.get('minute')
            }).toISOString(true)
          }];
      });
      currentDate = currentDate.add(1, 'day');
    }
    return result;
  }

  private getDailyProfileIndexByDay(startDate) {
    const day = moment.utc(startDate);
    const specialProfile = this.specialProfiles[day.format('YYYY-MM-DD')];
    if (!specialProfile) {
      return this.weeklyProfiles[+(day.day())];
    } else {
      return specialProfile;
    }
  }

  getRangeFromMeasure(measure) {
    const currentDailyProfile = this.getDailyProfileIndexByDay(moment.utc(measure.timeStamp, 'YYYY-MM-DD'));
    return this.dailyProfiles[currentDailyProfile][moment.utc(measure.timeStamp).hour()];
  }
}
