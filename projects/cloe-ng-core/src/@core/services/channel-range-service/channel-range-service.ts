import { Injectable } from '@angular/core';
import { ChannelRange } from './models/ChannelRange';
import { filter, forEach, get, groupBy, keys, map, set, split, range as lodashRange, findIndex } from 'lodash';
import { of, Observable } from 'rxjs';
import * as moment_ from 'moment';
import { JSONChannelRangeAlgorithm } from './models/JSONChannelRangeAlgorithm';
import { ChannelRangeAlgorithmService } from './channel-range-algorithm-service';
import { ChannelRangeTypeEnum } from '../../enums/ChannelRangeTypeEnum';
import { CloeSettings } from '../cloe.settings';
import { MsContextEnum } from '../../enums/MsContextEnum';
import { HttpClient } from '@angular/common/http';

const moment = moment_;

const CHANNEL_RANGE_PATH =  CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + '/channel-ranges';
const CHANNEL_RANGES_BY_SITE =  CHANNEL_RANGE_PATH + '/byRoot';
const CHANNEL_RANGES_BY_SITE_LIST =  CHANNEL_RANGE_PATH + '/byRootList';

@Injectable()
export class ChannelRangeService {
  channelRangeAlgorithm;

  constructor(
    private http: HttpClient
  ) {}

  createAlgorithmStringFromCSV(csvContent: string) {
    const range: any = { rangeType: ChannelRangeTypeEnum.PROFILE };
    const allTextLines = groupBy(
      filter(
        map(
          split(csvContent, /\r|\n|\r/), (row) => split(row, ';')),
        (row) => (row[0] === 'PR' || row[0] === 'FA' || row[0] === 'PI' || row[0] === 'PS' || row[0] === 'TA')
      ),
      (row) => row[0]
    );
    if (allTextLines.PR) {
      this.buildRangeBaseInfo(allTextLines.PR, range);
    }
    if (allTextLines.TA) {
      this.buildRangeRates(allTextLines.TA, range);
    }
    if (allTextLines.FA) {
      this.buildDailyProfiles(allTextLines.FA, range);
    }
    if (allTextLines.PI) {
      this.buildWeeklyProfiles(allTextLines.PI, range);
    }
    if (allTextLines.PS) {
      this.buildSpecialProfiles(allTextLines.PS, range);
    }
    return range;
  }

  buildRangeBaseInfo(rowPR, range) {
    range.name = rowPR[0][1];
    set(range, 'jsonAlgorithm.maxSamplingPeriod', +rowPR[0][2]);
    set(range, 'jsonAlgorithm.name', rowPR[0][1]);
    range.validStartDate = moment.utc([rowPR[0][4], (rowPR[0][3] - 1)]).startOf('M');
    range.validEndDate = moment.utc([rowPR[0][6], (rowPR[0][5] - 1)]).endOf('M');
  }

  buildRangeRates(rowTA, range) {
    range.rangeType = ChannelRangeTypeEnum.RATE;
    set(range, 'jsonAlgorithm.rates', []);
    forEach(rowTA, (row) => {
      range.jsonAlgorithm.rates.push({
        id: +row[1],
        name: row[2],
        color: row[3]
      });
    });
  }

  buildDailyProfiles(rowFA, range) {
    set(range, 'jsonAlgorithm.dailyProfiles', []);
    const dailyProfiles = groupBy(rowFA, (row) => row[1]);
    forEach(keys(dailyProfiles), (profileKey) => {
      const periods = [];
      forEach(get(dailyProfiles, profileKey), (profile, index) => {
        const succProfile = get(dailyProfiles, profileKey + '[' + (index + 1) + ']');
        periods.push({
          [range.rangeType === ChannelRangeTypeEnum.RATE ? 'rateId' : 'profileValue']: +profile[4],
          startPeriod: moment.utc({hour: profile[2], minute: profile[3]}).format('HH:mm'),
          endPeriod: succProfile ?
            moment.utc({hour: succProfile[2], minute: succProfile[3]}).format('HH:mm') :
            moment.utc({hour: 23, minute: 59}).format('HH:mm')
        });
      });
      range.jsonAlgorithm.dailyProfiles.push({
        id: +profileKey,
        periods: periods
      });
    });
  }

  buildWeeklyProfiles(rowPI, range) {
    set(range, 'jsonAlgorithm.weeklyProfiles', map(lodashRange(7), () => undefined));
    forEach(rowPI, (row) => {
      range.jsonAlgorithm.weeklyProfiles[row[1]] = +row[2];
    });
  }

  buildSpecialProfiles(rowPS, range) {
    set(range, 'jsonAlgorithm.specialProfiles', []);
    forEach(rowPS, (row) => {
      range.jsonAlgorithm.specialProfiles.push({
        startPeriod: moment.utc({ year: row[3], month: (row[2] - 1), day: row[1] }).format('YYYY-MM-DD'),
        endPeriod: moment.utc({ year: row[6], month: (row[5] - 1), day: row[4] }).format('YYYY-MM-DD'),
        idDailyProfile: +row[7]
      });
    });
  }

  createAlgorithm(channelRange: ChannelRange): ChannelRangeAlgorithmService {
    const jsonAlgorithm = <JSONChannelRangeAlgorithm>JSON.parse(channelRange.channelRangeAlgorithm);
    const specialProfiles: any = {};
    forEach(jsonAlgorithm.specialProfiles, (specialProfile) => {
      const specialDate = moment.utc(specialProfile.startPeriod);
      if (specialDate.year() === 0) {
        let startValidYear = moment.utc(channelRange.validStartDate).year();
        const endValidYear = moment.utc(channelRange.validEndDate).year();
        while (startValidYear <= endValidYear) {
          specialProfiles[specialDate.set('year', startValidYear++).format('YYYY-MM-DD')] = specialProfile.idDailyProfile;
        }
      } else {
        specialProfiles[specialDate.format('YYYY-MM-DD')] = specialProfile.idDailyProfile;
      }
    });
    const weeklyProfiles: any = {};
    let weeklyIndex = 0;
    forEach(jsonAlgorithm.weeklyProfiles, (weeklyProfile) => {
      weeklyProfiles[weeklyIndex++] = weeklyProfile;
    });
    const dailyProfiles: any = {};
    forEach(jsonAlgorithm.dailyProfiles, (dailyProfile: any) => {
      dailyProfiles[dailyProfile.id] = {};
      forEach(dailyProfile.periods, (dailyPeriod) => {
        let startPeriod = moment.utc(dailyPeriod.startPeriod, 'HH').hour();
        const endPeriod = moment.utc(dailyPeriod.endPeriod, 'HH').hour();
        for (startPeriod; startPeriod <= endPeriod; startPeriod++) {
          dailyProfiles[dailyProfile.id][startPeriod] = channelRange.rangeType === ChannelRangeTypeEnum.RATE ?
            dailyPeriod.rateId : dailyPeriod.profileValue;
        }
      });
    });
    this.channelRangeAlgorithm = new ChannelRangeAlgorithmService(
      specialProfiles,
      weeklyProfiles,
      dailyProfiles,
      jsonAlgorithm
    );
    return this.channelRangeAlgorithm;
  }

  upsertChannelRange(channelRange) {
    return channelRange.id ? this.http.put(CHANNEL_RANGE_PATH, channelRange) : this.http.post(CHANNEL_RANGE_PATH, channelRange);
  }

  getAllChannelRanges() {
    return this.http.get(CHANNEL_RANGE_PATH);
  }

  getChannelRangesBySite(siteUuid) {
    return this.http.get(CHANNEL_RANGES_BY_SITE, {
      params: {
        rootUuid: siteUuid
      }
    });
  }

  getChannelRangesBySiteList(siteUuidList) {
    return this.http.get(CHANNEL_RANGES_BY_SITE_LIST, {
      params: {
        rootUuidList: siteUuidList
      }
    });
  }

  addRatesHeader(
    selectedRange,
    headerRanges,
    tableHeader,
    options?: {
      fieldName?: {
        field: string;
        prefix: string;
        suffix: string;
      },
      backgroundColorField?: string
    }
    ) {
    const groupHeader = {
      columnName: selectedRange.name,
      colspan: headerRanges.length,
      isRate: true,
      children: []
    };

    const getFieldName = (header) => {
      if (options && options.fieldName) {
        return options.fieldName.prefix + header[options.fieldName.field] + options.fieldName.suffix;
      } else {
        return header['name'];
      }
    };

    const rangesList = [];
    forEach(headerRanges, headerRange => {
      const headerIndex = findIndex(tableHeader, (header: any) => {
        return header.columnName === headerRange.name;
      });
      if (headerIndex === -1) {
        const column: any = {
          fieldName: getFieldName(headerRange),
          columnName: headerRange.name,
          pipe: 'engFormatWithUnit',
          pipeArgField: 'integratedSymbol',
          backgroundColor: headerRange.color,
          nowrap: true
        };
        rangesList.push(column);
      }
    });
    groupHeader.children = rangesList;
    tableHeader.push(groupHeader);

    return tableHeader;
  }
}
