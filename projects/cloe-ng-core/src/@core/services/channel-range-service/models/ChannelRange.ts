import { ChannelRangeTypeEnum } from '../../../enums/ChannelRangeTypeEnum';

export class ChannelRange {
  id?: number;
  uuid?: string;
  name?: string;
  isDefault?: boolean;
  rangeType?: ChannelRangeTypeEnum;
  validStartDate?: string;
  validEndDate?: string;
  channelRangeAlgorithm?: string;
}
