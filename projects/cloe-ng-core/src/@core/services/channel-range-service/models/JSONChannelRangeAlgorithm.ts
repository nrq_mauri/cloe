export class JSONChannelRangeAlgorithm {
  name?: string;
  maxSamplingPeriod?: number;
  rates?: {
    id?: number,
    name?: string,
    color?: string
  }[];
  dailyProfiles?: {
    id?: number;
    periods?: {
      startPeriod?: string;
      endPeriod?: string;
      rateId?: number;
      profileValue?: number;
    } []
  } [];
  weeklyProfiles?: number[];
  specialProfiles?: {
    startPeriod?: string;
    endPeriod?: string;
    idDailyProfile?: number;
  } [];
}
