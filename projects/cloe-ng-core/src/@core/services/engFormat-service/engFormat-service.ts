import { Injectable } from '@angular/core';

const formats = {
  'default': {fromscale: -5, toscale: 5, precision: 5},
  'cosfi': {fromscale: 0, toscale: 0, precision: 3},
  'value-axis': {fromscale: -3, toscale: 3, precision: 3}
};

@Injectable()
export class EngFormatService {
  constructor() {}

   clipRange(min, max, value) {
    if (value < min) {
      value = min;
    } else if (value > max) {
      value = max;
    }
    return value;
  }

  engFormat(value, format?: string, unitSymbol?: string, formatJson?: any) {
    let formatObj;
    if (formatJson) {
      formatObj = JSON.parse(formatJson);
    }
    const key = format || 'default';
    const currentFormat = formatObj || formats[key] || formats.default;
    // costanti                    0/-6   1/-5   2/-4   3/-3  4/-2  5/-1  6/0  7/1  8/2  9/3  10/4  11/5  12/6
    const Moltiplicatori = [1e-18, 1e-15, 1e-12, 1e-9, 1e-6, 1e-3, 1e0, 1e3, 1e6, 1e9, 1e12, 1e15, 1e18];
    const MultLetters = ['f', 'p', 'n', 'µ', 'm', '', 'k', 'M', 'G', 'T', 'P'];
    const space = '&thinsp;';
    const SignChar = ' -';
    // variabili
    let negative = 0;
    let scale = 0;
    let s = '';
    let gest = 0;

    // verifica parametri
    const fromscale = this.clipRange(-5, 5, currentFormat.fromscale);
    const toscale = this.clipRange(currentFormat.fromscale, 5, currentFormat.toscale);
    const precision = this.clipRange(1, 12, currentFormat.precision);

    // se e' passato come stringa converte l'argomento in un numero o in NaN
    value = Number(value);

    // controlla e filtra NaN
    if (isNaN(value)) {
      value = 0;
      scale = 0;
      gest = 3;
    } else {
      // aggiusta il segno
      if (value < 0) {
        negative = 1;
        value = -value;
      }
      // scala il numero fino a farlo diventare compreso fra 1 e 999
      if (value < Moltiplicatori[fromscale + 6]) {
        gest = 1;  // usare toFixed(precision-1) al posto di toPrecision
        scale = fromscale;
        value /= Moltiplicatori[fromscale + 6];
      } else if (value >= Moltiplicatori[toscale + 7]) {
        value = 999.0;
        scale = toscale;
        gest = 2;
      } else if (value >= 1.0) {
        for (let i = 0; i < 5; i++) {
          if ((value >= 1000.0) && (scale < toscale)) {
            value /= 1000;
            scale++;
          } else {
            break;
          }
        }
      } else {
        for (let i = 0; i < 5; i++) {
          if ((value < 1.0) && (scale > fromscale)) {
            value *= 1000;
            scale--;
          } else {
            break;
          }
        }
      }
    }
    switch (gest) {
      case 1:
        s = value.toFixed(precision - 1);
        break;
      case 2:
        s = '>>>>>>>>>>>>';
        break;
      case 3:
        s = '------------';
        break;
      default:
        s = value.toPrecision(precision);
    }
    s = s.substr(0, precision + 1);
    if (value === 0) {
      return SignChar.charAt(negative) + s + ' ' +  (unitSymbol || '');
    }
    return SignChar.charAt(negative) + s + ' ' + MultLetters[scale + 5] + (unitSymbol || '');
  }

}
