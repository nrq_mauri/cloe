import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { of, Observable } from 'rxjs';
import {CloeSettings} from '../cloe.settings';
import { MsContextEnum } from '../../enums/MsContextEnum';

const CHANNEL_TYPE_PATH = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + '/channel-types';

@Injectable()
export class ChannelTypeService {

  constructor(private http: HttpClient) {
  }

  getChannelTypes() {
    return this.http.get(CHANNEL_TYPE_PATH)
      .pipe(
        catchError( (err) => {
          console.log(err);
          return of(err);
        })
      );
  }

  getChannelTypeById(channelTypeID) {
    return this.http.get(CHANNEL_TYPE_PATH + '/' + channelTypeID)
      .pipe(
        catchError( (err) => {
          console.log(err);
          return of(err);
        })
      );
  }
}
