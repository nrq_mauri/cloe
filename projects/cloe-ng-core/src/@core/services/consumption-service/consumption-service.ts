import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {CloeSettings} from '../cloe.settings';
import {Observable} from 'rxjs';
import {MsContextEnum} from '../../enums/MsContextEnum';

const CONSUMPTION_SITE = '/site/DAY';
const CONSUMPTION_CLUSTER = '/cluster/DAY';
const BASE_PATH = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT) + '/consumption';

@Injectable()
export class ConsumptionService {

  constructor(private http: HttpClient) {}

  getConsumptionsBySite(siteUuid, user) {
    return this.http.get(BASE_PATH + CONSUMPTION_SITE + '/' + siteUuid + '/' + user.cloeUser.uuid);
  }

  getConsumptionByCluster(treeUuid, user) {
    return this.http.get(BASE_PATH + CONSUMPTION_CLUSTER + '/' + treeUuid + '/' + user.cloeUser.uuid);
  }
}

