import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpResponse } from '@angular/common/http';
import { CloeSettings } from '../cloe.settings';
import { Observable } from 'rxjs';
import { MsContextEnum } from '../../enums/MsContextEnum';
import { DiagStatusEnum } from '../../enums/DiagStatus';
import { map } from 'rxjs/operators';
import { map as _map, keys, keyBy } from 'lodash';
import { DiagFileTypeEnum } from '../../enums/DiagFileTypeEnum';

const DIAGNOSIS_PATH = '/diagnosis';
const DIAG_DESCRIPTIONS_API = CloeSettings.baseApiPath(MsContextEnum.DIAGNOSIS) + '/diag-descriptions';
const DIAG_LIST_API = CloeSettings.baseApiPath(MsContextEnum.DIAGNOSIS) + DIAGNOSIS_PATH + '/by-user';
const DIAG_FIND_ONE = CloeSettings.baseApiPath(MsContextEnum.DIAGNOSIS) + DIAGNOSIS_PATH + '/by-code';
const DIAG_CREATE_API = CloeSettings.baseApiPath(MsContextEnum.DIAGNOSIS) + DIAGNOSIS_PATH + '/create';
const DIAG_UPDATE_API = CloeSettings.baseApiPath(MsContextEnum.DIAGNOSIS) + DIAGNOSIS_PATH + '/update';
const DIAG_TYPES_PATH = '/diag-types';
const DIAG_FILES_PATH = CloeSettings.basePath(MsContextEnum.DIAGNOSIS) + '/diag-files';
const DIAG_DOWNLOAD_API = CloeSettings.baseApiPath(MsContextEnum.DIAGNOSIS) + DIAGNOSIS_PATH + '/download';
const DIAG_FILE_LIST = CloeSettings.baseApiPath(MsContextEnum.DIAGNOSIS) + DIAGNOSIS_PATH + '/file-list';
const DIAG_FILE = CloeSettings.baseApiPath(MsContextEnum.DIAGNOSIS) + DIAGNOSIS_PATH + '/file';
const DIAG_NEW_FILE = CloeSettings.baseApiPath(MsContextEnum.DIAGNOSIS) + DIAGNOSIS_PATH + '/upload-new-file';
const DIAG_VALIDATE_THEN_UPLOAD = CloeSettings.baseApiPath(MsContextEnum.DIAGNOSIS) + DIAGNOSIS_PATH + '/validate-then-upload';
const DIAG_NEW_JSON_FILE = CloeSettings.baseApiPath(MsContextEnum.DIAGNOSIS) + DIAGNOSIS_PATH + '/upload-new-json-file';

@Injectable()
export class DiagnosisService {
  constructor(private http: HttpClient) {
  }

  getDiagListByUser(user) {
    return this.http.get(`${DIAG_LIST_API}/${user.cloeUser.uuid}`);
  }

  getFileDescriptions() {
    return this.http.get(DIAG_DESCRIPTIONS_API);
  }

  saveDiag(diag: any) {
    const url = CloeSettings.baseApiPath(MsContextEnum.DIAGNOSIS) + '/diagnoses';
    return this.http.put(url, diag);
  }

  getDiagByCode(diagCode) {
    const url = `${DIAG_FIND_ONE}/${diagCode}`;
    return this.http.get(url);
  }

  createDiag(file, user, diagName) {
    const formData: FormData = new FormData();
    formData.append('uploadedFile', file);
    formData.append('name', diagName);
    formData.append('userUuid', user.cloeUser.uuid);
    return this.http.post(DIAG_CREATE_API, formData);
  }

  updateDiag(diagStatus: DiagStatusEnum, diagCode: string, comment_path?: string) {
    const url = `${DIAG_UPDATE_API}/${diagStatus}/${diagCode}`;
    return this.http.put(url, {
      comment_path: comment_path ? comment_path : null
    });
  }

  downloadDiagFile(diagCode, fileName: string, fileType: DiagFileTypeEnum) {
    const url = `${DIAG_DOWNLOAD_API}/${diagCode}`;
    const params = {
      fileName,
      fileType
    };
    return this.http.get(url, {params, responseType: 'blob'});
  }

  downloadDiagJSONFile(diagCode, fileName) {
    const url = `${DIAG_DOWNLOAD_API}/${diagCode}`;
    const params = {
      fileName,
      fileType: DiagFileTypeEnum.OUTCOMES
    };
    return this.http.get(url, {params, responseType: 'json'});
  }

  getDiagFile(diagCode, fileName) {
    return this.http.get(this.getDiagFileUrl(diagCode, fileName));
  }

  getDiagFileUrl(diagCode, fileName) {
    return `${DIAG_FILES_PATH}/${diagCode}/${fileName}`;
  }


  getFileMeta(diagCode, fileName) {
    const url = `${DIAG_FILE}/${diagCode}`;
    const params = {'fileName': fileName};
    return this.http.get(url, {params}).pipe(map(
      (r: any) => {
        if (!r || !r.file) {
          return '';
        }
        const ext = this._parseExtension(r.file[0]);
        return {
          type: ext,
          diagCode,
          fileName: r.file[0].split('\/').pop(),
          isImage: this.isImage(ext)
        };
      }
    ));
  }

  isImage(ext) {
    const extMin = ext.toLowerCase();
    switch (extMin) {
      case 'png':
      case 'jpg':
      case 'jpeg':
        return true;
      default:
        return false;
    }
  }

  private _parseExtension(url) {
    const parts = url.split('\.');
    if (parts.length > 1) {
      return parts.pop();
    }

    return '';
  }

  uploadNewFile(file, diagCode, fileType: DiagFileTypeEnum) {
    const formData: FormData = new FormData();
    formData.append('uploadedFile', file);
    return this.http.post(DIAG_NEW_FILE, formData, {
      params: {
        diagCode,
        fileType
      }
    });
  }

  validateThenUpload(file, diagCode, fileType: DiagFileTypeEnum) {
    const formData: FormData = new FormData();
    formData.append('uploadedFile', file);
    return this.http.post(DIAG_VALIDATE_THEN_UPLOAD, formData, {
      params: {
        diagCode,
        fileType
      }
    });
  }

  updateMasterExcel(file, diagCode) {
    const formData: FormData = new FormData();
    formData.append('uploadedFile', file);
    const url = DIAG_CREATE_API + '/' + diagCode;
    return this.http.post(url, formData);
  }

  uploadJSONComment(jsonData, diagCode) {
    const formData: FormData = new FormData();
    formData.append('diagCode', diagCode);
    formData.append('jsonComment', JSON.stringify(this.buildJsonCommentParam(jsonData, diagCode)));
    return this.http.post(DIAG_NEW_JSON_FILE, formData);
  }

  private buildJsonCommentParam(jsonData, diagCode) {
    const jsonDataObject = JSON.parse(jsonData);
    const comments = _map(jsonDataObject, (v, k) => {
      return {
        'key': k,
        'value': v.comment,
        'imageName': v.removable ? v.fileName : undefined
      };
    });
    return {
      idDiagnosi: diagCode,
      timestamp: new Date().getTime(),
      comments: comments
    };
  }

  getDiagFileList(diagCode) {
    const url = DIAG_FILE_LIST + '/' + diagCode;
    return this.http.get(url).pipe(map(r => r['object_names']));
  }
}
