import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { map, differenceBy } from 'lodash';
import { of, Observable } from 'rxjs';
import { CloeSettings } from '../cloe.settings';
import { StatusEnum } from '../../enums/StatusEnum';

const TAG_PATH = '/tags';
const TAG_API = CloeSettings.baseApiPath() + TAG_PATH;
const TAGS_BY_REFER_UUID_API = TAG_API + '/byReferUuid';
const NEW_TAG_LIST_API = TAG_API + '/upsert/fromList';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class TagService {

  constructor(private http: HttpClient) {
  }

  getTagsByReferUuid(referUuid: string) {
    return this.http.get(TAGS_BY_REFER_UUID_API + '/' + referUuid);
  }

  upsertTagList(currentTags, tagsToUpdate, objectUuid, objectNature) {
    return this.http.post(
      NEW_TAG_LIST_API,
      this.buildUpsertTagsDTO(currentTags, tagsToUpdate, objectUuid, objectNature)
    );
  }

  private buildUpsertTagsDTO(currentTags, tagsToUpdate, objectUuid, objectNature) {
    return {
      tagsToUpdateList: map(tagsToUpdate, (tagModel) => {
        const tag: any = {};
        if (tagModel.id && tagModel.uuid && tagModel.referUuid === objectUuid) {
          tag.id = tagModel.id;
          tag.uuid = tagModel.uuid;
        }
        tag.referUuid = objectUuid;
        tag.nodeNature = objectNature;
        tag.status = StatusEnum.ACTIVE;
        tag.tag = tagModel.tag;
        return tag;
      }),
      tagsToDeleteList: differenceBy(currentTags, tagsToUpdate, 'id')
    };
  }

  upsertTag(tag) {
    if (!tag.id) {
      return this.http.post(TAG_API, tag, httpOptions);
    }
    return this.http.put(TAG_API, tag, httpOptions);
  }

  deleteTag(metadataId) {
    return this.http.delete(TAG_API + '/' + metadataId)
      .pipe(
        catchError((err) => {
          return of(err);
        })
      );
  }

}
