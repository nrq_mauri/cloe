import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CloeSettings } from '../cloe.settings';
import { catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { MsContextEnum } from '../../enums/MsContextEnum';
import { find, head } from 'lodash';
import { Observable } from 'rxjs';

const BASE_PATH = CloeSettings.baseApiPath(MsContextEnum.MANAGEMENT);
const NEW_SITE_PATH = '/sites';
const NEW_SITE_API = BASE_PATH + NEW_SITE_PATH;
const TREES_BY_SITE = BASE_PATH + '/site-trees/bySite';

const httpOptions = {
  headers: new HttpHeaders({'Content-Type': 'application/json'})
};

@Injectable()
export class SiteService {

  constructor(private http: HttpClient) {
  }

  upsertSite(site) {
    if (!site.id) {
      return this.http.post(NEW_SITE_API, site, httpOptions);
    }
    return this.http.put(NEW_SITE_API, site, httpOptions);
  }

  deleteSite(siteId) {
    return this.http.delete(NEW_SITE_API + '/' + siteId)
      .pipe(
        catchError((err) => {
          return of(err);
        })
      );
  }

  getSiteById(id: number) {
    return this.http.get(`${NEW_SITE_API}/${id}`);
  }
}
