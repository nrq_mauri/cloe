import { Injectable } from '@angular/core';
import { TreeFiltersTypeEnum } from './enums/TreeFiltersTypeEnum';
import { CloeTreeChannelFiltersService } from './cloe-tree-channel-filters.service';
import { CloeTreeSitesFiltersService } from './cloe-tree-sites-filters.service';

@Injectable()
export class CloeTreeFiltersService {

  constructor(
    private treeChannelFiltersService: CloeTreeChannelFiltersService,
    private treeSitesFiltersService: CloeTreeSitesFiltersService
  ) {}

  public addFiltersByType(treeFiltersType: TreeFiltersTypeEnum, nodes: any[]) {
    switch (treeFiltersType) {
      case TreeFiltersTypeEnum.SITE_FILTERS:
        return this.treeSitesFiltersService.addSitesTypeFilters(nodes);
      case TreeFiltersTypeEnum.CHANNEL_FILTERS:
        return this.treeChannelFiltersService.addChannelTypeFilters(nodes);
    }
  }
}
