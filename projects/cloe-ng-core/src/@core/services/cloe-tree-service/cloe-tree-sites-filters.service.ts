import { FilterTypeEnum } from '../../enums/FilterTypeEnum';
import { uniq, split, map, uniqBy } from 'lodash';
import { Injectable } from '@angular/core';

@Injectable()
export class CloeTreeSitesFiltersService {

  addSitesTypeFilters(nodes: any[]) {
    const filterData = this.getFilterData({
      metadata: [],
      channels: [],
      tags: []
    }, nodes[0]);
    const filtersConfig = [];
    this.addSitesFilter(filtersConfig, filterData);
    this.addCityFilter(filtersConfig);
    this.addMetadataFilter(filterData, filtersConfig);
    this.addTagsFilter(filterData, filtersConfig);
    this.addChannelTypeFilter(filterData, filtersConfig);
    return filtersConfig;
  }

  private addSitesFilter(filtersConfig, filterData) {
    filtersConfig.push({
      categoryName: 'component.site-picker.site',
      categoryBase: 'referObject',
      filters: [
        {
          filterType: FilterTypeEnum.TEXT,
          filterField: 'name',
          fieldLabel: 'global.name'
        },
        {
          filterType: FilterTypeEnum.DROPDOWN,
          filterField: 'status',
          fieldLabel: 'global.status',
          items: ['ACTIVE', 'DISABLE', 'CANCELLED']
        }
      ]
    });
  }
  private addCityFilter(filtersConfig) {
    filtersConfig.push({
      categoryName: 'component.site-picker.position',
      categoryBase: 'referObject',
      filters: [
        {
          filterType: FilterTypeEnum.TEXT,
          filterField: 'cityName',
          fieldLabel: 'component.site-picker.city'
        },
        {
          filterType: FilterTypeEnum.TEXT,
          filterField: 'provinceName',
          fieldLabel: 'component.site-picker.province'
        },
        {
          filterType: FilterTypeEnum.TEXT,
          filterField: 'regionName',
          fieldLabel: 'component.site-picker.region'
        }
      ]
    });
  }

  private addChannelTypeFilter(filterData, filtersConfig) {
    filtersConfig.push({
      categoryName: 'global.channel',
      categoryBase: 'referObject.channels',
      filters: [
        {
          filterType: FilterTypeEnum.TEXT,
          filterField: 'name',
          fieldLabel: 'global.name'
        },
        {
          filterType: FilterTypeEnum.DROPDOWN,
          filterField: 'channelTypeName',
          fieldLabel: 'component.site-picker.channelType',
          items: uniq(map(filterData.channels, 'channelTypeName'))
        }
      ]
    });
  }

  private addMetadataFilter(filterData, filtersConfig) {
    filtersConfig.push({
      categoryName: 'global.metadata',
      categoryBase: 'metadata',
      filters: [
        {
          filterType: FilterTypeEnum.METADATA,
          items: uniqBy(map(filterData.metadata), 'metadataKey')
        }
      ]
    });
  }

  private addTagsFilter(filterData, filtersConfig) {
    filtersConfig.push({
      categoryName: 'component.table.tags',
      categoryBase: 'tags',
      filters: [
        {
          filterType: FilterTypeEnum.DROPDOWN,
          filterField: 'tag',
          fieldLabel: 'global.name',
          items: map(filterData.tags, 'tag')
        }
      ]
    });
  }

  private getFilterData(filterData: any, root) {
    if (root.referObject && root.referObject.channels) {
      filterData.channels.push(...root.referObject.channels);
    }
    if (root.metadata && root.metadata.length > 0) {
      filterData.metadata.push(...root.metadata);
    }
    if (root.tags && root.tags.length > 0) {
      filterData.tags.push(...root.tags);
    }
    root.children.forEach((node) => {
      this.getFilterData(filterData, node);
    });
    return filterData;
  }
}
