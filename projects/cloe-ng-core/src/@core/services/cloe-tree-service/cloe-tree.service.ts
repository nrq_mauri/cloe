import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {CloeSettings} from '../cloe.settings';
import {catchError} from 'rxjs/operators';
import {of, Observable} from 'rxjs';

const CLOE_TREE_PATH = '/cloeTree';
const TREE_API = CLOE_TREE_PATH + '/_tree';
const ALL_TREE_API = CLOE_TREE_PATH + '/_allTree';

@Injectable()
export class CloeTreeService {
  constructor(private http: HttpClient) {
  }

  searchTreeListForUser(natureType, userUuid?, isCluster?) {
    return this.http.get(
      CloeSettings.getBaseApiPath(
        natureType,
        userUuid ? TREE_API + '/' + natureType + '/' + userUuid : ALL_TREE_API + '/' + natureType),
      {
        params: {
          isCluster: (!!isCluster).toString()
        }
      })
      .pipe(
        catchError( (err) => {
          console.log(err);
          return of(err);
        })
      );
  }
}
