import { FilterTypeEnum } from '../../enums/FilterTypeEnum';
import { uniq, split, map, uniqBy } from 'lodash';
import { Injectable } from '@angular/core';

@Injectable()
export class CloeTreeChannelFiltersService {

  addChannelTypeFilters(nodes: any[]) {
    const filterData = this.getFilterData({
      readIds: [],
      deviceTypes: [],
      channelTypeNames: [],
      measureUnits: [],
      deviceSerials: [],
      tags: []
    }, nodes[0]);
    const filtersConfig = [];
    this.addChannelFilters(filterData, filtersConfig);
    this.addMetadataFilter(filterData, filtersConfig);
    this.addDeviceFilters(filterData, filtersConfig);
    this.addTagsFilter(filterData, filtersConfig);
    return filtersConfig;
  }

  private getFilterData(filterData: any, root) {
    if (root.referObject) {
      if (root.referObject.readIdChannel) {
        filterData.readIds.push(root.referObject.readIdChannel);
      }
      if (root.referObject.deviceDTO) {
        if (root.referObject.deviceDTO.deviceTypeName) {
          filterData.readIds.push(root.referObject.deviceDTO.deviceTypeName);
        }
        if (root.referObject.deviceDTO.deviceSerial) {
          filterData.deviceSerials.push(root.referObject.deviceDTO.deviceSerial);
        }
      }
      if (root.referObject.channelTypeName) {
        filterData.channelTypeNames.push(root.referObject.channelTypeName);
      }
      if (root.referObject.unitSymbol) {
        filterData.measureUnits.push(root.referObject.unitSymbol);
      }
    }
    if (root.tags) {
      root.tags = split(root.tags, ',');
    }
    root.children.forEach( (node) => {
      this.getFilterData(filterData, node);
    });
    return filterData;
  }

  private addChannelFilters(filterData, filtersConfig) {
    filtersConfig.push({
      categoryName: 'global.channel',
      categoryBase: 'referObject',
      filters: [
        {
          filterType: FilterTypeEnum.TEXT,
          filterField: 'name',
          fieldLabel: 'global.name'
        },
        {
          filterType: FilterTypeEnum.DROPDOWN,
          filterField: 'status',
          fieldLabel: 'global.status',
          items: ['ACTIVE', 'DISABLE', 'CANCELLED']
        },
        {
          filterType: FilterTypeEnum.DROPDOWN,
          filterField: 'channelTypeName',
          fieldLabel: 'component.site-picker.channelType',
          items: uniq(filterData.channelTypeNames)
        },
        {
          filterType: FilterTypeEnum.DROPDOWN,
          filterField: 'readIdChannel',
          fieldLabel: 'page.channel-tree-view.readId',
          items: uniq(filterData.readIds)
        },
        {
          filterType: FilterTypeEnum.DROPDOWN,
          filterField: 'unitSymbol',
          fieldLabel: 'page.channel-tree-view.measure-unit',
          items: uniq(filterData.measureUnits)
        }
      ]
    });
  }

  private addDeviceFilters(filterData, filtersConfig) {
    filtersConfig.push({
      categoryName: 'global.device',
      categoryBase: 'deviceDTO',
      filters: [
        {
          filterType: FilterTypeEnum.DROPDOWN,
          filterField: 'deviceTypeName',
          fieldLabel: 'page.channel-tree-view.device-type',
          items: uniq(filterData.deviceTypes)
        },
        {
          filterType: FilterTypeEnum.DROPDOWN,
          filterField: 'deviceSerial',
          fieldLabel: 'page.channel-tree-view.device-serial',
          items: uniq(filterData.deviceSerials)
        }
      ]
    });
  }

  private addMetadataFilter(filterData, filtersConfig) {
    filtersConfig.push({
      categoryName: 'global.metadata',
      categoryBase: 'metadata',
      filters: [
        {
          filterType: FilterTypeEnum.METADATA,
          items: uniqBy(map(filterData.metadata), 'metadataKey')
        }
      ]
    });
  }

  private addTagsFilter(filterData, filtersConfig) {
    filtersConfig.push({
      categoryName: 'component.table.tags',
      categoryBase: 'tags',
      filters: [
        {
          filterType: FilterTypeEnum.DROPDOWN,
          filterField: 'tag',
          fieldLabel: 'global.name',
          items: map(filterData.tags, 'tag')
        }
      ]
    });
  }
}
