import { Component } from '@angular/core';

@Component({
  selector: 'cloe-core-page-root',
  template: `
    <ui-view></ui-view>
    <cloe-core-loader></cloe-core-loader>
  `,
})
export class PageRootComponent {
}
