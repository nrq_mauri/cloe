import { UIRouter } from '@uirouter/core';
import { Visualizer } from '@uirouter/visualizer';
import { requiresAuthHook } from './auth.hook';

export function routerConfigFn(router: UIRouter) {
  const transitionService = router.transitionService;
  // router.plugin(Visualizer);
  requiresAuthHook(transitionService);
}
