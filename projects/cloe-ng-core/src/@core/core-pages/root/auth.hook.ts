import { TransitionService } from '@uirouter/core';
import { indexOf } from 'lodash';
import { LoginService } from '../../services/login-service/login.service';
import { StorageService } from '../../services/storage-service/storage.service';

/**
 * This file contains a Transition Hook which protects a
 * route that requires authentication.
 *
 * This hook redirects to /login when both:
 * - The user is not authenticated
 * - The user is navigating to a state that requires authentication
 */
export function requiresAuthHook(transitionService: TransitionService) {
  // Matches if the destination state's data property has a truthy 'requiresAuth' property
  const requiresAuthCriteria = {
    to: state => state.data && state.data.requiresAuth
  };

  // Function that returns a redirect for the current transition to the login state
  // if the user is not currently authenticated (according to the AuthService)

  const redirectToLogin = trans => {
    const loginService: LoginService = trans.injector().get(LoginService);
    const $state = trans.router.stateService;
    if (!loginService.isAuthenticated()) {
      return $state.target('login', undefined, {location: false});
    }
  };

  const forbidden = trans => {
    const $state = trans.router.stateService;
    const permissions = StorageService.getPermissions();
    const toState = trans.$to();
    const customLandingPage = StorageService.getCurrentUser().cloeUser.customLandingPage;
    const customLandingPagePermission = indexOf(permissions, customLandingPage) > -1;
    const hasDirectPermission = indexOf(permissions, toState.name) > -1;
    const hasInheritedPermission = !!toState.data &&
      !!toState.data.stateAs &&
      indexOf(permissions, toState.data.stateAs) > -1;
    if (toState.name === 'home') {
      if (customLandingPagePermission) {
        return $state.target(customLandingPage, undefined, {location: false});
      }
    }
    if (!(hasDirectPermission || hasInheritedPermission)) {
      if (toState.name === 'home' && permissions.length > 0) {
        return $state.target(permissions[0], undefined, {location: false});
      }
      return $state.target('forbidden', undefined, {location: false});
    }
  };

  // Register the "requires auth" hook with the TransitionsService
  transitionService.onBefore(requiresAuthCriteria, redirectToLogin, {priority: 10});
  transitionService.onBefore(requiresAuthCriteria, forbidden, {priority: 0});
  // transitionService.onBefore(requiresAuthCriteria, chartHook, {priority: 5});

}
