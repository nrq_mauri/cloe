import { NgModule } from '@angular/core';
import { RootModule, UIRouterModule } from '@uirouter/angular';
import { PageRootComponent } from './root.component';
import { routerConfigFn } from './router.config';
import { CloeUiModule } from '../../@ui/cloe-ui.module';

const state = {
  name: 'app',
  abstract: true,
  component: PageRootComponent
};

const rootModule: RootModule = {
  states: [state],
  useHash: true,
  config: routerConfigFn,
  otherwise: { state: '404' },
};

@NgModule({
  imports: [UIRouterModule.forRoot(rootModule), CloeUiModule],
  declarations: [PageRootComponent],
  exports: [UIRouterModule]
})
export class PageRootModule {
}
