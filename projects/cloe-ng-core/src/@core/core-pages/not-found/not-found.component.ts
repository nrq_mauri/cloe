import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'cloe-core-page-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class NotFoundComponent {
  constructor() {}
}
