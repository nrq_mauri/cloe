import { NgModule } from '@angular/core';
import { UIRouterModule } from '@uirouter/angular';
import { NotFoundComponent } from './not-found.component';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

const state = {
  name: '404',
  url: '/404',
  parent: 'app',
  component: NotFoundComponent
};

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [state]}),
    TranslateModule.forChild(),
    CommonModule
  ],
  declarations: [NotFoundComponent]
})
export class NotFoundModule {}
