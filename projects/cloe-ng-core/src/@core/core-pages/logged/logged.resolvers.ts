import { ResolvableLiteral } from '@uirouter/angular';
import { UserInitService } from '../../services/user-init-service/user-init-service';

export function userBaseInfoResolverFn(userInitService) {
  return userInitService.getUserInitialInfo().toPromise();
}

export function userResolverFn(userBaseInfo) {
  return userBaseInfo.user;
}

export function menuResolverFn(userBaseInfo: any) {
  const menu = userBaseInfo.menu;
  if (!menu) {
    return [];
  }
  return userBaseInfo.menu.children;
}

export function customerResolverFn(userBaseInfo) {
  return userBaseInfo.customer;
}

const userBaseInfoResolver: ResolvableLiteral = {
  token: 'userBaseInfo',
  resolveFn: userBaseInfoResolverFn,
  deps: [UserInitService]
};

const userResolver = {
  token: 'user',
  resolveFn: userResolverFn,
  deps: ['userBaseInfo']
};

const menuResolver = {
  token: 'menu',
  resolveFn: menuResolverFn,
  deps: ['userBaseInfo']
};

const customerResolver = {
  token: 'customer',
  resolveFn: customerResolverFn,
  deps: ['userBaseInfo']
};

export const LOGGED_RESOLVERS = [
  userBaseInfoResolver,
  userResolver,
  menuResolver,
  customerResolver
];

