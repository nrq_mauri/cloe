import { NgModule } from '@angular/core';
import { StatesModule, UIRouterModule } from '@uirouter/angular';
import { PageLoggedComponent } from './logged.component';
import { CloeUiModule } from '../../@ui/cloe-ui.module';
import { LOGGED_RESOLVERS } from './logged.resolvers';

const stateModule: StatesModule = {
  states: [
    {
      name: 'logged',
      parent: 'app',
      data: {
        requiresAuth: true
      },
      abstract: true,
      component: PageLoggedComponent,
      resolve: [
        ...LOGGED_RESOLVERS
      ]
    }
  ]
};

@NgModule({
  imports: [
    UIRouterModule.forChild(stateModule),
    CloeUiModule
  ],
  declarations: [PageLoggedComponent],
  exports: [UIRouterModule]
})
export class PageLoggedModule {
}
