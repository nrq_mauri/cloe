import { Component, HostListener, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { SubjectsService } from '../../services/resize-service/subjects.service';
import { StorageService } from '../../services/storage-service/storage.service';

@Component({
  selector: 'cloe-core-page-logged',
  templateUrl: './logged.component.html',
  styleUrls: ['./logged.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PageLoggedComponent implements OnInit {

  isMenuClosed = true;
  sideNavTransitionDuration = 300;

  @Input() menu;
  @Input() user;
  @Input() customer;
  @Input() site;
  @Input() siteTree;

  private styleSheetUrl: string;

  constructor(
    private resizeService: SubjectsService,
    private translateService: TranslateService
  ) {

    this.onResize();
  }

  @HostListener('window:resize', [])
  onResize() {
    this.resizeService.resizeInformer$.next();
    if (this.isSmallWidth()) {
      this.isMenuClosed = true;
      setTimeout(() => this.resizeService.resizeInformer$.next(), this.sideNavTransitionDuration + 700);
    }
  }

  private isSmallWidth() {
    return window.innerWidth < 700;
  }

  ngOnInit(): void {
    const lang = StorageService.getCurrentLang();
    if (lang) {
      this.translateService.use(lang);
    } else if (this.user.langKey) {
      this.translateService.use(this.user.langKey);
    }
    // fallback IT defined on app.component.ts
  }
}
