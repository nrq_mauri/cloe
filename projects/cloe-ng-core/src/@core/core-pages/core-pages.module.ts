import { NgModule } from '@angular/core';
import { PageRootModule } from './root/root.module';
import { PageLoggedModule } from './logged/logged.module';
import { NotFoundModule } from './not-found/not-found.module';
import { ForbiddenModule } from './forbidden/forbidden.module';
import { CloeUiModule } from '../@ui/cloe-ui.module';
import { UserProfileModule } from './user-profile/user-profile.module';

const PAGE_MODULES = [
  PageRootModule,
  PageLoggedModule,
  NotFoundModule,
  ForbiddenModule,
  UserProfileModule
];

@NgModule({
  imports: [...PAGE_MODULES],
  exports: [ CloeUiModule ]
})
export class CorePagesModule {
}
