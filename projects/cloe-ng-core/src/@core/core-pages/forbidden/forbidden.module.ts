import { NgModule } from '@angular/core';
import { UIRouterModule } from '@uirouter/angular';
import { ForbiddenComponent } from './forbidden.component';
import { CommonModule } from '@angular/common';
import { TranslateModule } from '@ngx-translate/core';

const state = {
  name: 'forbidden',
  parent: 'app',
  url: '/403',
  component: ForbiddenComponent
};

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [state]}),
    TranslateModule.forChild(),
    CommonModule
  ],
  declarations: [ForbiddenComponent]
})
export class ForbiddenModule {}
