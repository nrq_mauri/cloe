import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'cloe-core-page-not-found',
  templateUrl: './forbidden.component.html',
  styleUrls: ['./forbidden.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ForbiddenComponent {
  constructor() {}
}
