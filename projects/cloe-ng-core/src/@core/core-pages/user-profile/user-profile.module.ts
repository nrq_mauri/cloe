import { NgModule } from '@angular/core';
import { UIRouterModule } from '@uirouter/angular';
import { CloeUiModule } from '../../@ui/cloe-ui.module';
import { UserProfileComponent } from './user-profile.component';

const state = {
  name: 'user-profile',
  parent: 'logged',
  url: '/user-profile',
  component: UserProfileComponent
};

@NgModule({
  imports: [
    UIRouterModule.forChild({ states: [state] } ),
    CloeUiModule
  ],
  declarations: [UserProfileComponent]
})
export class UserProfileModule {}
