import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { StorageService } from '../../services/storage-service/storage.service';
import * as moment_ from 'moment';
import { WeekDaysEnum } from '../../@ui/components/cloe-date-time-picker/enums/WeekDaysEnum';
import { findIndex } from 'lodash';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from '../../services/user-service/user-service';
import { NgModel } from '@angular/forms';
import { AccountService } from '../../services/account-service/account.service';
import { StateService } from '@uirouter/angular';
import { ModalService } from '../../services/modal-service/modal-service';
import { ConfirmModalComponent } from '../../@ui/components/modals';

const moment = moment_;

@Component({
  selector: 'cloe-core-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class UserProfileComponent implements OnInit {

  user;
  localDays: string[] = moment.weekdays();
  globalDays = Object.values(WeekDaysEnum);
  selectedFirstDay;
  passwordDTO;

  constructor(
    public translate: TranslateService,
    private userService: UserService,
    private stateService: StateService,
    private modalService: ModalService,
    private accountService: AccountService
  ) {
  }

  ngOnInit() {
    this.user = StorageService.getCurrentUser();
    this.selectedFirstDay = this.localDays
      [
      findIndex(this.globalDays, (day) => {
        return day === this.user.cloeUser.startWeek;
      })
      ];
  }

  onSubmit() {
    this.user.cloeUser.startWeek = this.globalDays
      [
      findIndex(this.localDays, (day) => {
        return day === this.selectedFirstDay;
      })
      ];
    this.userService.upsertUser(this.user).subscribe(resp => {
      StorageService.setCurrentUser(this.user);
    });
    if (this.passwordDTO && this.passwordDTO.oldPassword && this.passwordDTO.oldPassword !== '') {
      this.accountService.changePassword(
        {
          currentPassword: this.passwordDTO.oldPassword,
          newPassword: this.passwordDTO.newPassword
        }
      ).subscribe((res: any) => {
        const message = 'component.change-password.' + res.message;
        this.modalService.open(ConfirmModalComponent, {keyboard: false}).then(result => {
          if (result && res.success) {
            this.stateService.go(this.stateService.$current, {}, {reload: true});
          }
        }).catch(() => {
          // dummy catch, we don't use it for now
        });
        this.modalService.getModalInstance().componentInstance.confirmMessage = message;
      });
    }
  }

  compareWeekDays(r1, r2): boolean {
    return r1 && r2 ? r1.id === r2.id : r1 === r2;
  }

  getErrorMessage(changePWInput: NgModel) {
    return changePWInput.errors && changePWInput.errors.message ? this.translate.instant(changePWInput.errors.message) : '';
  }
}
