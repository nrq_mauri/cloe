import { NgModule } from '@angular/core';
import { LoginComponent } from './login.component';
import { UIRouterModule } from '@uirouter/angular';
import { CloeCoreModule } from '../../cloe-core.module';
import { CloeUiModule } from '../../@ui/cloe-ui.module';

const state = {
  name: 'login',
  parent: 'app',
  url: '/login',
  component: LoginComponent
};


@NgModule({
  imports: [
    UIRouterModule.forChild({states: [state]}),
    CloeUiModule
  ],
  declarations: [LoginComponent]
})
export class LoginModule {
}
