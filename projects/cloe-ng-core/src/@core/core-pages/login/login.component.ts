import { Component, Inject, OnInit, Optional, ViewEncapsulation } from '@angular/core';
import { StateService } from '@uirouter/angular';
import { LoginService } from '../../services/login-service/login.service';


@Component({
  selector: 'cloe-core-page-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit {
  model = {
    rememberMe: false
  };

  constructor(
    private loginService: LoginService,
    private $state: StateService,
    @Inject('CorePagesConfig') private corePageConfig
  ) {}

  ngOnInit() {
  }

  onSubmit() {
    this.loginService.doLogin(this.model, this.corePageConfig.feType).subscribe(success => {
      if (success) {
        this.$state.go('home');
      }
    });
  }

}
