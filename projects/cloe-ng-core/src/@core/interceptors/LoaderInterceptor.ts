import {Injectable} from '@angular/core';
import { HttpBackend, HttpErrorResponse, HttpEvent, HttpHandler, HttpInterceptor, HttpRequest } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { SubjectsService } from '../services/resize-service/subjects.service';
import { Observer } from 'rxjs/internal/types';
import {catchError, finalize, map} from 'rxjs/operators';
import { ConfirmModalComponent } from '../@ui/components/modals';
import { ModalService } from '../services/modal-service/modal-service';
import { StateService } from '@uirouter/angular';

@Injectable()
export class LoaderInterceptor implements HttpInterceptor {

  constructor(
    private subjectsService: SubjectsService,
    private backendHanlder:  HttpBackend,
    private modalService: ModalService,
    private stateService: StateService
  ) {}

  pendingRequest = 0;

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    this.pendingRequest++;
    this.subjectsService.httpResquestPendingInformer$.next(this.pendingRequest);
    return next.handle(req).pipe(
      map(event => {
        return event;
      }),
      catchError(error => {
        this.handleAuthError(error);
        return of(error);
      }),
      finalize(() => {
        this.pendingRequest--;
        this.subjectsService.httpResquestPendingInformer$.next(this.pendingRequest);
      })
    );
  }

  private handleAuthError(err: HttpErrorResponse) {
    if (err.status === 401) {
      if (this.stateService.$current.name !== 'login') {
        // this.stateService.transition.redirect(this.stateService.target('login', null, {location: true, reload: true}));
        this.stateService.transition.abort();
        this.stateService.go('login');
      }

      const message = 'page.login.login-error';
      setTimeout(() => {
        this.modalService.open(ConfirmModalComponent, {keyboard: false}).then(result => {
        }).catch(() => {
          // dummy catch, we don't use it for now
        });
        this.modalService.getModalInstance().componentInstance.confirmMessage = message;
      }, 500);
    }
  }
}
