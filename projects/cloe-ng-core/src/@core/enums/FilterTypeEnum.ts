export enum FilterTypeEnum {
  TEXT = 'TEXT',
  NUMBER = 'NUMBER',
  ISNULL = 'ISNULL',
  BOOLEAN = 'BOOLEAN',
  DROPDOWN = 'DROPDOWN',
  METADATA = 'METADATA'
}

export enum FilterNumberCompareEnum {
  LE = 'LE', LT = 'LT', GE = 'GE', GT = 'GT', EQ = 'EQ', BTW = 'BTW', NOT_EQ = 'NOT_EQ'
}
