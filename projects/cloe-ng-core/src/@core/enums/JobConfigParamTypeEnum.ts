export enum JobConfigParamTypeEnum {
  STRING = 'STRING',
  NUMBER = 'NUMBER',
  DATE = 'DATE',
  RANGE = 'RANGE',
  TIME = 'TIME',
  SITES_TREE = 'SITES_TREE',
  CHANNELS_TREE = 'CHANNELS_TREE',
  EMAIL = 'EMAIL'
}
