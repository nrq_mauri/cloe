export enum PeriodTypeEnum {
  MIN = 'MIN',
  PENTA_MIN = 'PENTA_MIN',
  DECA_MIN = 'DECA_MIN',
  QUARTER = 'QUARTER',
  HALF_HOUR = 'HALF_HOUR',
  HOUR = 'HOUR',
  DAY = 'DAY',
  WEEK = 'WEEK',
  MONTH = 'MONTH',
  YEAR = 'YEAR'
}
