export enum NodeCategoryEnum {
  ROOT = 'ROOT',
  GROUP = 'GROUP',
  LEAF = 'LEAF'
}
