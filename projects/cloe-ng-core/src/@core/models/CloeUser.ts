export class CloeUser {
  id: number;
  uuid: string;
  startWeek: string;
  status: string;
  defaultLanguage: string;
  tags: string;
  customerId: number;
  csvSeparator: string;

  constructor() {
    this.csvSeparator = '.';
  }
}
