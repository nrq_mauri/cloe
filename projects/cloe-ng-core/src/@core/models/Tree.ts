import {StatusEnum} from '../enums/StatusEnum';
import {NatureTypesEnum} from '../enums/NatureTypesEnum';
import {FeTypesEnum} from '../enums/FeTypesEnum';
import {Cluster} from './Cluster';

export class Tree {
  name: string;
  status: StatusEnum | string;
  validStartDate: Date;
  validEndDate: Date;
  nodeNature: NatureTypesEnum | string;
  feType: FeTypesEnum | string;
  cluster: Cluster;
}
