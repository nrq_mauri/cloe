import {JobConfigTypeEnum} from '../enums/JobConfigTypeEnum';
import {JobConfigParam} from './JobConfigParam';

export class JobConfig {
  id: number;
  name: string;
  javaClass: string;
  jobType: JobConfigTypeEnum;
  fileName: string;
  jobConfigParams: JobConfigParam[];
}
