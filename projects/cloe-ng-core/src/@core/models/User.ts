import {CloeUser} from './CloeUser';

export class User {
  id: number;
  login: string;
  firstName: string;
  password: string;
  passwordConfirm: string;
  lastName: string;
  email: string;
  authorities: string;
  cloeUser: CloeUser;

  constructor() {
    this.cloeUser = new CloeUser();
  }
}
