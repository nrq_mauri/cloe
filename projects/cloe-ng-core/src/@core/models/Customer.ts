export class Customer {
  id: number;
  uuid: string;
  name: string;
  customerCode: string;
  personalLogo: string;
  themeTemplateId: number;
  status: string;
}
