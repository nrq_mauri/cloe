import {ACTIONS} from '../enums/ActionsEnum';
import {Subject} from 'rxjs';

export interface ICommonTableHeader {
  fieldName?: string;
  columnName?: string;
  i18n?: I18n;
  pipe?: string;
}

export interface I18n {
  prefix: string;
  suffix: string;
}

export interface ICommonTableMetadata {
  tableTitle?: string;
  addNewButton?: IAddNewButton;
  sort?: string;
  sortType?: string;
}

export interface IAddNewButton {
  modalComponent?: any;
  buttonLabel?: string;
}

export interface ICommonTableAction {
  icon: string;
  actionType: ACTIONS;
  metadata: ITableMetadataAction;
}

export interface ITableMetadataAction {
  modalComponent?: any;
  altMessage?: string;
  deleteByField?: string;
  triggerSubject?: Subject<any>;
  action?: any;
}


