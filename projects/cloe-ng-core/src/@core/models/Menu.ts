export class Menu {
  id: number;
  uuid: string;
  description: string;
  name: string;
  ngState: string;
  externalUrl: string;
  iconCode: string;
  iconClass: string;
  opened: boolean;
  status: string;
}
