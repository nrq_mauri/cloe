import { StatusEnum } from '../enums/StatusEnum';
import { NatureTypesEnum } from '../enums/NatureTypesEnum';

export class Metadata {
  id: number;
  uuid: string;
  metadataKey: string;
  metadataType: string;
  metadataValue: any;
  nodeNature: NatureTypesEnum;
  referUuid: string;
  status: StatusEnum;
}
