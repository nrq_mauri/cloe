export class Cluster {
  id: number;
  uuid: string;
  name: string;
  customQuery: string;
  defaultChannelTypeId: number;
}
