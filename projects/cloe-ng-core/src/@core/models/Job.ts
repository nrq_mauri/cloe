import { JobConfig } from './JobConfig';

export class Job {
  id: number;
  cron: string;
  jsonParams: string;
  name: string;
  status: string;
  uuid: string;
  scheduledJobId: string;
  jobConfig: JobConfig;
  destinations: Destination[];
  constructor() {
    this.destinations = [];
  }
}

export class Destination {
  id: number;
  name: string;
  destinationType: DestinationTypeEnum;
  destinationJson: any;

  constructor(destinationType?) {
    if (destinationType) {
      switch (destinationType) {
        case DestinationTypeEnum.EMAIL:
          this.destinationJson = new EMAILDestination();
          break;
        case DestinationTypeEnum.FTP:
          this.destinationJson = new FTPDestination();
          break;
      }
    } else {
      this.destinationJson = {};
    }
  }
}

export enum DestinationTypeEnum {
  EMAIL = 'EMAIL', FTP = 'FTP'
}

export class EMAILDestination {
  email: string;
  cc: string[];
  ccn: string[];
  subject: string;
  body: string;
  destinationType: DestinationTypeEnum;

  constructor() {
    this.cc = [];
    this.ccn = [];
  }
}

export class FTPDestination {
  serverPath: string;
  username: string;
  password: string;
  destinationType: DestinationTypeEnum;
}
