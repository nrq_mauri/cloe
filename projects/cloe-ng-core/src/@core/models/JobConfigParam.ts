import {JobConfig} from './JobConfig';
import {JobConfigParamTypeEnum} from '../enums/JobConfigParamTypeEnum';

export class JobConfigParam {
  id: number;
  type: JobConfigParamTypeEnum;
  paramKey: number;
  paramValue: any;
  validationJson: string;
  defaulValue: string;
  paramLabel: string;
  jobConfigs: JobConfig;
}
