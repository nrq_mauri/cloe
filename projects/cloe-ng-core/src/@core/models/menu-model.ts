/**
 * Model for side menu
 */
export interface MenuModel {

  uuid: string;
  referUuid: string;
  title: string;
  ngState?: string;
  externalUrl?: string;
  iconClass?: string;
  iconCode?: string;
  count?: number;
  children?: MenuModel[];
  opened?: boolean;
  selected?: boolean;
  description?: string;
  hiddenMenu?: boolean;
}
