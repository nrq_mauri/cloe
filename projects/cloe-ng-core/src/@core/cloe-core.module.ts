import { InjectionToken, NgModule } from '@angular/core';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { CloeCoreServicesModule } from './services/cloe-core-services.module';
import { LoaderInterceptor } from './interceptors/LoaderInterceptor';
import { CorePagesModule } from './core-pages/core-pages.module';
import { FeTypesEnum } from './enums/FeTypesEnum';

const BASE_MODULES = [
  CorePagesModule
];

export interface ICorePagesConfig {
  feType: FeTypesEnum;
}
export const CorePagesConfig = new InjectionToken<ICorePagesConfig>('CorePagesConfig');


@NgModule({
  imports: [...BASE_MODULES],
  exports: [...BASE_MODULES, CloeCoreServicesModule],
  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true}
  ]
})
export class CloeCoreModule {
  static forRoot = function (config: ICorePagesConfig) {
    return {
      ngModule: CloeCoreServicesModule,
      providers: [
        { provide: 'CorePagesConfig', useValue: config },
      ]
    };
  };
}
