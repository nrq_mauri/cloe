import { ChangeDetectorRef, Injector, Pipe, PipeTransform } from '@angular/core';
import { EllipsisPipe } from './ellipsis.pipe';
import { EngFormatPipe } from './engFormat.pipe';
import { CloeDatePipe } from './cloe-date.pipe';
import { LowerCasePipe, UpperCasePipe } from '@angular/common';
import { CloeDateTimePipe } from './cloe-datetime.pipe';
import { EngFormatWithUnitPipe } from './engFormatWithUnit.pipe';
import { TranslatePipe, TranslateService } from '@ngx-translate/core';
import { CloePercentPipe } from './cloe-percent.pipe';
import { CloeDecimalSeparatorPipe } from './cloe-decimal-separator.pipe';
import { CloeDateTimeUTCPipe } from './cloe-datetime-utc.pipe';

@Pipe({
  name: 'dynamic'
})
export class DynamicPipe implements PipeTransform {
  private injector: Injector = Injector.create({
    providers: [
      {provide: 'percent', useExisting: CloePercentPipe},
      {provide: 'upperCase', useExisting: UpperCasePipe},
      {provide: 'lowerCase', useExisting: LowerCasePipe },
      {provide: 'cloeDate', useExisting: CloeDatePipe },
      {provide: 'cloeDateTime', useExisting: CloeDateTimePipe },
      {provide: 'cloeDateTimeUTC', useExisting: CloeDateTimeUTCPipe },
      {provide: 'engFormat', useExisting: EngFormatPipe },
      {provide: 'engFormatWithUnit', useExisting: EngFormatWithUnitPipe },
      {provide: 'ellipsis', useExisting: EllipsisPipe },
      {provide: 'translate', useClass: TranslatePipe, deps: [TranslateService, ChangeDetectorRef] },
      {provide: 'cloeDecimalFormatter', useExisting: CloeDecimalSeparatorPipe}
    ],
    parent: this.parentInjector
  });
  public constructor(private parentInjector: Injector) {
  }

  transform(value: any, pipeName: any, pipeArgs: any[], row?: any): any {
    if (pipeName === null || pipeName === undefined ) {
      return value;
    } else {
      const _args = [];
      if (pipeArgs)  {
        if (pipeArgs instanceof Array) {
          _args.push(...pipeArgs);
        } else {
          _args.push(pipeArgs);
        }

      }
      if ((pipeName === 'engFormatWithUnit' || pipeName === 'engFormat') && row) {
        _args.push(row.numberFormat);
      }
      const pipe = this.injector.get(pipeName);
      return pipe.transform(value, ..._args);
    }
  }

}
