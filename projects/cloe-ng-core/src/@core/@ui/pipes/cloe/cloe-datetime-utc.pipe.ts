import {Pipe, PipeTransform} from '@angular/core';
import * as moment_ from 'moment';

const moment = moment_;

@Pipe({
  name: 'cloeDateTimeUTC',
})
export class CloeDateTimeUTCPipe implements PipeTransform {

  public constructor(
  ) {}

  transform(value: any, inputFormat): any {
    if (!value) {
      return moment().utc().format('DD-MM-YYYY HH:mm');
    }
    return moment(value, inputFormat).utc().format('DD-MM-YYYY HH:mm');
  }
}
