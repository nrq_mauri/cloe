import { Pipe, PipeTransform } from '@angular/core';
import { EngFormatService } from '../../../services/engFormat-service/engFormat-service';

@Pipe({
  name: 'engFormat'
})

// pipe with multiple arguments to transform numeic inputs
// {{ myData | myPipe: 'arg1':'arg2':'arg3'... }}
export class EngFormatPipe implements PipeTransform {

  public constructor(
    private engFormatService: EngFormatService
  ) {
  }

  transform(value: any, format: string, dummy: string, formatJson: string): any {
    if (!value) {
      return value;
    }
    return this.engFormatService.engFormat(value, format, dummy, formatJson);
  }
}
