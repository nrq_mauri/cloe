import {Pipe, PipeTransform} from '@angular/core';
import * as moment_ from 'moment';

const moment = moment_;

@Pipe({
  name: 'cloeDate',
})
export class CloeDatePipe implements PipeTransform {

  public constructor(
  ) {}

  transform(value: any, inputFormat): any {
    if (!value) {
      return moment().format('L');
    }
    return moment(value, inputFormat).format('L');
  }
}
