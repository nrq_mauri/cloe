import { Pipe, PipeTransform } from '@angular/core';
import { isNaN } from 'lodash';

@Pipe({
  name: 'cloe-percent',
})
export class CloePercentPipe implements PipeTransform {

  public constructor(
  ) {}

  transform(value: any): any {
    if (isNaN(value) || value === 'undefined') {
      return '------ %';
    }
    return (Math.round(value * 100) / 100) + ' %';
  }

}
