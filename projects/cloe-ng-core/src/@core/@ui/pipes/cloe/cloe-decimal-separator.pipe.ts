import {Pipe} from '@angular/core';
import { DecimalPipe } from '@angular/common';
import { isNumber, replace } from 'lodash';

@Pipe({
  name: 'cloeDecimalFormatter',
})
export class CloeDecimalSeparatorPipe extends DecimalPipe {

  transform (value: any, separator: string, digits?: string): any {
    if (!isNumber(value)) {
      return value;
    }
    return replace(
      replace(super.transform(value, digits), /,/g, ''),
      '.',
      separator
    );
  }
}
