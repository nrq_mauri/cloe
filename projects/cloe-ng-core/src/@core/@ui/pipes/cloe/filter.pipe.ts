import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  public constructor(
  ) {}

  transform(items: any[], filter: Object): any {
    if (!items || !filter) {
      return items;
    }
    // console.log(items, filter);
    // filter items array, items which match and return true will be
    // kept, false will be filtered out
    return items.filter(item => {
      let check = true;
      Object.entries(filter).forEach(
        ([key, value]) => {
          switch (value) {
            case 'isNull':
              !item[key] ? check = true : check = false;
              break;
            case 'isNotNull':
              item[key] ? check = true : check = false;
              break;
            default:
              if (item[key].indexOf(value) === -1) {
                check = false;
              }
          }
        }
      );
      return check;
    });
  }
}
