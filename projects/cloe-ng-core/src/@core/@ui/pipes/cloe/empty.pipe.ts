import { Pipe, PipeTransform } from '@angular/core';
import { isNaN, isNil } from 'lodash';

@Pipe({
  name: 'empty',
})
export class EmptyPipe implements PipeTransform {

  public constructor(
  ) {}

  transform(value: any, emptyString?: string): any {
    if (isNil(value)) {
      return emptyString || '-';
    }
    return value;
  }

}
