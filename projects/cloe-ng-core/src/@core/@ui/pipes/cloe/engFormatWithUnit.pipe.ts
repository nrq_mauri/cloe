import { Pipe, PipeTransform } from '@angular/core';
import { EngFormatService } from '../../../services/engFormat-service/engFormat-service';

@Pipe({
  name: 'engFormatWithUnit'
})
export class EngFormatWithUnitPipe implements PipeTransform {

  public constructor(
    private engFormatService: EngFormatService
  ) {
  }

  transform(value: any, format: string, unitSymbol: string, formatJson: string): any {
    if (!value) {
      return value;
    }
    return this.engFormatService.engFormat(value, format, unitSymbol, formatJson);
  }
}
