import {Pipe, PipeTransform} from '@angular/core';
import { sortBy, reverse } from 'lodash';

@Pipe({
  name: 'sortArray',
  pure: false
})
export class ArraySortPipe implements PipeTransform {

  public constructor(
  ) {}

  // sort array of object in input
  // sort options must contain sort that is the field of array to sort
  // and sortType that is the type of sorting (asc or desc)
  transform(items: any[], sortOptions: any): any {
    if (!items || !sortOptions) {
      return items;
    }
    const sortedArray = sortBy(items, [sortOptions.sort]);

    if (sortOptions.sortType === 'desc') {
      return reverse(sortedArray);
    }

    return sortedArray;
  }
}
