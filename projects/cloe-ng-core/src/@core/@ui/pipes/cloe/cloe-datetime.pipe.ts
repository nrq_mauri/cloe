import {Pipe, PipeTransform} from '@angular/core';
import * as moment_ from 'moment';

const moment = moment_;

@Pipe({
  name: 'cloeDateTime',
})
export class CloeDateTimePipe implements PipeTransform {

  public constructor(
  ) {}

  transform(value: any, inputFormat): any {
    if (!value) {
      return moment().format('DD-MM-YYYY HH:mm');
    }
    return moment(value, inputFormat).format('DD-MM-YYYY HH:mm');
  }
}
