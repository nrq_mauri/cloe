import { animate, AnimationTriggerMetadata, state, style, transition, trigger } from '@angular/animations';

export const expandCollapseAnimation: AnimationTriggerMetadata = trigger('expandCollapse', [
  state('open', style({
    'height': '*'
  })),
  state('close', style({
    'height': '0px'
  })),
  transition('open <=> close', animate(300))
]);
