import { Component, EventEmitter, forwardRef, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { CloeItemsByNatureService } from '../../../services/cloe-itemsByNatureExceptUUID-service/cloe-itemsByNature.service';
import { NatureTypesEnum } from '../../../enums/NatureTypesEnum';
import { ControlValueAccessor, NG_VALUE_ACCESSOR, ValidatorFn } from '@angular/forms';
import { remove } from 'lodash';

export const DOMAIN_PICKER_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => DomainPickerComponent),
  multi: true
};

@Component({
  selector: 'cloe-core-domain-picker',
  providers: [ DOMAIN_PICKER_VALUE_ACCESSOR ],
  templateUrl: './domain-picker.component.html',
  styleUrls: ['./domain-picker.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class DomainPickerComponent implements OnInit, ControlValueAccessor {

  @Input() config: {
    labelSearch,
    itemAsObject?: {
      identifyBy: string,
      displayBy: string,
      nodeNature: NatureTypesEnum,
      fieldToSearch: any,
      autocompleteItems?
    },
    maxItems?,
    placeholder?,
    secondaryPlaceholder?,
    separatorKeys?: string[],
    validators?: ValidatorFn[],
    validationErrorMessages: any;
  };
  @Output() itemsChanged = new EventEmitter();
  value;
  onChange = (_: any) => { };

  constructor(
    private itemsByFilters: CloeItemsByNatureService
  ) {
  }

  ngOnInit() {
  }

  public requestAutocompleteItems = (text: string): Observable<Response> => {
    return this.itemsByFilters.multiFieldSearchByNature(this.config.itemAsObject, text).pipe(
      catchError(() => {
        return of([]);
      }));
  }

  onItemsChange() {
    this.writeValue(this.value);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  writeValue(obj: any): void {
    this.value = obj;
    this.onChange(this.value);
  }

  addString($event) {
    this.value.push($event.value);
    this.writeValue(this.value);
  }

  removeString($event) {
    remove(this.value, $event);
    this.writeValue(this.value);
  }
}
