import { NatureTypesEnum } from '../../../../enums/NatureTypesEnum';

export const tagsDomainPickerConf = {
  labelSearch: 'global.tags',
  itemAsObject: {
    identifyBy: 'tag',
    displayBy: 'tag',
    nodeNature: NatureTypesEnum.TAG,
    fieldToSearch: {
      tag: ''
    },
    autocompleteItems: true
  },
  maxItems: 8
};
