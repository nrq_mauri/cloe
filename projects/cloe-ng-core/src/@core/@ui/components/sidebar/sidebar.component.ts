import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { CustomerService } from '../../../services/customer-service/customer-service';
import { FileService } from '../../../services/file-service/file-service';
import { StorageService } from '../../../services/storage-service/storage.service';
import { FeTypesEnum } from '../../../enums/FeTypesEnum';

@Component({
  selector: 'cloe-core-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  @Input() isMenuClosed: boolean;
  @Input() menuModel;
  @Input() feType;
  @Output() isMenuClosedChange = new EventEmitter<boolean>();

  isSmallMenuMode = false;
  isMenuCollapsed = false;

  user;
  personalLogo;

  constructor(
    private customerService: CustomerService,
    private fileService: FileService
  ) {
  }

  ngOnInit() {
    this.personalLogo = '/assets/images/logo.png';
    this.user = StorageService.getCurrentUser();
    if (this.feType === FeTypesEnum.FE) {
      this.customerService.getCustomerByID(this.user.cloeUser.customerId).subscribe(customer => {
        if (customer.personalLogo) {
          this.loadLogo(customer.personalLogo);
        }
      });
    }
  }

  loadLogo(logoUuid) {
    const file: any = {};
    file.uuid = logoUuid;
    this.fileService.downloadOrViewFile(file, 'VIEW').subscribe(res => {
      this.createImageFromBlob(res);
    });
  }

  createImageFromBlob(image) {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      this.personalLogo = reader.result;
    }, false);

    if (image) {
      reader.readAsDataURL(image);
    }
  }

  toggleSideBar() {
    this.isMenuClosed = !this.isMenuClosed;
    this.isMenuClosedChange.emit(this.isMenuClosed);
  }
}
