import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewEncapsulation
} from '@angular/core';
import { MetadataService } from '../../../../services/metadata-service/metadata-service';
import { Metadata } from '../../../../models/Metadata';
import { NatureTypesEnum } from '../../../../enums/NatureTypesEnum';
import { CloeItemsByNatureService } from '../../../../services/cloe-itemsByNatureExceptUUID-service/cloe-itemsByNature.service';
import { StatusEnum } from '../../../../enums/StatusEnum';
import { assignIn, trim, transform, map, cloneDeep, uniqBy } from 'lodash';

@Component({
  selector: 'cloe-core-tree-states',
  templateUrl: './cloe-tree-states.component.html',
  styleUrls: ['./cloe-tree-states.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloeTreeStatesComponent implements OnInit {

  baseState;
  newStateClosed = true;
  loadTreeStateMode = true;
  treeStates: Metadata[];
  newState = new Metadata();
  selectedState;

  @Input() treeRef;
  @Input() rootReferUuids;
  @Input() activeNodes;
  @Output() stateLoaded = new EventEmitter();

  constructor(
    private metadataService: MetadataService,
    private cloeItemsByNatureService: CloeItemsByNatureService
  ) {}

  ngOnInit() {
    this.baseState = {
      status: StatusEnum.ACTIVE,
      nodeNature: NatureTypesEnum.TREE,
      metadataType: 'JSON_TREE_STATE'
    };
    this.initTreeStates();
  }

  initTreeStates() {
    this.cloeItemsByNatureService.searchByNatureWithFilters(
      NatureTypesEnum.METADATA,
      {
        referUuid: this.rootReferUuids,
        nodeNature: NatureTypesEnum.TREE,
        metadataType: 'JSON_TREE_STATE'
      })
      .subscribe((result) => {
        const defaultState: any = new Metadata();
        defaultState.metadataKey = 'page.channel-tree-view.choose-state';
        defaultState.skip = true;
        this.treeStates = uniqBy([defaultState, ...result], 'metadataKey');
        this.selectedState = this.treeStates[0];
      });
  }

  onSubmit(form) {
    if (trim(this.newState.metadataKey) === '') {
      return;
    }
    assignIn(this.newState, this.baseState);
    const metadataValue = transform(this.activeNodes, (result, v, k) => {
      result.push({
        id: k,
        value: v,
        axis: this.treeRef.treeModel.getNodeById(k).data.axis
      });
    }, []);
    this.newState.metadataValue = JSON.stringify(metadataValue);
    const newStates = map(this.rootReferUuids, (treeUuid) => {
      const state = cloneDeep(this.newState);
      state.referUuid = treeUuid;
      return state;
    });
    this.metadataService.upsertMetadataList(newStates, []).subscribe(() => {
      this.selectedState = this.newState;
      this.newState = new Metadata();
      form.controls.name.markAsPristine();
      this.initTreeStates();
    });
  }

  onOverwrite() {
    if (trim(this.selectedState.metadataKey) === '' || this.selectedState.skip) {
      return;
    }
    this.metadataService.upsertMetadata(this.selectedState).subscribe(() => {
      this.initTreeStates();
      this.loadTreeStateMode = !this.loadTreeStateMode;
    });
  }

  onRename() {
    if (this.selectedState && !this.selectedState.skip) {
      this.loadTreeStateMode = !this.loadTreeStateMode;
    }
  }

  onClearRename() {
    this.loadTreeStateMode = !this.loadTreeStateMode;
    this.initTreeStates();
  }

  onLoadTreeState() {
    if (this.selectedState.skip) {
      return;
    }
    this.stateLoaded.emit(this.selectedState);
  }

  onDeleteTreeState() {
    if (!this.selectedState || this.selectedState.skip) {
      return;
    }
    this.metadataService.deleteMetadata(this.selectedState.id).subscribe(() => {
      this.initTreeStates();
    });
  }

  compareFn(s1: Metadata, s2: Metadata) {
    return s1 && s2 ? s1.id === s2.id : s1 === s2;
  }
}
