import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import { get, isArray, isObject, remove, some, includes, toLower, isFunction } from 'lodash';
import { ITreeState, TreeComponent, TreeNode } from 'angular-tree-component';
import { IDType, IDTypeDictionary } from 'angular-tree-component/dist/defs/api';
import { Metadata } from '../../../models/Metadata';
import { FilterNumberCompareEnum, FilterTypeEnum } from '../../../enums/FilterTypeEnum';
import { Subject } from 'rxjs';
import { NodeCategoryEnum } from '../../../enums/NodeCategoryEnum';
import { ConfirmModalComponent } from '../modals';
import { ModalService } from '../../../services/modal-service/modal-service';

export const cloeTreeActionsSubject$ = new Subject();

@Component({
  selector: 'cloe-core-tree',
  templateUrl: './cloe-tree.component.html',
  styleUrls: ['./cloe-tree.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloeTreeComponent implements OnInit {
  @ViewChild(TreeComponent)
  tree: TreeComponent;

  @Input() treeInfo: {
    treeTitle?: string,
    treeUuid?: string,
    rootReferUuids?: string[],
    nodeLabelFunction?
  } = {};

  @Input() nodes;
  @Input() nodesConfig: {
    areFoldersAddable?: boolean,
    newFolderNotInRoot?: boolean,
    removableNodes?: any,
    confirmNodeRemoval?: boolean,
    notExpandRootAtStart?: boolean
  } = {};

  @Input() filtersConfig: any;

  @Input() styleConfig: {
    iconsNotPresent?: boolean,
    statusColorPath?,
    materialRootIcon?: string,
    materialGroupIcon?: string,
    materialLeafIcon?: string
  } = {};

  defaultIcons = {
    materialRootIcon: 'device_hub',
    materialGroupIcon: 'folder',
    materialLeafIcon: 'label_outline'
  };

  @Input() options;

  @Input() stateConfig: {
    state?: ITreeState,
    dynamicState?: boolean
  } = {};

  @Output() initialized = new EventEmitter();
  @Output() nodeActivate = new EventEmitter();
  @Output() nodeDeactivate = new EventEmitter();
  @Output() nodeMoved = new EventEmitter();
  @Output() nodeRemoved = new EventEmitter();
  @Output() update = new EventEmitter();
  @Output() filtersChanged = new EventEmitter();

  removalNodeConfirmSubscription;

  constructor(
    private modalService: ModalService
  ) {
    this.removalNodeConfirmSubscription = cloeTreeActionsSubject$.asObservable().subscribe((event: any) => {
      switch (event.action) {
        case CloeTreeActionsEnum.ADD_LEAF_FROM_OBJECT:
          this._addNodeToCurrentTree({name: event.data.name, referObject: event.data}, NodeCategoryEnum.LEAF);
          break;
      }
    });
  }

  get treeOptions() {
    return {
      ...this.options,
      useVirtualScroll: true,
      dropSlotHeight: 3
    };
  }

  ngOnInit() {
    if (!this.stateConfig.state) {
      this.stateConfig.state = new class implements ITreeState {
        activeNodeIds: IDTypeDictionary;
        expandedNodeIds: IDTypeDictionary;
        focusedNodeId: IDType;
        hiddenNodeIds: IDTypeDictionary;
        selectedLeafNodeIds: IDTypeDictionary;
        selectedNodeIds: IDTypeDictionary;
      };
    }
  }

  onInitialized($event) {
    this.initialized.emit($event);
    this.tree.treeModel.update();
  }

  onUpdate($event) {
    if ($event.treeModel.roots.length !== 0 && !this.nodesConfig.notExpandRootAtStart) {
      const firstRoot = $event.treeModel.roots[0];
      firstRoot.expand();
    }
    this.update.emit($event);
  }

  onNodeActivate($event) {
    this.nodeActivate.emit($event);
  }

  onNodeDeactivate($event) {
    this.nodeDeactivate.emit($event);
  }
  onNodeMoved($event) {
    this.nodeMoved.emit($event);
  }

  addGroupToTree(input) {
    const name = input.value;
    if (!name) {
      return;
    }
    this._addNodeToCurrentTree({name: name}, 'GROUP');
    this.tree.treeModel.update();
    input.value = '';
  }

  private _addNodeToCurrentTree(newNode, catergoryNode) {
    const node: any = {
      name: newNode.name.toUpperCase(),
      nodeCategory: catergoryNode,
      status: 'ACTIVE',
      isNew: true,
      nodeNature: this.tree.treeModel.nodes[0].nodeNature,
      children: []
    };
    if (newNode.referObject) {
      node.referObject = newNode.referObject;
      node.referUuid = newNode.referObject.uuid;
    }
    if (name === 'ROOT') {
      this.nodes.unshift(node);
    } else {
      if (this.tree.treeModel.activeNodes.length > 0) {
        this.tree.treeModel.activeNodes.forEach((activeNode) => {
          if (activeNode.data.nodeCategory === NodeCategoryEnum.LEAF) {
            activeNode.parent.data.children.unshift(node);
          } else {
            if (activeNode.path.length === 1) {
              if (!this.nodesConfig.newFolderNotInRoot) {
                activeNode.data.children.unshift(node);
              }
            } else {
              activeNode.data.children.unshift(node);
            }
          }
        });
      } else {
        if (!this.nodesConfig.newFolderNotInRoot) {
          this.nodes[0].children.unshift(node);
        }
      }
    }
    this.tree.treeModel.update();
  }

  iconForNode(node) {
    const nodeCategory = node.nodeCategory;
    switch (nodeCategory) {
      case 'ROOT':
        return this.styleConfig.materialRootIcon || this.defaultIcons.materialRootIcon;
      case 'GROUP':
        if (node.referObject) {
          return node.referObject.iconName || this.styleConfig.materialGroupIcon || this.defaultIcons.materialGroupIcon;
        }
        return this.styleConfig.materialGroupIcon || this.defaultIcons.materialGroupIcon;
      case 'LEAF':
        if (node.referObject) {
          return node.referObject.iconName || this.styleConfig.materialLeafIcon || this.defaultIcons.materialLeafIcon;
        }
        return this.styleConfig.materialLeafIcon || this.defaultIcons.materialLeafIcon;
      default:
        return this.styleConfig.materialLeafIcon || this.defaultIcons.materialLeafIcon;
    }
  }

  checkForRemovalConfirm(node) {
    if (this.nodesConfig.confirmNodeRemoval) {
      this.modalService.open(ConfirmModalComponent, {keyboard: false}).then(result => {
        if (result) {
          this.removeNode(node);
        }
      }).catch(() => {
        // dummy catch, we don't use it for now
      });
      this.modalService.getModalInstance().componentInstance.confirmMessage = 'component.confirm.messages.generic';
    } else {
      this.removeNode(node);
    }
  }

  removeNode(node) {
    remove(node.parent.data.children, (n: any) => n.id === node.id);
    this.tree.treeModel.update();
    this.nodeRemoved.emit(node);
  }

  onFilterChange(filters) {
    this.tree.treeModel.filterNodes(node => {
      let check = true;
      filters.forEach((filterConfig) => {
        const categoryBase = get(node.data, filterConfig.categoryBase);
        filterConfig.filters.forEach((filter) => {
            if (node.data.nodeCategory === 'LEAF') {
              switch (filter.filterType) {
                case FilterTypeEnum.ISNULL:
                  if (filter.filterValue) {
                    if (filter.filterValue === 'isNull') {
                      check = !get(categoryBase, filter.filterField) ? check : false;
                    }
                    if (filter.filterValue === 'isNotNull') {
                      check = get(categoryBase, filter.filterField) ? check : false;
                    }
                  }
                  break;
                case FilterTypeEnum.METADATA:
                  if (filter.metadataKey && filter.metadataValue) {
                    check = check && this.checkMetadataFilter(categoryBase, filter);
                  }
                  break;
                default:
                  if (filter.filterValue) {
                    isArray(categoryBase) ?
                      check = check && this.checkArrayFilter(categoryBase, filter) :
                      check = check && this.checkElement(
                        filter.filterNumberCompare,
                        filter.filterValue,
                        get(categoryBase, filter.filterField)
                      );
                  }
              }
            }
          }
        );
      });
      return check;
    });
    this.filtersChanged.emit(this.tree);
  }

  checkElement(filterNumberCompare, value, nodeElement) {
    if (filterNumberCompare) {
      switch (filterNumberCompare) {
        case FilterNumberCompareEnum.LE:
          return this.checkLEFilter(nodeElement, value);
        case FilterNumberCompareEnum.LT:
          return this.checkLTFilter(nodeElement, value);
        case FilterNumberCompareEnum.GE:
          return this.checkGEFilter(nodeElement, value);
        case FilterNumberCompareEnum.GT:
          return this.checkGTFilter(nodeElement, value);
        case FilterNumberCompareEnum.EQ:
          return this.checkEQFilter(nodeElement, value);
        case FilterNumberCompareEnum.NOT_EQ:
          return this.checkNOT_EQFilter(nodeElement, value);
        case FilterNumberCompareEnum.BTW:
          return this.checkBTWFilter(nodeElement, value);
      }
    } else {
      return this.checkStringFilter(nodeElement, value);
    }
  }

  private checkMetadataFilter(elements, filter) {
    return some(elements, (element) => {
      return ( filter.metadataKey === get(element, 'metadataKey') ) &&
        this.checkElement(filter.filterNumberCompare, filter.metadataValue, get(element, 'metadataValue'));
    });
  }

  private checkArrayFilter(elements, filter) {
    return some(elements, (element) => {
      if (isObject(element)) {
        return this.checkElement(filter.filterNumberCompare,  filter.filterValue, get(element, filter.filterField));
      } else {
        return this.checkElement(filter.filterNumberCompare,  filter.filterValue, element);
      }
    });
  }

  private checkStringFilter(nodeElement, value) {
    return includes(
      toLower(nodeElement),
      toLower(value)
    );
  }

  private checkLEFilter(nodeElement, value) {
    return +nodeElement <= +value;
  }

  private checkLTFilter(nodeElement, value) {
    return +nodeElement < +value;
  }

  private checkGEFilter(nodeElement, value) {
    return +nodeElement >= +value;
  }

  private checkGTFilter(nodeElement, value) {
    return +nodeElement > +value;
  }

  private checkEQFilter(nodeElement, value) {
    return +nodeElement === +value;
  }

  private checkNOT_EQFilter(nodeElement, value) {
    return +nodeElement !== +value;
  }

  private checkBTWFilter(nodeElement, value) {
    return +nodeElement >= +value.from && +nodeElement <= +value.to;
  }

  onMetadataStateChange(metadataState: Metadata) {
    const newActiveNodes: TreeNode[] = JSON.parse(metadataState.metadataValue);
    this.tree.treeModel.setActiveNode(this.tree.treeModel.getFirstRoot(), false, false);
    newActiveNodes.forEach((activeNode: any) => {
      if (activeNode.value) {
        const node = this.tree.treeModel.getNodeById(activeNode.id);
        node.data.axis = activeNode.axis;
        node.setActiveAndVisible(true);
      }
    });
  }

  getNodeName(node: any) {
    if (this.treeInfo && this.treeInfo.nodeLabelFunction) {
      return this.treeInfo.nodeLabelFunction(node);
    }
    if (node.data.referObject) {
      return node.data.referObject.name;
    }
    return node.data.name;
  }

  getStatusColor(node) {
    return get(node, this.styleConfig.statusColorPath);
  }

  isNodeRemovable(node) {
    if (node.data.nodeCategory !== 'ROOT') {
      if (isArray(this.nodesConfig.removableNodes)) {
        return this.nodesConfig.removableNodes.includes(node.data.nodeCategory);
      }
      if (isFunction(this.nodesConfig.removableNodes)) {
        return this.nodesConfig.removableNodes(node);
      }
    }
    return false;
  }

  forceStateTo(state: ITreeState) {
    this.tree.treeModel.setState(state);
  }
}

export enum CloeTreeActionsEnum {
  REMOVAL_NODE_CONFIRM, ADD_LEAF_FROM_OBJECT
}
