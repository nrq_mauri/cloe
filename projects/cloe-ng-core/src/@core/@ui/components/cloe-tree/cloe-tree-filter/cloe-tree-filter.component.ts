import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges, ViewEncapsulation } from '@angular/core';
import { FilterNumberCompareEnum, FilterTypeEnum } from '../../../../enums/FilterTypeEnum';
import { cloneDeep, find } from 'lodash';
import { MetadataTypeEnum } from '../../../../enums/MetadataTypeEnum';

@Component({
  selector: 'cloe-core-tree-filter',
  templateUrl: './cloe-tree-filter.component.html',
  styleUrls: ['./cloe-tree-filter.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloeTreeFilterComponent implements OnInit, OnChanges {
  @Input() filterConf;
  @Output() filterChange = new EventEmitter();
  filters: any = [];
  filterEnum = FilterTypeEnum;
  filterNumberCompareEnum = FilterNumberCompareEnum;
  metadataTypeEnum = MetadataTypeEnum;
  selectedMetadataKey: any;
  metadataValueFilter: any;

  ngOnInit(): void {
    this.initFilters();
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.filterConf) {
      this.initFilters();
    }
  }

  initFilters() {
    this.filters = cloneDeep(this.filterConf);
    this.metadataValueFilter =
      find(this.filters, (filterCategory: any) => filterCategory.categoryBase === 'metadata').filters[0];
  }

  doFilter() {
    this.filterChange.emit(this.filters);
  }
  clearFilter() {
    this.filters = cloneDeep(this.filterConf);
    this.filterChange.emit(this.filters);
  }

  onSelectMetadataKeyChange(filterItem) {
    filterItem['metadataKey'] = this.selectedMetadataKey.metadataKey;
    filterItem['metadataValue'] = '';
    if (this.metadataValueFilter) {
      this.metadataValueFilter.filterNumberCompare = undefined;
    }
  }

  onNumberCompareChange(value: any, filterItem) {
    if (value === this.filterNumberCompareEnum.BTW) {
      filterItem.metadataValue = { from: '', to: ''};
    } else {
      filterItem.metadataValue = '';
    }
  }
}
