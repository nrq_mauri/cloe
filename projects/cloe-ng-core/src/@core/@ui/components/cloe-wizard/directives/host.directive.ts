import { Directive, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[cloe-core-host]',
})
export class HostDirective {
  constructor(public viewContainerRef: ViewContainerRef) { }
}
