import { CloeWizardComponentInterface } from '../../../../interfaces/cloe-wizard-component.interface';

export class WizardTab {
  name: string;
  enabled?: boolean;
  component;
  data: any;
  isFirst?: boolean;
  isLast?: boolean;
  componentInstance?: CloeWizardComponentInterface;
}
