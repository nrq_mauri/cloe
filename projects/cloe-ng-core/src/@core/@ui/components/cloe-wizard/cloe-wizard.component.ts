import {
  AfterViewInit,
  Component,
  ComponentFactoryResolver, DoCheck, EventEmitter,
  Input, IterableDiffers,
  OnInit, Output,
  QueryList,
  ViewChildren,
  ViewEncapsulation
} from '@angular/core';
import { WizardTab } from './models/wizard-tab';
import { forEach, findIndex, head, last } from 'lodash';
import { HostDirective } from './directives/host.directive';
import { CloeWizardComponentInterface } from '../../../interfaces/cloe-wizard-component.interface';

@Component({
  selector: 'cloe-core-wizard',
  templateUrl: './cloe-wizard.component.html',
  styleUrls: ['./cloe-wizard.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloeWizardComponent implements OnInit, AfterViewInit, DoCheck {

  @ViewChildren(HostDirective) hosts: QueryList<HostDirective>;
  @Input() tabs: WizardTab[];
  @Output() submitted = new EventEmitter();
  activeTab: string;
  iterableDiffer;

  constructor(
    private componentFactoryResolver: ComponentFactoryResolver,
    private _iterableDiffers: IterableDiffers
  ) {
    this.iterableDiffer = this._iterableDiffers.find([]).create(null);
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    setTimeout( () => {
      if (this.tabs.length > 0) {
        this.initTabs();
      }
    }, 0);
  }

  ngDoCheck() {
    const changes = this.iterableDiffer.diff(this.tabs);
    if (changes) {
      setTimeout( () => {
        if (this.tabs.length > 0) {
          this.initTabs();
        }
      }, 0);
    }
  }

  private initTabs() {
    const firstTab = head(this.tabs);
    firstTab.isFirst = true;
    this.setActiveTab(firstTab);
    last(this.tabs).isLast = true;
  }

  private findTabIndexByName(tab: WizardTab) {
    return findIndex(this.tabs, (tabElement) => {
      return tab.name === tabElement.name;
    });
  }

  public setActiveTab(tab: WizardTab) {
    const containerRef = this.hosts.find( (host) => {
      return host.viewContainerRef.element.nativeElement.parentElement.id === tab.name;
    });
    containerRef.viewContainerRef.clear();
    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(tab.component);

    const componentRef = containerRef.viewContainerRef.createComponent(componentFactory);
    tab.componentInstance = <CloeWizardComponentInterface>componentRef.instance;
    tab.componentInstance.data = tab.data;
    this.activeTab = tab.name;
  }

  public onNext(tab: WizardTab) {
    if ( tab.componentInstance.isValid ) {
      const tabIndex = this.findTabIndexByName(tab);
      if (tab.isLast) {
        this.submitted.emit();
      } else {
        const nextTab = this.tabs[ tabIndex + 1 ];
        this.setActiveTab( nextTab);
      }
    }
  }

  onPrev(tab: WizardTab) {
    const tabIndex = this.findTabIndexByName(tab);
    if (!tab.isFirst) {
      const nextTab = this.tabs[ tabIndex - 1 ];
      this.setActiveTab( nextTab );
    }
  }
}
