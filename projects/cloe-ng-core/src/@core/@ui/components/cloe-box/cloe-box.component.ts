import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { isEqual } from 'lodash';
import { Subject } from 'rxjs';
import { expandCollapseAnimation } from '../../animations';

export const cloeBoxSubject$ = new Subject();

@Component({
  selector: 'cloe-core-box',
  templateUrl: './cloe-box.component.html',
  styleUrls: ['./cloe-box.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    expandCollapseAnimation
  ]
})
export class CloeBoxComponent implements OnInit {

  isExpanded = false;
  expandedStyle: any = {
    'z-index': '999',
    'position': 'absolute',
    'border': '1px solid #101010',
    'box-shadow': '8px 8px 5px #888888',
    'max-height': 'none',
    'height': 'auto',
    'right': '0'
  };
  currentStyle;
  isCollapsed = false;
  @Input() scrollbarStyle = {};
  @Input() isCollapsable = false;
  @Input() expandable = false;
  @Input() customStyle: any = {};
  @Input() boxTitle;
  @Input() customClass = '';

  constructor() {}

  ngOnInit(): void {
    this.initCustomStyle();
  }

  initCustomStyle() {
    if (this.customStyle) {
      this.currentStyle = this.customStyle;
    }
    if (this.isCollapsable) {
      this.scrollbarStyle['overflow'] = 'visible';
    }
  }

  onExpand(card: HTMLDivElement) {
    if (isEqual(this.currentStyle, this.expandedStyle)) {
      this.initCustomStyle();
      this.isExpanded = false;
    } else {
      this.currentStyle = this.expandedStyle;
      this.isExpanded = true;
    }
    cloeBoxSubject$.next({
      action: CloeBoxActionsEnum.TOGGLE_EXPANDED,
      data: {
        value: this.isExpanded
      }
    });
  }

  collapse() {
    this.isCollapsed = !this.isCollapsed;
    this.scrollbarStyle['overflow'] = this.isCollapsed ? 'hidden' : 'visible';
  }
}

export enum CloeBoxActionsEnum {
  TOGGLE_EXPANDED
}
