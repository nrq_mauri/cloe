import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { LoginService } from '../../../services/login-service/login.service';
import { StorageService } from '../../../services/storage-service/storage.service';
import { SubjectsService } from '../../../services/resize-service/subjects.service';

@Component({
  selector: 'cloe-core-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HeaderComponent implements OnInit {

  @Input() isMenuClosed: boolean;
  @Output() isMenuClosedChange = new EventEmitter<boolean>();
  user;

  constructor(
    private loginSerive: LoginService,
    public translate: TranslateService,
    private subjectsService: SubjectsService
  ) {
  }

  ngOnInit() {
    this.user = StorageService.getCurrentUser();
    this.subjectsService.sibarCollapseInformer$.next(this.isMenuClosed);
  }

  toggleSideBar() {
    this.isMenuClosed = !this.isMenuClosed;
    this.isMenuClosedChange.emit(this.isMenuClosed);
    this.subjectsService.sibarCollapseInformer$.next(this.isMenuClosed);
  }

  changeLanguage(lang) {
    this.translate.use(lang);
    StorageService.setCurrentLang(lang);
  }

  doLogout() {
    this.loginSerive.doLogout();
  }
}
