import {
  AfterViewInit,
  Component,
  ElementRef,
  EventEmitter,
  forwardRef,
  Input,
  OnChanges,
  Output,
  Renderer2,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator
} from '@angular/forms';
import { keys, find } from 'lodash';

@Component({
  selector: 'cloe-core-form-select-object',
  templateUrl: './cloe-form-select-object.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CloeFormSelectObjectComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => CloeFormSelectObjectComponent),
      multi: true,
    }
  ]
})
export class CloeFormSelectObjectComponent implements AfterViewInit, ControlValueAccessor, Validator, OnChanges {

  @Input()
  disabled = false;
  @Input()
  name: string;
  @Input()
  id: string;
  @Input()
  labelPrefix = 'global.';
  @Input()
  optionLabelField;
  @Input()
  optionLabelPrefix = '';
  @Input()
  optionKeyField;
  @Input()
  asObject = false;
  @Input()
  formPrefix = '';
  @Input()
  validationMessages = true;

  @Input()
  options: Array<any> = [];

  @ViewChild('select_input') private _inputElement: ElementRef;

  _value = '';
  hasErrors = false;
  optionsFetched = false;
  errors = [];

  @Output()
  select = new EventEmitter<any>();

  propagateChange: any = () => {};

  constructor(
    private _renderer: Renderer2
  ) {}

  ngAfterViewInit(): void {
  }

  get inputId() {
    return this.id || (this.formPrefix + this.name);
  }

  get value() {
    return this._value;
  }

  set value(v: any) {
    this._value = v;
    let valueToSend = this._value;
    if (this.asObject) {
      valueToSend = find(this.options, o => o[this.optionKeyField].toString() === v);
    }
    this.propagateChange(valueToSend);
    this.select.emit(valueToSend);
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
    this._renderer.setProperty(this._inputElement.nativeElement, 'disabled', isDisabled);
  }

  writeValue(value: any): void {
    if (this.asObject) {
      this._value = value[this.optionKeyField];
    } else {
      this._value = value;
    }
  }

  onChange(v) {
    if (v === '-1') {
      this.value = undefined;
    } else {
      this.value = v;
    }
  }

  registerOnValidatorChange(fn: () => void): void {
  }

  validate(control: AbstractControl): ValidationErrors | null {
    setTimeout(() => {
      this.hasErrors = !control.valid;
      if (this.hasErrors) {
        this.errors = [ ...keys(control.errors)];
      }
    }, 0);
    return control.valid ? null : control.errors;
  }

  compareFn(a, b) {
    return a && b ? a.id === b.id : a === b;
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (this.optionsFetched) {
      return;
    }
    const options = changes.options.currentValue;
    if (options && options.length) {
      if (this._value) {
        if (this.asObject) {
          this.value = this._value[this.optionKeyField];
        } else {
          this.value = this._value;
        }
      }
      this.optionsFetched = true;
    }
  }
}
