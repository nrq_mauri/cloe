import { AfterViewInit, Component, forwardRef, Input } from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator
} from '@angular/forms';
import { keys, isPlainObject } from 'lodash';

@Component({
  selector: 'cloe-core-form-select',
  templateUrl: './cloe-form-select.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CloeFormSelectComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => CloeFormSelectComponent),
      multi: true,
    }
  ]
})
export class CloeFormSelectComponent implements AfterViewInit, ControlValueAccessor, Validator {

  @Input()
  disabled = false;
  @Input()
  name: string;
  @Input()
  id: string;
  @Input()
  labelPrefix = 'global.';
  @Input()
  optionLabelPrefix = 'global.';
  @Input()
  formPrefix = '';

  @Input()
  options = [];

  _value = '';
  hasErrors = false;
  errors = [];

  propagateChange: any = () => {};

  ngAfterViewInit(): void {
  }

  get inputId() {
    return this.id || (this.formPrefix + this.name);
  }

  get value() {
    return this._value;
  }

  set value(v: any) {
    this._value = v;
    this.propagateChange(v);
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  writeValue(value: any): void {
    if (!this.disabled) {
      this.value = value;
    }
  }

  onChange(v) {
    this.value = v === '-1' ? undefined : v;
  }

  registerOnValidatorChange(fn: () => void): void {
  }

  validate(control: AbstractControl): ValidationErrors | null {
    setTimeout(() => {
      this.hasErrors = !control.valid && !control.pristine;
      if (this.hasErrors) {
        this.errors = [ ...keys(control.errors)];
      }
    }, 0);
    return control.valid ? null : control.errors;
  }
}
