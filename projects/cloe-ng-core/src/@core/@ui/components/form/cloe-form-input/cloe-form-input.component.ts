import { AfterViewInit, Component, forwardRef, Input } from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator
} from '@angular/forms';
import { keys } from 'lodash';

@Component({
  selector: 'cloe-core-form-input',
  templateUrl: './cloe-form-input.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CloeFormInputComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => CloeFormInputComponent),
      multi: true,
    }
  ]
})
export class CloeFormInputComponent implements AfterViewInit, ControlValueAccessor, Validator {
  @Input()
  type = 'text';
  @Input()
  disabled = false;
  @Input()
  name: string;
  @Input()
  id: string;
  @Input()
  labelPrefix = 'global.';
  @Input()
  formPrefix = '';
  @Input()
  validationMessages = true;
  @Input()
  accept = '';

  _value = '';
  hasErrors = false;
  errors = [];

  propagateChange: any = () => {};


  ngAfterViewInit(): void {
  }

  get value() {
    return this._value || '';
  }

  get inputId() {
    return this.id || (this.formPrefix + this.name);
  }

  set value(v: any) {
    this._value = v;
    this.propagateChange(v);
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  writeValue(value: any): void {
    if (!this.disabled) {
      this.value = value || '';
    }
  }

  onChange(v) {
    this.value = v;
  }

  registerOnValidatorChange(fn: () => void): void {
  }

  validate(control: AbstractControl): ValidationErrors | null {
    setTimeout(() => {
      this.hasErrors = !control.valid;
      if (this.hasErrors) {
        this.errors = [ ...keys(control.errors)];
      }
    }, 0);
    return control.valid ? null : control.errors;
  }
}
