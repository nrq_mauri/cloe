import { AfterViewInit, Component, forwardRef, Input } from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  ValidationErrors,
  Validator
} from '@angular/forms';
import { keys } from 'lodash';

@Component({
  selector: 'cloe-core-form-color-input',
  templateUrl: './cloe-form-color-input.component.html',
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => CloeFormColorInputComponent),
      multi: true
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => CloeFormColorInputComponent),
      multi: true,
    }
  ]
})
export class CloeFormColorInputComponent implements AfterViewInit, ControlValueAccessor, Validator {
  @Input()
  disabled = false;
  @Input()
  name: string;
  @Input()
  label: string;

  _value: any;
  hasErrors = false;
  errors = [];

  propagateChange: any = () => {};


  ngAfterViewInit(): void {
  }

  get id() {
    return this.name;
  }

  get value() {
    return this._value || '';
  }

  set value(v: any) {
    this._value = v;
    this.propagateChange(v);
  }

  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  writeValue(value: any): void {
    if (value && !this.disabled) {
      this.value = value;
    }
  }

  onChange(v) {
    this.value = v;
  }

  registerOnValidatorChange(fn: () => void): void {
  }

  validate(control: AbstractControl): ValidationErrors | null {
    setTimeout(() => {
      this.hasErrors = !control.valid;
      if (this.hasErrors) {
        this.errors = [ ...keys(control.errors)];
      }
    }, 0);
    return control.valid ? null : control.errors;
  }
}
