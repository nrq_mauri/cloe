import { Component, Input } from '@angular/core';
import { CloeUploadComponent } from '../cloe-upload/upload.component';
import { ModalService } from '../../../services/modal-service/modal-service';

@Component({
  selector: 'cloe-core-upload-wrapper',
  templateUrl: './upload-wrapper.component.html'
})
export class CloeUploadWrapperComponent {
  @Input() metadata;
  @Input() uploadType;
  @Input() miniMode;

  constructor(private modalService: ModalService) {
  }

  open() {
    this.modalService.open(CloeUploadComponent, {size: 'lg', centered: true})
      .then(dialog => {
      })
      .catch(() => {
      });
    this.modalService.getModalInstance().componentInstance.uploadType = this.uploadType;
    this.modalService.getModalInstance().componentInstance.metadata = this.metadata;
    this.modalService.getModalInstance().componentInstance.miniMOde = this.miniMode;
  }
}
