import { AbstractControl, ValidatorFn } from '@angular/forms';
import { trim } from 'lodash';

export function changePWValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} | null => {
    const oldPW = control.value ? control.value.oldPassword : undefined;
    const newPW = control.value ? control.value.newPassword : undefined;
    const repeatPW = control.value ? control.value.repeatPassword : undefined;
    if ((!newPW || trim(newPW) === '') && (!repeatPW || trim(repeatPW) === '') && (!oldPW || trim(oldPW) === '')) {
      return null;
    }
    const changePWValidation = (
      (oldPW && trim(oldPW) !== '') && (newPW && trim(newPW) !== '') && (repeatPW && trim(repeatPW) !== '') && (newPW === repeatPW)
    );
    return changePWValidation ? null : {'message': 'component.change-password.new-password-incorrect'};
  };
}
