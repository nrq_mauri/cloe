import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator } from '@angular/forms';
import { changePWValidator } from '../ChangePWValidator';

@Directive({
  selector: '[cloe-core-change-pw-validator-directive]',
  providers: [{provide: NG_VALIDATORS, useExisting: ChangePWValidatorDirective, multi: true}]
})
export class ChangePWValidatorDirective implements Validator {
  validate(control: AbstractControl): {[key: string]: any} {
    return changePWValidator()(control);
  }
}
