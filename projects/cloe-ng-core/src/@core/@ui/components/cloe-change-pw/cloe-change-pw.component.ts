import { Component, forwardRef, OnInit, ViewEncapsulation } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

export const CHANGE_PW_INPUT_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => CloeChangePwComponent),
  multi: true
};

@Component({
  selector: 'cloe-core-input-change-pw',
  providers: [ CHANGE_PW_INPUT_VALUE_ACCESSOR ],
  templateUrl: './cloe-change-pw.component.html',
  styleUrls: ['./cloe-change-pw.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloeChangePwComponent implements OnInit, ControlValueAccessor {

  passwordDTO = {
    oldPassword: undefined,
    newPassword: undefined,
    repeatPassword: undefined
  };

  value;
  onChange = (_: any) => { };

  constructor() {
  }

  ngOnInit(): void {
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  writeValue(obj: any): void {
    this.value = obj;
    this.onChange(this.value);
  }

}
