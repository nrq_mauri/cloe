import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { StateService } from '@uirouter/angular';

@Component({
  selector: 'cloe-core-tabs',
  templateUrl: './cloe-tabs.component.html',
  styleUrls: ['./cloe-tabs.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloeTabsComponent implements OnInit {

  @Input() tabs;

  constructor(private stateService: StateService) {
  }

  ngOnInit() {
  }

  private _isTabActive(tab) {
    return tab.ngState === this.stateService.$current.name;
  }

  getTabIndex(tab) {
    return this._isTabActive(tab) ? undefined : '-1';
  }

}
