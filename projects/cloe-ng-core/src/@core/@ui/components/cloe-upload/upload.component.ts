import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { FormControl, FormGroup, Validators } from '@angular/forms';
import { StorageService } from '../../../services/storage-service/storage.service';
import { FileService } from '../../../services/file-service/file-service';

@Component({
  selector: 'cloe-core-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})

export class CloeUploadComponent implements OnInit {

  uploadTypes = [];

  currentUser = StorageService.getCurrentUser();
  uploadForm: FormGroup;

  @Input() uploadTitle;
  @Input() uploadPlaceholder;

  @Input() metadata;
  @Input() miniMode = false;
  @Output() fileUploadedListener = new EventEmitter();

  @Input() uploadType;  // type from input
  fileType: '';         // type from dropdown

  file: File;           // file to upload; setFile() from form
  fileUploaded = '';    // file uploaded; response from service

  metaDataObj = {};     // metadata JSON to be filled

  objectKeys = Object.keys;

  show = false;
  details = false;

  constructor(private fileService: FileService) {
  }


  ngOnInit() {
    this.fileService.getEnumUploadTypes().subscribe(
      (resp: Array<string>) => {
        this.uploadTypes = resp;
      });
    this.createForm();
  }

  createForm() {
    this.uploadForm = new FormGroup({
      uploadType: new FormControl({value: (this.uploadType || this.fileType), disabled: !!this.uploadType}),
      fileToUpload: new FormControl('', Validators.required),
      newMetaDataKey: new FormControl('', Validators.required),
      newMetaDataValue: new FormControl('', Validators.required),
    });
  }

  setFile(files: FileList) {
    this.file = files.item(0);

    if (this.miniMode) {
      this.fileService.uploadFile(this.file, this.currentUser, (this.fileType || this.uploadType))
        .subscribe(resp => {
          this.fileUploadedListener.emit(resp);
        });
    }
  }

  uploadFile() {
    this.fileService.uploadFile(this.file, this.currentUser, (this.fileType || this.uploadType))
      .subscribe(
        (resp: string) => {
          this.fileUploaded = resp,
            this.uploadMetaDataToFile(this.metaDataObj, this.fileUploaded);
        });
  }

  onSubmit() {
    this.formValues();
    this.uploadFile();
    this.details = true;
  }

  formValues() {
    const formModel = this.uploadForm.value;
    this.fileType = formModel.uploadType;
  }

  uploadMetaDataToFile(metaData, fileUuid) {
    if (metaData !== '') {
      this.fileService.uploadMetaDataToFile(metaData, fileUuid);
    }
  }

  addMetadata(newMetaDataKey, newMetaDataValue) {
    this.metaDataObj[newMetaDataKey] = newMetaDataValue;
  }

  removeMetadata(key: string) {
    delete this.metaDataObj[key];
  }

  seeDetails() {
    this.show = !this.show;
  }

  downloadOrViewFile(op) {
    this.fileService.downloadOrViewFile(this.fileUploaded, op)
      .subscribe(
        data => console.log(data)
      );
  }

  deleteFile() {
    this.fileService.deleteFile(this.fileUploaded)
      .subscribe(
        data => console.log(data)
      );
  }

}
