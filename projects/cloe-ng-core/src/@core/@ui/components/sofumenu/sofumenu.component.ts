import {
  Component,
  ElementRef, EventEmitter,
  HostBinding,
  HostListener,
  Input,
  OnInit, Output,
  QueryList,
  ViewChild,
  ViewChildren, ViewEncapsulation
} from '@angular/core';
import { StateService, TransitionService } from '@uirouter/angular';
import {MenuModel} from '../../../models/menu-model';
import { filter } from 'lodash';

@Component({
  selector: 'cloe-core-sofu-menu',
  templateUrl: './sofumenu.component.html',
  styleUrls: ['./sofumenu.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SofumenuComponent implements OnInit {

  // Model with menu items
  @Input()
  menuModel: MenuModel[];
  // Set true for compact mode
  @HostBinding('class.small-menu')
  isSmallMenu;
  // Store the items, that has pointer near it
  pointedMenuItem;
  // Query with menu item elements
  @ViewChildren('menuItem')
  pointedListItem: QueryList<ElementRef>;
  // Pointer element reference
  @ViewChild('pointer')
  pointer: ElementRef;
  // Temporary stores states of items while menu is collapsed in compact mode
  openStates;
  // Informs when menu collapses
  @Output()
  collapse: EventEmitter<boolean> = new EventEmitter();

  constructor(private stateService: StateService, private transitionService: TransitionService) {
  }

  ngOnInit() {
    let firstCall = true;
    const onNavigation = () => {
      this.menuModel.forEach(d => this.checkSelection(d));
      this.pointedMenuItem = undefined;
      this.menuModel.forEach(d => this.findPointedMenuItem(d, true));
      this.menuModel = this.checkHiddenMenu(this.menuModel);
      this.movePointer();
      if (firstCall) {
        this.onMouseLeave();
        firstCall = false;
      }
    };
    this.transitionService.onSuccess({}, onNavigation);
    setTimeout(() => onNavigation());
  }

  checkHiddenMenu(items: MenuModel[]) {
    return filter(items, (item: MenuModel) => {
      return !item.hiddenMenu;
    });
  }

  /**
   * Set selected flag to model if any of children is selected.
   *  {MenuModel} item
   */
  checkSelection(item: MenuModel) {
    const virtualMenuActive = this.stateService.$current.data && item.ngState === this.stateService.$current.data.stateAs;
    const menuActive = this.stateService.$current.name === item.ngState;
    item.selected = (menuActive || virtualMenuActive) && !item.hiddenMenu;
    let childrenSelected = false;
    if (item.children) {
      childrenSelected = item.children.reduce((accumulator, _item) => {
        this.checkSelection(_item);
        return _item.selected || accumulator;
      }, false);
      item.selected = item.selected || childrenSelected;
    }
    if (item.selected) {
      item.opened = true;
    }
  }

  /**
   * Opens menu and moves pointer
   *  item
   */
  toggleOpen(item) {
    item.opened = !item.opened;
    this.menuModel.forEach(d => this.findPointedMenuItem(d, true));
    this.movePointer();
  }

  /**
   * Finds menu items, that is selected
   *  item
   *  parentOpened
   */
  findPointedMenuItem(item, parentOpened) {
    if (item.selected && parentOpened) {
      this.pointedMenuItem = item;
    }
    if (item.opened && item.children) {
      item.children.forEach(d => this.findPointedMenuItem(d, item.opened));
    }
  }

  /**
   * Moves pointer to selected item
   */
  movePointer() {
    const moveFunction = () => {
      const pointed = this.pointedListItem.find(item =>
        item.nativeElement.classList.contains('pointed')
      );
      if (pointed) {
        this.pointer.nativeElement.style.display = 'block';
        this.pointer.nativeElement.style.top = pointed.nativeElement.offsetTop + 'px';
      } else {
        this.pointer.nativeElement.style.display = 'none';
      }
    };
    // Move pointer 2 times - before open animation and after
    setTimeout(moveFunction, 0);
    setTimeout(moveFunction, 350);
  }

  /**
   * Changes menu mode
   *  value
   */
  @Input()
  set smallMenu(value) {
    if (value) {
      this.isSmallMenu = value;
      this.onMouseLeave();
    } else {
      this.onMouseEnter();
      this.isSmallMenu = value;
    }
  }

  @HostListener('mouseenter')
  onMouseEnter() {
    if (this.isSmallMenu) {
      this.menuModel.forEach( (item, i) => {
        item.opened = this.openStates[i];
      });
      this.menuModel.forEach(d => this.findPointedMenuItem(d, true));
      this.movePointer();
      this.openStates = null;
      this.collapse.emit(false);
    }
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    if (this.isSmallMenu) {
      this.openStates = this.menuModel.map(item => {
        const state = item.opened;
        item.opened = false;
        return state;
      });
      this.menuModel.forEach(d => this.findPointedMenuItem(d, true));
      this.movePointer();
      this.collapse.emit(true);
    }
  }

}
