import { Component, Input } from '@angular/core';
import { ModalService } from '../../../services/modal-service/modal-service';

@Component({
  selector: 'cloe-core-modal',
  templateUrl: './cloe-modal.component.html',
  styleUrls: ['./cloe-modal.component.scss']
})
export class CloeModalComponent {
  constructor(private modalService: ModalService) {
  }

  @Input() modalTitle: string;

  dismissModal() {
    this.modalService.getModalInstance().dismiss();
  }
}
