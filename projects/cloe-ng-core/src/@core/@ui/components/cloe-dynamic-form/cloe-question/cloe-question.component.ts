import { AfterViewInit, Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { InputTypeEnum } from '../models/enums/InputTypeEnum';

@Component({
  selector: 'cloe-core-question',
  templateUrl: './cloe-question.component.html',
  styleUrls: ['./cloe-question.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloeQuestionComponent implements OnInit, AfterViewInit {

  @Input() question: any;
  @Input() form: FormGroup;
  inputType = InputTypeEnum;
  get isValid() {
    return this.form.controls[this.question.key].valid;
  }

  constructor() {}

  ngOnInit() {
  }

  ngAfterViewInit() {
  }
}
