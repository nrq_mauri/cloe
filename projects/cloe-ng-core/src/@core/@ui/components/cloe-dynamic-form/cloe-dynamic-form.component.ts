import { AfterViewInit, Component, DoCheck, EventEmitter, Input, IterableDiffers, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { QuestionBase } from './models/QuestionBase';
import { FormGroup } from '@angular/forms';
import { DynamicFormService } from '../../../services/dynamic-form-service/dynamic-form-service';

@Component({
  selector: 'cloe-core-dyamic-form',
  templateUrl: './cloe-dynamic-form.component.html',
  styleUrls: ['./cloe-dynamic-form.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloeDynamicFormComponent implements OnInit, AfterViewInit, DoCheck {

  @Input() questions: QuestionBase<any>[] = [];
  @Input() config = {
    submitButton: false
  };
  @Output() valueChanged = new EventEmitter();
  @Output() cancelled = new EventEmitter();
  @Output() submitted = new EventEmitter();
  form: FormGroup;
  isValid;
  iterableDiffer;

  constructor(
    private dynamicFormService: DynamicFormService,
    private _iterableDiffers: IterableDiffers
  ) {
    this.iterableDiffer = this._iterableDiffers.find([]).create(null);
  }

  ngOnInit() {
    this.initForm();
  }

  initForm() {
    this.form = this.dynamicFormService.toFormGroup(this.questions);
    this.form.statusChanges.subscribe( (status) => {
      this.isValid = status === 'VALID';
      if (this.isValid && !this.config.submitButton) {
        this.valueChanged.emit(this.form.value);
      }
    });
    setTimeout(() => this.isValid = this.form && this.form.valid);
  }

  ngAfterViewInit(): void {
    setTimeout( () => {
      this.isValid = this.form.valid;
      if (this.isValid && !this.config.submitButton) {
        this.valueChanged.emit(this.form.value);
      }
    }, 0);
  }

  ngDoCheck() {
    const changes = this.iterableDiffer.diff(this.questions);
    if (changes) {
      this.initForm();
    }
  }

  onSubmit() {
    this.submitted.emit(this.form.value);
  }

  onCancel() {
    this.cancelled.emit();
  }
}
