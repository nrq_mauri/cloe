import { QuestionBase } from './QuestionBase';
import { InputTypeEnum } from './enums/InputTypeEnum';

export class ColorPickerQuestion extends QuestionBase<string> {
  controlType = InputTypeEnum.COLOR_PICKER;

  constructor(options: {} = {}) {
    super(options);
  }
}
