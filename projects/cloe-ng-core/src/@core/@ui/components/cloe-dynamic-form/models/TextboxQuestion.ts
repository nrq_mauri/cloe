import { QuestionBase } from './QuestionBase';
import { InputTypeEnum } from './enums/InputTypeEnum';

export class TextboxQuestion extends QuestionBase<string> {
  controlType = InputTypeEnum.TEXTBOX;
  type: string;

  constructor(options: {} = {}) {
    super(options);
    this.type = options['type'] || '';
  }
}
