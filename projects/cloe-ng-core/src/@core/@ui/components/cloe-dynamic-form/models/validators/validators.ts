import { ValidatorsEnum } from '../enums/ValidatorsEnum';

export const REQUIRED_VALIDATOR = {
  name: 'required',
  errorMessage: 'validation.generic.required',
  validatorType: ValidatorsEnum.REQUIRED
};

export const EMAIL_VALIDATOR = {
  name: 'email',
  errorMessage: 'validation.generic.email',
  validatorType: ValidatorsEnum.EMAIL
};

export const DATE_VALIDATOR = {
  name: 'date',
  errorMessage: 'validation.generic.date',
  validatorType: ValidatorsEnum.DATE
};

export const DATE_RANGE_VALIDATOR = {
  name: 'dateRange',
  errorMessage: 'validation.generic.date-range',
  validatorType: ValidatorsEnum.DATE_RANGE
};

export const NUMBER_VALIDATOR = {
  name: 'pattern',
  errorMessage: 'validation.generic.number',
  validatorType: ValidatorsEnum.NUMBER
};

export const PATTERN_VALIDATOR = {
  name: 'pattern',
  errorMessage: 'validation.generic.pattern',
  validatorType: ValidatorsEnum.PATTERN
};
