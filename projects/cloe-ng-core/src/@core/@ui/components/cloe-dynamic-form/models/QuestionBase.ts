import { InputTypeEnum } from './enums/InputTypeEnum';
import { CloeValidator } from './validators/CloeValidator';

export class QuestionBase<T> {
  value: T;
  key: string;
  label: string;
  validators: CloeValidator[];
  order: number;
  controlType: InputTypeEnum;

  constructor(options: {
    value?: T,
    key?: string,
    label?: string,
    validators?: CloeValidator[],
    order?: number,
    controlType?: InputTypeEnum
  } = {}) {
    this.value = options.value;
    this.key = options.key || '';
    this.label = options.label || '';
    this.validators = options.validators || [];
    this.order = options.order === undefined ? 1 : options.order;
    this.controlType = options.controlType || undefined;
  }
}
