export enum ValidatorsEnum {
  NUMBER, EMAIL, PATTERN, DATE, DATE_RANGE, REQUIRED
}
