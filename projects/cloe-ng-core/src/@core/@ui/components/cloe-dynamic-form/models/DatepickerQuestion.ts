import { QuestionBase } from './QuestionBase';
import { InputTypeEnum } from './enums/InputTypeEnum';

export class DatepickerQuestion extends QuestionBase<string> {
  controlType = InputTypeEnum.DATEPICKER;
  config: {
    onlyCalendar?: {
      isRange?,
      placeholder?
    },
    onlyTimer?: {
      isRange?,
      placeholder?
    },
    calendarAndTimer?: {
      isRange?,
      placeholder?
    }
  };

  constructor(options: {} = {}) {
    super(options);
    this.config = options['config'] || [];
  }
}
