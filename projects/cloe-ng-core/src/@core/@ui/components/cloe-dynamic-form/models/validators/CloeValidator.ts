import { ValidatorsEnum } from '../enums/ValidatorsEnum';

export class CloeValidator {
  validatorType: ValidatorsEnum;
  name?: string;
  params?: string;
  errorMessage?: string;
}
