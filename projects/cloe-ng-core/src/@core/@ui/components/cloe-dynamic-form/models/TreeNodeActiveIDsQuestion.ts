import { QuestionBase } from './QuestionBase';
import { InputTypeEnum } from './enums/InputTypeEnum';
import { TreeFiltersTypeEnum } from '../../../../services/cloe-tree-service/enums/TreeFiltersTypeEnum';

export class TreeNodeActiveIDsQuestion extends QuestionBase<string> {
  controlType = InputTypeEnum.TREE_NODE_ACTIVE_IDS;
  filtersType: TreeFiltersTypeEnum;
  nodes: any[];
  nodes$;

  constructor(options: {} = {}) {
    super(options);
    this.nodes = options['nodes'];
    this.nodes$ = options['nodes$'];
    this.filtersType = options['filtersType'];
  }
}
