import { Injectable } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { MomentDateTimeAdapter } from 'ng-pick-datetime-moment';

@Injectable()
export class CloeDateTimeAdapter extends MomentDateTimeAdapter {
  constructor(
    private translateService: TranslateService
  ) {
    super(translateService.currentLang);
    this.translateService.onLangChange.subscribe(() => {
      this.setLocale(this.translateService.currentLang);
    });
  }

}
