import { StorageService } from '../../../../services/storage-service/storage.service';
import { WeekDaysEnum } from '../enums/WeekDaysEnum';
import { findIndex, toLower } from 'lodash';

export class CloeDateTimeUtils {
  static getFirstDayOfWeek(): number {
    if (StorageService.getCurrentUser().cloeUser.startWeek) {
      const index = findIndex(Object.keys(WeekDaysEnum), (weekDay) => {
        return toLower(weekDay) === toLower(StorageService.getCurrentUser().cloeUser.startWeek);
      });
      return index !== -1 ? index : 1;
    }
    return 1;
  }
}
