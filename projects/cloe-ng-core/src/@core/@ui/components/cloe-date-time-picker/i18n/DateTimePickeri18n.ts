import { OwlDateTimeIntl } from 'ng-pick-datetime';
import { TranslateService } from '@ngx-translate/core';

export class DateTimePickeri18n extends OwlDateTimeIntl {

  /** A label for the cancel button */
  cancelBtnLabel = this.translateService.instant('global.delete');
  /** A label for the set button */
  setBtnLabel = this.translateService.instant('global.save');

  /** A label for the range 'from' in picker info */
  rangeFromLabel = this.translateService.instant('global.from');

  /** A label for the range 'to' in picker info */
  rangeToLabel = this.translateService.instant('global.to');

  constructor(
    private translateService: TranslateService
  ) {
    super();
    this.translateService.onLangChange.subscribe(() => {
      this.translate();
    });
  }

  private translate() {
    this.cancelBtnLabel = this.translateService.instant('global.delete');
    this.setBtnLabel = this.translateService.instant('global.save');
    this.rangeFromLabel = this.translateService.instant('global.from');
    this.rangeToLabel = this.translateService.instant('global.to');
  }
}
