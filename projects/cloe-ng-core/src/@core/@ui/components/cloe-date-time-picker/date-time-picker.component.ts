import {
  AfterViewInit,
  Component,
  EventEmitter, forwardRef,
  Input,
  OnInit,
  Output,
  ViewEncapsulation
} from '@angular/core';
import { isArray } from 'lodash';
import { DateTimeAdapter, OwlDateTimeIntl } from 'ng-pick-datetime';
import { CloeDateTimeAdapter } from './adapter/CloeDateTimeAdapter';
import { DateTimePickeri18n } from './i18n/DateTimePickeri18n';
import { TranslateService } from '@ngx-translate/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { DateTimeTypeEnum } from './enums/DateTimeTypeEnum';
import { isMoment, Moment } from 'moment';
import * as moment_ from 'moment';
import { CloeDateTimeUtils } from './utils/CloeDateTimeUtils';
const moment = moment_;

export const DATE_TIME_PICKER_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => CloeDateTimePickerComponent),
  multi: true
};

@Component({
  selector: 'cloe-core-date-time-picker',
  templateUrl: './date-time-picker.component.html',
  styleUrls: ['./date-time-picker.component.scss'],
  providers: [
    DATE_TIME_PICKER_VALUE_ACCESSOR,
    {provide: OwlDateTimeIntl, useClass: DateTimePickeri18n, deps: [TranslateService]},
    {provide: DateTimeAdapter, useClass: CloeDateTimeAdapter}
  ],
  encapsulation: ViewEncapsulation.None
})

export class CloeDateTimePickerComponent implements OnInit, AfterViewInit, ControlValueAccessor {
  firstWeekDay;
  @Input() config: {
    inputType: DateTimeTypeEnum;
    isRange?,
    placeholder?
  };
  dateTimeType = DateTimeTypeEnum;
  @Output() dateChanged = new EventEmitter();

  dateModel;
  value;
  onChange = (_: any) => { };

  constructor() {}

  ngOnInit() {
    this.firstWeekDay = CloeDateTimeUtils.getFirstDayOfWeek();
  }

  ngAfterViewInit() {
  }
  private formatDate(value: Moment) {
    if (this.config.inputType === this.dateTimeType.TIMER) {
      return value.format('HH:mm:ss');
    } else {
      return value.toISOString(true);
    }
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
  }

  writeValue(obj: any): void {
    this.dateModel = this.checkInitDate(obj);
    const value: any = {};
    if (obj && isArray(obj)) {
      value['fromDate'] = this.formatDate(obj[0]);
      value['toDate'] = this.formatDate(obj[1]);
      this.value = value;
      this.onChange(this.value);
    } else {
      this.value = obj;
      this.onChange(this.dateModel);
    }
  }

  checkInitDate(date) {
    if (isMoment(date) || isArray(date)) {
      return date;
    }
    if (date && date.fromDate && date.toDate) {
      return [
        moment(date.fromDate),
        moment(date.toDate)
      ];
    } else {
      return moment(date);
    }
  }
}
