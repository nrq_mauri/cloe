import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CloeModalComponentInterface } from '../../../../interfaces/cloe-modal-component.interface';

@Component({
  selector: 'cloe-core-help-modal',
  templateUrl: './help-modal.component.html',
  styleUrls: ['./help-modal.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HelpModalComponent implements OnInit, CloeModalComponentInterface {
  modalTitle: string;
  bodyMessage;

  constructor(
    private modalRef: NgbActiveModal
  ) {}

  ngOnInit() {}

  initData(data?: any) {
    this.modalTitle = 'component.cloe-help-online.modalTitle';
    this.bodyMessage = data;
  }

  ok() {
    this.modalRef.close(true);
  }
}
