import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'cloe-core-confirm-modal',
  templateUrl: './confirm-modal.component.html'
})
export class ConfirmModalComponent {
  constructor(private modalRef: NgbActiveModal) {
  }

  confirmMessage;

  ok() {
    this.modalRef.close(true);
  }

  cancel() {
    this.modalRef.close(false);
  }
}
