import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TransitionService, StateObject, StateService } from '@uirouter/angular';
import { CloeModalComponentInterface } from '../../../interfaces/cloe-modal-component.interface';
import { ModalService } from '../../../services/modal-service/modal-service';
import { HelpModalComponent } from '../modals';
import { PageHelpService } from '../../../services/page-help-service/page-help-service';

@Component({
  selector: 'cloe-core-help-online',
  templateUrl: './cloe-help-online.component.html',
  styleUrls: ['./cloe-help-online.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloeHelpOnlineComponent implements OnInit {

  currentStateHierarchy: string[] = [];

  constructor(
    private pageHelpService: PageHelpService,
    private stateService: StateService,
    private transitionService: TransitionService,
    private modalService: ModalService
  ) {
    this.transitionService.onSuccess({to: '*'}, transition => {
      this.buildStateHierarchy(transition.targetState().$state().path);
    });
  }

  ngOnInit() {
    this.buildStateHierarchy(this.stateService.$current.path);
  }

  buildStateHierarchy(states: StateObject[]) {
    this.currentStateHierarchy = [];
    states.forEach((state) => {
      this.currentStateHierarchy.push(state.name);
    });
  }

  onHelpClicked() {
    this.pageHelpService.getPageHelpByStateList(this.currentStateHierarchy).subscribe((res: any) => {
      this.modalService.open(HelpModalComponent, {size: 'lg', keyboard: false})
        .then((result: any) => {
        }).catch(() => {
        // dummy catch, we don't use it for now
      });
      (<CloeModalComponentInterface>this.modalService.getModalInstance().componentInstance)
        .initData(res ? res.htmlHelpText : undefined);
    });
  }
}
