import { Component, DoCheck, forwardRef, Input, IterableDiffers, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { CloeTreeComponent } from '../cloe-tree/cloe-tree.component';
import { ITreeState, TREE_ACTIONS } from 'angular-tree-component';
import { IDType, IDTypeDictionary } from 'angular-tree-component/dist/defs/api';
import { TreeFiltersTypeEnum } from '../../../services/cloe-tree-service/enums/TreeFiltersTypeEnum';
import { CloeTreeFiltersService } from '../../../services/cloe-tree-service/cloe-tree-filters.service';

export const TREE_INPUT_VALUE_ACCESSOR: any = {
  provide: NG_VALUE_ACCESSOR,
  useExisting: forwardRef(() => CloeTreeInputNodeIDsComponent),
  multi: true
};

@Component({
  selector: 'cloe-core-input-node-ids',
  providers: [ TREE_INPUT_VALUE_ACCESSOR ],
  templateUrl: './cloe-tree-input-nodeIDs.component.html',
  styleUrls: ['./cloe-tree-input-nodeIDs.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloeTreeInputNodeIDsComponent implements OnInit, DoCheck, ControlValueAccessor {

  @ViewChild(CloeTreeComponent) cloeTree: CloeTreeComponent;
  @Input() nodes;
  treeState = new class implements ITreeState {
    activeNodeIds: IDTypeDictionary;
    expandedNodeIds: IDTypeDictionary;
    focusedNodeId: IDType;
    hiddenNodeIds: IDTypeDictionary;
    selectedLeafNodeIds: IDTypeDictionary;
    selectedNodeIds: IDTypeDictionary;
  };
  options = {
    allowDrag: node => !node.isActive,
    useCheckbox: false,
    actionMapping: {
      mouse: {
        click: (tree, node, $event) => {
          if (node.data.nodeCategory !== 'LEAF') {
            return;
          }
          node.toggleSelected();
          TREE_ACTIONS.TOGGLE_ACTIVE_MULTI(tree, node, $event);
        }
      }
    }
  };
  iterableDiffer;
  @Input() treeFiltersType: TreeFiltersTypeEnum;
  filtersConfig;
  value;
  onChange = (_: any) => { };

  constructor(
    private treeFiltersService: CloeTreeFiltersService,
    private _iterableDiffers: IterableDiffers
  ) {
    this.iterableDiffer = this._iterableDiffers.find([]).create(null);
  }

  ngOnInit(): void {
  }

  ngDoCheck() {
    const changes = this.iterableDiffer.diff(this.nodes);
    if (changes) {
      if (this.treeFiltersType) {
        this.filtersConfig = this.treeFiltersService.addFiltersByType(this.treeFiltersType, this.nodes);
      }
    }
  }

  onNodeActive($event) {
    this.writeValue(this.cloeTree.tree.treeModel.activeNodeIds);
  }

  onNodeDeactive($event) {
    this.writeValue(this.cloeTree.tree.treeModel.activeNodeIds);
  }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  writeValue(obj: any): void {
    if (!this.value) {
      this.treeState.activeNodeIds = obj;
    }
    this.value = obj;
    this.onChange(this.value);
  }

}
