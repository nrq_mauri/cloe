import { Component, EventEmitter, Input, OnInit, Output, QueryList, ViewChildren } from '@angular/core';
import { AgmMarker, LatLngBounds } from '@agm/core';

declare var google: any;

@Component({
  selector: 'cloe-core-map',
  templateUrl: './cloe-map.component.html',
  styleUrls: ['./cloe-map.component.scss']
})
export class CloeMapComponent implements OnInit {
  @Input() sites;
  @Output() mapReady = new EventEmitter();
  @Output() markerCentered = new EventEmitter();
  bounds: LatLngBounds;
  map: any;
  @ViewChildren(AgmMarker)
  markers: QueryList<AgmMarker>;

  onCenterChange(coodinates) {
  }

  onMapReady(_map) {
    this.map = _map;
    this.mapReady.emit(_map);
    this.bounds = new google.maps.LatLngBounds();
    this.sites.forEach((site) => {
      this.bounds.extend(new google.maps.LatLng(site.latitude, site.longitude));
    });
  }

  ngOnInit(): void {
  }
}
