import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TransitionService, StateObject, StateService } from '@uirouter/angular';

@Component({
  selector: 'cloe-core-breadcrumb',
  templateUrl: './cloe-breadcrumb.component.html',
  styleUrls: ['./cloe-breadcrumb.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloeBreadcrumbComponent implements OnInit {

  breadcrumbs: BreadCrumb[] = [];

  constructor(
    private stateService: StateService,
    private transitionService: TransitionService
  ) {
    this.transitionService.onSuccess({to: '*'}, transition => {
      this.buildBreadCrumb(transition.targetState().$state().path);
    });
  }

  ngOnInit() {
    this.buildBreadCrumb(this.stateService.$current.path);
  }

  buildBreadCrumb(states: StateObject[]) {
    this.breadcrumbs = [];
    states.forEach((state) => {
      if (state.data && state.data.breadcrumbLabel) {
        this.breadcrumbs.push({
          label: state.data.breadcrumbLabel,
          stateName: state.name,
          params: state.params
        });
      }
    });
  }
}

export interface BreadCrumb {
  label: any;
  stateName: any;
  params: any;
}
