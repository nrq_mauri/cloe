import { AfterViewInit, Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { NgbAccordion, NgbPanel } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'cloe-core-accordion',
  templateUrl: './cloe-accordion.component.html',
  styleUrls: ['./cloe-accordion.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class CloeAccordionComponent implements OnInit {

  @Input() accordionTitle;
  @Input() initOpen;
  panelId;
  activeIds = [];

  constructor() {
    this.panelId = 'A-' + Math.floor(Math.random() * 10000);
  }

  ngOnInit(): void {
    if (this.initOpen) {
      this.activeIds.push(this.panelId);
    }
  }
}
