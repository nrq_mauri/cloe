import {
  Component, DoCheck,
  EventEmitter,
  Input,
  IterableDiffers,
  OnDestroy,
  OnInit,
  Output,
  ViewEncapsulation
} from '@angular/core';
import { PAGINATION } from '../configuration/cloe-table-static-conf';
import { cloneDeep, get, forEach, values, isString } from 'lodash';
import { CsvService } from '../../../../services/csv-service/csv.service';
import { ACTIONS } from '../../../../enums/ActionsEnum';
import { Subject, Subscription } from 'rxjs';
import { CloeBoxActionsEnum, cloeBoxSubject$ } from '../../cloe-box/cloe-box.component';
import { CloeModalComponentInterface } from '../../../../interfaces/cloe-modal-component.interface';
import { ModalService } from '../../../../services/modal-service/modal-service';
import {ICloeTableStatic} from '../cloe-table-static-interface/cloe-table-static-interface';

export const staticTableSubject$ = new Subject();

@Component({
  selector: 'cloe-core-table-static',
  templateUrl: './cloe-table-static.component.html',
  styleUrls: ['./cloe-table-static.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloeTableStaticComponent implements OnInit, OnDestroy, DoCheck {
  header: any;
  groupedHeader: any = {};
  headerLeaves = [];
  metadata: any;
  actions: any;
  pagination: any;
  filtersConfig: any;
  filters: { [k: string]: string } = {};
  conditionalPipesDisabled = false;
  iterableDiffer;
  cloeBoxSubscription: Subscription;
  _data: any;

  @Input() data: any;
  @Input() filtersInit;
  @Input() tableConfig: ICloeTableStatic;
  @Input() tableTitleArgs = {};
  @Output() scrollbarHeightToggled = new EventEmitter();

  constructor(
    private csvService: CsvService,
    private modalService: ModalService,
    private _iterableDiffers: IterableDiffers
  ) {
    this.iterableDiffer = this._iterableDiffers.find([]).create(null);
  }

  ngOnInit() {
    if (!this._data) { this._data = []; }
    this.header = this.tableConfig.header;
    this.metadata = this.tableConfig.metadata;
    this.actions = this.tableConfig.actions;
    this.filtersConfig = this.tableConfig.filters;
    this.pagination = this.tableConfig.pagination || cloneDeep(PAGINATION);
    if (this.metadata && this.metadata.fixPageSize) { this.pagination.size = this.metadata.fixPageSize; }

    this.filters = Object.assign(this.filters, this.filtersInit);
    this.cloeBoxSubscription = cloeBoxSubject$.asObservable().subscribe((e: any) => {
      switch (e.action) {
        case CloeBoxActionsEnum.TOGGLE_EXPANDED:
          this.conditionalPipesDisabled = e.data.value;
          break;
      }
    });
    forEach(this.header, (element) => {
      this.buildTree(element, 0, this.groupedHeader);
    });
    this.groupedHeader = values(this.groupedHeader);
  }

  ngDoCheck() {
    const changes = this.iterableDiffer.diff(this.header);
    if (changes) {
      this.groupedHeader = {};
      this.headerLeaves = [];
      forEach(this.header, (element) => {
        this.buildTree(element, 0, this.groupedHeader);
      });
      this.groupedHeader = values(this.groupedHeader);
    }
    const dataChanges = this.iterableDiffer.diff(this.data);
    if (dataChanges) {
      this._data = [...this.data];
    }
  }

  filterData() {
    this.filters = cloneDeep(this.filters);
  }

  loadPage(page: number) {
    this.pagination.page = page;
  }

  sizeChange(size) {
    this.pagination.size = size;
  }

  getValueFromRow(row, field) {
    return get(row, field);
  }

  isI18nEnabled(row, field) {
    // if not set, we translate it by default
    if (!field) { return true; }
    return !row[field];
  }

  downloadCSV() {
    const dataToExportCSV = this.csvService.generateDataToExportCSV(this.header, this._data);
    this.csvService.downloadCSV(dataToExportCSV);
  }

  onActionClick(actionType: ACTIONS, metadata: any, data: any) {
    switch (actionType) {
      case ACTIONS.TRIGGER_SUBJECT:
        metadata.triggerSubject.next({
          action: metadata.action,
          data: data,
          metadata: metadata
        });
        break;
      case ACTIONS.OPEN_MODAL:

        if (!metadata.modalComponent) {
          throw Error(
            `No component found for ${metadata.modalComponent}, are you sure you mapped your component in modal.components.config?`
          );
        }

        this.modalService.open(metadata.modalComponent, {size: 'lg', keyboard: false})
          .then((result: any) => {
            result = isString(result) ? result : result.actionPostClose;
            switch (result) {
              case 'RELOAD':
                staticTableSubject$.next({action: ACTIONS.RELOAD_DATA});
                break;
            }
          }).catch(() => {
          // dummy catch, we don't use it for now
        });

        data.metadata = this.metadata;
        data.btnMetadata = metadata;
        (<CloeModalComponentInterface>this.modalService.getModalInstance().componentInstance).initData(data);
        break;
    }
  }

  addNewEntry() {
    const actionType = this.metadata.addNewButton.actionType;
    const triggerSubject = this.metadata.addNewButton.triggerSubject;
    const action = this.metadata.addNewButton.action;
    switch (actionType) {
      case ACTIONS.TRIGGER_SUBJECT:
        triggerSubject.next({
          action: action
        });
        break;
      case ACTIONS.OPEN_MODAL:
      default:
        const modalComponent = this.metadata.addNewButton.modalComponent;
        if (!modalComponent) {
          throw Error(
            `No component found for ${modalComponent}, are you sure you mapped your component in modal.components.config?`
          );
        }

        this.modalService.open(modalComponent, {size: 'lg', keyboard: false})
          .then((result: any) => {
            result = isString(result) ? result : result.actionPostClose;
            switch (result) {
              case 'RELOAD':
                staticTableSubject$.next({action: ACTIONS.RELOAD_DATA});
                break;
            }
          }).catch(() => {
          // dummy catch, we don't use it for now
        });
        (<CloeModalComponentInterface>this.modalService.getModalInstance().componentInstance).initData();
    }
  }

  // Table static's headers normalization
  private buildTree(parent, level, result: any) {
    result[level] = result[level] ? [...result[level], parent] : [parent];
    if (parent.children) {
      level++;
    } else {
      this.headerLeaves.push(parent); // add parent to headerLeaves and return headerLeaves's length
    }
    forEach(parent.children, (element) => {this.buildTree(element, level, result); });
  }

  ngOnDestroy() {
    this.cloeBoxSubscription.unsubscribe();
  }
}
