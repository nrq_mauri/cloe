export const PAGINATION = {
  'size': 3,
  'page': 1,
  'sizeOptions': [3, 10, 15, 30]
};
