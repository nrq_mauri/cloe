import {ITableHeader} from './header-interface';
import {ITableMetadata} from './metadata-interface';
import {ITablePagination} from './pagination-interface';
import {ICommonTableAction} from '../../../../models/common-table.interface';
import {ITableActions} from './actions-interface';


export interface ICloeTableStatic {
  header: ITableHeader[];
  actions?: ITableActions | ICommonTableAction[] ;
  metadata: ITableMetadata;
  pagination?: ITablePagination;
  filters?;
}
