import {I18n, ICommonTableHeader} from '../../../../models/common-table.interface';

export interface ITableHeader extends ICommonTableHeader{
  i18n?: I18nStatic;
  cellFormatter?: Function;
  pipeArgs?: any;
  skipPipeOnExport?: boolean;
  conditionalPipe?: boolean;
  pipeArgField?: string;
  immutableSymbol?: boolean;
  rowHeader?: boolean;
  rowspan?: number;
}

  export interface I18nStatic extends I18n {
    conditionalField?: string;
  }
