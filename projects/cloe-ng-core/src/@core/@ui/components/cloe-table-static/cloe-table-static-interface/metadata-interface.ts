import {IAddNewButton, ICommonTableMetadata} from '../../../../models/common-table.interface';

export interface ITableMetadata extends ICommonTableMetadata {
  addNewButton?: IAddNewButtonStatic;

  disablePagination?: boolean;
  fixPageSize?: number;
  secondarySort?: string;
  disableSort?: boolean;
  tableTitleKey?: string;
}

  export interface IAddNewButtonStatic extends IAddNewButton{
    actionType?: any;
    action?: any;
    triggerSubject?: any;
  }
