export interface ITablePagination {
  size: number;
  page: number;
  sizeOptions: number[];
  maxSize: number;
}
