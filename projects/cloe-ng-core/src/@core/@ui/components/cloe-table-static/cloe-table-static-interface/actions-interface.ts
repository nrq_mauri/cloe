
import {ACTIONS} from '../../../../enums/ActionsEnum';
import {Subject} from 'rxjs';

export interface ITableActions {
  icon: string;
  actionType: ACTIONS;
  metadata: ITableMetadataAction;
}

  export interface ITableMetadataAction {
    triggerSubject?: any;
    action?: any;
     deleteByField?: string;
     altMessage?: string;
    modalComponent?: any;
  }

