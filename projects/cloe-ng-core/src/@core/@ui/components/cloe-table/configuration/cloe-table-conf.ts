export const PAGINATION = {
  'size': 15,
  'page': 1,
  'sizeOptions': [15, 30, 50, 100]
};
