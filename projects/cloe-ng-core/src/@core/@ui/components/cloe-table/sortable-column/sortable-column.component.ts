import {Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges} from '@angular/core';
@Component({
  selector: 'cloe-core-sortable-column',
  templateUrl: './sortable-column.component.html',
  styleUrls: ['./sortable-column.component.css']
})
export class SortableColumnComponent implements OnInit, OnChanges {

  @Input() columnName;
  @Input() natureType;
  @Input() sortedColumn;
  @Input() fieldName;
  @Input() metadata;
  @Output() sorted = new EventEmitter();
  sortDirection = '';
  constructor(
  ) { }

  ngOnInit() {
    if (this.metadata && this.metadata.sort === this.fieldName && this.metadata.sortType !== null) {
      this.sortDirection = this.metadata.sortType;
    }
  }

  sort() {
    if (this.metadata.disableSort) {
      return;
    }
    this.sortDirection = this.sortDirection === 'asc' ? 'desc' : 'asc';
    this.metadata.sortType = this.sortDirection;
    this.metadata.sort = this.fieldName;
    this.sorted.emit();
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.sortedColumn.currentValue !== this.fieldName) {
      this.sortDirection = '';
    }
  }
}
