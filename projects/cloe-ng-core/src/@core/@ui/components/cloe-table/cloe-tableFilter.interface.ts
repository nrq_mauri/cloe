import {FilterTypeEnum} from '../../../enums/FilterTypeEnum';

export interface ITableFilter {
  filterType: FilterTypeEnum;
  filterField: string;
  fieldLabel: string;
  i18nPrefix?: string;
  items?: string[];
}


