import {ITableMetadata} from './cloe-metadata.interface';
import {ITableAction} from './cloe-tableAction.interface';
import {ITableFilter} from './cloe-tableFilter.interface';
import {ICommonTableHeader} from '../../../models/common-table.interface';


export interface ITableConfig {
  header: ICommonTableHeader[];
  metadata?: ITableMetadata;
  actions?: ITableAction[];
  filters?: ITableFilter[];
}


