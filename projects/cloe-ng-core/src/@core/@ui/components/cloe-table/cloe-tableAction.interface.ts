import {ICommonTableAction} from '../../../models/common-table.interface';
import {Subject} from 'rxjs';

export interface ITableAction extends ICommonTableAction {
  icon: string;
  actionType: any;
  metadata: ITableMetadataHeader;
  iconType?: any;
}


export interface ITableMetadataHeader {
  modalComponent?: any;
  altMessage: string;
  deleteByField?: string;
  confirmMessage?: string;
  fileType?: any;
  visible?: Function;
  triggerSubject?: Subject<any>;
  action?: any;
  nodeNatureType?: string;
  toState?: string;
  cityId?: string;
  filters?: IFiltersMetadatiActionInterface[];
}


export interface IFiltersMetadatiActionInterface {
  filterField: string;
  filterValueField: string;
}
