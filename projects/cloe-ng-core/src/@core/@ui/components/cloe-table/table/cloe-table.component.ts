import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { StateService, TransitionService } from '@uirouter/angular';
import { PAGINATION } from '../configuration/cloe-table-conf';
import { ConfirmModalComponent } from '../../modals';
import { cloneDeep, get, isString } from 'lodash';
import { UserService } from '../../../../services/user-service/user-service';
import { CloeSearchService } from '../../../../services/cloe-search-service/cloe.search.service';
import { CloeDeleteService } from '../../../../services/cloe-delete-service/cloe-delete.service';
import { ModalService } from '../../../../services/modal-service/modal-service';
import { CloeUser } from '../../../../models/CloeUser';
import { CloeModalComponentInterface } from '../../../../interfaces/cloe-modal-component.interface';
import { ACTIONS } from '../../../../enums/ActionsEnum';
import { Subject, Subscription } from 'rxjs';
import {ITableConfig} from '../cloe-table.interface';

export const reloadTableDataSubject$ = new Subject();

@Component({
  selector: 'cloe-core-table',
  templateUrl: './cloe-table.component.html',
  styleUrls: ['./cloe-table.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloeTableComponent implements OnInit {
  data: any;
  header: any;
  metadata: any;
  actions: any;
  pagination: any;
  filtersConfig: any;
  filters: { [k: string]: string } = {};
  reloadTableDataSubscription: Subscription;


  @Input() filtersInit;
  @Input() natureType: String;
  @Input() tableConfig: ITableConfig;


  constructor(
    private cloeSearchService: CloeSearchService,
    private cloeDeleteService: CloeDeleteService,
    private stateService: StateService,
    private modalService: ModalService,
    private userService: UserService
  ) {
    this.reloadTableDataSubscription = reloadTableDataSubject$.asObservable().subscribe(() => this.loadData());
  }

  ngOnInit() {
    this.header = this.tableConfig.header;
    this.metadata = this.tableConfig.metadata;
    this.actions = this.tableConfig.actions;
    this.filtersConfig = this.tableConfig.filters;
    this.pagination = cloneDeep(PAGINATION);
    this.filters = Object.assign(this.filters, this.filtersInit);
    this.loadData();
  }

  onFilterChange() {
    this.loadData();
  }

  loadPage(page: number) {
    this.pagination.page = page;
    this.loadData();
  }

  sortColumn() {
    this.loadData();
  }

  sizeChange(size) {
    this.pagination.size = size;
    this.loadData();
  }

  loadData() {
    this.cloeSearchService.search(this.natureType, this.metadata, this.pagination, this.filters).subscribe(result => {
      this.data = result.content;
      this.pagination.totalItems = parseInt(result.pageMetadata.totalItems, 10);
    });
  }

  getValueFromRow(row, field) {
    return get(row, field);
  }

  deleteByUuid(uuid: String) {
    this.cloeDeleteService.deleteByUuid(this.natureType, uuid).subscribe(() => this.loadData());
  }

  deleteUserCloeUser(cloeUser: CloeUser) {
    this.userService.deleteUserCloeUser(cloeUser.id).subscribe(() => this.loadData());
  }

  reload() {
    this.loadData();
  }

  /*
  * Metodo che viene invocato quando si clicca sul bottone di inserimento;
  * Se si verifica qualche errore si entra nell'if e si genera un messaggio di errore;
  * altrimenti si richiama il servizio riguardante la modale;
  * */
  addNewEntry() {
    const modalComponent = this.metadata.addNewButton.modalComponent;
    if (!modalComponent) {
      throw Error(
        `No component found for ${modalComponent}, are you sure you mapped your component in modal.components.config?`
      );
    }

    this.modalService.open(modalComponent, {size: 'lg', keyboard: false})
      .then((result: any) => {
        result = isString(result) ? result : result.actionPostClose;
        switch (result) {
          case 'RELOAD':
            this.loadData();
            break;
        }
      }).catch(() => {
      // dummy catch, we don't use it for now
    });
    (<CloeModalComponentInterface>this.modalService.getModalInstance().componentInstance).initData();
  }

  /*
  * Metodo che permette di generare un evento quando si clicca su un bottone;
  * in base al bottone si entra in un case anzichè in un altro;
  * CASE OPEN_MODAL: caso in cui si verifica l'apertura di una modale;
  * CASE DELETE: caso in cui avviene la rimozione di un elemento all'interno del sistema;
  * CASE DELETE_USER: caso in cui avviene la rimozione di un "utente" all'interno del sistema;
  * CASE TRIGGER_SUBJECT: caso in cui
  * CASE REDIRECT_TO: caso in cui avviene il redirect con i dati aggiornati;
  * */
  onActionClick(actionType: ACTIONS, metadata: any, data: any) {
    switch (actionType) {
      case ACTIONS.OPEN_MODAL:

        if (!metadata.modalComponent) {
          throw Error(
            `No component found for ${metadata.modalComponent}, are you sure you mapped your component in modal.components.config?`
          );
        }

        this.modalService.open(metadata.modalComponent, {size: 'lg', keyboard: false})
          .then((result: any) => {
            result = isString(result) ? result : result.actionPostClose;
            switch (result) {
              case 'RELOAD':
                this.loadData();
                break;
            }
          }).catch(() => {
          // dummy catch, we don't use it for now
        });

        data.metadata = this.metadata;
        data.natureType = this.natureType;
        data.btnMetadata = metadata;
        (<CloeModalComponentInterface>this.modalService.getModalInstance().componentInstance).initData(data);
        break;

      case ACTIONS.DELETE:
        this.modalService.open(ConfirmModalComponent, {keyboard: false}).then(result => {
          if (result) {
            this.deleteByUuid(data[metadata.deleteByField]);
          }
        }).catch(() => {
          // dummy catch, we don't use it for now
        });
        this.modalService.getModalInstance().componentInstance.confirmMessage = metadata.confirmMessage;
        break;

      case ACTIONS.DELETE_USER:
        this.modalService.open(ConfirmModalComponent, {keyboard: false}).then(result => {
          if (result) {
            this.deleteUserCloeUser(data[metadata.deleteByField]);
          }
        }).catch(() => {
          // dummy catch, we don't use it for now
        });
        this.modalService.getModalInstance().componentInstance.confirmMessage = metadata.confirmMessage;
        break;
      case ACTIONS.TRIGGER_SUBJECT:
        metadata.triggerSubject.next({
          action: metadata.action,
          data: data,
          metadata: metadata
        });
        break;
      case ACTIONS.REDIRECT_TO:
        this.stateService.go(
          metadata.toState,
          {
            data: data,
            metadata: metadata
          }
        );
        break;
    }
  }
}
