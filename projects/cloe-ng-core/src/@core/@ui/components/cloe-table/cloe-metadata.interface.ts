import {ICommonTableMetadata} from '../../../models/common-table.interface';

export interface ITableMetadata extends ICommonTableMetadata {
  msContext?: any;
  reloadButton?: boolean;
  search?: ITableSearchMetadata;
}


export interface ITableSearchMetadata {
  showedFieldName?: string;
  fieldToSearch?: any;
  nodeNature?: string;
}
