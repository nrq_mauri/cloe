import { Component, EventEmitter, Input, Output, ViewEncapsulation } from '@angular/core';
import { FilterTypeEnum } from '../../../../enums/FilterTypeEnum';
import { set, assign, map, get } from 'lodash';

@Component({
  selector: 'cloe-core-table-filter',
  templateUrl: './cloe-table-filter.component.html',
  styleUrls: ['./cloe-table-filter.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloeTableFilterComponent {
  @Input() filterConf;
  @Input() filters = {};
  filterEnum = FilterTypeEnum;
  @Output() filterChange = new EventEmitter();

  setFilter(value, filterField) {
    set(this.filters, filterField, value);
  }

  doFilter() {
    console.log('filters from', this.filters);
    this.filterChange.emit();
  }
}
