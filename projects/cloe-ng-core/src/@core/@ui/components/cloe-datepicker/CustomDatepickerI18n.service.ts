import {Injectable} from '@angular/core';
import {NgbDatepickerI18n, NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';
import {TranslateService} from '@ngx-translate/core';

@Injectable()
export class CustomDatepickerI18n extends NgbDatepickerI18n {

  i18nValues: any;
  constructor(
    private translateService: TranslateService
  ) {
    super();
    this.i18nValues = this.translateService.instant('component.date-picker.i18n');
    this.translateService.onLangChange.subscribe(() => {
      this.i18nValues = this.translateService.instant('component.date-picker.i18n');
    });
  }

  getWeekdayShortName(weekday: number): string {
    return this.i18nValues.weekdays[weekday - 1];
  }
  getMonthShortName(month: number): string {
    return this.i18nValues.shortMonths[month - 1];
  }
  getMonthFullName(month: number): string {
    return this.i18nValues.months[month - 1];
  }

  getDayAriaLabel(date: NgbDateStruct): string {
    return `${date.day}-${date.month}-${date.year}`;
  }
}
