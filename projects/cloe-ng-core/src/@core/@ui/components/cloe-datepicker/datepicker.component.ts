import { Component, EventEmitter, Input, OnChanges, OnInit, Output } from '@angular/core';
import { NgbDatepickerI18n, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { CloeDatePipe } from '../../pipes';
import * as moment_ from 'moment';
import { CustomDatepickerI18n } from './CustomDatepickerI18n.service';

const moment = moment_;

const equals = (one: NgbDateStruct, two: NgbDateStruct) =>
  one && two && two.year === one.year && two.month === one.month && two.day === one.day;

const before = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day < two.day : one.month < two.month : one.year < two.year;

const after = (one: NgbDateStruct, two: NgbDateStruct) =>
  !one || !two ? false : one.year === two.year ? one.month === two.month ? one.day === two.day
    ? false : one.day > two.day : one.month > two.month : one.year > two.year;

@Component({
  selector: 'cloe-core-date-picker',
  templateUrl: './datepicker.component.html',
  styleUrls: ['./datepicker.component.scss'],
  providers: [{provide: NgbDatepickerI18n, useClass: CustomDatepickerI18n}]
})

export class DatepickerComponent implements OnInit, OnChanges {
  @Input() isRange;
  @Input() cardTitle;
  @Input() initStartDate;
  @Input() initEndDate;
  @Input() initSingleDate;
  @Output() dateChanged = new EventEmitter();
  // Model for range selection
  hoveredDate: NgbDateStruct;
  fromDate: NgbDateStruct;
  toDate: NgbDateStruct;
  singleDate: NgbDateStruct;

  constructor(
    private cloeDatePipe: CloeDatePipe
  ) {
  }

  ngOnInit() {
    this.initializeData();
  }

  initializeData() {
    if (this.initStartDate) {
      this.initStartDate = new Date(this.initStartDate);
      this.fromDate = {
        day: this.initStartDate.getUTCDate(),
        month: this.initStartDate.getUTCMonth() + 1,
        year: this.initStartDate.getUTCFullYear()
      };
    }
    if (this.initEndDate) {
      this.initEndDate = new Date(this.initEndDate);
      this.toDate = {
        day: this.initEndDate.getUTCDate(),
        month: this.initEndDate.getUTCMonth() + 1,
        year: this.initEndDate.getUTCFullYear()
      };
    }
    if (this.initSingleDate) {
      this.initSingleDate = new Date(this.initSingleDate);
      this.singleDate = {
        day: this.initSingleDate.getUTCDate(),
        month: this.initSingleDate.getUTCMonth() + 1,
        year: this.initSingleDate.getUTCFullYear()
      };
    }
  }

  ngOnChanges() {
    this.initializeData();
  }

  // Methods for range selection
  isHovered = date => this.fromDate && !this.toDate && this.hoveredDate && after(date, this.fromDate) && before(date, this.hoveredDate);

  isInside = date => after(date, this.fromDate) && before(date, this.toDate);

  isFrom = date => equals(date, this.fromDate);

  isTo = date => equals(date, this.toDate);

  onDateChange(date: NgbDateStruct, datePicker) {
    if (!this.fromDate && !this.toDate) {
      this.fromDate = date;
    } else if (this.fromDate && !this.toDate && after(date, this.fromDate)) {
      this.toDate = date;
      this.dateChanged.emit([
        moment.utc(this.parseDate(this.fromDate), 'YYYY-MM-DD'),
        moment.utc(this.parseDate(this.toDate), 'YYYY-MM-DD')
      ]);
      datePicker.close();
    } else {
      this.toDate = null;
      this.fromDate = date;
    }
  }

  onSingleDateChange(date: NgbDateStruct) {
    this.dateChanged.emit([
      moment.utc(this.parseDate(date), 'YYYY-MM-DD')
    ]);
  }

  get formattedSingleDate(): string {
    if (!this.singleDate) {
      return 'component.date-picker.startDateNotExist';
    }

    return this.formatDate(this.singleDate);
  }

  get formattedDateRange(): string {
    if (!this.fromDate) {
      return 'component.date-picker.startDateNotExist';
    }
    const fromDateFormatted = this.formatDate(this.fromDate);

    return this.toDate ? fromDateFormatted + ' - ' + this.formatDate(this.toDate) : fromDateFormatted;
  }

  formatDate(date: NgbDateStruct) {
    return this.cloeDatePipe.transform(this.parseDate(date), 'YYYY-MM-DD');
  }

  parseDate(date: NgbDateStruct) {
    return date.year + '/' + date.month + '/' + date.day;
  }
}
