import {
  ConfirmModalComponent,
  HelpModalComponent,
} from './modals';

export const MODAL_COMPONENTS = [
  // Generic modal components
  ConfirmModalComponent,
  HelpModalComponent,
];
