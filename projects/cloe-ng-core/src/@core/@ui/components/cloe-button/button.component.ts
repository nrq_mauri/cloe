import {Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation} from '@angular/core';

@Component({
  selector: 'cloe-core-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class CloeButtonComponent implements OnInit {

  private base = '';
  class = '';

  @Input() type = 'primary';
  @Input() buttonType = 'button';
  @Input() outline = false;
  @Input() disable = false;
  @Input() size = 'sm';
  @Input() customClass: string;
  @Input() icon: string;
  @Input() iconType: string;
  @Input() text: string;
  @Input() altLabel: string;
  @Input() isIconButton: string;

  @Output() clickEvent = new EventEmitter();

  constructor() {
  }

  ngOnInit() {
    this.button();
  }

  button() {

    // CUSTOM CLASS
    if (this.isValid(this.customClass)) {
      this.base = this.customClass;
    }

    // TYPE
    if (this.isValid(this.type) && !this.isValid(this.customClass)) {
      this.base = this.base + ' btn-' + this.type;
    }

    // OUTLINE
    if (this.outline) {
      this.base = this.base + ' btn-outline-' + this.type;
    }

    // SIZE
    if (this.isValid(this.size)) {
      this.base = this.base + ' btn-' + this.size;
      if (this.isIconButton) {
        this.base += ' btn-icon';
      }
    }

    this.class = 'btn ' + this.base + ' ml-1';
  }

  isValid(srt: string): boolean {
    return !!srt;
  }

  clickHandler($event) {
    this.clickEvent.emit($event);
  }

}
