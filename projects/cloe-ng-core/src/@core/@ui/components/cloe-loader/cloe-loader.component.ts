import { Component, ViewEncapsulation } from '@angular/core';
import { SubjectsService } from '../../../services/resize-service/subjects.service';
import { delay } from 'rxjs/operators';

@Component({
  selector: 'cloe-core-loader',
  templateUrl: './cloe-loader.component.html',
  styleUrls: ['./cloe-loader.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloeLoaderComponent {
  loading$;

  constructor(public subjectsService: SubjectsService) {
    this.loading$ = this.subjectsService.httpResquestPendingInformer$
      .pipe(
        delay(0)
      );
  }

}
