import { AfterViewInit, Directive, ElementRef, Input, OnChanges, OnInit } from '@angular/core';

@Directive({
  selector: '[cloe-core-focusable]'
})
export class CloeFocusableDirective implements OnChanges, AfterViewInit {

  @Input()
  focusable = true;

  constructor(private element: ElementRef) {
  }

  ngOnChanges(): void {
    this._setAllElementsNotFocusable();
  }

  ngAfterViewInit(): void {
    this._setAllElementsNotFocusable();
  }

  private _setAllElementsNotFocusable() {
    const elements = this.element.nativeElement.querySelectorAll('button, input, select, a');
    if (elements && elements.length) {
      elements.forEach(e => {
        e.setAttribute('tabindex', this.focusable ? '0' : '-1');
      });
    }
  }

}
