import {
  CloeButtonComponent,
  CloeLoaderComponent,
  CloeMapComponent,
  CloeModalComponent,
  CloeTableComponent,
  CloeTableFilterComponent,
  CloeTableStaticComponent,
  CloeTabsComponent,
  CloeTreeComponent,
  CloeTreeFilterComponent,
  CloeTreeStatesComponent,
  CloeUploadComponent,
  CloeUploadWrapperComponent,
  DatepickerComponent,
  DomainPickerComponent,
  HeaderComponent,
  SidebarComponent,
  SofumenuComponent,
  SortableColumnComponent,
  CloeAccordionComponent,
  CloeBoxComponent,
  CloeBreadcrumbComponent,
  CloeWizardComponent,
  HostDirective,
  CloeDynamicFormComponent,
  CloeQuestionComponent,
  CloeDateTimePickerComponent,
  CloeTreeInputNodeIDsComponent,
  CloeFormColorInputComponent,
  CloeFormInputComponent,
  CloeFormSelectComponent,
  CloeFormSelectObjectComponent,
  CloeHelpOnlineComponent,
} from './components';

import {
  ArraySortPipe,
  CloeDatePipe,
  CloeDateTimePipe,
  DynamicPipe,
  EmptyPipe,
  EngFormatPipe,
  FilterPipe,
  KeysPipe,
  EllipsisPipe, EngFormatWithUnitPipe, CloePercentPipe, CloeDecimalSeparatorPipe, CloeDateTimeUTCPipe
} from './pipes';
import { MODAL_COMPONENTS } from './components/modal-components.config';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TranslateModule } from '@ngx-translate/core';
import { NgbButtonsModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TreeModule } from 'angular-tree-component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxPaginationModule } from 'ngx-pagination';
import { PerfectScrollbarModule } from 'ngx-perfect-scrollbar';
import { AgmCoreModule } from '@agm/core';
import { UIRouterModule } from '@uirouter/angular';
import { BrowserModule } from '@angular/platform-browser';
import { OwlDateTimeModule } from 'ng-pick-datetime';
import { OwlMomentDateTimeModule } from 'ng-pick-datetime-moment/moment-adapter/moment-date-time.module';
import { TagInputModule } from 'ngx-chips';
import { CloeChangePwComponent } from './components/cloe-change-pw/cloe-change-pw.component';
import { ChangePWValidatorDirective } from './components/cloe-change-pw/validator/validator-directive/ChangePWValidator.directive';
import { ColorPickerModule } from 'ngx-color-picker';
import { CloeFocusableDirective } from './directives';
import { CloeAngularMultiSelectModule } from './components/form/cloe-dropdown-multiselect/multiselect.component';

const COMPONENTS = [
  ...MODAL_COMPONENTS,
  CloeButtonComponent,
  DatepickerComponent,
  DomainPickerComponent,
  CloeLoaderComponent,
  CloeModalComponent,
  CloeTableComponent,
  CloeTableStaticComponent,
  CloeTableFilterComponent,
  SortableColumnComponent,
  CloeTreeComponent,
  CloeTreeFilterComponent,
  CloeTreeStatesComponent,
  CloeUploadComponent,
  CloeUploadWrapperComponent,
  HeaderComponent,
  CloeModalComponent,
  SidebarComponent,
  SofumenuComponent,
  CloeMapComponent,
  CloeTabsComponent,
  CloeAccordionComponent,
  CloeBoxComponent,
  CloeBreadcrumbComponent,
  CloeWizardComponent,
  HostDirective,
  CloeDynamicFormComponent,
  CloeQuestionComponent,
  CloeDateTimePickerComponent,
  CloeTreeInputNodeIDsComponent,
  CloeChangePwComponent,
  CloeFormInputComponent,
  CloeFormColorInputComponent,
  CloeFormSelectComponent,
  CloeFormSelectObjectComponent,
  CloeHelpOnlineComponent,
];

const PIPES = [
  ArraySortPipe,
  CloeDatePipe,
  CloeDateTimePipe,
  CloeDateTimeUTCPipe,
  DynamicPipe,
  EmptyPipe,
  FilterPipe,
  KeysPipe,
  EngFormatPipe,
  EllipsisPipe,
  EngFormatWithUnitPipe,
  CloePercentPipe,
  CloeDecimalSeparatorPipe,
];

const BASE_MODULES = [
  BrowserModule,
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  TranslateModule,
  NgbButtonsModule,
  NgbModule,
  TreeModule,
  BrowserAnimationsModule,
  NgxPaginationModule,
  PerfectScrollbarModule,
  AgmCoreModule,
  UIRouterModule,
  OwlDateTimeModule,
  OwlMomentDateTimeModule,
  TagInputModule,
  ColorPickerModule,
  CloeAngularMultiSelectModule,
];

const DIRECTIVES = [
  ChangePWValidatorDirective,
  CloeFocusableDirective
];

@NgModule({
  imports: [...BASE_MODULES],
  exports: [...BASE_MODULES, ...COMPONENTS, ...PIPES, ...DIRECTIVES],
  declarations: [...COMPONENTS, ...PIPES, ...DIRECTIVES],
  entryComponents: [...MODAL_COMPONENTS],
  providers: [...PIPES]
})
export class CloeUiModule {
}
