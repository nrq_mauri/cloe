import {FilterTypeEnum} from '@cloe-core/@core/enums/FilterTypeEnum';
import {ACTIONS} from '@cloe-core/@core/enums/ActionsEnum';
import {MetadataManagementComponent, SiteUpsertComponent} from '@bo-core/components/modals';
import {ITableConfig} from '@cloe-core/@core/@ui/components/cloe-table/cloe-table.interface';


export const TABLE_CONFIG: ITableConfig = {
  header: [
    {
      fieldName: 'name',
      columnName: 'global.name',
    },
    {
      fieldName: 'status',
      columnName: 'global.status',
    },
    {
      fieldName: 'tags',
      columnName: 'global.tags',
    },
    {
      fieldName: 'address',
      columnName: 'global.address',
    },
    {
      fieldName: 'city.name',
      columnName: 'component.table.city',
    },
    {
      fieldName: 'latitude',
      columnName: 'component.table.latitude',
    },
    {
      fieldName: 'longitude',
      columnName: 'component.table.longitude',
    },
    {
      fieldName: 'defaultChannelId',
      columnName: 'page.site.defaultChannelId',
    }
  ],
  actions: [
    {
      icon: 'edit',
      actionType: ACTIONS.OPEN_MODAL,
      metadata: {
        modalComponent: SiteUpsertComponent,
        altMessage: 'global.edit'
      }
    },
    {
      icon: 'delete',
      actionType: ACTIONS.DELETE,
      metadata: {
        deleteByField: 'uuid',
        confirmMessage: 'component.confirm.messages.generic',
        altMessage: 'global.delete'
      }
    },
    {
      icon: 'info',
      actionType: ACTIONS.OPEN_MODAL,
      metadata: {
        modalComponent: MetadataManagementComponent,
        cityId: 'cityId',
        altMessage: 'global.metadata'
      }
    }
  ],
  metadata: {
    tableTitle: 'page.site.tableTitle',
    addNewButton: {
      modalComponent: SiteUpsertComponent,
      buttonLabel: 'global.addNew'
    },
    sort: 'name',
    sortType: 'asc'
  },
  filters: [
    {
      filterType: FilterTypeEnum.TEXT,
      filterField: 'name',
      fieldLabel: 'global.name'
    },
    {
      filterType: FilterTypeEnum.TEXT,
      filterField: 'tags',
      fieldLabel: 'global.tags'
    },
    {
      filterType: FilterTypeEnum.TEXT,
      filterField: 'cityId',
      fieldLabel: 'component.table.city'
    }
    ]
};
