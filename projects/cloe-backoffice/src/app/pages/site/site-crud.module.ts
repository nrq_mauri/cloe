import { SiteCrudComponent } from './site-crud.component';
import { NgModule } from '@angular/core';
import { UIRouterModule } from '@uirouter/angular';
import {BoCoreModule} from '@bo-core/bo-core.module';

const siteCrud = {
  name: 'site-crud',
  parent: 'logged',
  url: '/site/list',
  component: SiteCrudComponent
};

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [siteCrud]}),
    BoCoreModule
  ],
  declarations: [SiteCrudComponent],
  exports: [UIRouterModule]
})
export class SiteCrudModule {}
