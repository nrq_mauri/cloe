import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TABLE_CONFIG } from './site-table.config';

@Component({
  selector: 'cloe-site-crud',
  templateUrl: './site-crud.component.html',
  styleUrls: ['./site-crud.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SiteCrudComponent implements OnInit {
  constructor() {}

  tableConfig = TABLE_CONFIG;
  filters = {
    status: 'ACTIVE'
  };

  ngOnInit() {
  }
}
