import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { CloeItemsByNatureService } from '@cloe-core/@core/services/cloe-itemsByNatureExceptUUID-service/cloe-itemsByNature.service';
import { NatureTypesEnum } from '@cloe-core/@core/enums/NatureTypesEnum';
import { CloeTreeNodeService } from '@cloe-core/@core/services/cloe-treeNode-service/cloe-tree-node.service';
import { ModalService } from '@cloe-core/@core/services/modal-service/modal-service';
import { ITreeOptions, TREE_ACTIONS, TreeModel } from 'angular-tree-component';
import {
  CloeTreeActionsEnum,
  cloeTreeActionsSubject$,
  CloeTreeComponent
} from '@cloe-core/@core/@ui/components/cloe-tree/cloe-tree.component';
import { CloeModalComponentInterface } from '@cloe-core/@core/interfaces/cloe-modal-component.interface';
import { NodeCategoryEnum } from '@cloe-core/@core/enums/NodeCategoryEnum';
import { EMPTY_SITE_SELECTED_ACTIONS, SiteTreeActionsEnum, siteTreeSubject$, } from '../actions-constants/sites-tree-actions-constants';
import { SiteAndChannelsTreeActionsEnum } from '../sites-channels-tree-actions/sites-and-channels-tree-actions.component';
import { ITreeNode } from 'angular-tree-component/dist/defs/api';
import { SiteTreeManagementService } from '@bo-core/service/channel-site-tree-management-service/site-tree-management.service';
import { ChannelSiteTreeManagementService } from '@bo-core/service/channel-site-tree-management-service/channel-site-tree-management.service';
import { filter, set } from 'lodash';
import { ChannelTreeManagementService } from '@bo-core/service/channel-site-tree-management-service/channel-tree-management.service';
import { UserTreeRootService } from '@cloe-core/@core/services/user-tree-root-service/user-tree-root-service';
import { StorageService } from '@cloe-core/@core/services/storage-service/storage.service';
import { StatusEnum } from '@cloe-core/@core/enums/StatusEnum';

@Component({
  selector: 'cloe-sites-tree-management',
  templateUrl: './sites-tree-management.component.html',
  styleUrls: ['./sites-tree-management.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SitesTreeManagementComponent implements OnInit, OnDestroy {
  @ViewChild(CloeTreeComponent) siteTree: CloeTreeComponent;
  siteTrees;
  selectedSiteTree;
  siteTreeOptions: ITreeOptions = {
    allowDrag: (node: any) => node.data.nodeCategory !== 'ROOT',
    allowDrop: (element, { parent, index }) =>
      parent.data.nodeCategory !== NodeCategoryEnum.ROOT && element.path[1] === parent.path[1],
    actionMapping: {
      mouse: {
        click: (tree, node, $event) => {
          $event.ctrlKey ? TREE_ACTIONS.TOGGLE_ACTIVE_MULTI(tree, node, $event)
            : TREE_ACTIONS.TOGGLE_ACTIVE(tree, node, $event);
        }
      }
    }
  };
  currentSiteTreeNodes = [];
  siteTreeNodeConfig = {
    removableNodes: ['GROUP', 'LEAF'],
    confirmNodeRemoval: true,
    areFoldersAddable: true,
    newFolderNotInRoot: true
  };
  updateTreeNodes: {
    treeNodes: any[],
    nodeIdsToDelete: number[],
    referUuid: string
  }[] = [];
  deleteTreeNodes: {
    treeNodes: any[],
    referUuid: string
  }[] = [];
  sitesTreeActions = EMPTY_SITE_SELECTED_ACTIONS;
  siteTreeSubscription;

  channelTree;
  channelTreeNodesToDelete = [];

  constructor(
    private searchItems: CloeItemsByNatureService,
    private siteTreeManagementService: SiteTreeManagementService,
    private channelTreeManagementService: ChannelTreeManagementService,
    private channelSiteTreeManagementService: ChannelSiteTreeManagementService,
    private modalService: ModalService,
    private treeNodeService: CloeTreeNodeService
  ) {
    this.siteTreeSubscription = siteTreeSubject$.asObservable().subscribe((event: any) => {
      switch (event.siteTreeAction) {
        case SiteTreeActionsEnum.COMBINE_CHANNELS:
          set(
            event,
            'data.selectedSites',
            filter(
              this.siteTree.tree.treeModel.activeNodes,
              (node) => node.data.nodeCategory === NodeCategoryEnum.LEAF
            )
          );
          break;
      }
      switch (event.action) {
        case SiteAndChannelsTreeActionsEnum.OPEN_MODAL:
          this.modalService.open(event.modalComponent, {keyboard: false}).then((result: any) => {
            if (result) {
              if (event.siteTreeAction === SiteTreeActionsEnum.ADD_SITE) {
                cloeTreeActionsSubject$.next({
                  action: CloeTreeActionsEnum.ADD_LEAF_FROM_OBJECT,
                  data: result.data
                });
                this.channelSiteTreeManagementService.checkAndAddUpdateTreeNodes(
                  this.updateTreeNodes,
                  this.siteTree.tree.treeModel,
                  this.siteTree.tree.treeModel.activeNodes[0]
                );
                this.saveTree(result.data);
              } else {
                if (result.actionPostClose === 'RELOAD') {
                  this.loadSiteTree();
                }
              }
            }
          }).catch(() => {
            // dummy catch, we don't use it for now
          });
          (<CloeModalComponentInterface>this.modalService.getModalInstance().componentInstance).initData(event.data);
          break;
      }
    });
  }

  ngOnInit() {
    this.searchItems.searchByNatureWithFilters(
      NatureTypesEnum.TREE,
      {
        nodeNature: NatureTypesEnum.SITE,
        isCluster: false
      }
    ).subscribe((res) => {
      this.siteTrees = res ? res : [];
      if (this.siteTrees.length > 0) {
        this.selectedSiteTree = this.siteTrees[0];
        this.loadSiteTree();
      }
    });

    this.searchItems.searchByNatureWithFilters(
      NatureTypesEnum.TREE,
      {
        nodeNature: NatureTypesEnum.CHANNEL,
        isCluster: false
      }
    ).subscribe((res) => {
      this.channelTree = res[0];
    });
  }

  loadSiteTree() {
    this.currentSiteTreeNodes = [];
    this.siteTreeManagementService.loadSiteTree(
      this.selectedSiteTree,
      this.currentSiteTreeNodes,
      this.siteTree.tree.treeModel
    );
  }

  onNodeRemoved(node: any) {
    this.channelSiteTreeManagementService.checkAndAddDeleteTreeNodes(
      this.deleteTreeNodes,
      this.updateTreeNodes,
      this.siteTree.tree.treeModel,
      node
    );
    this.channelTreeManagementService.checkAndAddChannelTreeNodesToDeleteFromSite(
      this.channelTreeNodesToDelete,
      this.siteTree.tree.treeModel,
      node
    );
  }

  onNodeActive($event) {
    this.sitesTreeActions = this.siteTreeManagementService.checkSitesTreeAction($event);
    siteTreeSubject$.next({ action: SiteAndChannelsTreeActionsEnum.NODE_ACTIVATED, data: $event });
  }

  onNodeDeactive($event) {
    this.sitesTreeActions = this.siteTreeManagementService.checkSitesTreeAction($event);
    siteTreeSubject$.next({ action: SiteAndChannelsTreeActionsEnum.NODE_DEACTIVE, data: $event });
  }

  onNodeMoved($event: { node, to: { parent: ITreeNode, index: number }, treeModel: TreeModel }) {
    const node = $event.treeModel.getNodeById($event.node.id);
    this.channelSiteTreeManagementService.checkAndAddUpdateTreeNodes(
      this.updateTreeNodes,
      this.siteTree.tree.treeModel,
      node
    );
  }

  saveTree(newChannelRoot?) {
    this.treeNodeService.saveTreeNodesList(
      this.updateTreeNodes,
      this.deleteTreeNodes,
      this.selectedSiteTree
    ).subscribe(() => {
      this.updateTreeNodes = [];
      this.deleteTreeNodes = [];
      this.loadSiteTree();
      this.treeNodeService.saveTreeNodesList(
        newChannelRoot ? [{
          treeNodes: [{
            name: newChannelRoot.name,
            rootReferUuid: newChannelRoot.uuid,
            nodeCategory: NodeCategoryEnum.GROUP,
            status: StatusEnum.ACTIVE,
            nodeNature: NatureTypesEnum.CHANNEL,
            children: []
          }],
          nodeIdsToDelete: [],
          referUuid: newChannelRoot.uuid
        }] : [],
        this.channelTreeNodesToDelete,
        this.channelTree
      ).subscribe( () => {
        this.channelTreeNodesToDelete = [];
      });
    });
  }

  ngOnDestroy(): void {
    this.siteTreeSubscription.unsubscribe();
  }
}
