import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'cloe-site-channel-trees',
  templateUrl: './channel-site-tree.component.html',
  styleUrls: ['./channel-site-tree.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ChannelSiteTreeComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }
}
