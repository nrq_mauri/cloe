import { Component, OnDestroy, OnInit, ViewEncapsulation } from '@angular/core';
import { NodeCategoryEnum } from '@cloe-core/@core/enums/NodeCategoryEnum';
import { filter, transform } from 'lodash';
import { SiteAndChannelsTreeActionsEnum } from '../sites-channels-tree-actions/sites-and-channels-tree-actions.component';
import { ChannelUpsertComponent, SiteUpsertComponent } from '@bo-core/components/modals';
import { SiteTreeActionsEnum, siteTreeSubject$ } from '../actions-constants/sites-tree-actions-constants';
import { ChannelTreeActionEnum, channelTreeSubject$ } from '../actions-constants/channels-tree-actions-constants';

@Component({
  selector: 'cloe-channel-site-tree-management-detail',
  templateUrl: './channel-site-tree-management-detail.component.html',
  styleUrls: ['./channel-site-tree-management-detail.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ChannelSiteTreeManagementDetailComponent implements OnInit, OnDestroy {

  activeSite;
  activeChannel;
  sitesTreeActionsSubscription;
  channelsTreeActionsSubscription;

  constructor() {
    this.sitesTreeActionsSubscription = siteTreeSubject$.asObservable().subscribe((event: any) => {
      const action = event.action;
      switch (action) {
        case SiteAndChannelsTreeActionsEnum.NODE_DEACTIVE:
        case SiteAndChannelsTreeActionsEnum.NODE_ACTIVATED:
          const node = event.data.node;
          const treeModel = event.data.treeModel;
          const filteredNodes = filter(treeModel.activeNodes, (activeNode) => {
            return activeNode.data.nodeCategory === NodeCategoryEnum.LEAF;
          });
          this.activeSite = filteredNodes.length === 1 ? filteredNodes[0].data : undefined;
          break;
      }
    });

    this.channelsTreeActionsSubscription = channelTreeSubject$.asObservable().subscribe((event: any) => {
      const action = event.action;
      switch (action) {
        case SiteAndChannelsTreeActionsEnum.NODE_ACTIVATED:
        case SiteAndChannelsTreeActionsEnum.NODE_DEACTIVE:
          const node = event.data.node;
          const treeModel = event.data.treeModel;
          const filteredNodes = filter(treeModel.activeNodes, (activeNode) => {
            return activeNode.data.nodeCategory === NodeCategoryEnum.LEAF;
          });
          this.activeChannel = filteredNodes.length === 1 ? filteredNodes[0].data : undefined;
          console.log(this.activeChannel);
          break;
      }
    });
  }

  ngOnInit() {
  }

  getRangesByType(rate: string) {
    const activeChannelRanges = this.activeChannel.referObject.channelRanges;
    if (activeChannelRanges && activeChannelRanges.length > 0) {
      return transform(
        activeChannelRanges,
        (result, range: any) => {
          if (range.rangeType === rate) {
            result.push(range);
            return result;
          }
        }, []
      );
    }
    return null;
  }

  editSite(activeSite) {
    siteTreeSubject$.next({
      action: SiteAndChannelsTreeActionsEnum.OPEN_MODAL,
      channelTreeAction: SiteTreeActionsEnum.EDIT_SITE,
      modalComponent: SiteUpsertComponent,
      data: activeSite.referObject
    });
  }

  editChannel(activeChannel) {
    channelTreeSubject$.next({
      action: SiteAndChannelsTreeActionsEnum.OPEN_MODAL,
      channelTreeAction: ChannelTreeActionEnum.EDIT_CHANNEL,
      modalComponent: ChannelUpsertComponent,
      data: activeChannel.referObject
    });
  }

  ngOnDestroy(): void {
    this.sitesTreeActionsSubscription.unsubscribe();
    this.channelsTreeActionsSubscription.unsubscribe();
  }
}
