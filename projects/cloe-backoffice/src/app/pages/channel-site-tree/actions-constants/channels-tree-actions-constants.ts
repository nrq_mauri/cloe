import { SiteAndChannelsTreeActionsEnum } from '../sites-channels-tree-actions/sites-and-channels-tree-actions.component';
import { ChannelRangesCombineComponent, ChannelUpsertComponent } from '@bo-core/components/modals';
import { Subject } from 'rxjs';

export const channelTreeSubject$ = new Subject();

export enum ChannelTreeActionEnum {
  ADD_CHANNEL = 'ADD_CHANNEL',
  EDIT_CHANNEL = 'EDIT_CHANNEL',
  COMBINE_RANGES = 'COMBINE_RANGES'
}

const BASE_CHANNEL_ACTIONS = [
];

export const EMPTY_SELECTION_ACTIONS = [ ...BASE_CHANNEL_ACTIONS ];

export const SINGLE_FOLDER_SELECTED_ACTIONS = [
  ...BASE_CHANNEL_ACTIONS,
  {
    action: SiteAndChannelsTreeActionsEnum.OPEN_MODAL,
    channelTreeAction: ChannelTreeActionEnum.ADD_CHANNEL,
    modalComponent: ChannelUpsertComponent,
    subject: channelTreeSubject$,
    label: 'page.channel-site-tree.channels-tree-actions.add-channel',
    icon: 'fa-plus'
  }
];

export const MULTI_CHANNEL_SELECTED_ACTIONS = [
  ...BASE_CHANNEL_ACTIONS,
  {
    action: SiteAndChannelsTreeActionsEnum.OPEN_MODAL,
    channelTreeAction: ChannelTreeActionEnum.COMBINE_RANGES,
    modalComponent: ChannelRangesCombineComponent,
    subject: channelTreeSubject$,
    label: 'page.channel-site-tree.channels-tree-actions.combine-ranges',
    icon: 'fa-share-alt'
  }
];
