import { SiteAndChannelsTreeActionsEnum } from '../sites-channels-tree-actions/sites-and-channels-tree-actions.component';
import { ChannelCombineComponent, SiteUpsertComponent } from '@bo-core/components/modals';
import { Subject } from 'rxjs';

export const siteTreeSubject$ = new Subject();

export enum SiteTreeActionsEnum {
  ADD_SITE = 'ADD_SITE',
  EDIT_SITE = 'EDIT_SITE',
  COMBINE_CHANNELS = 'COMBINE_CHANNELS'
}

const BASE_SITE_ACTIONS = [
];

export const EMPTY_SITE_SELECTED_ACTIONS = [
  ...BASE_SITE_ACTIONS,
];

export const SINGLE_FOLDER_SELECTED_ACTIONS = [
  ...BASE_SITE_ACTIONS,
  {
    action: SiteAndChannelsTreeActionsEnum.OPEN_MODAL,
    siteTreeAction: SiteTreeActionsEnum.ADD_SITE,
    modalComponent: SiteUpsertComponent,
    subject: siteTreeSubject$,
    label: 'page.channel-site-tree.sites-tree-actions.add-site',
    icon: 'fa-plus'
  }
];

export const SINGLE_SITE_SELECTED_ACTIONS = [
  ...BASE_SITE_ACTIONS,
  {
    action: SiteAndChannelsTreeActionsEnum.OPEN_MODAL,
    siteTreeAction: SiteTreeActionsEnum.COMBINE_CHANNELS,
    modalComponent: ChannelCombineComponent,
    subject: siteTreeSubject$,
    label: 'page.channel-site-tree.sites-tree-actions.combine-channels',
    icon: 'fa-eye'
  }
];

export const MULTI_SITE_SELECTED_ACTIONS = [
  ...BASE_SITE_ACTIONS,
  {
    action: SiteAndChannelsTreeActionsEnum.OPEN_MODAL,
    siteTreeAction: SiteTreeActionsEnum.COMBINE_CHANNELS,
    modalComponent: ChannelCombineComponent,
    subject: siteTreeSubject$,
    label: 'page.channel-site-tree.sites-tree-actions.combine-channels',
    icon: 'fa-share-alt'
  }
];
