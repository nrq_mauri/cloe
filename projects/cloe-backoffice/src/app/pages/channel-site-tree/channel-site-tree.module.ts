import { NgModule } from '@angular/core';
import { UIRouterModule } from '@uirouter/angular';
import { ChannelSiteTreeComponent } from './channel-site-tree.component';
import { BoCoreModule } from '@bo-core/bo-core.module';
import { SitesAndChannelsTreeActionsComponent } from './sites-channels-tree-actions/sites-and-channels-tree-actions.component';
import { SitesTreeManagementComponent } from './sites-tree-management/sites-tree-management.component';
import { ChannelsTreeManagementComponent } from './channels-tree-management/channels-tree-management.component';
import { ChannelSiteTreeManagementDetailComponent } from './channel-site-tree-management-detail/channel-site-tree-management-detail.component';


const channelSiteTreesState = {
  name: 'site-channel-trees',
  parent: 'logged',
  url: '/site-channel-trees',
  component: ChannelSiteTreeComponent
};

const COMPONENTS = [
  SitesAndChannelsTreeActionsComponent,
  SitesTreeManagementComponent,
  ChannelsTreeManagementComponent,
  ChannelSiteTreeManagementDetailComponent,
];

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [channelSiteTreesState]}),
    BoCoreModule
  ],
  declarations: [ChannelSiteTreeComponent, ...COMPONENTS],
  exports: [UIRouterModule]
})
export class ChannelSiteTreeModule {}
