import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { CloeTreeNodeService } from '@cloe-core/@core/services/cloe-treeNode-service/cloe-tree-node.service';
import { NatureTypesEnum } from '@cloe-core/@core/enums/NatureTypesEnum';
import { TREE_ACTIONS, TreeModel } from 'angular-tree-component';
import { CloeTreeActionsEnum, cloeTreeActionsSubject$, CloeTreeComponent } from '@cloe-core/@core/@ui/components';
import { NodeCategoryEnum } from '@cloe-core/@core/enums/NodeCategoryEnum';
import { remove, set, filter } from 'lodash';
import { ChannelTreeActionEnum, channelTreeSubject$, EMPTY_SELECTION_ACTIONS } from '../actions-constants/channels-tree-actions-constants';
import { CloeModalComponentInterface } from '@cloe-core/@core/interfaces/cloe-modal-component.interface';
import { ModalService } from '@cloe-core/@core/services/modal-service/modal-service';
import { ITreeNode } from 'angular-tree-component/dist/defs/api';
import {
  CloeItemsByNatureService
} from '@cloe-core/@core/services/cloe-itemsByNatureExceptUUID-service/cloe-itemsByNature.service';
import {
  ChannelTreeManagementService
} from '@bo-core/service/channel-site-tree-management-service/channel-tree-management.service';
import {
  ChannelSiteTreeManagementService
} from '@bo-core/service/channel-site-tree-management-service/channel-site-tree-management.service';
import { SiteAndChannelsTreeActionsEnum } from '../sites-channels-tree-actions/sites-and-channels-tree-actions.component';
import { siteTreeSubject$ } from '../actions-constants/sites-tree-actions-constants';


@Component({
  selector: 'cloe-channels-tree-management',
  templateUrl: './channels-tree-management.component.html',
  styleUrls: ['./channels-tree-management.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ChannelsTreeManagementComponent implements OnInit, OnDestroy {
  @ViewChild(CloeTreeComponent) channelTreeComponent: CloeTreeComponent;

  channelNodes = [{ name: 'ROOT', nodeCategory: 'ROOT', children: [] }];
  channelSiteMap: any = {};
  siteTreeSubscription;
  channelsTreeSubscription;
  channelsTreeActions = EMPTY_SELECTION_ACTIONS;
  channelsTreeOptions = {
    allowDrag: (node: any) => node.data.nodeCategory !== 'ROOT',
    allowDrop: (element, { parent, index }) =>
      parent.data.nodeCategory !== NodeCategoryEnum.ROOT && element.path[1] === parent.path[1],
    actionMapping: {
      mouse: {
        click: (tree, node, $event) => {
          $event.ctrlKey ? TREE_ACTIONS.TOGGLE_ACTIVE_MULTI(tree, node, $event)
            : TREE_ACTIONS.TOGGLE_ACTIVE(tree, node, $event);
        }
      }
    }
  };
  channelTree;
  updateTreeNodes: {
    treeNodes: any[],
    nodeIdsToDelete: number[],
    referUuid: string
  }[] = [];
  deleteTreeNodes: {
    treeNodes: any[],
    referUuid: string
  }[] = [];

  channelNodesConfig = {
    removableNodes: ['GROUP', 'LEAF'],
    confirmNodeRemoval: true,
    areFoldersAddable: true,
    newFolderNotInRoot: true,
    expandRootAtStart: true
  };

  constructor(
    private searchItems: CloeItemsByNatureService,
    private treeNodeService: CloeTreeNodeService,
    private channelTreeManagementService: ChannelTreeManagementService,
    private channelSiteTreeManagementService: ChannelSiteTreeManagementService,
    private modalService: ModalService
  ) {
    this.siteTreeSubscription = siteTreeSubject$.asObservable().subscribe((event: any) => {
      const action = event.action;
      switch (action) {
        case SiteAndChannelsTreeActionsEnum.NODE_ACTIVATED:
          const activatedNode = event.data.node;
          if (activatedNode.data.nodeCategory === NodeCategoryEnum.LEAF && !this.channelSiteMap[activatedNode.data.referUuid]) {
            this.channelTreeManagementService.loadChannelTree(
              this.channelSiteMap,
              this.channelNodes,
              activatedNode.data.referUuid,
              this.channelTreeComponent.tree.treeModel
            );
          }
          break;
        case SiteAndChannelsTreeActionsEnum.NODE_DEACTIVE:
          const deactivatedNode = event.data.node;
          if (this.channelSiteMap[deactivatedNode.data.referUuid]) {
            remove(
              this.channelNodes[0].children,
              (channel) => channel.id === this.channelSiteMap[deactivatedNode.data.referUuid]
            );
            this.channelSiteMap[deactivatedNode.data.referUuid] = undefined;
            this.channelTreeComponent.tree.treeModel.update();
          }
          break;
      }
    });

    this.channelsTreeSubscription = channelTreeSubject$.asObservable().subscribe((event: any) => {
      switch (event.channelTreeAction) {
        case ChannelTreeActionEnum.COMBINE_RANGES:
          set(
            event,
            'data.selectedChannels',
            filter(
              this.channelTreeComponent.tree.treeModel.activeNodes,
              (node) => node.data.nodeCategory === NodeCategoryEnum.LEAF
            )
          );
          break;
      }
      switch (event.action) {
        case SiteAndChannelsTreeActionsEnum.OPEN_MODAL:
          this.modalService.open(event.modalComponent, {keyboard: false}).then((result: any) => {
            if (result) {
              if (event.channelTreeAction === ChannelTreeActionEnum.ADD_CHANNEL) {
                cloeTreeActionsSubject$.next({
                  action: CloeTreeActionsEnum.ADD_LEAF_FROM_OBJECT,
                  data: result.data
                });
                this.channelSiteTreeManagementService.checkAndAddUpdateTreeNodes(
                  this.updateTreeNodes,
                  this.channelTreeComponent.tree.treeModel,
                  this.channelTreeComponent.tree.treeModel.activeNodes[0]
                );
              } else {
                if (result.actionPostClose === 'RELOAD') {
                  this.channelTreeManagementService.reloadChannelTree(this.channelSiteMap, this.channelTreeComponent.tree.treeModel);
                }
              }
            }
          }).catch(() => {
            // dummy catch, we don't use it for now
          });
          (<CloeModalComponentInterface>this.modalService.getModalInstance().componentInstance).initData(event.data);
          break;
      }
    });
  }

  ngOnInit() {
    this.searchItems.searchByNatureWithFilters(
      NatureTypesEnum.TREE,
      {
        nodeNature: NatureTypesEnum.CHANNEL,
        isCluster: false
      }
    ).subscribe((res) => {
      this.channelTree = res[0];
    });
  }

  onNodeActive($event: any) {
    this.channelsTreeActions = this.channelTreeManagementService.checkChannelsTreeAction($event);
    channelTreeSubject$.next({ action: SiteAndChannelsTreeActionsEnum.NODE_ACTIVATED, data: $event });
  }

  onNodeDeactive($event: any) {
    this.channelsTreeActions = this.channelTreeManagementService.checkChannelsTreeAction($event);
    channelTreeSubject$.next({ action: SiteAndChannelsTreeActionsEnum.NODE_DEACTIVE, data: $event });
  }

  onNodeMoved($event: { node, to: { parent: ITreeNode, index: number }, treeModel: TreeModel }) {
    const node = $event.treeModel.getNodeById($event.node.id);
    this.channelSiteTreeManagementService.checkAndAddUpdateTreeNodes(
      this.updateTreeNodes,
      this.channelTreeComponent.tree.treeModel,
      node
    );
  }

  onNodeRemoved(node: any) {
    this.channelSiteTreeManagementService.checkAndAddDeleteTreeNodes(
      this.deleteTreeNodes,
      this.updateTreeNodes,
      this.channelTreeComponent.tree.treeModel,
      node
    );
  }

  saveTree() {
    this.treeNodeService.saveTreeNodesList(this.updateTreeNodes, this.deleteTreeNodes, this.channelTree).subscribe(() => {
      this.updateTreeNodes = [];
      this.deleteTreeNodes = [];
      this.channelTreeManagementService.reloadChannelTree(this.channelSiteMap, this.channelTreeComponent.tree.treeModel);
    });
  }

  ngOnDestroy(): void {
    this.siteTreeSubscription.unsubscribe();
    this.channelsTreeSubscription.unsubscribe();
  }
}
