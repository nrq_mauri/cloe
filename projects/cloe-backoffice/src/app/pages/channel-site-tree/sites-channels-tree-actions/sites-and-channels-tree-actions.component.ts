import { Component, Input, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'cloe-sites-channels-tree-actions',
  templateUrl: './sites-and-channels-tree-actions.component.html',
  styleUrls: ['./sites-and-channels-tree-actions.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SitesAndChannelsTreeActionsComponent implements OnInit {

  @Input() treeActions: Array<any> = [];

  constructor() {}

  ngOnInit() {
  }

  handleAction(e: any) {
    e.subject.next(e);
  }
}

export enum SiteAndChannelsTreeActionsEnum {
  OPEN_MODAL = 'OPEN_MODAL',
  NODE_ACTIVATED = 'NODE_ACTIVATED',
  NODE_DEACTIVE = 'NODE_DEACTIVE'
}
