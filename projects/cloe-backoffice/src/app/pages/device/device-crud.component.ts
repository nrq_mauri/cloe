import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TABLE_CONFIG } from './device-table.config';

@Component({
  selector: 'cloe-device-crud',
  templateUrl: './device-crud.component.html',
  styleUrls: ['./device-crud.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DeviceCrudComponent implements OnInit {
  constructor() {}

  tableConfig = TABLE_CONFIG;
  filters = {
    status: 'ACTIVE'
  };

  ngOnInit() {
  }
}
