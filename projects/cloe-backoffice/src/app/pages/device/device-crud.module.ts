import { DeviceCrudComponent } from './device-crud.component';
import { NgModule } from '@angular/core';
import { UIRouterModule } from '@uirouter/angular';
import {BoCoreModule} from '@bo-core/bo-core.module';

const deviceCrud = {
  name: 'device-crud',
  parent: 'logged',
  url: '/device/list',
  component: DeviceCrudComponent
};

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [deviceCrud]}),
    BoCoreModule
  ],
  declarations: [DeviceCrudComponent],
  exports: [UIRouterModule]
})
export class DeviceCrudModule {}
