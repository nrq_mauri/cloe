import {FilterTypeEnum} from '@cloe-core/@core/enums/FilterTypeEnum';
import {ACTIONS} from '@cloe-core/@core/enums/ActionsEnum';
import {DeviceUpsertComponent, MetadataManagementComponent} from '@bo-core/components/modals';
import {ITableConfig} from '@cloe-core/@core/@ui/components/cloe-table/cloe-table.interface';

export const TABLE_CONFIG: ITableConfig = {
  header: [
    {
      fieldName: 'readIdDevice',
      columnName: 'page.device.readIdDevice',
    },
    {
      fieldName: 'name',
      columnName: 'global.name',
    },
    {
      fieldName: 'latitude',
      columnName: 'component.table.latitude',
    },
    {
      fieldName: 'longitude',
      columnName: 'component.table.longitude',
    },
    {
      fieldName: 'status',
      columnName: 'global.status',
    },
    {
      fieldName: 'tags',
      columnName: 'global.tags',
    },
    {
      fieldName: 'installationDate',
      columnName: 'component.table.installationDate',
    },
    {
      fieldName: 'lastServiceDate',
      columnName: 'component.table.lastServiceDate',
    },
    {
      fieldName: 'serviceInterval',
      columnName: 'component.table.serviceInterval',
    },
    {
      fieldName: 'deviceTypeName',
      columnName: 'component.table.deviceType',
    }
  ],
  actions: [
    {
      icon: 'edit',
      actionType: ACTIONS.OPEN_MODAL,
      metadata: {
        modalComponent: DeviceUpsertComponent,
        altMessage: 'global.edit'
      }
    },
    {
      icon: 'delete',
      actionType: ACTIONS.DELETE,
      metadata: {
        deleteByField: 'uuid',
        confirmMessage: 'component.confirm.messages.generic',
        altMessage: 'global.delete'
      }
    },
    {
      icon: 'info',
      actionType: ACTIONS.OPEN_MODAL,
      metadata: {
        modalComponent: MetadataManagementComponent,
        altMessage: 'global.metadata'
      }
    }
  ],
  metadata: {
    tableTitle: 'page.device.tableTitle',
    addNewButton: {
      modalComponent: DeviceUpsertComponent,
      buttonLabel: 'global.addNew'
    },
    sort: 'name',
    sortType: 'asc'
  },
  filters: [
    {
      filterType: FilterTypeEnum.TEXT,
      filterField: 'name',
      fieldLabel: 'global.name'
    },
    {
      filterType: FilterTypeEnum.TEXT,
      filterField: 'tags',
      fieldLabel: 'global.tags'
    },
    {
      filterType: FilterTypeEnum.TEXT,
      filterField: 'deviceType',
      fieldLabel: 'component.table.deviceType'
    }
  ]
};
