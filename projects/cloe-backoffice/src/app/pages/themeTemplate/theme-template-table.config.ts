import {FilterTypeEnum} from '@cloe-core/@core/enums/FilterTypeEnum';
import {ACTIONS} from '@cloe-core/@core/enums/ActionsEnum';
import {ThemeTemplateUpsertComponent} from '@bo-core/components/modals';
import {ITableConfig} from '@cloe-core/@core/@ui/components/cloe-table/cloe-table.interface';


export const TABLE_CONFIG: ITableConfig = {
  header: [
    {
      fieldName: 'name',
      columnName: 'global.name',
    },
    {
      fieldName: 'status',
      columnName: 'global.status'
    },
    {
      fieldName: 'cssFileSuffix',
      columnName: 'page.themeTemplate.cssFileSuffix'
    }
  ],
  actions: [
    {
      icon: 'edit',
      actionType: ACTIONS.OPEN_MODAL,
      metadata: {
        modalComponent: ThemeTemplateUpsertComponent,
        altMessage: 'global.edit'
      }
    },
    {
      icon: 'delete',
      actionType: ACTIONS.DELETE,
      metadata: {
        deleteByField: 'uuid',
        confirmMessage: 'component.confirm.messages.generic',
        altMessage: 'global.delete'
      }
    }
  ],
  metadata: {
    tableTitle: 'page.themeTemplate.tableTitle',
    sort: 'id',
    sortType: 'asc',
    addNewButton: {
      modalComponent: ThemeTemplateUpsertComponent,
      buttonLabel: 'global.addNew'
    }
  },
  filters: [
    {
      filterType: FilterTypeEnum.TEXT,
      filterField: 'name',
      fieldLabel: 'global.name'
    }
  ]
};
