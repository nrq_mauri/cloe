import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TABLE_CONFIG } from './theme-template-table.config';

@Component({
  selector: 'cloe-theme-template-crud',
  templateUrl: './theme-template.component.html',
  styleUrls: ['./theme-template.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ThemeTemplateComponent implements OnInit {
  constructor() {}

  tableConfig = TABLE_CONFIG;
  filters = {
    status: ['ACTIVE', 'DISABLE']
  };

  ngOnInit() {
  }
}
