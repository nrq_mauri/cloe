import { NgModule } from '@angular/core';
import { UIRouterModule } from '@uirouter/angular';
import {ThemeTemplateComponent} from './theme-template.component';
import {BoCoreModule} from '@bo-core/bo-core.module';

const themeTemplateCrud = {
  name: 'theme-template-crud',
  parent: 'logged',
  url: '/themeTemplate/list',
  component: ThemeTemplateComponent
};

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [themeTemplateCrud]}),
    BoCoreModule
  ],
  declarations: [ThemeTemplateComponent],
  exports: [UIRouterModule]
})
export class ThemeTemplateModule {}
