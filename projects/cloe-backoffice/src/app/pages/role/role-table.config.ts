import {ICloeTableStatic} from '@cloe-core/@core/@ui/components/cloe-table-static/cloe-table-static-interface/cloe-table-static-interface';

export const TABLE_CONFIG: ICloeTableStatic = {
  header: [
    {
      fieldName: 'name',
      columnName: 'global.name',
      i18n: {
        prefix: 'role.name.',
        suffix: '',
      }
    },
    {
      fieldName: 'description',
      columnName: 'page.role.description',
      i18n: {
        prefix: 'role.description.',
        suffix: '',
      }
    }
  ],
  metadata: {
    tableTitle: 'page.role.tableTitle',
    disableSort: true,
    disablePagination: true,
    fixPageSize: 3
  }
};
