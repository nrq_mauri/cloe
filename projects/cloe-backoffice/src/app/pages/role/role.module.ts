import {NgModule} from '@angular/core';
import {UIRouterModule} from '@uirouter/angular';
import {RoleComponent} from './role.component';
import {BoCoreModule} from '@bo-core/bo-core.module';

const state = {
  name: 'role-crud',
  parent: 'logged',
  url: '/role/list',
  component: RoleComponent
};

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [state]}),
    BoCoreModule
  ],
  declarations: [RoleComponent]
})
export class RoleModule {}
