import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {TABLE_CONFIG} from './role-table.config';
import {CommonService} from '@cloe-core/@core/services/common-service/common-service';


@Component({
  selector: 'cloe-page-role',
  templateUrl: './role.component.html',
  styleUrls: ['./role.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RoleComponent implements OnInit {

  roles = [];
  tableConfig = TABLE_CONFIG;

  constructor(
    private commonService: CommonService,
  ) {
  }

  ngOnInit() {
    this.getAuthorities();
  }

  getAuthorities() {
    this.commonService.getAuthorities()
      .subscribe(
        (resp: String[]) => {
          resp.forEach(value => {
            this.roles.push({
              name: value,
              description: value
            });
          });
          this.roles = this.roles.slice();
        });
  }


}
