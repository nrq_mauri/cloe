import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { StateService, TransitionService } from '@uirouter/angular';
import { get, set, forEach } from 'lodash';

@Component({
  selector: 'cloe-crud-bo-page',
  templateUrl: './crud-bo.component.html',
  styleUrls: ['./crud-bo.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CrudBoComponent implements OnInit {

  constructor(
    private stateService: StateService
  ) {}

  ngOnInit(): void {
  }

  setParamsFilters() {
    const filters = {};
    const metadata = this.stateService.params.metadata;
    const data = this.stateService.params.data;
    if (metadata && metadata.filters && data ) {
      const filterFields = metadata.filters;
      forEach(filterFields, (filterField: any) => {
        const value = get(data, filterField.filterValueField);
        set(filters, filterField.filterField, value);
      });
    }
    return filters;
  }

  isBackButtonVisible() {
    const metadata = this.stateService.params.metadata;
    const data = this.stateService.params.data;
    return metadata && metadata.filters && data;
  }

  goBack() {
    window.history.back();
  }
}
