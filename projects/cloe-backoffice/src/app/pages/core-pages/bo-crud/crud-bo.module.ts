import { NgModule } from '@angular/core';
import { StatesModule, UIRouterModule } from '@uirouter/angular';
import { CrudBoComponent } from './crud-bo.component';
import { BoCoreModule } from '@bo-core/bo-core.module';

const stateModule: StatesModule = {
  states: [
    {
      name: 'crud-bo',
      parent: 'logged',
      params: {
        data: null,
        metadata: null
      },
      data: {
        requiresAuth: true
      },
      abstract: true,
      component: CrudBoComponent
    }
  ]
};

@NgModule({
  imports: [
    UIRouterModule.forChild(stateModule),
    BoCoreModule,
  ],
  declarations: [CrudBoComponent],
  exports: [UIRouterModule]
})
export class CrudBoModule {
}
