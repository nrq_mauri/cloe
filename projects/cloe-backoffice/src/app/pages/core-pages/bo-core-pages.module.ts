import { NgModule } from '@angular/core';
import { CrudBoModule } from './bo-crud/crud-bo.module';

@NgModule({
  imports: [
    CrudBoModule
  ]
})
export class BoCorePagesModule {
}
