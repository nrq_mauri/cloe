import {Component, Input, OnInit, ViewEncapsulation} from '@angular/core';
import {StateService} from '@uirouter/core';
import {TranslateService} from '@ngx-translate/core';
import {LoginService} from '@cloe-core/@core/services/login-service/login.service';

@Component({
  selector: 'cloe-page-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HomeComponent implements OnInit {
  @Input() user;

  constructor(
    private loginService: LoginService,
    private stateService: StateService,
    public translate: TranslateService
  ) {
  }

  ngOnInit() {
  }
}
