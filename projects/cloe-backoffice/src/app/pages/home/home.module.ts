import { NgModule } from '@angular/core';
import { HomeComponent } from './home.component';
import { UIRouterModule } from '@uirouter/angular';
import {BoCoreModule} from '@bo-core/bo-core.module';

const state = {
  name: 'home',
  parent: 'logged',
  url: '',
  component: HomeComponent
};

const rootState = {
  name: 'root',
  url: '/',
  redirectTo: 'home'
};

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [state, rootState]}),
    BoCoreModule
  ],
  declarations: [HomeComponent]
})
export class HomeModule {}
