import {FilterTypeEnum} from '@cloe-core/@core/enums/FilterTypeEnum';
import {ACTIONS} from '@cloe-core/@core/enums/ActionsEnum';
import {MsContextEnum} from '@cloe-core/@core/enums/MsContextEnum';
import {TreeNodeManagementComponent, UserUpsertComponent} from '@bo-core/components/modals';
import { Subject } from 'rxjs/internal/Subject';
import {ITableConfig} from '@cloe-core/@core/@ui/components/cloe-table/cloe-table.interface';

export const tableButtonsSubject$ = new Subject();
export const TABLE_CONFIG: ITableConfig = {
  header: [
    {
      fieldName: 'login',
      columnName: 'page.user.username',
    },
    {
      fieldName: 'firstName',
      columnName: 'global.name',
    },
    {
      fieldName: 'lastName',
      columnName: 'global.lastName'
    },
    {
      fieldName: 'email',
      columnName: 'global.email'
    },
    {
      fieldName: 'cloeUser.status',
      columnName: 'global.status',
      i18n: {
        prefix: 'status.',
        suffix: '',
      }
    },
    {
      fieldName: 'createdDate',
      columnName: 'page.user.created-date'
    },
    {
      fieldName: 'cloeUser.validEndDate',
      columnName: 'page.user.valid-end-date'
    },
    {
      fieldName: 'cloeUser.lastAccessDate',
      columnName: 'page.user.last-access-date'
    }
  ],
  actions: [
    {
      icon: 'edit',
      actionType: ACTIONS.OPEN_MODAL,
      metadata: {
        modalComponent: UserUpsertComponent,
        altMessage: 'global.edit'
      }
    },
    {
      icon: 'delete',
      actionType: ACTIONS.DELETE_USER,
      metadata: {
        deleteByField: 'cloeUser',
        confirmMessage: 'component.confirm.messages.generic',
        altMessage: 'global.delete'
      }
    },
    {
      icon: 'menu',
      actionType: ACTIONS.OPEN_MODAL,
      metadata: {
        modalComponent: TreeNodeManagementComponent,
        nodeNatureType: 'MENU',
        altMessage: 'page.user.edit-menu-tree'
      },
    },
    {
      icon: 'location_on',
      actionType: ACTIONS.OPEN_MODAL,
      metadata: {
        modalComponent: TreeNodeManagementComponent,
        nodeNatureType: 'SITE',
        altMessage: 'page.user.edit-site-tree'
      },
    },
    {
      icon: 'accessibility',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: tableButtonsSubject$,
        altMessage: 'page.user.impersonate'
      },
    }
  ],
  metadata: {
    tableTitle: 'page.user.tableTitle',
    msContext: MsContextEnum.GATEWAY,
    addNewButton: {
      modalComponent: UserUpsertComponent,
      buttonLabel: 'global.addNew'
    },
    sort: 'firstName',
    sortType: 'asc',
    search: {
      showedFieldName: 'user.firstName',
      fieldToSearch: {
        user: {
          firstName: '',
          email: ''
        },
        tags: ''
      },
      nodeNature: 'CLOEUSER'
    }
  },
  filters: [
    {
      filterType: FilterTypeEnum.TEXT,
      filterField: 'firstName',
      fieldLabel: 'global.name'
    },
    {
      filterType: FilterTypeEnum.TEXT,
      filterField: 'email',
      fieldLabel: 'global.email'
    },
    {
      filterType: FilterTypeEnum.TEXT,
      filterField: 'cloeUser.customer.customerCode',
      fieldLabel: 'page.user.client-code'
    }
  ]
};
