import { NgModule } from '@angular/core';
import { UIRouterModule } from '@uirouter/angular';
import { UserCrudComponent } from './user-crud.component';
import {BoCoreModule} from '@bo-core/bo-core.module';

const userCrud = {
  name: 'user-crud',
  parent: 'crud-bo',
  url: '/user/list',
  component: UserCrudComponent
};

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [userCrud]}),
    BoCoreModule
  ],
  declarations: [UserCrudComponent],
  exports: [UIRouterModule]
})
export class UserCrudModule {}
