import { Component, Inject, OnInit, ViewEncapsulation } from '@angular/core';
import { TABLE_CONFIG, tableButtonsSubject$ } from './user-table.config';
import { extend } from 'lodash';
import { CrudBoComponent } from '../core-pages/bo-crud/crud-bo.component';
import { LoginService } from '@cloe-core/@core/services/login-service/login.service';
import { switchMap } from 'rxjs/operators';
import { StateService } from '@uirouter/angular';


@Component({
  selector: 'cloe-user-crud',
  templateUrl: './user-crud.component.html',
  styleUrls: ['./user-crud.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class UserCrudComponent extends CrudBoComponent implements OnInit {

  tableConfig = TABLE_CONFIG;
  filters: any = {
  };

  constructor(private loginService: LoginService, protected s: StateService, @Inject('feHostUrl') private feHostUrl: string) {
    super(s);
  }

  tableButtonsSubscription;

  ngOnInit() {
    this.filters = extend({}, this.setParamsFilters());
    this.tableButtonsSubscription = tableButtonsSubject$.asObservable().pipe(
      switchMap( (row: any) => {
        return this.loginService.generateTempToken(row.data.cloeUser.uuid);
      })
    ).subscribe( (resp: any) => {
      const tokenCode = resp.data.tempToken;
      if (resp.success) {
        window.open(this.feHostUrl + '/#/impersonate/' + tokenCode, '_blank');
      }
    });
  }
}
