import {FilterTypeEnum} from '@cloe-core/@core/enums/FilterTypeEnum';
import {ACTIONS} from '@cloe-core/@core/enums/ActionsEnum';
import {MsContextEnum} from '@cloe-core/@core/enums/MsContextEnum';
import {ChannelUpsertComponent, MetadataManagementComponent} from '@bo-core/components/modals';
import {ITableConfig} from '@cloe-core/@core/@ui/components/cloe-table/cloe-table.interface';

export const TABLE_CONFIG: ITableConfig = {
  header: [
    {
      fieldName: 'name',
      columnName: 'global.name'
    },
    {
      fieldName: 'readIdChannel',
      columnName: 'page.channel.readIdChannel'
    },
    {
      fieldName: 'sampling',
      columnName: 'page.channel.sampling'
    },
    {
      fieldName: 'status',
      columnName: 'global.status'
    },
    {
      fieldName: 'tags',
      columnName: 'global.tags'
    },
    {
      fieldName: 'deviceName',
      columnName: 'component.table.device'
    },
    {
      fieldName: 'unitSymbol',
      columnName: 'component.table.unit'
    },
    {
      fieldName: 'channelTypeName',
      columnName: 'component.table.channelType'
    },
    {
      fieldName: 'historyDataTable',
      columnName: 'page.channel.history-data-table'
    },
    {
      fieldName: 'timeZoneCode',
      columnName: 'component.table.timezone'
    },
    {
      fieldName: 'channelRangeSlotName',
      columnName: 'component.table.channelRangeSlotName'
    },
    {
      fieldName: 'channelValidityStartDate',
      columnName: 'component.table.channelValidityStartDate'
    },
    {
      fieldName: 'channelValidityEndDate',
      columnName: 'component.table.channelValidityEndDate'
    }
  ],
  actions: [
    {
      icon: 'edit',
      actionType: ACTIONS.OPEN_MODAL,
      metadata: {
        modalComponent: ChannelUpsertComponent,
        altMessage: 'global.edit'
      }
    },
    {
      icon: 'delete',
      actionType: ACTIONS.DELETE,
      metadata: {
        deleteByField: 'uuid',
        confirmMessage: 'component.confirm.messages.generic',
        altMessage: 'global.delete'
      }
    },
    {
      icon: 'info',
      actionType: ACTIONS.OPEN_MODAL,
      metadata: {
        modalComponent: MetadataManagementComponent,
        altMessage: 'global.metadata'
      }
    }
  ],
  metadata: {
    tableTitle: 'page.channel.tableTitle',
    msContext: MsContextEnum.GATEWAY,
    addNewButton: {
      modalComponent: ChannelUpsertComponent,
      buttonLabel: 'global.addNew'
    },
    sort: 'name',
    sortType: 'asc'
  },
  filters: [
    {
      filterType: FilterTypeEnum.TEXT,
      filterField: 'name',
      fieldLabel: 'global.name'
    },
    {
      filterType: FilterTypeEnum.TEXT,
      filterField: 'channelTypeName',
      fieldLabel: 'component.table.channelType'
    },
    {
      filterType: FilterTypeEnum.TEXT,
      filterField: 'deviceName',
      fieldLabel: 'component.table.device'
    }
  ]
};

