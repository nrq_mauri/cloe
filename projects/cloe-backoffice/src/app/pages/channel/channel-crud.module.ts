import { NgModule } from '@angular/core';
import { UIRouterModule } from '@uirouter/angular';
import { ChannelCrudComponent } from './channel-crud.component';
import {BoCoreModule} from '@bo-core/bo-core.module';


const channelCrud = {
  name: 'channel-crud',
  parent: 'logged',
  url: '/channel/list',
  component: ChannelCrudComponent
};

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [channelCrud]}),
    BoCoreModule
  ],
  declarations: [ChannelCrudComponent],
  exports: [UIRouterModule]
})
export class ChannelCrudModule {}
