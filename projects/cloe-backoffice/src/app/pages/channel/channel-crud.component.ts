import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TABLE_CONFIG } from './channel-table.config';

@Component({
  selector: 'cloe-channel-crud',
  templateUrl: './channel-crud.component.html',
  styleUrls: ['./channel-crud.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ChannelCrudComponent implements OnInit {
  constructor() {}

  tableConfig = TABLE_CONFIG;
  filters = {
    status: 'ACTIVE'
  };

  ngOnInit() {
  }
}
