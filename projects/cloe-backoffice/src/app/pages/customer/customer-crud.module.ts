import { NgModule } from '@angular/core';
import { UIRouterModule } from '@uirouter/angular';
import { CustomerCrudComponent } from './customer-crud.component';
import {BoCoreModule} from '@bo-core/bo-core.module';


const customerCrud = {
  name: 'customer-crud',
  parent: 'logged',
  url: '/customer/list',
  component: CustomerCrudComponent
};

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [customerCrud]}),
    BoCoreModule
  ],
  declarations: [CustomerCrudComponent],
  exports: [UIRouterModule]
})
export class CustomerCrudModule {}
