import {FilterTypeEnum} from '@cloe-core/@core/enums/FilterTypeEnum';
import {ACTIONS} from '@cloe-core/@core/enums/ActionsEnum';
import {MsContextEnum} from '@cloe-core/@core/enums/MsContextEnum';
import {
  CustomerUpsertComponent,
  MetadataManagementComponent
} from '@bo-core/components/modals';
import {ITableConfig} from '@cloe-core/@core/@ui/components/cloe-table/cloe-table.interface';

export const TABLE_CONFIG: ITableConfig = {
  header: [
    {
      fieldName: 'customerCode',
      columnName: 'global.customerCode',
    },
    {
      fieldName: 'name',
      columnName: 'global.name'
    },
    {
      fieldName: 'personalLogo',
      columnName: 'global.personalLogo'
    }
  ],
  actions: [
    {
      icon: 'edit',
      actionType: ACTIONS.OPEN_MODAL,
      metadata: {
        modalComponent: CustomerUpsertComponent,
        altMessage: 'global.edit'
      }
    },
    {
      icon: 'delete',
      actionType: ACTIONS.DELETE,
      metadata: {
        deleteByField: 'uuid',
        confirmMessage: 'component.confirm.messages.generic',
        altMessage: 'global.delete'
      }
    },
    {
      icon: 'info',
      actionType: ACTIONS.OPEN_MODAL,
      metadata: {
        modalComponent: MetadataManagementComponent,
        altMessage: 'global.metadata'
      }
    },
    {
      icon: 'account_circle',
      actionType: ACTIONS.REDIRECT_TO,
      metadata: {
        toState: 'user-crud',
        filters: [
          {
            filterField: 'cloeUser.customer.id',
            filterValueField: 'id'
          }
        ],
        altMessage: 'page.user.tableTitle'
      }
    }
  ],
  metadata: {
    tableTitle: 'page.customer.tableTitle',
    msContext: MsContextEnum.GATEWAY,
    addNewButton: {
      modalComponent: CustomerUpsertComponent,
      buttonLabel: 'global.addNew'
    },
    sort: 'customerCode',
    sortType: 'asc'
  },
  filters: [
    {
      filterType: FilterTypeEnum.TEXT,
      filterField: 'name',
      fieldLabel: 'global.name'
    },
    {
      filterType: FilterTypeEnum.TEXT,
      filterField: 'customerCode',
      fieldLabel: 'global.customerCode'
    },
    {
      filterType: FilterTypeEnum.ISNULL,
      filterField: 'personalLogo',
      fieldLabel: 'global.personalLogo'
    }
  ]
};

