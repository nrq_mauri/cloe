import {FilterTypeEnum} from '@cloe-core/@core/enums/FilterTypeEnum';
import {ACTIONS} from '@cloe-core/@core/enums/ActionsEnum';
import {MetadataUpsertComponent} from '@bo-core/components/modals';
import {ITableConfig} from '@cloe-core/@core/@ui/components/cloe-table/cloe-table.interface';


export const TABLE_CONFIG: ITableConfig = {
  header: [
    {
      fieldName: 'metadataType',
      columnName: 'page.metadata.metadata-type',
    },
    {
      fieldName: 'metadataKey',
      columnName: 'global.key'
    },
    {
      fieldName: 'metadataValue',
      columnName: 'global.value'
    },
    {
      fieldName: 'nodeNature',
      columnName: 'component.table.nodeNature'
    },
    {
      fieldName: 'referUuid',
      columnName: 'page.metadata.refer-uuid'
    }
  ],
  actions: [
    {
      icon: 'edit',
      actionType: ACTIONS.OPEN_MODAL,
      metadata: {
        modalComponent: MetadataUpsertComponent,
        altMessage: 'global.edit'
      }
    },
    {
      icon: 'delete',
      actionType: ACTIONS.DELETE,
      metadata: {
        deleteByField: 'uuid',
        confirmMessage: 'component.confirm.messages.generic',
        altMessage: 'global.delete'
      }
    }
  ],
  metadata: {
    tableTitle: 'page.metadata.tableTitle',
    sort: 'id',
    sortType: 'asc'
  },
  filters: [
    {
      filterType: FilterTypeEnum.DROPDOWN,
      filterField: 'nodeNature',
      fieldLabel: 'component.table.nodeNature',
      items: ['MENU', 'CLOEUSER', 'CUSTOMER', 'CHANNEL', 'DEVICE']
    }
  ]
};
