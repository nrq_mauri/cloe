import { MetadataCrudComponent } from './metadata-crud.component';
import { NgModule } from '@angular/core';
import { UIRouterModule } from '@uirouter/angular';
import {BoCoreModule} from '@bo-core/bo-core.module';

const metadataCrud = {
  name: 'metadata-crud',
  parent: 'logged',
  url: '/metadata/list',
  component: MetadataCrudComponent
};

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [metadataCrud]}),
    BoCoreModule
  ],
  declarations: [MetadataCrudComponent],
  exports: [UIRouterModule]
})
export class MetadataCrudModule {}
