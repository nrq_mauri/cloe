import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TABLE_CONFIG } from './metadata-table.config';

@Component({
  selector: 'cloe-metadata-crud',
  templateUrl: './metadata-crud.component.html',
  styleUrls: ['./metadata-crud.component.css'],
  encapsulation: ViewEncapsulation.None
})

export class MetadataCrudComponent implements OnInit {
  constructor() {}

  tableConfig = TABLE_CONFIG;

  ngOnInit() {
  }
}
