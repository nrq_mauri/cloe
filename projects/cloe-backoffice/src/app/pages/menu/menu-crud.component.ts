import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {TABLE_CONFIG} from './menu-table.config';

@Component({
  selector: 'cloe-menu-crud',
  templateUrl: './menu-crud.component.html',
  styleUrls: ['./menu-crud.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MenuCrudComponent implements OnInit {
  constructor() {
  }

  tableConfig = TABLE_CONFIG;
  filters = {
  };

  ngOnInit() {
  }
}
