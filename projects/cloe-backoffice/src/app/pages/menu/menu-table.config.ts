import {FilterTypeEnum} from '@cloe-core/@core/enums/FilterTypeEnum';
import {ACTIONS} from '@cloe-core/@core/enums/ActionsEnum';
import {MsContextEnum} from '@cloe-core/@core/enums/MsContextEnum';
import {MenuUpsertComponent} from '@bo-core/components/modals';
import {ITableConfig} from '@cloe-core/@core/@ui/components/cloe-table/cloe-table.interface';

export const TABLE_CONFIG: ITableConfig = {
  header: [
    {
      fieldName: 'name',
      columnName: 'global.name',
    },
    {
      fieldName: 'externalUrl',
      columnName: 'page.menu.url',
    },
    {
      fieldName: 'ngState',
      columnName: 'page.menu.ng-state',
    },
    {
      fieldName: 'iconCode',
      columnName: 'page.menu.icon-code',
    },
    {
      fieldName: 'iconClass',
      columnName: 'page.menu.icon-class',
    },
    {
      fieldName: 'description',
      columnName: 'page.menu.description',
    },
    {
      fieldName: 'opened',
      columnName: 'page.menu.opened',
    },
    {
      fieldName: 'status',
      columnName: 'global.status',
    }
  ],
  actions: [
    {
      icon: 'edit',
      actionType: ACTIONS.OPEN_MODAL,
      metadata: {
        modalComponent: MenuUpsertComponent,
        altMessage: 'global.edit'
      }
    },
    {
      icon: 'delete',
      actionType: ACTIONS.DELETE,
      metadata: {
        deleteByField: 'uuid',
        confirmMessage: 'component.confirm.messages.generic',
        altMessage: 'global.delete'
      }
    }
  ],
  metadata: {
    tableTitle: 'page.menu.tableTitle',
    msContext: MsContextEnum.GATEWAY,
    addNewButton: {
      modalComponent: MenuUpsertComponent,
      buttonLabel: 'global.addNew'
    },
    sort: 'name',
    sortType: 'asc'
  },
  filters: [
    {
      filterType: FilterTypeEnum.TEXT,
      filterField: 'name',
      fieldLabel: 'global.name'
    }
  ]
};

