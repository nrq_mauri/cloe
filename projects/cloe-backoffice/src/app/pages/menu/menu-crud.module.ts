import { NgModule } from '@angular/core';
import { UIRouterModule } from '@uirouter/angular';
import { MenuCrudComponent } from './menu-crud.component';
import { BoCoreModule } from '@bo-core/bo-core.module';


const menuCrud = {
  name: 'menu-crud',
  parent: 'logged',
  url: '/menu/list',
  component: MenuCrudComponent
};

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [menuCrud]}),
    BoCoreModule
  ],
  declarations: [MenuCrudComponent],
  exports: [UIRouterModule]
})
export class MenuCrudModule {}
