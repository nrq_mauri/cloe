import {FilterTypeEnum} from '@cloe-core/@core/enums/FilterTypeEnum';
import {ACTIONS} from '@cloe-core/@core/enums/ActionsEnum';
import {TreeUpsertComponent} from '@bo-core/components/modals';
import {ITableConfig} from '@cloe-core/@core/@ui/components/cloe-table/cloe-table.interface';


export const TABLE_CONFIG: ITableConfig = {
  header: [
    {
      fieldName: 'name',
      columnName: 'global.name',
    },
    {
      fieldName: 'nodeNature',
      columnName: 'component.table.nodeNature'
    },
    {
      fieldName: 'status',
      columnName: 'global.status'
    },
    {
      fieldName: 'validStartDate',
      columnName: 'page.tree.validStartDate'
    },
    {
      fieldName: 'validEndDate',
      columnName: 'page.tree.validEndDate'
    }
  ],
  actions: [
    {
      icon: 'edit',
      actionType: ACTIONS.OPEN_MODAL,
      metadata: {
        modalComponent: TreeUpsertComponent,
        altMessage: 'global.edit'
      }
    },
    {
      icon: 'delete',
      actionType: ACTIONS.DELETE,
      metadata: {
        deleteByField: 'uuid',
        confirmMessage: 'component.confirm.messages.generic',
        altMessage: 'global.delete'
      }
    }
  ],
  metadata: {
    tableTitle: 'page.tree.tableTitle',
    sort: 'id',
    sortType: 'asc',
    addNewButton: {
      modalComponent: TreeUpsertComponent,
      buttonLabel: 'global.addNew'
    },
    search: {
      showedFieldName: 'user.firstName',
      fieldToSearch: {
        user: {
          firstName: '',
          email: ''
        },
        tags: ''
      },
      nodeNature: 'CLOEUSER'
    }
  },
  filters: [
    {
      filterType: FilterTypeEnum.DROPDOWN,
      filterField: 'nodeNature',
      fieldLabel: 'component.table.nodeNature',
      items: ['MENU', 'CLOEUSER', 'CUSTOMER', 'CHANNEL', 'DEVICE']
    },
    {
      filterType: FilterTypeEnum.DROPDOWN,
      filterField: 'status',
      fieldLabel: 'global.status',
      items: ['ACTIVE', 'DISABLE', 'CANCELLED']
    }
  ]
};
