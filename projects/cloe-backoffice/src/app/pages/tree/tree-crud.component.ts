import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TABLE_CONFIG } from './tree-table.config';

@Component({
  selector: 'cloe-tree-crud',
  templateUrl: './tree-crud.component.html',
  styleUrls: ['./tree-crud.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TreeCrudComponent implements OnInit {
  constructor() {}

  tableConfig = TABLE_CONFIG;
  filters = {
    status: 'ACTIVE'
  };

  ngOnInit() {
  }
}
