import { NgModule } from '@angular/core';
import { UIRouterModule } from '@uirouter/angular';
import { TreeCrudComponent } from './tree-crud.component';
import {BoCoreModule} from '@bo-core/bo-core.module';


const treeCrud = {
  name: 'tree-crud',
  parent: 'logged',
  url: '/tree/list',
  component: TreeCrudComponent
};

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [treeCrud]}),
    BoCoreModule
  ],
  declarations: [TreeCrudComponent],
  exports: [UIRouterModule]
})
export class TreeCrudModule {}
