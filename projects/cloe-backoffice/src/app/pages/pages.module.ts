import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HomeModule } from './home/home.module';
import { CustomerCrudModule } from './customer/customer-crud.module';
import { DummyModule } from './dummy/dummy.module';
import { MenuCrudModule } from './menu/menu-crud.module';
import { RoleModule } from './role/role.module';
import { TreeCrudModule } from './tree/tree-crud.module';
import { UserCrudModule } from './user/user-crud.module';
import { MetadataCrudModule } from './metadata/metadata-crud.module';
import { ThemeTemplateModule } from './themeTemplate/theme-template.module';
import { SiteCrudModule } from './site/site-crud.module';
import { DeviceCrudModule } from './device/device-crud.module';
import { ChannelCrudModule } from './channel/channel-crud.module';
import { UnitCrudModule } from './unit/unit-crud.module';
import { FeTypesEnum } from '@cloe-core/@core/enums/FeTypesEnum';
import { CloeCoreModule, ICorePagesConfig } from '@cloe-core/@core/cloe-core.module';
import {JobCrudModule} from './job/job-crud.module';
import { BoCorePagesModule } from './core-pages/bo-core-pages.module';
import { ChannelSiteTreeModule } from './channel-site-tree/channel-site-tree.module';
import { ChannelRangesCrudModule } from './channel-ranges/channel-ranges-crud.module';
import { LoginModule } from '@cloe-core/@core/core-pages/login/login.module';
import { HelpOnlineModule } from './help-online/help-online.module';

const corePageConfig: ICorePagesConfig = {feType: FeTypesEnum.BO};

@NgModule({
  imports: [
    // CLoE Core Pages
    CloeCoreModule.forRoot(corePageConfig),
    BoCorePagesModule,
    // CloE Modules
    FormsModule,
    HomeModule,
    LoginModule,
    CustomerCrudModule,
    DummyModule,
    MenuCrudModule,
    RoleModule,
    TreeCrudModule,
    MetadataCrudModule,
    UserCrudModule,
    ThemeTemplateModule,
    SiteCrudModule,
    DeviceCrudModule,
    ChannelCrudModule,
    UnitCrudModule,
    JobCrudModule,
    ChannelSiteTreeModule,
    ChannelRangesCrudModule,
    HelpOnlineModule,
  ],
  declarations: []
})
export class PagesModule {
}
