import { UIRouterModule } from '@uirouter/angular';
import { JobCrudComponent } from './job-crud.component';
import {BoCoreModule} from '@bo-core/bo-core.module';
import {NgModule} from '@angular/core';

const jobCrud = {
  name: 'job-crud',
  parent: 'logged',
  url: '/job/list',
  component: JobCrudComponent
};

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [jobCrud]}),
    BoCoreModule
  ],
  declarations: [JobCrudComponent],
  exports: [UIRouterModule]
})

export class JobCrudModule {}
