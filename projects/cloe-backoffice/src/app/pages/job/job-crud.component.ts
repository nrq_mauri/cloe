import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TABLE_CONFIG } from './job-table.config';
import { StatusEnum } from '@cloe-core/@core/enums/StatusEnum';
import { JobActionsEnum, JobService, jobSubject$ } from '@cloe-core/@core/services/job-service/job-service';
import { ConfirmModalComponent } from '@cloe-core/@core/@ui/components';
import { reloadTableDataSubject$ } from '@cloe-core/@core/@ui/components/cloe-table/table/cloe-table.component';
import { ModalService } from '@cloe-core/@core/services/modal-service/modal-service';

@Component({
  selector: 'cloe-job-crud',
  templateUrl: './job-crud.component.html',
  styleUrls: ['./job-crud.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class JobCrudComponent implements OnInit {

  jobSubscription;
  tableConfig = TABLE_CONFIG;
  filters = {
    status: StatusEnum.ACTIVE
  };

  constructor(
    private jobService: JobService,
    private modalService: ModalService,
  ) {
    this.jobSubscription = jobSubject$.asObservable().subscribe( (e: any) => {
      switch (e.action) {
        case JobActionsEnum.EXECUTE_JOB:
          this.jobService.executeJobByID(e.data.id).subscribe();
          break;
        case JobActionsEnum.DELETE_JOB:
          this.modalService.open(ConfirmModalComponent, {keyboard: false}).then(result => {
            if (result) {
              this.jobService.deleteJob(e.data).subscribe(() => reloadTableDataSubject$.next());
            }
          }).catch(() => {
            // dummy catch, we don't use it for now
          });
          this.modalService.getModalInstance().componentInstance.confirmMessage = e.metadata.confirmMessage;
      }
    });
  }

  ngOnInit() {}
}
