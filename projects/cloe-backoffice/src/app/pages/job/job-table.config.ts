import {FilterTypeEnum} from '@cloe-core/@core/enums/FilterTypeEnum';
import {MsContextEnum} from '@cloe-core/@core/enums/MsContextEnum';
import { JobUpsertComponent } from '@bo-core/components/modals';
import { ACTIONS } from '@cloe-core/@core/enums/ActionsEnum';
import { JobActionsEnum, jobSubject$ } from '@cloe-core/@core/services/job-service/job-service';
import {ITableConfig} from '@cloe-core/@core/@ui/components/cloe-table/cloe-table.interface';


export const TABLE_CONFIG: ITableConfig = {
  header: [
    {
      fieldName: 'name',
      columnName: 'global.name'
    },
    {
      fieldName: 'status',
      columnName: 'global.status'
    },
    {
      fieldName: 'jobConfig.name',
      columnName: 'page.job.config-name'
    },
    {
      fieldName: 'jobConfig.jobType',
      columnName: 'page.job.config-type'
    },
    {
      fieldName: 'jobConfig.fileName',
      columnName: 'page.job.generated-file'
    }
  ],
  actions: [
    {
      icon: 'play_arrow',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: jobSubject$,
        action: JobActionsEnum.EXECUTE_JOB,
        altMessage: 'global.execute'
      }
    },
    {
      icon: 'edit',
      actionType: ACTIONS.OPEN_MODAL,
      metadata: {
        modalComponent: JobUpsertComponent,
        altMessage: 'global.edit'
      }
    },
    {
      icon: 'delete',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: jobSubject$,
        action: JobActionsEnum.DELETE_JOB,
        confirmMessage: 'component.confirm.messages.generic',
        altMessage: 'global.delete'
      }
    }
  ],
  metadata: {
    tableTitle: 'page.job.tableTitle',
    msContext: MsContextEnum.GATEWAY,
    addNewButton: {
      modalComponent: JobUpsertComponent,
      buttonLabel: 'global.addNew'
    },
    sort: 'name',
    sortType: 'asc'
  },
  filters: [
    {
      filterType: FilterTypeEnum.TEXT,
      filterField: 'name',
      fieldLabel: 'global.name'
    }
  ]
};
