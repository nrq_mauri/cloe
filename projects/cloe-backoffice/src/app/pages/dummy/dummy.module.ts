import { NgModule } from '@angular/core';
import { UIRouterModule } from '@uirouter/angular';
import { DummyComponent } from './dummy.component';
import {BoCoreModule} from '@bo-core/bo-core.module';
import { NgbDropdown, NgbDropdownModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CloeUiModule } from '@cloe-core/@core/@ui/cloe-ui.module';

const state = {
  name: 'node-crud',
  parent: 'logged',
  url: '/dummy',
  component: DummyComponent
};

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [state]}),
    BoCoreModule
  ],
  declarations: [DummyComponent]
})
export class DummyModule {}
