import { Component, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'cloe-page-dummy',
  templateUrl: './dummy.component.html',
  styleUrls: ['./dummy-component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DummyComponent implements OnInit {
  ngOnInit(): void {
  }
}
