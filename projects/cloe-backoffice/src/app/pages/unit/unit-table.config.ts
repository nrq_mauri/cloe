import {FilterTypeEnum} from '@cloe-core/@core/enums/FilterTypeEnum';
import {ACTIONS} from '@cloe-core/@core/enums/ActionsEnum';
import {MetadataManagementComponent, UnitUpsertComponent} from '@bo-core/components/modals';
import {ITableConfig} from '@cloe-core/@core/@ui/components/cloe-table/cloe-table.interface';


export const TABLE_CONFIG: ITableConfig = {
  header: [
    {
      fieldName: 'name',
      columnName: 'global.name',
    },
    {
      fieldName: 'status',
      columnName: 'global.status',
    },
    {
      fieldName: 'symbol',
      columnName: 'page.unit.symbol'
    },
    {
      fieldName: 'angularCoefficient',
      columnName: 'page.unit.angular_coefficient'
    },
    {
      fieldName: 'intercept',
      columnName: 'page.unit.intercept'
    },
    {
      fieldName: 'scaleFactor',
      columnName: 'page.unit.scaleFactor'
    },
    {
      fieldName: 'integratedEquivalentId',
      columnName: 'page.unit.integratedEquivalentId'
    },
    {
      fieldName: 'differentiatedEquivalentId',
      columnName: 'page.unit.differentiatedEquivalentId'
    },
    {
      fieldName: 'integrationTime',
      columnName: 'page.unit.integrationTime'
    }
  ],
  actions: [
    {
      icon: 'edit',
      actionType: ACTIONS.OPEN_MODAL,
      metadata: {
        modalComponent: UnitUpsertComponent,
        altMessage: 'global.edit'
      }
    },
    {
      icon: 'delete',
      actionType: ACTIONS.DELETE,
      metadata: {
        deleteByField: 'uuid',
        confirmMessage: 'component.confirm.messages.generic',
        altMessage: 'global.delete'
      }
    },
    {
      icon: 'info',
      actionType: ACTIONS.OPEN_MODAL,
      metadata: {
        modalComponent: MetadataManagementComponent,
        altMessage: 'global.metadata'
      }
    }
  ],
  metadata: {
    tableTitle: 'page.unit.tableTitle',
    addNewButton: {
      modalComponent: UnitUpsertComponent,
      buttonLabel: 'global.addNew'
    },
    sort: 'name',
    sortType: 'asc'
  },
  filters: [
    {
      filterType: FilterTypeEnum.TEXT,
      filterField: 'name',
      fieldLabel: 'global.name'
    },
    {
      filterType: FilterTypeEnum.TEXT,
      filterField: 'angularCoefficient',
      fieldLabel: 'page.unit.angular_coefficient'
    },
    {
      filterType: FilterTypeEnum.TEXT,
      filterField: 'symbol',
      fieldLabel: 'page.unit.symbol'
    }
  ]
};
