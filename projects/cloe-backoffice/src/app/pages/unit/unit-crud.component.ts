import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TABLE_CONFIG } from './unit-table.config';

@Component({
  selector: 'cloe-unit-crud',
  templateUrl: './unit-crud.component.html',
  styleUrls: ['./unit-crud.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class UnitCrudComponent implements OnInit {
  constructor() {}

  tableConfig = TABLE_CONFIG;
  filters = {
    status: 'ACTIVE'
  };

  ngOnInit() {
  }
}
