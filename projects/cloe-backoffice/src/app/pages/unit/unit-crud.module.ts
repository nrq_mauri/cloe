import { UnitCrudComponent } from './unit-crud.component';
import { NgModule } from '@angular/core';
import { UIRouterModule } from '@uirouter/angular';
import {BoCoreModule} from '@bo-core/bo-core.module';

const unitCrud = {
  name: 'unit-crud',
  parent: 'logged',
  url: '/unit/list',
  component: UnitCrudComponent
};

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [unitCrud]}),
    BoCoreModule
  ],
  declarations: [UnitCrudComponent],
  exports: [UIRouterModule]
})
export class UnitCrudModule {}
