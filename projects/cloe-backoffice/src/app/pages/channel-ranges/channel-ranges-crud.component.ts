import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TABLE_CONFIG } from './channel-ranges-table.config';

@Component({
  selector: 'cloe-channel-crud',
  templateUrl: './channel-ranges-crud.component.html',
  styleUrls: ['./channel-ranges-crud.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ChannelRangesCrudComponent implements OnInit {
  constructor() {}

  tableConfig = TABLE_CONFIG;
  filters = {
    status: 'ACTIVE'
  };

  ngOnInit() {
  }
}
