import {ACTIONS} from '@cloe-core/@core/enums/ActionsEnum';
import { ChannelRangesUpsertComponent } from '@bo-core/components/modals/channel-ranges-upsert/channel-ranges-upsert.component';
import {ITableConfig} from '@cloe-core/@core/@ui/components/cloe-table/cloe-table.interface';

export const TABLE_CONFIG: ITableConfig = {
  header: [
    {
      fieldName: 'name',
      columnName: 'global.name'
    },
    {
      fieldName: 'status',
      columnName: 'global.status',
      i18n: {
        prefix: 'status.',
        suffix: ''
      }
    },
    {
      fieldName: 'rangeType',
      columnName: 'global.type',
      i18n: {
        prefix: 'component.channel-range-combine.range-types.',
        suffix: ''
      }
    },
    {
      fieldName: 'validStartDate',
      columnName: 'page.tree.validStartDate',
      pipe: 'cloeDate'
    },
    {
      fieldName: 'validEndDate',
      columnName: 'page.tree.validEndDate',
      pipe: 'cloeDate'
    }
  ],
  actions: [
    {
      icon: 'edit',
      actionType: ACTIONS.OPEN_MODAL,
      metadata: {
        modalComponent: ChannelRangesUpsertComponent,
        altMessage: 'global.edit'
      }
    },
    {
      icon: 'delete',
      actionType: ACTIONS.DELETE,
      metadata: {
        deleteByField: 'uuid',
        confirmMessage: 'component.confirm.messages.generic',
        altMessage: 'global.delete'
      }
    }
  ],
  metadata: {
    tableTitle: 'page.channel-ranges.tableTitle',
    addNewButton: {
      modalComponent: ChannelRangesUpsertComponent,
      buttonLabel: 'global.addNew'
    },
    sort: 'name',
    sortType: 'asc'
  }
};

