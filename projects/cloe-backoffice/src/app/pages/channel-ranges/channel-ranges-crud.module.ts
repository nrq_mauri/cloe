import { NgModule } from '@angular/core';
import { UIRouterModule } from '@uirouter/angular';
import { ChannelRangesCrudComponent } from './channel-ranges-crud.component';
import { BoCoreModule } from '@bo-core/bo-core.module';


const channelRangesCrud = {
  name: 'channel-ranges',
  parent: 'crud-bo',
  url: '/channel-ranges/list',
  component: ChannelRangesCrudComponent
};

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [channelRangesCrud]}),
    BoCoreModule
  ],
  declarations: [ChannelRangesCrudComponent],
  exports: [UIRouterModule]
})
export class ChannelRangesCrudModule {}
