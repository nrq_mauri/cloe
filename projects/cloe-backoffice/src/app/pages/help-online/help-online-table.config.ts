import {FilterTypeEnum} from '@cloe-core/@core/enums/FilterTypeEnum';
import {ACTIONS} from '@cloe-core/@core/enums/ActionsEnum';
import { PageHelpUpsertComponent } from '@bo-core/components';
import {ITableConfig} from '@cloe-core/@core/@ui/components/cloe-table/cloe-table.interface';


export const TABLE_CONFIG: ITableConfig = {
  header: [
    {
      fieldName: 'pageState',
      columnName: 'page.help-online.page-state',
    }
  ],
  actions: [
    {
      icon: 'edit',
      actionType: ACTIONS.OPEN_MODAL,
      metadata: {
        modalComponent: PageHelpUpsertComponent,
        altMessage: 'global.edit'
      }
    },
    {
      icon: 'delete',
      actionType: ACTIONS.DELETE,
      metadata: {
        deleteByField: 'uuid',
        confirmMessage: 'component.confirm.messages.generic',
        altMessage: 'global.delete'
      }
    }
  ],
  metadata: {
    tableTitle: 'menu.PAGE_HELP_ONLINE',
    sort: 'id',
    sortType: 'asc',
    addNewButton: {
      modalComponent: PageHelpUpsertComponent,
      buttonLabel: 'global.addNew'
    }
  },
  filters: [
    {
      filterType: FilterTypeEnum.TEXT,
      filterField: 'pageState',
      fieldLabel: 'page.help-online.page-state'
    }
  ]
};
