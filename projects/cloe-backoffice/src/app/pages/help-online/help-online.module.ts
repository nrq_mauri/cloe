import { NgModule } from '@angular/core';
import { UIRouterModule } from '@uirouter/angular';
import {BoCoreModule} from '@bo-core/bo-core.module';
import { HelpOnlineComponent } from './help-online.component';

const helpOnlineCrud = {
  name: 'help-online-crud',
  parent: 'crud-bo',
  url: '/help-online',
  component: HelpOnlineComponent
};

@NgModule({
  imports: [
    UIRouterModule.forChild({states: [helpOnlineCrud]}),
    BoCoreModule
  ],
  declarations: [HelpOnlineComponent],
  exports: [UIRouterModule]
})
export class HelpOnlineModule {}
