import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { TABLE_CONFIG } from './help-online-table.config';

@Component({
  selector: 'cloe-help-online-crud',
  templateUrl: './help-online.component.html',
  styleUrls: ['./help-online.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class HelpOnlineComponent implements OnInit {

  constructor() {}

  tableConfig = TABLE_CONFIG;
  filters = {
    status: 'ACTIVE'
  };

  ngOnInit() {
  }
}
