import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import * as moment_ from 'moment';

const moment = moment_;

@Component({
  selector: 'cloe-app',
  templateUrl: './app.component.html'
})
export class AppComponent {
  constructor(public translate: TranslateService) {
    translate.addLangs(['en', 'it', 'es']);
    translate.setDefaultLang('en');
    translate.use('it');
    moment.locale('it');
    this.translate.onLangChange.subscribe(() => {
      moment.locale(this.translate.currentLang);
    });
  }
}
