import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { UIRouterModule } from '@uirouter/angular';
import { PagesModule } from './pages/pages.module';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { NgbButtonsModule, NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';

import { AppComponent } from './app.component';
import {CloeCoreModule} from '@cloe-core/@core/cloe-core.module';
import { environment } from '../environments/environment';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    UIRouterModule,
    HttpClientModule,
    NgbModule.forRoot(),
    NgbButtonsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    }),
    // Custom
    CloeCoreModule,
    PagesModule,
  ],
  providers: [
    { provide: 'feHostUrl', useValue: environment.feHostUrl }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
