import {NgModule} from '@angular/core';

import {CloeCoreModule} from '@cloe-core/@core/cloe-core.module';
import {MODAL_COMPONENTS} from './components/modal-components.config';
import {
  TimePickerComponent,
  CronGenComponent,
  CloeFormulaButtonComponent,
  CloeFormulaPickerComponent,
  CloeFormulaDependenciesComponent,
  CloeCalculatedChannelComponent
} from './components';
import { WIZARD_TABS_COMPONENTS } from './components/wizard-tabs-components.config';
import { ChannelSiteTreeManagementService } from './service/channel-site-tree-management-service/channel-site-tree-management.service';
import { ChannelTreeManagementService } from './service/channel-site-tree-management-service/channel-tree-management.service';
import { SiteTreeManagementService } from './service/channel-site-tree-management-service/site-tree-management.service';

const COMPONENTS = [
  ...MODAL_COMPONENTS,
  ...WIZARD_TABS_COMPONENTS,
  CronGenComponent,
  TimePickerComponent,
  CloeFormulaButtonComponent,
  CloeFormulaPickerComponent,
  CloeFormulaDependenciesComponent,
  CloeCalculatedChannelComponent
];

const PIPES = [];

const SERVICES = [
  ChannelSiteTreeManagementService,
  ChannelTreeManagementService,
  SiteTreeManagementService,
];

@NgModule({
  imports: [CloeCoreModule],
  exports: [CloeCoreModule,  ...COMPONENTS, ...PIPES],
  declarations: [ ...COMPONENTS, ...PIPES],
  entryComponents: [...MODAL_COMPONENTS, ...WIZARD_TABS_COMPONENTS],
  providers: [
    ...PIPES,
    ...SERVICES
  ]
})
export class BoCoreModule {

}
