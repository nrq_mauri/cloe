import { Injectable } from '@angular/core';
import {
  EMPTY_SITE_SELECTED_ACTIONS,
  MULTI_SITE_SELECTED_ACTIONS, SINGLE_FOLDER_SELECTED_ACTIONS,
  SINGLE_SITE_SELECTED_ACTIONS
} from '../../../pages/channel-site-tree/actions-constants/sites-tree-actions-constants';
import { NodeCategoryEnum } from '@cloe-core/@core/enums/NodeCategoryEnum';
import { StorageService } from '@cloe-core/@core/services/storage-service/storage.service';
import { ITreeNode } from 'angular-tree-component/dist/defs/api';
import { keys } from 'lodash';
import { CloeTreeNodeService } from '@cloe-core/@core/services/cloe-treeNode-service/cloe-tree-node.service';
import { TreeModel } from 'angular-tree-component';

@Injectable()
export class SiteTreeManagementService {
  constructor(
    private treeNodeService: CloeTreeNodeService
  ) {}

  loadSiteTree(selectedSiteTree, resultTreeNodes, treeModel: TreeModel) {
    this.treeNodeService.getAllTreeNodeFromTree(
      selectedSiteTree.nodeNature,
      selectedSiteTree.uuid,
      StorageService.getCurrentUser().cloeUser.uuid
    ).subscribe((res) => {
      resultTreeNodes.push(res);
      const activeNodeIds = treeModel.activeNodeIds;
      treeModel.activeNodes.forEach((node: ITreeNode) => node.setIsActive(false, true));
      setTimeout(() => {
        keys(activeNodeIds).forEach((nodeId) => {
          const node = treeModel.getNodeById(nodeId);
          if (node) {
            node.setIsActive(true, true);
          }
        });
      }, 100);
      treeModel.update();
    });
  }

  checkSitesTreeAction($event: any) {
    if ($event.treeModel.activeNodes.length === 0) {
      return EMPTY_SITE_SELECTED_ACTIONS;
    }
    if ($event.treeModel.activeNodes.length === 1) {
      if ($event.node.data.nodeCategory === NodeCategoryEnum.GROUP) {
        return SINGLE_FOLDER_SELECTED_ACTIONS;
      }
      if ($event.node.data.nodeCategory === NodeCategoryEnum.LEAF) {
        return SINGLE_SITE_SELECTED_ACTIONS;
      }
    }
    if ($event.treeModel.activeNodes.length > 1) {
      let onlyLeaves = true;
      $event.treeModel.activeNodes.forEach((node) => {
        if (node.data.nodeCategory !== NodeCategoryEnum.LEAF) {
          onlyLeaves = false;
        }
      });
      return onlyLeaves ? MULTI_SITE_SELECTED_ACTIONS : [];
    }
    return [];
  }
}
