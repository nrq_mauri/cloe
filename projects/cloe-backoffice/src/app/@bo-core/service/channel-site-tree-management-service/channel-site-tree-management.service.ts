import { Injectable } from '@angular/core';
import { TreeModel } from 'angular-tree-component';
import { findIndex, remove, find } from 'lodash';
import { ITreeNode } from 'angular-tree-component/dist/defs/api';
import { NodeCategoryEnum } from '@cloe-core/@core/enums/NodeCategoryEnum';

@Injectable()
export class ChannelSiteTreeManagementService {
  constructor() {}

  checkAndAddUpdateTreeNodes(updateTreeNodes, treeModel: TreeModel, node: ITreeNode) {
    if (findIndex(updateTreeNodes, (treeNode: any) => treeNode.treeNodes[0].id === node.path[1]) === -1) {
      updateTreeNodes.push({
        treeNodes: [treeModel.getNodeById(node.path[1]).data],
        nodeIdsToDelete: [],
        referUuid: treeModel.getNodeById(node.path[1]).data.rootReferUuid
      });
    }
  }

  checkAndAddDeleteTreeNodes(deleteTreeNodes, updateTreeNodes, treeModel: TreeModel, node: ITreeNode) {
    if (node.path.length === 2 && node.data.nodeCategory === NodeCategoryEnum.GROUP) {
      deleteTreeNodes.push({
        treeNodes: [node.data],
        referUuid: node.data.rootReferUuid
      });
      remove(
        updateTreeNodes, (treeNode: any) => treeNode.treeNodes[0].id === node.path[1]);
    } else {
      let updateTreeNode = find(updateTreeNodes, (treeNode) => treeNode.treeNodes[0].id === node.path[1]);
      if (updateTreeNode) {
        this.addDeletedNodesToList(node, updateTreeNode.nodeIdsToDelete);
      } else {
        updateTreeNode = {
          treeNodes: [treeModel.getNodeById(node.path[1]).data],
          nodeIdsToDelete: [],
          referUuid: treeModel.getNodeById(node.path[1]).data.rootReferUuid
        };
        this.addDeletedNodesToList(node, updateTreeNode.nodeIdsToDelete);
        updateTreeNodes.push(updateTreeNode);
      }
    }
  }

  addDeletedNodesToList(node, result) {
    result.push(node.data.id);
    if (node.children) {
      node.children.forEach((child) => this.addDeletedNodesToList(child, result));
    }
  }
}
