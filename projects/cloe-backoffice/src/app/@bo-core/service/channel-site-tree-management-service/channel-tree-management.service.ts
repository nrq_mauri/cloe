import { Injectable } from '@angular/core';
import { NatureTypesEnum } from '@cloe-core/@core/enums/NatureTypesEnum';
import { StorageService } from '@cloe-core/@core/services/storage-service/storage.service';
import { forkJoin } from 'rxjs';
import { keys } from 'lodash';
import { ITreeNode } from 'angular-tree-component/dist/defs/api';
import { CloeTreeNodeService } from '@cloe-core/@core/services/cloe-treeNode-service/cloe-tree-node.service';
import { TreeModel } from 'angular-tree-component';
import { NodeCategoryEnum } from '@cloe-core/@core/enums/NodeCategoryEnum';
import {
  EMPTY_SELECTION_ACTIONS,
  MULTI_CHANNEL_SELECTED_ACTIONS,
  SINGLE_FOLDER_SELECTED_ACTIONS
} from '../../../pages/channel-site-tree/actions-constants/channels-tree-actions-constants';

@Injectable()
export class ChannelTreeManagementService {
  constructor(
    private treeNodeService: CloeTreeNodeService
  ) {}

  reloadChannelTree(channelSiteMap, treeModel: TreeModel) {
    const observableChannelTreeNodeArray =  [];
    keys(channelSiteMap).forEach((referUuid) => observableChannelTreeNodeArray.push(
      this.treeNodeService.getTreeNodeByReferUuid(
        NatureTypesEnum.CHANNEL,
        StorageService.getCurrentUser(),
        false, // TODO aggiungere user object per i channels
        referUuid
      ))
    );
    forkJoin(observableChannelTreeNodeArray).subscribe((resp) => {
      const activeNodeIds = treeModel.activeNodeIds;
      treeModel.activeNodes.forEach((node: ITreeNode) => node.setIsActive(false, true));
      setTimeout(() => {
        keys(activeNodeIds).forEach(
          (nodeId) => {
            const node = treeModel.getNodeById(nodeId);
            if (node) {
              node.setIsActive(true, true);
            }
          });
      }, 100);
    });
  }

  loadChannelTree(channelSiteMap, channelNodes, siteReferUuid, treeModel: TreeModel) {
    this.treeNodeService.getTreeNodeByReferUuid(
      NatureTypesEnum.CHANNEL,
      StorageService.getCurrentUser(),
      false, // TODO aggiungere user object per i channels
      siteReferUuid
    ).subscribe((res: any) => {
      if (res) {
        channelSiteMap[siteReferUuid] = res.id;
        channelNodes[0].children.push(res);
        treeModel.update();
      }
    });
  }

  checkAndAddChannelTreeNodesToDeleteFromSite(deletedTreeNodes, treeModel: TreeModel, node: ITreeNode) {
    if (node.data.nodeCategory === NodeCategoryEnum.LEAF && node.isActive) {
      node.setIsActive(false);
    }
    const observableArray = [];
    this.buildChannelTreeNodesRequest(observableArray, node);
    forkJoin(observableArray).subscribe((resp) => {
      resp.forEach((res) => {
        deletedTreeNodes.push({
          treeNodes: [res],
          referUuid: res.rootReferUuid
        });
      });
    });
  }

  private buildChannelTreeNodesRequest(observableArray, node) {
    if (node.data.nodeCategory === NodeCategoryEnum.LEAF) {
      observableArray.push(
        this.treeNodeService.getTreeNodeByReferUuid(
          NatureTypesEnum.CHANNEL,
          StorageService.getCurrentUser(),
          false, // TODO aggiungere user object per i channels
          node.data.referUuid
        )
      );
    }
    if (node.children) {
      node.children.forEach((child) => this.buildChannelTreeNodesRequest(observableArray, child));
    }
  }

  checkChannelsTreeAction($event: any) {
    if ($event.treeModel.activeNodes.length === 1) {
      if ($event.node.data.nodeCategory === NodeCategoryEnum.GROUP) {
        return SINGLE_FOLDER_SELECTED_ACTIONS;
      }
      if ($event.node.data.nodeCategory === NodeCategoryEnum.LEAF) {
        return MULTI_CHANNEL_SELECTED_ACTIONS;
      }
    }
    if ($event.treeModel.activeNodes.length > 1) {
      let onlyLeaves = true;
      $event.treeModel.activeNodes.forEach((node) => {
        if (node.data.nodeCategory !== NodeCategoryEnum.LEAF) {
          onlyLeaves = false;
        }
      });
      return onlyLeaves ? MULTI_CHANNEL_SELECTED_ACTIONS : EMPTY_SELECTION_ACTIONS;
    }
    return EMPTY_SELECTION_ACTIONS;
  }
}
