import {
  CustomerUpsertComponent,
  TreeUpsertComponent,
  TreeNodeManagementComponent,
  MetadataUpsertComponent,
  UserUpsertComponent,
  MetadataManagementComponent,
  ThemeTemplateUpsertComponent,
  SiteUpsertComponent,
  DeviceUpsertComponent,
  ChannelUpsertComponent,
  UnitUpsertComponent,
  MenuUpsertComponent,
  JobUpsertComponent,
  ChannelCombineComponent,
  ChannelRangesCombineComponent,
  PageHelpUpsertComponent,
} from './modals';
import { ChannelRangesUpsertComponent } from './modals/channel-ranges-upsert/channel-ranges-upsert.component';
import { CloeCalculatedChannelComponent } from './cloe-math';

export const MODAL_COMPONENTS = [
  // Components from MODAL_COMPONENTS_MAP
  CustomerUpsertComponent,
  MenuUpsertComponent,
  TreeUpsertComponent,
  TreeNodeManagementComponent,
  UserUpsertComponent,
  ThemeTemplateUpsertComponent,
  MetadataUpsertComponent,
  MetadataManagementComponent,
  SiteUpsertComponent,
  DeviceUpsertComponent,
  ChannelUpsertComponent,
  UnitUpsertComponent,
  JobUpsertComponent,
  ChannelCombineComponent,
  ChannelRangesCombineComponent,
  ChannelRangesUpsertComponent,
  CloeCalculatedChannelComponent,
  PageHelpUpsertComponent,
];
