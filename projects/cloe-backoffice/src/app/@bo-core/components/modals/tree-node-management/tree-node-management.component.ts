import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { IActionMapping, ITreeOptions, ITreeState, TREE_ACTIONS } from 'angular-tree-component';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { remove, uniq, reverse } from 'lodash';
import { CloeTreeComponent } from '@cloe-core/@core/@ui/components';
import { CloeModalComponentInterface } from '@cloe-core/@core/interfaces/cloe-modal-component.interface';
import { CommonService } from '@cloe-core/@core/services/common-service/common-service';
import { CloeTreeService } from '@cloe-core/@core/services/cloe-tree-service/cloe-tree.service';
import { CloeTreeNodeService } from '@cloe-core/@core/services/cloe-treeNode-service/cloe-tree-node.service';
import { StatusEnum } from '@cloe-core/@core/enums/StatusEnum';
import * as moment_ from 'moment';
import { ITreeNode } from 'angular-tree-component/dist/defs/api';
import { TreeFiltersTypeEnum } from '@cloe-core/@core/services/cloe-tree-service/enums/TreeFiltersTypeEnum';
import { CloeTreeFiltersService } from '@cloe-core/@core/services/cloe-tree-service/cloe-tree-filters.service';
import { NatureTypesEnum } from '@cloe-core/@core/enums/NatureTypesEnum';
import { UserTreeRootService } from '@cloe-core/@core/services/user-tree-root-service/user-tree-root-service';

const moment = moment_;

@Component({
  selector: 'cloe-core-tree-node-management',
  templateUrl: './tree-node-management.component.html',
  styleUrls: ['./tree-node-management.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TreeNodeManagementComponent implements OnInit, CloeModalComponentInterface {
  @ViewChild(CloeTreeComponent) cloeTree: CloeTreeComponent;

  modalTitle;
  user;
  trees = [];
  treeNodes = [];
  statuses;
  selectedTree;
  selectedNode;
  selectedNodes: ITreeNode[] = [];
  activeNodesToggle;
  nodeModel: any = {};
  state: ITreeState;
  nodeNatureType;

  actionMapping: IActionMapping = {
    mouse: {
      click: (tree, node, $event) => {
        $event.ctrlKey
          ? TREE_ACTIONS.TOGGLE_ACTIVE_MULTI(tree, node, $event)
          : TREE_ACTIONS.TOGGLE_ACTIVE(tree, node, $event);
      }
    }
  };
  options: ITreeOptions = {
    allowDrag: false,
    allowDrop: true,
    actionMapping: this.actionMapping
  };
  filtersConfig;

  constructor(
    public modalRef: NgbActiveModal,
    private treeService: CloeTreeService,
    private treeNodeService: CloeTreeNodeService,
    private treeFiltersService: CloeTreeFiltersService,
    private userTreeRootService: UserTreeRootService,
    private commonService: CommonService
  ) {
  }

  ngOnInit() {
    this.commonService.getNodeStatus().subscribe(resp => {
      this.statuses = resp;
    });
  }

  initData(data ?: any) {
    this.modalTitle = 'component.tree-node-management.modalTitle';
    if (data) {
      this.user = data;
      this.nodeNatureType = this.user.btnMetadata.nodeNatureType;

      if (this.user.cloeUser) {
        this.treeService.searchTreeListForUser(this.nodeNatureType, this.user.cloeUser.uuid)
          .subscribe(trees => {
            this.trees = trees;
          });
      }
    }
  }

  loadTreeData() {
    this.treeNodeService.getTreeNodeFromRoot(
      this.selectedTree.nodeNature,
      this.selectedTree,
      this.user.cloeUser.uuid,
      false,
      false,
      false
    )
      .subscribe(treeNodes => {
        this.treeNodes = [];
        this.treeNodes.push(treeNodes);
        if (this.selectedTree.nodeNature === NatureTypesEnum.SITE) {
          this.filtersConfig = this.treeFiltersService.addFiltersByType(TreeFiltersTypeEnum.SITE_FILTERS, this.treeNodes);
        }
        this.updateTree();
      });
  }

  updateTree() {
    this.cloeTree.tree.treeModel.update();
  }

  selectNode(node) {
    this.selectedNode = node;
    this.selectedNodes.push(node);
    if (this.selectedNodes.length === 1 && node.data.userNode) {
      this.nodeModel.status = node.data.userNode.status;
      this.nodeModel.validStartDate = node.data.userNode.validStartDate;
      this.nodeModel.validEndDate = node.data.userNode.validEndDate;
    } else {
      this.nodeModel = {};
    }
  }

  deselectNode(node) {
    remove(this.selectedNodes, n => n.id === node.id);
  }

  onDateChange(data) {
    this.nodeModel.validStartDate = data[0];
    this.nodeModel.validEndDate = data[1];
  }

  onSubmit() {
    this.updateChildrenNodes();
    this.updateAncestorNodes();
    const result = [];
    this.getAllUserNodes(this.cloeTree.tree.treeModel.getFirstRoot(), result);
    const objectUuidToAdd = [];
    const objectUuidToRemove = [];
    this.buildUserTreeRoots(result, objectUuidToAdd, objectUuidToRemove);
    remove(result, (userNode) => !userNode.objectUuid);
    if (this.nodeNatureType === 'SITE') {
      this.userTreeRootService.upsertUserTreeRoot({
        userUuid: this.user.cloeUser.uuid,
        natureType: 'CHANNEL',
        objectUuidToAddList: objectUuidToAdd,
        objectUuidToRemoveList: objectUuidToRemove
      }).subscribe();
    }
    this.treeNodeService.saveAllUserNodes(result).subscribe();
    this.selectedNodes = [];
  }

  private buildUserTreeRoots(userObjects, objectUuidToAddList, objectUuidToRemoveList) {
    userObjects.forEach((userObject) => {
      if (userObject.objectUuid) {
        if (userObject.status === StatusEnum.ACTIVE) {
          objectUuidToAddList.push(userObject.objectUuid);
        } else {
          objectUuidToRemoveList.push(userObject.objectUuid);
        }
      }
    });
  }

  getAllUserNodes(element, result) {
    if (element.data.userNode) {
      result.push(element.data.userNode);
    }
    if (element.children) {
      element.children.forEach((child) => {
        this.getAllUserNodes(child, result);
      });
    }
  }

  updateAncestorNodes() {
    let selectedNodesAncestors = [];
    for (let i = 0; i < this.selectedNodes.length; i++) {
      selectedNodesAncestors.push(...this.selectedNodes[i].path.splice(0, this.selectedNodes[i].path.length - 1));
    }
    selectedNodesAncestors = reverse(uniq(selectedNodesAncestors));
    selectedNodesAncestors.forEach((nodeId) => {
      const node: ITreeNode = this.cloeTree.tree.treeModel.getNodeById(nodeId);
      if (node) {
        this.checkAndUpdateAncestorNode(node);
      }
    });
  }

  checkAndUpdateAncestorNode(node: ITreeNode) {
    if (node.data.userNode) {
      if (this.nodeModel.validStartDate && this.nodeModel.validEndDate) {
        if (
          !node.data.userNode.validStartDate ||
          this.isBefore(this.nodeModel.validStartDate, node.data.userNode.validStartDate)
        ) {
          node.data.userNode.validStartDate = this.nodeModel.validStartDate;
        }
        if (
          !node.data.userNode.validEndDate ||
          this.isBefore(node.data.userNode.validEndDate, this.nodeModel.validEndDate)
        ) {
          node.data.userNode.validEndDate = this.nodeModel.validEndDate;
        }
      }
      if (!node.isRoot) {
        const status = { status: StatusEnum.DISABLE };
        this.setStatusAncestorFromChildren(node, status);
        node.data.userNode.status = status.status;
      }
    } else {
      node.data.userNode = {};
      node.data.userNode.userUuid = this.user.cloeUser.uuid;
      node.data.userNode.objectUuid = node.data.referUuid;
      if (this.nodeModel.validStartDate && this.nodeModel.validEndDate) {
        node.data.userNode.validStartDate = this.nodeModel.validStartDate;
        node.data.userNode.validEndDate = this.nodeModel.validEndDate;
      }
      if (!node.isRoot) {
        const status = { status: StatusEnum.DISABLE };
        this.setStatusAncestorFromChildren(node, status);
        node.data.userNode.status = status.status;
      }
    }
  }

  setStatusAncestorFromChildren(node: ITreeNode, statusResult) {
    if (node.data.userNode && node.data.userNode.status === StatusEnum.ACTIVE) {
      statusResult.status = StatusEnum.ACTIVE;
    }
    node.children.forEach((child) => {
      this.setStatusAncestorFromChildren(child, statusResult);
    });
  }

  isBefore(one, two) {
    return moment(one).isBefore(moment(two));
  }

  updateChildrenNodes() {
    this.selectedNodes.forEach((node) => {
      this.updateDescendantNodes(node);
    });
  }

  updateDescendantNodes(node: ITreeNode) {
    if (node.data.userNode) {
      if (this.nodeModel.validStartDate && this.nodeModel.validEndDate) {
        node.data.userNode.validStartDate = this.nodeModel.validStartDate;
        node.data.userNode.validEndDate = this.nodeModel.validEndDate;
      }
      if (this.nodeModel.status) {
        node.data.userNode.status = this.nodeModel.status;
      }
    } else {
      node.data.userNode = {};
      node.data.userNode.userUuid = this.user.cloeUser.uuid;
      node.data.userNode.objectUuid = node.data.referUuid;
      if (this.nodeModel.validStartDate && this.nodeModel.validEndDate) {
        node.data.userNode.validStartDate = this.nodeModel.validStartDate;
        node.data.userNode.validEndDate = this.nodeModel.validEndDate;
      }
      if (this.nodeModel.status) {
        node.data.userNode.status = this.nodeModel.status;
      }
    }
    if (node.children) {
      node.children.forEach((child) => {
        this.updateDescendantNodes(child);
      });
    }
  }

  updateChildNode(node, status) {
    if (status) {
      node.data.userNode.status = status;
    }
    if (this.nodeModel.validStartDate) {
      node.data.userNode.validStartDate = this.nodeModel.validStartDate;
    }
    if (this.nodeModel.validEndDate) {
      node.data.userNode.validEndDate = this.nodeModel.validEndDate;
    }

    this.treeNodeService.updateUserNode(this.user.cloeUser.uuid, node.data);
    if (node.children) {
      node.children.forEach((child) => this.updateChildNode(child, status));
    }
  }

  onToggleChange(toggle) {
    if (toggle) {
      this.hideNodes();
    } else {
      this.state = {
        ...this.state,
        hiddenNodeIds: {}
      };
    }
  }

  hideNodes() {
    const hiddenNodeIds = {};
    this.recursiveHideNodes(this.cloeTree.tree.treeModel.getFirstRoot(false).data, hiddenNodeIds);
    this.state = {
      ...this.state,
      hiddenNodeIds
    };
  }

  recursiveHideNodes(element, hiddenNodeIds) {
    hiddenNodeIds[element.id] = element.userNode.status !== StatusEnum.ACTIVE;
    if (element.children) {
      element.children.forEach((child) => {
        this.recursiveHideNodes(child, hiddenNodeIds);
      });
    }
  }
}
