import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { CloeModalComponentInterface } from '@cloe-core/@core/interfaces/cloe-modal-component.interface';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { cloneDeep, extend, assign, extendWith, unionWith, unionBy, isEqual, findIndex, forEach, map, isNil } from 'lodash';
import { ChannelRangeTypeEnum } from '@cloe-core/@core/enums/ChannelRangeTypeEnum';
import { ChannelRangeService } from '@cloe-core/@core/services/channel-range-service/channel-range-service';
import { WizardTab } from '@cloe-core/@core/@ui/components/cloe-wizard/models/wizard-tab';
import {
  JSONChannelRangeAlgorithm
} from '@cloe-core/@core/services/channel-range-service/models/JSONChannelRangeAlgorithm';
import { getBaseChannelRangeForm } from './channel-ranges-base-info-form-costant';
import { getChannelRateTabs } from './channel-range-rate-tabs-constant';
import { CloeDynamicFormComponent } from '@cloe-core/@core/@ui/components';
import { getChannelProfileTabs } from './channel-range-profile-tabs-constant';
import { StatusEnum } from '@cloe-core/@core/enums/StatusEnum';

@Component({
  selector: 'cloe-core-channel-ranges-upsert',
  templateUrl: './channel-ranges-upsert.component.html',
  styleUrls: ['./channel-ranges-upsert.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ChannelRangesUpsertComponent implements OnInit, CloeModalComponentInterface {
  @ViewChild(CloeDynamicFormComponent) dynamicFormComponent: CloeDynamicFormComponent;
  modalTitle;
  rangeModel: any;
  tabs: WizardTab[] = [];
  questions = [];

  constructor(
    private channelRangeService: ChannelRangeService,
    public modalRef: NgbActiveModal
  ) {
  }

  ngOnInit() {
  }

  initData(data?: any) {
    if (data) {
      this.rangeModel = cloneDeep(data);
      this.rangeModel.jsonAlgorithm = <JSONChannelRangeAlgorithm>JSON.parse(this.rangeModel.channelRangeAlgorithm);
      this.modalTitle = this.rangeModel.rangeType === ChannelRangeTypeEnum.RATE ?
        'component.channel-range-upsert.edit-rate' : 'component.channel-range-upsert.edit-profile';
    } else {
      this.modalTitle = 'component.channel-range-upsert.new-channel-range';
      this.rangeModel = {};
    }
    this.setTabsFromRangeType(this.rangeModel.rangeType);
    this.setQuestionsFromModel();
    console.log(this.rangeModel);
  }

  setTabsFromRangeType(type) {
    if (type === ChannelRangeTypeEnum.RATE) {
      this.tabs = getChannelRateTabs(this.rangeModel);
    }
    if (type === ChannelRangeTypeEnum.PROFILE) {
      this.tabs = getChannelProfileTabs(this.rangeModel);
    }
  }

  setQuestionsFromModel() {
    this.questions = getBaseChannelRangeForm(this.rangeModel);
  }

  onFileLoad(fileLoadedEvent) {
    const csvContent = fileLoadedEvent.target.result;
    const range = this.channelRangeService.createAlgorithmStringFromCSV(csvContent);
    const jsonAlgorithm = range.jsonAlgorithm;
    delete range.jsonAlgorithm;
    this.rangeModel = extend(this.rangeModel, range);
    this.rangeModel.jsonAlgorithm = extendWith(this.rangeModel.jsonAlgorithm, jsonAlgorithm, (objValue, srcValue, fieldName) => {
      switch (fieldName) {
        case 'dailyProfiles':
        case 'rates':
          return unionBy(srcValue, objValue, 'id');
        case 'specialProfiles':
          return unionWith(srcValue, objValue, isEqual);
        case 'weeklyProfiles':
          return map(srcValue, (obj, index) => {
            return !isNil(obj) ? obj : objValue[index];
          });
        default:
          return srcValue;
      }
    });
    this.setTabsFromRangeType(this.rangeModel.rangeType);
    this.setQuestionsFromModel();
  }

  onFileSelected(files) {
    if (files && files.length) {
      const fileToRead = files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e) => this.onFileLoad(e);
      fileReader.readAsText(fileToRead, 'UTF-8');
    }
  }

  public onDynamicFormChanged(values) {
    if (values.rangeType !== this.rangeModel.rangeType) {
      this.setTabsFromRangeType(values.rangeType);
    }
    this.rangeModel = assign(this.rangeModel, values);
  }

  onSubmit() {
    this.rangeModel.jsonAlgorithm.maxSamplingPeriod = this.rangeModel.maxSamplingPeriod;
    this.rangeModel.channelRangeAlgorithm = JSON.stringify(this.rangeModel.jsonAlgorithm);
    this.rangeModel.status = StatusEnum.ACTIVE;
    this.channelRangeService.upsertChannelRange(this.rangeModel).subscribe(() => this.modalRef.close('RELOAD'));
  }
}

