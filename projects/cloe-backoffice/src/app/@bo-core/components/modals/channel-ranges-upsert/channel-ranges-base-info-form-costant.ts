import { TextboxQuestion } from '@cloe-core/@core/@ui/components/cloe-dynamic-form/models/TextboxQuestion';
import {
  DATE_VALIDATOR,
  REQUIRED_VALIDATOR
} from '@cloe-core/@core/@ui/components/cloe-dynamic-form/models/validators/validators';
import { DatepickerQuestion } from '@cloe-core/@core/@ui/components/cloe-dynamic-form/models/DatepickerQuestion';
import { DateTimeTypeEnum } from '@cloe-core/@core/@ui/components/cloe-date-time-picker/enums/DateTimeTypeEnum';
import * as moment_ from 'moment';
import { DropdownQuestion } from '@cloe-core/@core/@ui/components/cloe-dynamic-form/models/DropdownQuestion';
import { ChannelRangeTypeEnum } from '@cloe-core/@core/enums/ChannelRangeTypeEnum';
import { keys, map } from 'lodash';

const moment = moment_;

export const getBaseChannelRangeForm = (channelRange) => {
  return [
    new TextboxQuestion({
      key: 'name',
      label: 'global.name',
      value: channelRange.name,
      type: 'text',
      validators: [ REQUIRED_VALIDATOR ],
      order: 1
    }),
    new TextboxQuestion({
      key: 'maxSamplingPeriod',
      label: 'component.channel-range-upsert.max-sampling-period',
      value: channelRange.jsonAlgorithm ? channelRange.jsonAlgorithm.maxSamplingPeriod : undefined,
      type: 'number',
      validators: [ REQUIRED_VALIDATOR ],
      order: 1
    }),
    new DatepickerQuestion({
      key: 'validStartDate',
      label: 'page.tree.validStartDate',
      value: moment(channelRange.validStartDate),
      validators: [ REQUIRED_VALIDATOR, DATE_VALIDATOR ],
      order: 2,
      config: {
        isRange: false,
        placeholder: 'component.date-time-picker.select-date',
        inputType: DateTimeTypeEnum.CALENDAR
      }
    }),
    new DatepickerQuestion({
      key: 'validEndDate',
      label: 'page.tree.validEndDate',
      value: moment(channelRange.validEndDate),
      validators: [ REQUIRED_VALIDATOR, DATE_VALIDATOR ],
      order: 2,
      config: {
        isRange: false,
        placeholder: 'component.date-time-picker.select-date',
        inputType: DateTimeTypeEnum.CALENDAR
      }
    }),
    new DropdownQuestion({
      key: 'rangeType',
      label: 'global.type',
      value: channelRange.rangeType,
      validators: [ REQUIRED_VALIDATOR ],
      options: channelRange.rangeType ?
        [{key: channelRange.rangeType, value: channelRange.rangeType }] :
        map(keys(ChannelRangeTypeEnum), (key) => ({key: key, value: key })),
      order: 2
    })
  ];
};
