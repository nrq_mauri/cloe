import {
  ChannelRangesRatesComponent
} from '../../wizard-tabs/channel-ranges-upsert-tabs/channel-ranges-upsert/channel-ranges-rates.component';
import { DailyProfilesComponent } from '../../wizard-tabs/channel-ranges-upsert-tabs/daily-profiles/daily-profiles.component';
import { SpecialProfilesComponent, WeeklyProfilesComponent } from '../../wizard-tabs';

export const getChannelRateTabs = (channelRange) => {
  return [
    {
      name: 'component.channel-range-upsert.rates',
      component: ChannelRangesRatesComponent,
      data: channelRange
    },
    {
      name: 'component.channel-range-upsert.daily-profiles',
      component: DailyProfilesComponent,
      data: channelRange
    },
    {
      name: 'component.channel-range-upsert.weekly-profiles',
      component: WeeklyProfilesComponent,
      data: channelRange
    },
    {
      name: 'component.channel-range-upsert.special-profiles',
      component: SpecialProfilesComponent,
      data: channelRange
    }
  ];
};
