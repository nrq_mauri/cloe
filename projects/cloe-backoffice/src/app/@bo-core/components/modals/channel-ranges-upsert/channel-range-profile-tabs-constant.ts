import { DailyProfilesComponent } from '../../wizard-tabs/channel-ranges-upsert-tabs/daily-profiles/daily-profiles.component';
import { SpecialProfilesComponent, WeeklyProfilesComponent } from '../../wizard-tabs';

export const getChannelProfileTabs = (channelRange) => {
  return [
    {
      name: 'component.channel-range-upsert.daily-profiles',
      component: DailyProfilesComponent,
      data: channelRange
    },
    {
      name: 'component.channel-range-upsert.weekly-profiles',
      component: WeeklyProfilesComponent,
      data: channelRange
    },
    {
      name: 'component.channel-range-upsert.special-profiles',
      component: SpecialProfilesComponent,
      data: channelRange
    }
  ];
};
