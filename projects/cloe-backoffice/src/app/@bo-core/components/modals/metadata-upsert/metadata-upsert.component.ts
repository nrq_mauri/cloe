import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {CloeModalComponentInterface} from '@cloe-core/@core/interfaces/cloe-modal-component.interface';
import {MetadataService} from '@cloe-core/@core/services/metadata-service/metadata-service';
import { CommonService } from '@cloe-core/@core/services/common-service/common-service';

@Component({
  selector: 'cloe-core-upsert-metadata',
  templateUrl: './metadata-upsert.component.html',
  styleUrls: ['./metadata-upsert.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MetadataUpsertComponent implements OnInit, CloeModalComponentInterface {
  override = true;
  modalTitle;
  metadata;
  meta: any = {};
  metadataTypeList;
  nodeNatureList;

  constructor(
    private metadataService: MetadataService,
    private commonService: CommonService,
    public modalRef: NgbActiveModal
  ) {}

  ngOnInit() {
    this.metadataService.getMetadataTypeEnum().subscribe(resp => {
      this.metadataTypeList = resp;
    });
    this.commonService.getNodeNatureEnum().subscribe(resp => {
      this.nodeNatureList = resp;
    });
  }

  onSubmit() {
    this.metadataService.upsertMetadata(this.meta).subscribe(resp => {
      this.modalRef.close('RELOAD');
    });
  }

  initData(_meta: any) {
    this.meta = _meta;
    this.metadata = _meta.metadata;
    this.modalTitle = 'page.metadata.modalTitle';
  }

}
