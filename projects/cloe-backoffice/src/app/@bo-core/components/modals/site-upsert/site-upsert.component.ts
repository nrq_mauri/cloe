import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';
import { debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { cloneDeep, filter } from 'lodash';
import {CloeModalComponentInterface} from '@cloe-core/@core/interfaces/cloe-modal-component.interface';
import {SiteService} from '@cloe-core/@core/services/site-service/site.service';
import {CommonService} from '@cloe-core/@core/services/common-service/common-service';
import { tagsDomainPickerConf } from '@cloe-core/@core/@ui/components/cloe-domain-picker/standard-configurations/tags-conf';
import { TagService } from '@cloe-core/@core/services/tag-service/tag-service';
import { NatureTypesEnum } from '@cloe-core/@core/enums/NatureTypesEnum';
import { NodeService } from '@cloe-core/@core/services/node-service/node.service';
import { StorageService } from '@cloe-core/@core/services/storage-service/storage.service';

@Component({
  selector: 'cloe-core-upsert-site',
  templateUrl: './site-upsert.component.html',
  styleUrls: ['./site-upsert.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class SiteUpsertComponent implements OnInit, CloeModalComponentInterface {
  override = true;
  modalTitle;
  metadata;
  statuses;
  site: any = {};
  siteChannels = [];

  domainPickerConfig = tagsDomainPickerConf;
  tags = [];
  tagPickerModels = [];

  constructor(
    private siteService: SiteService,
    private tagService: TagService,
    private commonService: CommonService,
    private nodeService: NodeService,
    public modalRef: NgbActiveModal
  ) {}

  ngOnInit() {
    this.commonService.getCommonStatus().subscribe(resp => {
      this.statuses = resp;
    });
  }

  onSubmit() {
    this.siteService.upsertSite(this.site).subscribe((resp: any) => {
      this.tagService.upsertTagList(this.tags, this.tagPickerModels, resp.uuid, NatureTypesEnum.SITE).subscribe(() => {
        this.modalRef.close({
          actionPostClose: 'RELOAD',
          data: resp
        });
      });
    });
  }

  initData(_site: any) {
    if (_site) {
      this.site = cloneDeep(_site);
      this.metadata = _site.metadata;
      this.tagService.getTagsByReferUuid(this.site.uuid).subscribe((res: any[]) => {
        this.tagPickerModels = res;
        this.tags = res;
      });
      this.nodeService.getNodesByReferUuidForUser(
        NatureTypesEnum.CHANNEL,
        StorageService.getCurrentUser().cloeUser.uuid,
        this.site.uuid
      ).subscribe( (resp: any[]) => {
        this.siteChannels = filter(resp, (channel) => {
          return channel.nodeCategory === 'LEAF';
        });
      });
    }
    this.modalTitle = this.site.id ? 'page.site.edit-site' : 'page.site.new-site';
  }

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      switchMap(term => {
        if (term.length < 3) {
          return of([]);
        } else {
          return this.commonService.getCityList(term);
        }
      })
    )

  cityConverter(city) {
    return city.name;
  }

}

