import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { cloneDeep } from 'lodash';
import {CloeModalComponentInterface} from '@cloe-core/@core/interfaces/cloe-modal-component.interface';
import {DeviceService} from '@cloe-core/@core/services/device-service/device-service';
import {CommonService} from '@cloe-core/@core/services/common-service/common-service';
import { NatureTypesEnum } from '@cloe-core/@core/enums/NatureTypesEnum';
import { tagsDomainPickerConf } from '@cloe-core/@core/@ui/components/cloe-domain-picker/standard-configurations/tags-conf';
import { TagService } from '@cloe-core/@core/services/tag-service/tag-service';

@Component({
  selector: 'cloe-core-upsert-device',
  templateUrl: './device-upsert.component.html',
  styleUrls: ['./device-upsert.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DeviceUpsertComponent implements OnInit, CloeModalComponentInterface {
  modalTitle;
  metadata;
  statuses;
  units;
  dTipes;
  device: any = {};

  domainPickerConfig = tagsDomainPickerConf;
  tags = [];
  tagPickerModels = [];

  constructor(
    private deviceService: DeviceService,
    private tagService: TagService,
    private commonService: CommonService,
    public modalRef: NgbActiveModal
  ) {}

  ngOnInit() {
    this.commonService.getCommonStatus().subscribe(resp => {
      this.statuses = resp;
    });

    this.commonService.getUnitList().subscribe(resp => {
      this.units = resp;
    });

    this.commonService.getDeviceTypes().subscribe(resp => {
      this.dTipes = resp;
    });
  }

  onSubmit() {
    this.deviceService.upsertDevice(this.device).subscribe((resp: any) => {
      this.tagService.upsertTagList(this.tags, this.tagPickerModels, resp.uuid, NatureTypesEnum.DEVICE).subscribe();
      this.modalRef.close('RELOAD');
    });
  }

  initData(_meta: any) {
    if (_meta) {
      this.device = cloneDeep(_meta);
      this.metadata = _meta.metadata;
      this.tagService.getTagsByReferUuid(this.device.uuid).subscribe((res: any[]) => {
        this.tagPickerModels = res;
        this.tags = res;
      });
    }
    this.modalTitle = this.device.id ? 'page.device.edit-device' : 'page.device.new-device';
  }

  changeInstallationDate($event) {
    this.device.installationDate = $event[0];
  }

  changeLastServiceDate($event) {
    this.device.lastServiceDate = $event[0];
  }
}

