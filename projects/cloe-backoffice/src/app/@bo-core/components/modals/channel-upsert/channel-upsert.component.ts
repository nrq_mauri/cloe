import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {CloeModalComponentInterface} from '@cloe-core/@core/interfaces/cloe-modal-component.interface';
import {ChannelService} from '@cloe-core/@core/services/channel-service/channel-service';
import {CommonService} from '@cloe-core/@core/services/common-service/common-service';
import { TagService } from '@cloe-core/@core/services/tag-service/tag-service';
import { tagsDomainPickerConf } from '@cloe-core/@core/@ui/components/cloe-domain-picker/standard-configurations/tags-conf';
import { NatureTypesEnum } from '@cloe-core/@core/enums/NatureTypesEnum';
import { cloneDeep, keys } from 'lodash';
import { PeriodTypeEnum } from '@cloe-core/@core/enums/PeriodTypeEnum';

@Component({
  selector: 'cloe-core-upsert-channel',
  templateUrl: './channel-upsert.component.html',
  styleUrls: ['./channel-upsert.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class ChannelUpsertComponent implements OnInit, CloeModalComponentInterface {
  modalTitle;
  metadata;
  statuses;
  devices;
  units;
  timeZone;
  channelTypes;
  channelValidities;
  channel: any = {};
  calculated = false;
  calculatedValid = false;

  domainPickerConfig = tagsDomainPickerConf;
  tags = [];
  tagPickerModels = [];

  constructor(
    private channelService: ChannelService,
    private tagService: TagService,
    private commonService: CommonService,
    public modalRef: NgbActiveModal
  ) {}

  ngOnInit() {
    this.commonService.getCommonStatus().subscribe(resp => {
      this.statuses = resp;
    });

    this.commonService.getDevices().subscribe(resp => {
      this.devices = resp;
    });

    this.commonService.getUnitList().subscribe(resp => {
      this.units = resp;
    });

    this.commonService.getChannelTypes().subscribe(resp => {
      this.channelTypes = resp;
    });

    this.commonService.getTimeZone().subscribe(resp => {
      this.timeZone = resp;
    });

    this.commonService.getChannelValidities().subscribe(resp => {
      this.channelValidities = resp;
    });
  }

  onSubmit() {
    this.channel.sampling = this._getPeriodMin(this.channel.chPeriodType);
    this.channelService.upsertChannel(this.channel).subscribe((resp: any) => {
      this.tagService.upsertTagList(this.tags, this.tagPickerModels, resp.uuid, NatureTypesEnum.CHANNEL).subscribe( () => {
        this.modalRef.close({
          actionPostClose: 'RELOAD',
          data: resp
        });
      });
    });
  }

  private _getPeriodMin(period: PeriodTypeEnum) {
    switch (period) {
      case PeriodTypeEnum.MIN:
        return 1;
      case PeriodTypeEnum.PENTA_MIN:
        return 5;
      case PeriodTypeEnum.DECA_MIN:
        return 10;
      case PeriodTypeEnum.QUARTER:
        return 15;
      case PeriodTypeEnum.HALF_HOUR:
        return 30;
      case PeriodTypeEnum.HOUR:
        return 60;
      default:
        return 15;
    }
  }

  initData(_meta: any) {
    if (_meta) {
      this.channel = _meta;
      this.calculated = !!_meta.calculatedChannel;
      this.metadata = _meta.metadata;
      this.tagService.getTagsByReferUuid(this.channel.uuid).subscribe((res: any[]) => {
        this.tagPickerModels = res;
        this.tags = res;
      });
    }
    this.modalTitle = this.channel ? 'page.channel.edit-channel' : 'page.channel.new-channel';
  }

  get calculatedChannel() {
    return cloneDeep(this.channel.calculatedChannel);
  }

  get periodTypeOptions() {
    return keys(PeriodTypeEnum);
  }

  onValidChannel(calculatedChannel: any) {
    setTimeout(() => {
      this.calculatedValid = true;
    }, 0);
    this.channel['calculatedChannel'] = calculatedChannel;
  }
}

