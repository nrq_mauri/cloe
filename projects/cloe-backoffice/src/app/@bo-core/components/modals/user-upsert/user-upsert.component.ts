import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {cloneDeep} from 'lodash';
import {User} from '@cloe-core/@core/models/User';
import {CloeModalComponentInterface} from '@cloe-core/@core/interfaces/cloe-modal-component.interface';
import {UserService} from '@cloe-core/@core/services/user-service/user-service';
import {CustomerService} from '@cloe-core/@core/services/customer-service/customer-service';
import {CommonService} from '@cloe-core/@core/services/common-service/common-service';
import { tagsDomainPickerConf } from '@cloe-core/@core/@ui/components/cloe-domain-picker/standard-configurations/tags-conf';
import { TagService } from '@cloe-core/@core/services/tag-service/tag-service';
import { NatureTypesEnum } from '@cloe-core/@core/enums/NatureTypesEnum';
@Component({
  selector: 'cloe-core-upsert-user',
  templateUrl: './user-upsert.component.html',
  styleUrls: ['./user-upsert.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class UserUpsertComponent implements OnInit, CloeModalComponentInterface {
  user: User = new User();
  modalTitle;
  authorities;
  customers;
  statuses;
  weekdays;
  uuid = '';
  id = '';

  domainPickerConfig = tagsDomainPickerConf;
  tags = [];
  tagPickerModels = [];
  validationErrors: string[];

  constructor(private userService: UserService,
              public translate: TranslateService,
              private tagService: TagService,
              public modalRef: NgbActiveModal,
              private customerService: CustomerService,
              private commonService: CommonService) {
  }

  ngOnInit() {
    this.commonService.getAuthorities().subscribe((res) => {
      this.authorities = res;
    });
    this.customerService.getAllCustomers().subscribe((res) => {
      this.customers = res;
    });
    this.commonService.getWeekDays().subscribe((res) => {
      this.weekdays = res;
    });
    this.commonService.getCommonStatus().subscribe((res) => {
      this.statuses = res;
    });
  }

  onSubmit() {
    this.userService.upsertUser(this.user).subscribe((resp: any) => {
      if (!resp.errors) {
        this.tagService.upsertTagList(this.tags, this.tagPickerModels, resp.data.cloeUser.uuid, NatureTypesEnum.CLOEUSER).subscribe();
        this.modalRef.close('RELOAD');
      } else {
        this.validationErrors = resp.errors;
      }
    });
  }

  initData(_user: User) {
    if (_user) {
      this.user = cloneDeep(_user);
      this.tagService.getTagsByReferUuid(this.user.cloeUser.uuid).subscribe((res: any[]) => {
        this.tagPickerModels = res;
        this.tags = res;
      });
    }
    this.modalTitle = !this.user.id ? 'page.user.new-user' : 'page.user.edit-user';
  }
}
