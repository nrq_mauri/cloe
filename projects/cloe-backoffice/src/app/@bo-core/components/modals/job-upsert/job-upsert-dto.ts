import { Job } from '@cloe-core/@core/models/Job';

export class JobUpsertDto {
  job: Job;
  jobConfigs: any;
}
