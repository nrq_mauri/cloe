import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {CloeModalComponentInterface} from '@cloe-core/@core/interfaces/cloe-modal-component.interface';
import {Job} from '@cloe-core/@core/models/Job';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { cloneDeep } from 'lodash';
import {JobService} from '@cloe-core/@core/services/job-service/job-service';
import { JobUpsertDto } from './job-upsert-dto';
import { WizardTab } from '@cloe-core/@core/@ui/components/cloe-wizard/models/wizard-tab';
import {
  JobConfigParamsStepComponent,
  JobConfigSelectionStepComponent,
  JobCronExpressionStepComponent,
  JobDestinationsStepComponent
} from '../../wizard-tabs';

@Component({
  selector: 'cloe-core-upsert-job',
  templateUrl: './job-upsert.component.html',
  styleUrls: ['./job-upsert.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class JobUpsertComponent implements OnInit, CloeModalComponentInterface {

  jobUpsertDTO: JobUpsertDto = {
    job: new Job(),
    jobConfigs: undefined
  };
  modalTitle;

  tabs: WizardTab[] = [
    {
      name: 'component.job-upsert.first-step',
      component: JobConfigSelectionStepComponent,
      data: this.jobUpsertDTO
    },
    {
      name: 'component.job-upsert.second-step',
      component: JobConfigParamsStepComponent,
      data: this.jobUpsertDTO
    },
    {
      name: 'component.job-upsert.third-step',
      component: JobCronExpressionStepComponent,
      data: this.jobUpsertDTO
    },
    {
      name: 'component.job-upsert.fourth-step',
      component: JobDestinationsStepComponent,
      data: this.jobUpsertDTO
    }
  ];

  constructor(
    private jobService: JobService,
    public modalRef: NgbActiveModal
  ) {}

  ngOnInit() {
    this.jobService.getAllJobConfigs().subscribe((res) => {
      this.jobUpsertDTO.jobConfigs = res;
    });
  }

  onSubmit() {
    this.jobUpsertDTO.job.jobConfig.jobConfigParams = undefined;
    this.jobService.upsertJob(this.jobUpsertDTO.job).subscribe(resp => {
      this.modalRef.close('RELOAD');
    });
  }

  initData(_job: Job) {
    if (_job) {
      this.jobUpsertDTO.job = cloneDeep(_job);
    }
    this.modalTitle = !this.jobUpsertDTO.job.id ? 'page.job.new-job' : 'page.job.edit-job';
  }

}
