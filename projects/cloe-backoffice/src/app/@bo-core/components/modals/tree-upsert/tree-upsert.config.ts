import {FilterTypeEnum} from '@cloe-core/@core/enums/FilterTypeEnum';

export const TEMP_TREE_FILTER_CONFIG = [
  {
    filterType: FilterTypeEnum.TEXT,
    filterField: 'name',
    fieldLabel: 'global.name'
  }];
