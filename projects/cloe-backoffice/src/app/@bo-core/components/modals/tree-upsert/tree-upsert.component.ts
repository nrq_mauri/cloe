import {AfterViewInit, Component, OnInit, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {ITreeOptions, TreeNode, TreeModel, TREE_ACTIONS} from 'angular-tree-component';
import { cloneDeep, omit } from 'lodash';
import {TEMP_TREE_FILTER_CONFIG} from './tree-upsert.config';
import {CloeModalComponentInterface} from '@cloe-core/@core/interfaces/cloe-modal-component.interface';
import {CommonService} from '@cloe-core/@core/services/common-service/common-service';
import {CloeItemsByNatureService} from '@cloe-core/@core/services/cloe-itemsByNatureExceptUUID-service/cloe-itemsByNature.service';
import {NodeCategoryEnum} from '@cloe-core/@core/enums/NodeCategoryEnum';
import { CloeTreeNodeService } from '@cloe-core/@core/services/cloe-treeNode-service/cloe-tree-node.service';
import { StorageService } from '@cloe-core/@core/services/storage-service/storage.service';

@Component({
  selector: 'cloe-core-upsert-tree',
  templateUrl: './tree-upsert.component.html',
  styleUrls: ['./tree-upsert.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TreeUpsertComponent implements OnInit, CloeModalComponentInterface, AfterViewInit {
  tree: any = {};
  treeNodes = [];
  tempNodes = [];
  removableNodes = [NodeCategoryEnum.LEAF, NodeCategoryEnum.GROUP];
  tempTreeFilterConfig = TEMP_TREE_FILTER_CONFIG;
  modalTitle;
  options: ITreeOptions = {
    allowDrag: (node: any) => node.data.nodeCategory !== 'ROOT',
    allowDrop: true,
    actionMapping: {
      mouse: {
        drop: (tree: TreeModel, node: TreeNode, $event: any, {from , to}: {from: any, to: any}) => {
          if (from.treeModel.virtualRoot.data.id === to.parent.treeModel.virtualRoot.data.id) {
            tree.moveNode(from, to);
          } else {
            this.treeTempRef.treeModel.activeNodes.forEach(n => {
              tree.copyNode(n, to);
            });
          }
        }
      }
    },
    getNodeClone: (node: any) => {
      const newNode: any = cloneDeep(node.data);
      newNode.isNew = true;
      return newNode;
    }
  };
  optionsTempTree: ITreeOptions = {
    allowDrag: (node: any) => node.data.nodeCategory !== 'ROOT',
    allowDrop: false,
    actionMapping: {
      mouse: {
        click: (tree, node, $event) => {
          $event.ctrlKey
            ? TREE_ACTIONS.TOGGLE_ACTIVE_MULTI(tree, node, $event)
            : TREE_ACTIONS.TOGGLE_ACTIVE(tree, node, $event);
        },
        dragStart: (tree, node, $event) => {
          if (tree.activeNodes.length < 2) {
            TREE_ACTIONS.ACTIVATE(tree, node, $event);
          }
        }
      }
    }
  };

  tempTreeConfig = {
    nodeLabelFunction: (node: any) => {
      return `${node.data.referUuid} - ${node.data.name}`;
    },
    treeTitle: 'page.tree.tempNodes'
  };

  statuses;
  natureTypes;
  treeTempRef;

  currentTree;

  constructor(
    private treeService: CloeTreeNodeService,
    private commonService: CommonService,
    private itemsByNature: CloeItemsByNatureService,
    public modalRef: NgbActiveModal
  ) {
  }

  ngOnInit() {
    this.commonService.getCommonStatus().subscribe(resp => {
      this.statuses = resp;
    });

    this.commonService.getNatureTypes('TREE').subscribe(resp => {
      this.natureTypes = resp;
    });
  }

  onSubmit() {
    const treeUsers = [];
    this.treeService.saveTreeNodes(this.treeNodes, this.tree, treeUsers, []).subscribe(() => {
      this.modalRef.close('RELOAD');
    });
  }

  initData(_tree: any) {
    if (_tree) {
      this.tree = cloneDeep(_tree);
    }
    this.modalTitle = this.tree.id ? 'page.tree.edit-tree' : 'page.tree.new-tree';
  }

  onNatureSelected() {
    if (this.currentTree) {
      this.loadTreeData(this.currentTree);
    }
  }

  loadTreeData(tree) {
    this.currentTree = tree;
    this.treeNodes = [];
    if (this.tree.uuid) {
      this.treeService.getAllTreeNodeFromTree(
        this.tree.nodeNature,
        this.tree.uuid,
        StorageService.getCurrentUser().cloeUser.uuid,
        true
      )
        .subscribe(result => {
          this.treeNodes.push(result);
          tree.treeModel.update();
          tree.treeModel.expandAll();

          this.loadTempTreeData();
        });
    } else {
      this._addNodeToCurrentTree('ROOT', 'ROOT');
      this.loadTempTreeData();
      tree.treeModel.update();
    }
  }

  loadTempTreeData() {
    this.tempNodes = [];
    this.itemsByNature.searchByNature(this.tree.nodeNature).subscribe((res) => {
      Object.entries(res).forEach(
        ([key, value]) => {
          const node = {
            name: res[key].name,
            status: 'ACTIVE',
            nodeCategory: 'LEAF',
            referUuid: res[key].uuid,
            nodeNature: this.tree.nodeNature,
            children: []
          };
          this.tempNodes.push(node);
        }
      );
      this.treeTempRef.treeModel.update();
    });
  }

  private _addNodeToCurrentTree(name, catergoryNode) {
    const node = {
      name: name.toUpperCase(),
      nodeCategory: catergoryNode,
      status: 'ACTIVE',
      isNew: true,
      nodeNature: this.tree.nodeNature,
      children: []
    };
    if (name === 'ROOT') {
      this.treeNodes.unshift(node);
    } else {
      this.treeNodes[0].children.unshift(node);
    }
  }

  initTempTree(tree) {
    this.treeTempRef = tree;
  }

  changeValidationDate($event) {
    this.tree.validStartDate = $event[0];
    this.tree.validEndDate = $event[1];
  }

  ngAfterViewInit(): void {

  }
}
