import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { CloeModalComponentInterface } from '@cloe-core/@core/interfaces/cloe-modal-component.interface';
import { NatureTypesEnum } from '@cloe-core/@core/enums/NatureTypesEnum';
import { flatMap, forEach, uniqBy, cloneDeep, set, get, map } from 'lodash';
import { CloeTreeComponent } from '@cloe-core/@core/@ui/components';
import { NodeCategoryEnum } from '@cloe-core/@core/enums/NodeCategoryEnum';
import {
  CloeItemsByNatureService
} from '@cloe-core/@core/services/cloe-itemsByNatureExceptUUID-service/cloe-itemsByNature.service';
import { ChannelRangeTypeEnum } from '@cloe-core/@core/enums/ChannelRangeTypeEnum';
import { TranslateService } from '@ngx-translate/core';
import { ITreeNode, ITreeOptions } from 'angular-tree-component/dist/defs/api';
import { TREE_ACTIONS, TreeModel, TreeNode } from 'angular-tree-component';
import { ChannelRangeService } from '@cloe-core/@core/services/channel-range-service/channel-range-service';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ChannelService } from '@cloe-core/@core/services/channel-service/channel-service';

@Component({
  selector: 'cloe-core-channel-ranges-combine',
  templateUrl: './channel-ranges-combine.component.html',
  styleUrls: ['./channel-ranges-combine.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ChannelRangesCombineComponent implements OnInit, CloeModalComponentInterface {
  @ViewChild('combinedRatesTree') combinedRatesTreeRef: CloeTreeComponent;
  @ViewChild('combinedProfilesTree') combinedProfilesTreeRef: CloeTreeComponent;
  @ViewChild('ratesToCombineTree') ratesToCombineTreeRef: CloeTreeComponent;
  @ViewChild('profilesToCombineTree') profilesToCombineTreeRef: CloeTreeComponent;
  selectedRatesNodes = [
    {
      name: this.translateService.instant('component.channel-range-combine.combined-rates'),
      nodeCategory: NodeCategoryEnum.ROOT,
      rangeType: ChannelRangeTypeEnum.RATE,
      children: []
    }
  ];
  selectedProfilesNodes = [
    {
      name: this.translateService.instant('component.channel-range-combine.combined-profiles'),
      nodeCategory: NodeCategoryEnum.ROOT,
      rangeType: ChannelRangeTypeEnum.PROFILE,
      children: []
    }
  ];
  ratesToCombineNodes = [
    {
      name: this.translateService.instant('component.channel-range-combine.rates-to-combine'),
      nodeCategory: NodeCategoryEnum.ROOT,
      children: []
    }
  ];
  profilesToCombineNodes = [
    {
      name: this.translateService.instant('component.channel-range-combine.profiles-to-combine'),
      nodeCategory: NodeCategoryEnum.ROOT,
      children: []
    }
  ];

  rangesNodeConfig = {
    removableNodes: (node: ITreeNode) => node.isLeaf
  };

  combinedRangesTreesOptions: ITreeOptions = {
    allowDrag: false,
    allowDrop: (element, {parent, index}) =>
      parent.data.nodeCategory === NodeCategoryEnum.ROOT && parent.data.rangeType === element.data.rangeType,
    actionMapping: {
      mouse: {
        drop: (tree: TreeModel, node: TreeNode, $event: any, {from, to}: { from: any, to: any }) => {
          if (from.data.rangeType === ChannelRangeTypeEnum.RATE) {
            this.ratesToCombineTreeRef.tree.treeModel.activeNodes.forEach(n => {
              n.setIsHidden(true);
              tree.copyNode(n, to);
            });
          } else {
            this.profilesToCombineTreeRef.tree.treeModel.activeNodes.forEach(n => {
              n.setIsHidden(true);
              tree.copyNode(n, to);
            });
          }
        }
      }
    },
    getNodeClone: (node: any) => {
      const newNode: any = cloneDeep(node.data);
      newNode.isNew = true;
      return newNode;
    }
  };

  rangesToCombineTreesOptions: ITreeOptions = {
    allowDrag: (element) => element.isLeaf,
    allowDrop: false,
    actionMapping: {
      mouse: {
        click: (tree, node, $event) => {
          $event.ctrlKey ? TREE_ACTIONS.TOGGLE_ACTIVE_MULTI(tree, node, $event)
            : TREE_ACTIONS.TOGGLE_ACTIVE(tree, node, $event);
        },
        // drag single node without activating it first
        dragStart: (tree, node, $event) => {
          if (tree.activeNodes.length < 2) {
            tree.setActiveNode(node, false, false);
            if (node.data.nodeCategory === 'GROUP') {
              this.recursiveActiveLeaves(tree, node, $event);
            } else {
              TREE_ACTIONS.ACTIVATE(tree, node, $event);
            }
          }
        }
      }
    }
  };

  modalTitle;
  selectedChannels = [];

  rangesAlgorithmMap = {};
  activeRange;

  constructor(
    private searchItems: CloeItemsByNatureService,
    private channelRangeService: ChannelRangeService,
    private translateService: TranslateService,
    private channelService: ChannelService,
    public modalRef: NgbActiveModal
  ) {
  }

  ngOnInit() {
    Object.keys(ChannelRangeTypeEnum).forEach((key) => set(this.rangesAlgorithmMap, key, {}));
  }

  initData(data?: any) {
    this.modalTitle = 'component.channel-range-combine.combine-ranges';
    if (data) {
      this.selectedChannels = data.selectedChannels;
      this.searchItems.searchByNatureWithFilters(
        NatureTypesEnum.CHANNEL_RANGE,
        {}
      ).subscribe((res) => {
        this.buildRangesNodes(res);
      });
    }
  }

  private buildRangesNodes(ranges) {
    this.buildCompleteRangeList(ranges);
    this.buildSelectedRangeList();
    this.hidePresentRanges();
  }

  private hidePresentRanges() {
    this.selectedProfilesNodes[0].children.forEach((selectedProfile) => {
      const profile = this.profilesToCombineTreeRef.tree.treeModel.getNodeBy((treeNode) => {
        return treeNode.data.uuid === selectedProfile.uuid;
      });
      if (profile) {
        profile.setIsHidden(true);
      }
    });
    this.selectedRatesNodes[0].children.forEach((selectedRate) => {
      const rate = this.ratesToCombineTreeRef.tree.treeModel.getNodeBy((treeNode) => {
        return treeNode.data.uuid === selectedRate.uuid;
      });
      if (rate) {
        rate.setIsHidden(true);
      }
    });
  }

  private buildCompleteRangeList(ranges) {
    ranges.forEach((range) => {
      range.rangeType === ChannelRangeTypeEnum.RATE ?
        this.ratesToCombineNodes[0].children.push(range) : this.profilesToCombineNodes[0].children.push(range);
    });
    this.ratesToCombineTreeRef.tree.treeModel.update();
    this.profilesToCombineTreeRef.tree.treeModel.update();
  }

  private buildSelectedRangeList() {
    forEach(
      uniqBy(
        flatMap(this.selectedChannels, (channelNode) => channelNode.data.referObject.channelRanges),
        (range) => range.uuid
      ),
      (rangeElement) =>
        rangeElement.rangeType === ChannelRangeTypeEnum.RATE ?
          this.selectedRatesNodes[0].children.push(rangeElement) : this.selectedProfilesNodes[0].children.push(rangeElement)
    );
    this.combinedRatesTreeRef.tree.treeModel.update();
    this.combinedProfilesTreeRef.tree.treeModel.update();
  }

  onNodeActivated($event: any) {
    const node = $event.node;
    const algorithm = get(this.rangesAlgorithmMap, node.data.rangeType + '.' + node.data.uuid);
    if (!algorithm) {
      set(
        this.rangesAlgorithmMap,
        node.data.rangeType + '.' + node.data.uuid,
        {
          algorithm: this.channelRangeService.createAlgorithm(node.data),
          range: node.data
        }
      );
    }
    this.activeRange = node.data.rangeType + '.' + node.data.uuid;
  }

  onNodeRemoved($event: any) {
    if ($event.data.rangeType === ChannelRangeTypeEnum.RATE) {
      this.ratesToCombineTreeRef.tree.treeModel.getNodeBy((node) => {
        return node.data.uuid === $event.data.uuid;
      }).setIsHidden(false);
    } else {
      this.profilesToCombineTreeRef.tree.treeModel.getNodeBy((node) => {
        return node.data.uuid === $event.data.uuid;
      }).setIsHidden(false);
    }
  }

  private recursiveActiveLeaves(tree, node, $event) {
    if (node.data.nodeCategory === 'LEAF') {
      TREE_ACTIONS.TOGGLE_ACTIVE_MULTI(tree, node, $event);
    }
    node.children.forEach(child => {
      this.recursiveActiveLeaves(tree, child, $event);
    });
  }

  getActiveRange() {
    return get(this.rangesAlgorithmMap, this.activeRange);
  }

  saveChannelsRange() {
    this.channelService.upsertChannelList(
      map(
        flatMap(this.selectedChannels, (channelNode) => channelNode.data.referObject),
        (channel) => {
          const newChannel = channel;
          newChannel.channelRanges = [...this.selectedRatesNodes[0].children, ...this.selectedProfilesNodes[0].children];
          return newChannel;
        }
      )
    ).subscribe(() => this.modalRef.close({ actionPostClose: 'RELOAD' }));
  }
}

