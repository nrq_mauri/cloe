import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { cloneDeep } from 'lodash';
import {CloeModalComponentInterface} from '@cloe-core/@core/interfaces/cloe-modal-component.interface';
import {UnitService} from '@cloe-core/@core/services/unit-service/unit.service';
import {CommonService} from '@cloe-core/@core/services/common-service/common-service';

@Component({
  selector: 'cloe-core-upsert-unit',
  templateUrl: './unit-upsert.component.html',
  styleUrls: ['./unit-upsert.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class UnitUpsertComponent implements OnInit, CloeModalComponentInterface {
  override = true;
  modalTitle;
  metadata;
  statuses;
  unit: any = {};

  constructor(
    private unitService: UnitService,
    private commonService: CommonService,
    private modalRef: NgbActiveModal
  ) {}

  ngOnInit() {
    this.commonService.getCommonStatus().subscribe(resp => {
      this.statuses = resp;
    });
  }

  onSubmit() {
    this.unitService.upsertUnit(this.unit).subscribe(resp => {
      this.modalRef.close('RELOAD');
    });
  }

  initData(_unit: any) {
    if (_unit) {
      this.unit = cloneDeep(_unit);
      this.metadata = _unit.metadata;
    }
    this.modalTitle = this.unit.id ? 'page.unit.edit-unit' : 'page.unit.new-unit';
  }
}

