import { ACTIONS } from '@cloe-core/@core/enums/ActionsEnum';
import { Subject } from 'rxjs';
import {ICloeTableStatic} from '@cloe-core/@core/@ui/components/cloe-table-static/cloe-table-static-interface/cloe-table-static-interface';

export const metadataManagementSubject$ = new Subject();

export const METADATA_TABLE_CONFIG: ICloeTableStatic = {
  header: [
    {
      fieldName: 'metadataType',
      columnName: 'page.metadata.metadata-type',
    },
    {
      fieldName: 'metadataKey',
      columnName: 'global.key'
    },
    {
      fieldName: 'metadataValue',
      columnName: 'global.value'
    }
  ],
  actions: [
    {
      icon: 'edit',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: metadataManagementSubject$,
        action: ACTIONS.EDIT_ROW,
        altMessage: 'global.edit'
      }
    },
    {
      icon: 'delete',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: metadataManagementSubject$,
        action: ACTIONS.DELETE,
        altMessage: 'global.delete'
      }
    }
  ],
  metadata: {
    tableTitle: 'page.metadata.tableTitle',
    sort: 'id',
    sortType: 'asc'
  }
};

export const METADATA_TO_DELETE_TABLE_CONFIG: ICloeTableStatic = {
  header: [
    {
      fieldName: 'metadataType',
      columnName: 'page.metadata.metadata-type',
    },
    {
      fieldName: 'metadataKey',
      columnName: 'global.key'
    },
    {
      fieldName: 'metadataValue',
      columnName: 'global.value'
    }
  ],
  actions: [
    {
      icon: 'settings_backup_restore',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: metadataManagementSubject$,
        action: ACTIONS.RESTORE_ROW,
      }
    }
  ],
  metadata: {
    tableTitle: 'page.metadata.removed_metadata',
    sort: 'id',
    sortType: 'asc'
  }
};
