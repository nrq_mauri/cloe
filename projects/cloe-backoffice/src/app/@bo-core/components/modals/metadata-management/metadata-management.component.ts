import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {
  METADATA_TABLE_CONFIG,
  METADATA_TO_DELETE_TABLE_CONFIG,
  metadataManagementSubject$
} from './metadata-management-table.config';
import { CloeModalComponentInterface } from '@cloe-core/@core/interfaces/cloe-modal-component.interface';
import { MetadataService } from '@cloe-core/@core/services/metadata-service/metadata-service';
import { CloeSearchService } from '@cloe-core/@core/services/cloe-search-service/cloe.search.service';
import { NatureTypesEnum } from '@cloe-core/@core/enums/NatureTypesEnum';
import { cloneDeep, findIndex, remove } from 'lodash';
import { PAGINATION } from '@cloe-core/@core/@ui/components/cloe-table-static/configuration/cloe-table-static-conf';
import { ACTIONS } from '@cloe-core/@core/enums/ActionsEnum';
import { CommonService } from '@cloe-core/@core/services/common-service/common-service';
import { CloeItemsByNatureService } from '@cloe-core/@core/services/cloe-itemsByNatureExceptUUID-service/cloe-itemsByNature.service';

@Component({
  selector: 'cloe-core-metadata-management',
  templateUrl: './metadata-management.component.html',
  styleUrls: ['./metadata-management.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class MetadataManagementComponent implements OnInit, CloeModalComponentInterface {
  modalTitle;
  metadataTypeList;
  nodeNatureList;
  metadataModel: any = {};
  baseMetadata;
  tableConfig = METADATA_TABLE_CONFIG;
  deleteTableConfig = METADATA_TO_DELETE_TABLE_CONFIG;
  metadataList = [];
  metadataToDeleteList = [];
  upsertMetadataTitle;
  metadataSubjectSubsction;

  domainPickerConfig = {
    labelSearch: 'global.key',
    itemAsObject: {
      identifyBy: 'metadataKey',
      displayBy: 'metadataKey',
      nodeNature: NatureTypesEnum.METADATA,
      fieldToSearch: {
        metadataKey: ''
      },
      autocompleteItems: true
    },
    maxItems: 1
  };
  metadataKeyPickerModel;

  constructor(
    private metadataService: MetadataService,
    private commonService: CommonService,
    private searchService: CloeItemsByNatureService,
    public modalRef: NgbActiveModal
  ) {
    this.metadataSubjectSubsction = metadataManagementSubject$.asObservable().subscribe((event: any) => {
      switch (event.action) {
        case ACTIONS.DELETE:
          this.addMetadataToList(this.metadataToDeleteList, event.data);
          this.removeMetadataFromList(this.metadataList, event.data);
          this.metadataToDeleteList = [...this.metadataToDeleteList];
          this.metadataList = [...this.metadataList];
          break;
        case ACTIONS.EDIT_ROW:
          this.metadataModel = event.data;
          this.metadataKeyPickerModel = [event.data];
          this.upsertMetadataTitle = 'page.metadata.edit_metadata';
          break;
        case ACTIONS.RESTORE_ROW:
          this.removeMetadataFromList(this.metadataToDeleteList, event.data);
          this.addMetadataToList(this.metadataList, event.data);
          this.metadataToDeleteList = [...this.metadataToDeleteList];
          this.metadataList = [...this.metadataList];
          break;
      }
    });
  }

  ngOnInit() {
    this.metadataService.getMetadataTypeEnum().subscribe(resp => {
      this.metadataTypeList = resp;
    });
    this.commonService.getNodeNatureEnum().subscribe(resp => {
      this.nodeNatureList = resp;
    });
  }

  initData(_meta: any) {
    this.modalTitle = 'page.metadata.tableTitle';
    this.baseMetadata = cloneDeep(_meta);
    this.initMetadata();
    this.loadMetadata();
  }

  initMetadata() {
    this.metadataModel = {};
    this.metadataModel.referUuid = this.baseMetadata.uuid;
    this.metadataModel.nodeNature = this.baseMetadata.natureType;
  }

  addMetadataToList(list: any[], metadata: any) {
    let index;
    index = metadata.id ?
      findIndex(list, (element) => {
        return element.id === metadata.id;
      }) :
      findIndex(list, metadata);
    if (index === -1) {
      list.push(metadata);
    } else {
      list.splice(index, 1, metadata);
    }
  }

  removeMetadataFromList(list: any[], metadata) {
    remove(list, (element) => {
      if (element.id) {
        return element.id === metadata.id;
      } else {
        return element.metadataKey === metadata.metadataKey && element.metadataValue === metadata.metadataValue;
      }
    });
  }

  loadMetadata() {
    this.searchService.searchByNatureWithFilters(
      NatureTypesEnum.METADATA,
      {
        referUuid: this.metadataModel.referUuid
      }
    ).subscribe((res: any) => {
      this.metadataList = res;
    });
  }

  newMetadata() {
    this.initMetadata();
    this.upsertMetadataTitle = 'page.metadata.new_metadata';
  }

  onSubmit() {
    this.metadataModel.metadataKey = this.metadataKeyPickerModel[0].metadataKey;
    this.addMetadataToList(this.metadataList, this.metadataModel);
    this.metadataList = [...this.metadataList];
    this.initMetadata();
    this.upsertMetadataTitle = undefined;
  }

  onSave() {
    this.metadataService.upsertMetadataList(this.metadataList, this.metadataToDeleteList).subscribe((res) => {
      this.modalRef.close('RELOAD');
    });
  }
}
