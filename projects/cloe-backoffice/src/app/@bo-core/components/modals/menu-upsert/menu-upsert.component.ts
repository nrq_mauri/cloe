import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {cloneDeep} from 'lodash';
import {CommonService} from '@cloe-core/@core/services/common-service/common-service';
import {MenuService} from '@cloe-core/@core/services/menu-service/menu.service';
import {Menu} from '@cloe-core/@core/models/Menu';
import {CloeModalComponentInterface} from '@cloe-core/@core/interfaces/cloe-modal-component.interface';

@Component({
  selector: 'cloe-core-upsert-menu',
  templateUrl: './menu-upsert.component.html',
  styleUrls: ['./menu-upsert.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class MenuUpsertComponent implements OnInit, CloeModalComponentInterface {
  menu: Menu = new Menu();
  statuses;
  modalTitle;

  constructor(
    private menuService: MenuService,
    private commonService: CommonService,
    public modalRef: NgbActiveModal
  ) {
  }

  ngOnInit() {
    this.commonService.getCommonStatus().subscribe(resp => {
      this.statuses = resp;
    });

  }

  onSubmit() {
    this.menuService.upsertMenu(this.menu).subscribe(resp => {
      this.modalRef.close('RELOAD');
    });
  }

  initData(_menu: Menu) {
    if (_menu) {
      this.menu = cloneDeep(_menu);
    }
    this.modalTitle = !this.menu.id ? 'page.menu.new-menu' : 'page.menu.edit-menu';
  }
}
