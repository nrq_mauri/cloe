import { Component, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { CloeModalComponentInterface } from '@cloe-core/@core/interfaces/cloe-modal-component.interface';
import { NatureTypesEnum } from '@cloe-core/@core/enums/NatureTypesEnum';
import { StorageService } from '@cloe-core/@core/services/storage-service/storage.service';
import { CloeTreeNodeService } from '@cloe-core/@core/services/cloe-treeNode-service/cloe-tree-node.service';
import { ChannelService } from '@cloe-core/@core/services/channel-service/channel-service';
import { ITreeOptions, TREE_ACTIONS, TreeModel, TreeNode } from 'angular-tree-component';
import { cloneDeep, map, remove } from 'lodash';
import { CloeTreeComponent } from '@cloe-core/@core/@ui/components';
import { ITreeNode } from 'angular-tree-component/dist/defs/api';
import { StatusEnum } from '@cloe-core/@core/enums/StatusEnum';
import { NodeCategoryEnum } from '@cloe-core/@core/enums/NodeCategoryEnum';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {
  CloeItemsByNatureService
} from '@cloe-core/@core/services/cloe-itemsByNatureExceptUUID-service/cloe-itemsByNature.service';
import { forkJoin } from 'rxjs';

@Component({
  selector: 'cloe-core-channel-combine',
  templateUrl: './channel-combine.component.html',
  styleUrls: ['./channel-combine.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ChannelCombineComponent implements OnInit, CloeModalComponentInterface {
  @ViewChild('channelTree') channelTreeRef: CloeTreeComponent;
  @ViewChild('channelNotInNodeTree') channelNodInNodeTreeRef: CloeTreeComponent;
  @ViewChild('extChannelsTree') extChannelsTreeRef: CloeTreeComponent;
  modalTitle;
  channelNodes: any = [{name: 'ROOT', nodeCategory: 'ROOT', children: []}];
  channelNotCombinedNodes = [];
  channelsNotInNodeTreeOptions: ITreeOptions = {
    allowDrag: (node: any) => node.data.nodeCategory !== 'ROOT',
    allowDrop: false,
    actionMapping: {
      mouse: {
        click: (tree, node, $event) => {
          $event.ctrlKey ? TREE_ACTIONS.TOGGLE_ACTIVE_MULTI(tree, node, $event)
            : TREE_ACTIONS.TOGGLE_ACTIVE(tree, node, $event);
        },
        // drag single node without activating it first
        dragStart: (tree, node, $event) => {
          if (tree.activeNodes.length < 2) {
            tree.setActiveNode(node, false, false);
            if (node.data.nodeCategory === 'GROUP') {
              this.recursiveActiveLeaves(tree, node, $event);
            } else {
              TREE_ACTIONS.ACTIVATE(tree, node, $event);
            }
          }
        }
      }
    }
  };

  extChannelNodes = [];

  channelTreeOptions: ITreeOptions = {
    allowDrag: false,
    allowDrop: (element, {parent, index}) => parent.data.nodeCategory !== NodeCategoryEnum.ROOT,
    actionMapping: {
      mouse: {
        drop: (tree: TreeModel, node: TreeNode, $event: any, {from, to}: { from: any, to: any }) => {
          if (from.path[0] === to.parent.path[0]) {
            tree.moveNode(from, to);
          } else {
            if (!from.data.referObject.extChannelType || from.data.referObject.extChannelType === '') {
              this.channelNodInNodeTreeRef.tree.treeModel.activeNodes.forEach(n => {
                n.setIsHidden(true);
                tree.copyNode(n, to);
              });
            } else {
              this.extChannelsTreeRef.tree.treeModel.activeNodes.forEach(n => {
                if (
                  !tree.getNodeBy((treeNode) =>
                    treeNode.data.referObject && treeNode.data.referObject.uuid === n.data.referObject.uuid,
                    tree.getNodeById(to.parent.path[1])
                  )
                ) {
                  tree.copyNode(n, to);
                }
              });
            }
          }
        }
      }
    },
    getNodeClone: (node: any) => {
      const newNode: any = cloneDeep(node.data);
      delete newNode.id;
      newNode.isNew = true;
      return newNode;
    }
  };
  channelNodesConfig = {
    removableNodes: (node: ITreeNode) => node.data.isNew
  };

  channelTree;

  constructor(
    private treeNodeService: CloeTreeNodeService,
    private channelService: ChannelService,
    private searchItems: CloeItemsByNatureService,
    public modalRef: NgbActiveModal
  ) {
  }

  ngOnInit() {
    this.searchItems.searchByNatureWithFilters(
      NatureTypesEnum.TREE,
      {
        nodeNature: NatureTypesEnum.CHANNEL,
        isCluster: false
      }
    ).subscribe((res) => {
      this.channelTree = res[0];
    });
  }

  initData(data?: any) {
    if (data) {
      this.modalTitle = 'page.channel-site-tree.sites-tree-actions.combine-channels';
      this.loadChannelTreNodes(data.selectedSites);
      this.loadChannelNotInNodes();
      this.loadExtChannelNodes();
    }
  }

  loadChannelNotInNodes() {
    this.channelService.getChannelsNotInNode().subscribe((res: any) => {
      this.channelNotCombinedNodes = [this.createNodesFromObjects(res)];
      this.channelNodInNodeTreeRef.tree.treeModel.update();
    });
  }

  loadExtChannelNodes() {
    this.channelService.getAllExternalChannels().subscribe((res: any) => {
      this.extChannelNodes = [this.createNodesFromObjects(res)];
      this.extChannelsTreeRef.tree.treeModel.update();
    });
  }

  createNodesFromObjects(objects) {
    return {
      name: 'ROOT',
      nodeCategory: 'ROOT',
      children: map(objects, (channel) => {
        return {
          name: channel.name,
          status: StatusEnum.ACTIVE,
          nodeCategory: NodeCategoryEnum.LEAF,
          nodeNature: NatureTypesEnum.CHANNEL,
          referUuid: channel.uuid,
          referObject: channel
        };
      })
    };
  }

  loadChannelTreNodes(sites) {
    const observableChannelTreeNodeArray = [];
    sites.forEach((site) => observableChannelTreeNodeArray.push(
      this.treeNodeService.getTreeNodeByReferUuid(
        NatureTypesEnum.CHANNEL,
        StorageService.getCurrentUser(),
        false, // TODO aggiungere user object per i channels
        site.data.referUuid
      ))
    );
    forkJoin(observableChannelTreeNodeArray).subscribe((resp) => {
      resp.forEach((res, index) => {
        if (res) {
          this.channelNodes[0].children.push(res);
        } else {
          this.channelNodes[0].children.push({
            name: sites[index].referObject.name,
            status: StatusEnum.ACTIVE,
            nodeCategory: NodeCategoryEnum.GROUP,
            rootReferUuid: sites[index].referUuid,
            children: []
          });
        }
      });
      this.channelTreeRef.tree.treeModel.update();
    });
  }

  recursiveActiveLeaves(tree, node, $event) {
    if (node.data.nodeCategory === 'LEAF') {
      TREE_ACTIONS.TOGGLE_ACTIVE_MULTI(tree, node, $event);
    }
    node.children.forEach(child => {
      this.recursiveActiveLeaves(tree, child, $event);
    });
  }

  onNodeRemoved($event: any) {
    if (!$event.data.referObject.extChannelType || $event.data.referObject.extChannelType === '') {
      this.channelNodInNodeTreeRef.tree.treeModel.getNodeBy((node) => {
        return !!node.data.referObject && node.data.referObject.uuid === $event.data.referObject.uuid;
      }).setIsHidden(false);
    }
  }

  saveTree() {
    this.treeNodeService.saveTreeNodesList(
      map(this.channelNodes[0].children, (node) => {
        const treeNode: any = {};
        treeNode.treeNodes = [node];
        treeNode.referUuid = node.rootReferUuid;
        treeNode.nodeIdsToDelete = [];
        return treeNode;
      }),
      [],
      this.channelTree
    ).subscribe(() => {
      this.modalRef.close({actionPostClose: 'RELOAD'});
    });
  }
}

