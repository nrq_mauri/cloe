import {Component, OnInit, ViewEncapsulation} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { cloneDeep } from 'lodash';
import {CommonService} from '@cloe-core/@core/services/common-service/common-service';
import {CloeThemeTemplateService} from '@cloe-core/@core/services/cloe-themeTemplate-service/cloe-themeTemplate.service';
import {CloeModalComponentInterface} from '@cloe-core/@core/interfaces/cloe-modal-component.interface';

@Component({
  selector: 'cloe-core-theme-template-upsert',
  templateUrl: './theme-template-upsert.component.html',
  styleUrls: ['./theme-template-upsert.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ThemeTemplateUpsertComponent implements OnInit, CloeModalComponentInterface {
  template: any = {};
  modalTitle;
  statuses;

  constructor(
    private themeTemplateService: CloeThemeTemplateService,
    private commonService: CommonService,
    public modalRef: NgbActiveModal
  ) {
  }

  ngOnInit() {
    this.commonService.getCommonStatus().subscribe(resp => {
      this.statuses = resp;
    });
  }

  onSubmit() {
    this.themeTemplateService.saveThemeTemplate(this.template).subscribe(() => {
      this.modalRef.close('RELOAD');
    });
  }

  initData(_template: any) {
    if (_template) {
      this.template = cloneDeep(_template);
    }
    this.modalTitle = !this.template.uuid ? 'page.themeTemplate.templateUpsert' : 'page.themeTemplate.editTemplate';
  }
}
