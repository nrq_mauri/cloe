import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { cloneDeep, difference, filter, find, flatMap, isNil } from 'lodash';
import { CloeModalComponentInterface } from '@cloe-core/@core/interfaces/cloe-modal-component.interface';
import { CloeItemsByNatureService } from '@cloe-core/@core/services/cloe-itemsByNatureExceptUUID-service/cloe-itemsByNature.service';
import { NatureTypesEnum } from '@cloe-core/@core/enums/NatureTypesEnum';
import { forkJoin } from 'rxjs';
import { PageHelpService } from '@cloe-core/@core/services/page-help-service/page-help-service';
import { StatusEnum } from '@cloe-core/@core/enums/StatusEnum';
import { CommonService } from '@cloe-core/@core/services/common-service/common-service';

@Component({
  selector: 'cloe-upsert-page-help',
  templateUrl: './page-help-upsert.component.html',
  styleUrls: ['./page-help-upsert.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PageHelpUpsertComponent implements OnInit, CloeModalComponentInterface {
  modalTitle;
  pageHelp: any = {
    status: StatusEnum.ACTIVE
  };
  pageStateList = [];
  selectablePageStateList = [];
  statuses = [];

  constructor(
    public searchItems: CloeItemsByNatureService,
    public pageHelpService: PageHelpService,
    public commonService: CommonService,
    public modalRef: NgbActiveModal
  ) {
  }

  ngOnInit() {
    forkJoin(
      this.searchItems.searchByNature(NatureTypesEnum.PAGE_HELP_ONLINE),
      this.searchItems.searchByNature(NatureTypesEnum.MENU),
      this.commonService.getCommonStatus()
    ).subscribe((resps: any[]) => {
      this.pageStateList = resps[1];
      this.selectablePageStateList = filter(
        resps[1],
        (menu) => {
          return difference(flatMap(resps[1], 'ngState'), flatMap(resps[0], 'pageState')).includes(menu.ngState);
        }
      );
      this.statuses = resps[2];
    });
  }

  onSubmit() {
    this.pageHelpService.pageHelpUpsert(this.pageHelp).subscribe((res) => {
      if (res) {
        this.modalRef.close('RELOAD');
      }
    });
  }

  initData(_pageHelp) {
    if (_pageHelp) {
      this.pageHelp = cloneDeep(_pageHelp);
    }
    this.modalTitle = this.pageHelp.id ? 'page.help-online.edit-help-online' : 'page.help-online.new-help-online';
  }

  onFileSelected(files: any) {
    if (files && files.length) {
      const fileToRead = files[0];
      const fileReader = new FileReader();
      fileReader.onload = (e: any) => this.pageHelp.htmlHelpText = e.target.result;
      fileReader.readAsText(fileToRead, 'UTF-8');
    }
  }

  isEditMode() {
    return !isNil(this.pageHelp.id);
  }

  findPageStateByNgState(ngState) {
    return find(this.pageStateList, (state) => state.ngState === ngState) || {};
  }
}
