import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import {cloneDeep} from 'lodash';
import {CloeModalComponentInterface} from '@cloe-core/@core/interfaces/cloe-modal-component.interface';
import {Customer} from '@cloe-core/@core/models/Customer';
import {CustomerService} from '@cloe-core/@core/services/customer-service/customer-service';
import {CommonService} from '@cloe-core/@core/services/common-service/common-service';
import {FileService} from '@cloe-core/@core/services/file-service/file-service';
import {CloeThemeTemplateService} from '@cloe-core/@core/services/cloe-themeTemplate-service/cloe-themeTemplate.service';

@Component({
  selector: 'cloe-core-upsert-customer',
  templateUrl: './customer-upsert.component.html',
  styleUrls: ['./customer-upsert.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class CustomerUpsertComponent implements OnInit, CloeModalComponentInterface {
  customer: Customer = new Customer();
  uploadedLogo: any;
  statuses;
  modalTitle;
  themeTemplates;

  constructor(
    private customerService: CustomerService,
    private commonService: CommonService,
    private fileService: FileService,
    private themeTemplateService: CloeThemeTemplateService,
    public modalRef: NgbActiveModal
  ) {
  }

  ngOnInit() {
    this.commonService.getCommonStatus().subscribe(resp => {
      this.statuses = resp;
    });
    this.themeTemplateService.getThemeTemplates().subscribe(result => {
      this.themeTemplates = result;
      this.themeTemplates = [...this.themeTemplates, {id: null, name: 'Energy Team'}];
    });
  }
  initData(_customer: Customer) {
    if (_customer) {
      this.customer = cloneDeep(_customer);
    }
    if (this.customer.personalLogo) {
      this.loadLogo();
    }
    this.modalTitle = !this.customer.id ? 'page.customer.new-customer' : 'page.customer.edit-customer';
  }

  onSubmit() {
    this.customerService.upsertCustomer(this.customer).subscribe(resp => {
      this.modalRef.close('RELOAD');
    });
  }

  onFileSelected(event) {
    this.customer.personalLogo = event.uuid;
    this.loadLogo();
  }

  loadLogo() {
    const file: any = {};
    file.uuid = this.customer.personalLogo;
    this.fileService.downloadOrViewFile(file, 'VIEW').subscribe(res => {
      this.createImageFromBlob(res);
    });
  }
  createImageFromBlob(image) {
    const reader = new FileReader();
    reader.addEventListener('load', () => {
      this.uploadedLogo = reader.result;
    }, false);

    if (image) {
      reader.readAsDataURL(image);
    }
  }
}
