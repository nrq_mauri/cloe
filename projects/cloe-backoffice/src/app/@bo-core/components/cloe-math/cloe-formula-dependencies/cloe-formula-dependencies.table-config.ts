import {
  FormulaButtonAction,
  MathArgumentTypeEnum
} from '@cloe-core/@core/services/cloe-math-service/math.interfaces';
import { ACTIONS } from '@cloe-core/@core/enums/ActionsEnum';
import { formulaButtonSubject$ } from '@cloe-core/@core/services/cloe-math-service/cloe.math.service';
import {ICloeTableStatic} from '@cloe-core/@core/@ui/components/cloe-table-static/cloe-table-static-interface/cloe-table-static-interface';

export const CLOE_FORMULA_TABLE_CONFIG: ICloeTableStatic = {
  header: [
    {
      fieldName: 'name',
      columnName: 'global.name'
    },
    {
      fieldName: 'argumentType',
      columnName: 'global.type',
      i18n: {
        prefix: 'component.cloe-math.arg-type.',
        suffix: ''
      }
    },
    {
      fieldName: 'origin', // not used
      columnName: 'global.value',
      cellFormatter: (row: any) => {
        switch (row.argumentType) {
          case MathArgumentTypeEnum.FUNCTION:
            return row.definition;
          case MathArgumentTypeEnum.CHANNEL:
          case MathArgumentTypeEnum.CONSTANT:
            return row.value;
        }
      }
    }
  ],
  actions: [
    {
      icon: 'edit',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: formulaButtonSubject$,
        action: FormulaButtonAction.EDIT,
        altMessage: 'global.edit'
      }
    },
    {
      icon: 'delete',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: formulaButtonSubject$,
        action: FormulaButtonAction.REMOVE,
        altMessage: 'global.delete'
      }
    }
  ],
  metadata: {
    tableTitle: '',
    sort: 'name',
    sortType: 'asc',
    disablePagination: true,
    fixPageSize: 50
  },
};
