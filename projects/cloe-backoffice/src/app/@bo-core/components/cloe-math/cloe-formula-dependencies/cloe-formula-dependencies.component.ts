import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import { values, flatMap, map } from 'lodash';
import { CLOE_FORMULA_TABLE_CONFIG } from './cloe-formula-dependencies.table-config';
import {
  FormulaButtonAction,
  MathArgumentTypeEnum
} from '@cloe-core/@core/services/cloe-math-service/math.interfaces';
import {
  CloeMathService,
  formulaButtonSubject$
} from '@cloe-core/@core/services/cloe-math-service/cloe.math.service';

@Component({
  selector: 'cloe-formula-dependencies',
  templateUrl: './cloe-formula-dependencies.component.html',
  styleUrls: ['./cloe-formula-dependencies.component.scss']
})
export class CloeFormulaDependenciesComponent implements OnInit {

  @Input()
  argumentsObject;

  @Input()
  origin;

  @Input() title;

  @Input() hideConstant;
  @Input() hideFunction;
  @Input() hideChannel;

  tableConfig = CLOE_FORMULA_TABLE_CONFIG;

  constructor(private mathService: CloeMathService) { }

  ngOnInit() {
  }

  get arguments() {
    return flatMap(map(values(this.argumentsObject), values));
  }

  addNew(argumentType: MathArgumentTypeEnum) {
    formulaButtonSubject$.next({
      action: FormulaButtonAction.ADD_NEW,
      data: {
        argumentType,
        origin : this.origin,
        formModel: this.mathService.getFormModel(argumentType),
      }
    });
  }

}
