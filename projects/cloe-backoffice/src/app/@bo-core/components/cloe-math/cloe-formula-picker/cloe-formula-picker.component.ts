import { Component, Input, OnDestroy, OnInit, QueryList, ViewChildren, ViewEncapsulation } from '@angular/core';
import {
  CloeMathService,
  formulaBehaviorSubject$, formulaButtonSubject$,
} from '@cloe-core/@core/services/cloe-math-service/cloe.math.service';
import { keys } from 'lodash';
import {
  FormulaButtonAction, IFormulaPickerGroup,
  IFormulaPickerState,
  MathArgumentTypeEnum
} from '@cloe-core/@core/services/cloe-math-service/math.interfaces';
import { forkJoin } from 'rxjs';
import { filter } from 'lodash';

@Component({
  selector: 'cloe-formula-picker',
  templateUrl: './cloe-formula-picker.component.html',
  styleUrls: ['./cloe-formula-picker.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloeFormulaPickerComponent implements OnInit, OnDestroy {

  state: IFormulaPickerState;
  filter = {};

  @Input()
  origin: string;

  @Input()
  groupsFor: Array<string>;

  formulaBehaviorSubscription;

  constructor(private mathService: CloeMathService) {
  this.formulaBehaviorSubscription = formulaBehaviorSubject$.asObservable().subscribe(state => {
      this.state = state;
    });
  }

  ngOnInit() {
    if (!this.state.fetched) {

      forkJoin(
        this.mathService.getFunctions(),
        this.mathService.getArguments()
      ).subscribe((resp: Array<any>) => {
        // load defined formulas
        this._addGroupToState('defined-formulas', MathArgumentTypeEnum.FUNCTION, resp[0]);

        // load defined constant
        this._addGroupToState('defined-constants',
          MathArgumentTypeEnum.CONSTANT,
          filter(resp[1], { argumentType: MathArgumentTypeEnum.CONSTANT })
        );
        this.state.fetched = true;
        formulaBehaviorSubject$.next(this.state);
      });
    }
  }

  private _addGroupToState(name: string, argumentType: MathArgumentTypeEnum, items: Array<any>) {
    this.state.groups[name] = {
      name,
      argumentType,
      items,
      addButton: true,
      editable: true,
    };
  }

  get groups() {
    return this.groupsFor || keys(this.state.groups);
  }

  ngOnDestroy(): void {
    this.formulaBehaviorSubscription.unsubscribe();
  }

}
