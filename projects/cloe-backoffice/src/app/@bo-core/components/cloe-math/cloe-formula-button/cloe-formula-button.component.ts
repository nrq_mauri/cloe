import {
  AfterContentChecked,
  AfterViewInit,
  Component,
  Input,
  OnDestroy,
  OnInit,
  ViewEncapsulation
} from '@angular/core';
import {
  CloeMathService,
  formulaButtonSubject$
} from '@cloe-core/@core/services/cloe-math-service/cloe.math.service';
import { FormulaButtonAction } from '@cloe-core/@core/services/cloe-math-service/math.interfaces';

@Component({
  selector: 'cloe-formula-button',
  templateUrl: './cloe-formula-button.component.html',
  styleUrls: ['./cloe-formula-button.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloeFormulaButtonComponent {

  @Input()
  editable: boolean;

  @Input()
  formula: any;

  @Input()
  origin: string;

  constructor(private mathService: CloeMathService) { }

  insertFormula() {
    this._sendFormulaEvent(FormulaButtonAction.INSERT);
  }

  editFormula(e) {
    e.stopPropagation();
    this._sendFormulaEvent(FormulaButtonAction.EDIT);
  }

  private _sendFormulaEvent(formulaAction: FormulaButtonAction) {
    this.formula.origin = this.origin;
    this.formula.formModel = this.mathService.getFormModel(this.formula.argumentType);
    formulaButtonSubject$.next({
      action: formulaAction,
      data: this.formula
    });
  }

}
