import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
  ViewEncapsulation
} from '@angular/core';
import {
  CloeMathService,
  formulaBehaviorSubject$, formulaButtonSubject$
} from '@cloe-core/@core/services/cloe-math-service/cloe.math.service';
import {
  FormulaButtonAction,
  IFormulaEvent,
  MathArgumentTypeEnum
} from '@cloe-core/@core/services/cloe-math-service/math.interfaces';
import { PeriodTypeEnum } from '@cloe-core/@core/enums/PeriodTypeEnum';
import { CloeTreeComponent } from '@cloe-core/@core/@ui/components';
import { CloeTreeFiltersService } from '@cloe-core/@core/services/cloe-tree-service/cloe-tree-filters.service';
import { StorageService } from '@cloe-core/@core/services/storage-service/storage.service';
import { NatureTypesEnum } from '@cloe-core/@core/enums/NatureTypesEnum';
import { TreeFiltersTypeEnum } from '@cloe-core/@core/services/cloe-tree-service/enums/TreeFiltersTypeEnum';
import { CloeTreeNodeService } from '@cloe-core/@core/services/cloe-treeNode-service/cloe-tree-node.service';
import { keys, cloneDeep, values, find, extend, isEqual, forEach } from 'lodash';

const initFormModel = {
  'form-insert': { arguments: [], functions: []},
  'form-formula-insert': { open: false, arguments: []},
  'form-constant-insert': { open: false },
  'form-channel-insert': { open: false }
};

@Component({
  selector: 'cloe-calculated-channel',
  templateUrl: './cloe-calculated-channel.component.html',
  styleUrls: ['./cloe-calculated-channel.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class CloeCalculatedChannelComponent implements OnInit, OnDestroy {
  formulaButtonSubscription;

  @ViewChild('cloeTree')
  cloeTree: CloeTreeComponent;

  formModel: any = cloneDeep(initFormModel);

  @Input() channelCalculated;

  @Output() valid = new EventEmitter<any>();

  validated = false;

  mathArguments = {
    'form-insert': {
      [MathArgumentTypeEnum.FUNCTION]: {},
      [MathArgumentTypeEnum.CONSTANT]: {},
      [MathArgumentTypeEnum.CHANNEL]: {}
    },
    'form-formula-insert': {
      [MathArgumentTypeEnum.CONSTANT]: {},
    }
  };

  syntaxErrors = {
    'form-insert': {},
    'form-formula-insert': {}
  };

  groupsForChannelInsert = ['constants', 'defined-constants'];

  nodes;
  filtersConfig;

  units;

  domainPickerConfig = {
    itemAsObject: {
      identifyBy: 'id',
      displayBy: 'name',
      nodeNature: NatureTypesEnum.UNIT,
      fieldToSearch: 'name',
    },
    maxItems: 1
  };

  constructor(
    private mathService: CloeMathService,
    private cloeTreeService: CloeTreeNodeService,
    private treeFiltersService: CloeTreeFiltersService
  ) {
    this.formulaButtonSubscription = formulaButtonSubject$.asObservable().subscribe( resp => {
      this._handleFormulaEvent(resp);
    });
  }

  private _handleFormulaEvent(event: IFormulaEvent) {
    let textAreaField = 'definition';
    if ('form-insert' === event.data.origin) {
      textAreaField = 'expression';
    }
    const data = event.data;
    switch (event.action) {
      case FormulaButtonAction.ADD_NEW:
      case FormulaButtonAction.EDIT:
        // this.formModel = cloneDeep(initFormModel);
        this.formModel[data.formModel] = {
          open: true,
          ...data
        };
        if (data.formModel === 'form-channel-insert') {
          setTimeout(() => {
            const channelId = this.formModel[data.formModel].channelId;
            if (channelId) {
              const node = this.cloeTree.tree.treeModel.getNodeBy(
                n => {
                  return n.data.referObject && n.data.referObject.id === channelId;
                }
              );
              if (node) {
                node.setActiveAndVisible();
              }
            } else {
              this.cloeTree.tree.treeModel.getFirstRoot().expand();
            }
            this.cloeTree.tree.sizeChanged();
          }, 0);
        }
        break;
      case FormulaButtonAction.INSERT:
        this._insertArgument(event);
        break;
      case FormulaButtonAction.REMOVE:
        const stringValue = this.formModel[data.origin][textAreaField] || '';
        this.formModel[data.origin][textAreaField] = stringValue.replace(new RegExp(event.data.name, 'g'), '').trim();
        delete this.mathArguments[data.origin][data.argumentType][data.name];
        break;
    }
  }

  private _insertArgument(event) {
    const data = event.data;
    let textAreaField = 'definition';
    if ('form-insert' === data.origin) {
      textAreaField = 'expression';
    }
    this.formModel[data.origin][textAreaField] = (this.formModel[data.origin][textAreaField] || '') + ' ' + data.name;
    if (data.argumentType) {
      const secondForm = data.origin === 'form-insert' ? 'form-formula-insert' : 'form-insert';
      this.mathArguments[data.origin][data.argumentType][data.name] = { ...data };
      if (
        this.mathArguments[secondForm] &&
        this.mathArguments[secondForm][data.argumentType] &&
        this.mathArguments[secondForm][data.argumentType][data.name]
      ) {
        this.mathArguments[secondForm][data.argumentType][data.name] = {
          ...data,
          origin: secondForm
        };
      }
    }
  }

  ngOnDestroy(): void {
    this.formulaButtonSubscription.unsubscribe();
  }

  closeForm(origin) {
    this.formModel[origin] = { ...initFormModel[origin] };
  }

  get periodTypeOptions() {
    return keys(PeriodTypeEnum);
  }

  createCalculatedChannel() {
    const formModel = this.formModel['form-insert'];
    formModel.arguments = [
      ...values(this.mathArguments['form-insert'].CONSTANT),
      ...values(this.mathArguments['form-insert'].CHANNEL)
    ];
    formModel.functions = [
      ...values(this.mathArguments['form-insert'].FUNCTION)
    ];
    this.mathService.checkExpression(formModel).subscribe((resp: any) => {
      if (resp.success) {
        this.syntaxErrors['form-insert'] = {};
        if (formModel.startDateMoment) {
          formModel.startDate = formModel.startDateMoment.toISOString();
        }
        this.valid.emit(formModel);
        this.validated = true;
      } else {
        this.syntaxErrors['form-insert'] =  this._createExpressionHtml(
          formModel.expression,
          resp.errors[0]
        );
      }
    });
  }

  createNewFunction() {
    const functionModel = this.formModel['form-formula-insert'];
    functionModel.argumentType = MathArgumentTypeEnum.FUNCTION;
    functionModel.arguments = [
      ...values(this.mathArguments['form-formula-insert'].CONSTANT)
    ];
    this.mathService.checkFunction(functionModel).subscribe((resp: any) => {
      if (resp.success) {
        this._insertArgument({
          origin: functionModel.origin,
          data: functionModel
        });
        this.syntaxErrors['form-formula-insert'] = {};
        const currentState = formulaBehaviorSubject$.value;

        const functionItems = currentState.groups['defined-formulas'].items;
        const functionArg = find(functionItems, { name: functionModel.name});

        if (functionArg) {
          extend(functionArg, functionModel);
        } else {
          currentState.groups['defined-formulas'].items = [
            ...functionItems,
            functionModel
          ];
        }

        formulaBehaviorSubject$.next(currentState);
        this.mathArguments['form-formula-insert'].CONSTANT = {};
        this.closeForm('form-formula-insert');
      } else {
        this.syntaxErrors['form-formula-insert'] =  this._createExpressionHtml(
          this.formModel['form-formula-insert'].definition,
          resp.errors[0]
        );
      }
    });

  }

  private _createExpressionHtml(expression: string, errors: any) {
    const chars = expression.split('');
    const tokens = errors['TOKENS'];
    const syntaxIndexes = errors['SYNTAX'];
    const result: any = {};
    if (tokens && tokens.length) {
      result.tokens = true;
      result.message = tokens.map(char => `<b class="text-danger">${char}</b>`).join(', ');
    }

    if (syntaxIndexes && syntaxIndexes.length) {
      result.syntax = true;
      syntaxIndexes.forEach( index => {
        chars.splice(index, 1, `<b class="text-danger">${chars[index]}</b>`);
      });
      result.message = chars.join('');
    }
    return result;
  }

  createNewConstant() {
    const constantModel = this.formModel['form-constant-insert'];
    constantModel.argumentType = MathArgumentTypeEnum.CONSTANT;
    this._insertArgument({
      origin: constantModel.origin,
      data: constantModel
    });

    const currentState = formulaBehaviorSubject$.value;

    const constantItems = currentState.groups['defined-constants'].items;
    const constant = find(constantItems, { name: constantModel.name});
    if (constant) {
      extend(constant, constantModel);
    } else {
      currentState.groups['defined-constants'].items = [
        ...constantItems,
        constantModel
      ];
    }

    formulaBehaviorSubject$.next(currentState);
    this.closeForm('form-constant-insert');
  }

  createNewChannel() {
    const channelModel = this.formModel['form-channel-insert'];
    const channel = channelModel.node.referObject;
    channelModel.argumentType = MathArgumentTypeEnum.CHANNEL;
    channelModel.channelId = channel.id;
    channelModel.timeZoneCode = channel.timeZoneCode;
    channelModel.value = `${channel.id} - ${channel.name} (${channel.unitSymbol})`;
    this._insertArgument({
      origin: channelModel.origin,
      data: channelModel
    });
    this.closeForm('form-channel-insert');
  }

  onNodeActive(evt: any) {
    const node = evt.node.data;
    const channelModel = this.formModel['form-channel-insert'];
    channelModel.node = node;
  }

  ngOnInit(): void {
    if (this.channelCalculated) {
      this.formModel['form-insert'] = this.channelCalculated;
      this.validated = true;
      forEach(this.channelCalculated.arguments, argument => {
        const { argumentType, name } = argument;
        argument.formModel = this.mathService.getFormModel(argumentType);
        argument.origin = 'form-insert';
        this.mathArguments['form-insert'][argumentType][name] = argument;
      });
      forEach(this.channelCalculated.functions, fun => {
        const { name } = fun;
        fun.formModel = this.mathService.getFormModel(MathArgumentTypeEnum.FUNCTION);
        fun.origin = 'form-insert';
        this.mathArguments['form-insert'][MathArgumentTypeEnum.FUNCTION][name] = fun;
      });
      this.valid.emit(this.channelCalculated);
    }
    this.cloeTreeService.getAllTreeNodeFromTree(
      NatureTypesEnum.CHANNEL,
      'f4ea9646-30c8-478d-94c3-bbc5eb26a61f',
      StorageService.getCurrentUser().cloeUser.uuid,
      true,
      true
    ).subscribe(r => {
        this.nodes = [r];
        this.filtersConfig = this.treeFiltersService.addFiltersByType(
          TreeFiltersTypeEnum.CHANNEL_FILTERS,
          this.nodes
        );
      }
    );
  }

}
