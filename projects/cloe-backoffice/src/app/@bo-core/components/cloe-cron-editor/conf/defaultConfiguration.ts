import { CronOptions } from '../CronOptions';

export const DEFAULT_CRON_EDITOR_CONF: CronOptions  = {
  formInputClass: 'form-control cron-editor-input',
  formSelectClass: 'form-control cron-editor-select',
  formRadioClass: 'cron-editor-radio',
  formCheckboxClass: 'cron-editor-checkbox',

  defaultTime: '00:00:00',

  hideMinutesTab: false,
  hideHourlyTab: false,
  hideDailyTab: false,
  hideWeeklyTab: false,
  hideMonthlyTab: false,
  hideYearlyTab: false,
  hideAdvancedTab: false,
  hideSpecificWeekDayTab : false,
  hideSpecificMonthWeekTab : false,

  use24HourTime: true,
  hideSeconds: false,

  currentExpressionLabel: true,

  cronFlavor: 'quartz'
};
