import {
  ChannelRangesRatesComponent,
  DailyProfilesComponent,
  JobConfigParamsStepComponent,
  JobConfigSelectionStepComponent,
  JobCronExpressionStepComponent,
  JobDestinationsStepComponent,
  SpecialProfilesComponent,
  WeeklyProfilesComponent,
} from './wizard-tabs';

export const WIZARD_TABS_COMPONENTS = [
  JobConfigSelectionStepComponent,
  JobConfigParamsStepComponent,
  JobCronExpressionStepComponent,
  JobDestinationsStepComponent,
  ChannelRangesRatesComponent,
  DailyProfilesComponent,
  WeeklyProfilesComponent,
  SpecialProfilesComponent,
];
