import { ACTIONS } from '@cloe-core/@core/enums/ActionsEnum';
import { Subject } from 'rxjs';
import {
  ICloeTableStatic
} from '@cloe-core/@core/@ui/components/cloe-table-static/cloe-table-static-interface/cloe-table-static-interface';

export const weeklyProfileSubject$ = new Subject();

export const TABLE_CONFIG: ICloeTableStatic = {
  header: [
    {
      fieldName: 'day',
      columnName: 'global.day'
    },
    {
      fieldName: 'dailyProfile',
      columnName: 'component.channel-range-upsert.daily-profile'
    }
  ],
  actions: [
    {
      icon: 'edit',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: weeklyProfileSubject$,
        action: ACTIONS.EDIT_ROW
      }
    }
  ],
  metadata: {
    tableTitle: 'component.channel-range-upsert.weekly-profiles',
    disableSort: true
  }
};
