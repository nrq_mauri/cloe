import {
  REQUIRED_VALIDATOR
} from '@cloe-core/@core/@ui/components/cloe-dynamic-form/models/validators/validators';
import { TextboxQuestion } from '@cloe-core/@core/@ui/components/cloe-dynamic-form/models/TextboxQuestion';
import {
  ColorPickerQuestion
} from '@cloe-core/@core/@ui/components/cloe-dynamic-form/models/ColorPickerQuestion';

export const getRatesForm = (rate) => {
  return [
    new TextboxQuestion({
      key: 'name',
      label: 'global.name',
      value: rate.name,
      type: 'text',
      validators: [ REQUIRED_VALIDATOR ],
      order: 1
    }),
    new ColorPickerQuestion({
      key: 'color',
      label: 'global.color',
      value: rate.color,
      type: 'text',
      validators: [ REQUIRED_VALIDATOR ],
      order: 2
    })
  ];
};
