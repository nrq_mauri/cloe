import { ACTIONS } from '@cloe-core/@core/enums/ActionsEnum';
import { Subject } from 'rxjs';
import {ICloeTableStatic} from '@cloe-core/@core/@ui/components/cloe-table-static/cloe-table-static-interface/cloe-table-static-interface';

export const dailyProfileSubject$ = new Subject();

export const TABLE_CONFIG: ICloeTableStatic = {
  header: [
    {
      fieldName: 'id',
      columnName: 'Id'
    }
  ],
  actions: [
    {
      icon: 'visibility',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: dailyProfileSubject$,
        action: ACTIONS.SHOW_ROW
      }
    },
    {
      icon: 'delete',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: dailyProfileSubject$,
        action: ACTIONS.DELETE,
        deleteByField: 'name',
        altMessage: 'global.delete'
      }
    }
  ],
  metadata: {
    tableTitle: 'component.channel-range-upsert.daily-profiles',
    addNewButton: {
      actionType: ACTIONS.TRIGGER_SUBJECT,
      action: ACTIONS.NEW_ROW,
      triggerSubject: dailyProfileSubject$
    }
  }
};
