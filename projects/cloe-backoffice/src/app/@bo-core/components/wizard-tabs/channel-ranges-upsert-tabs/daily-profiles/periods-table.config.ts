import { ACTIONS } from '@cloe-core/@core/enums/ActionsEnum';
import { Subject } from 'rxjs';
import { ChannelRangeTypeEnum } from '@cloe-core/@core/enums/ChannelRangeTypeEnum';
import {ICloeTableStatic} from '@cloe-core/@core/@ui/components/cloe-table-static/cloe-table-static-interface/cloe-table-static-interface';

export const periodsTableSubject$ = new Subject();

export const getPeriodsTableConfig = (channelRange): ICloeTableStatic => {
  return {
    header: [
      {
        fieldName: 'startPeriod',
        columnName: 'global.from'
      },
      {
        fieldName: 'endPeriod',
        columnName: 'global.to'
      },
      {
        fieldName: channelRange.rangeType === ChannelRangeTypeEnum.RATE ? 'rateId' : 'profileValue',
        columnName: channelRange.rangeType === ChannelRangeTypeEnum.RATE ?
          'component.channel-range-upsert.rate-id' : 'component.channel-range-upsert.profile-value'
      }
    ],
    actions: [
      {
        icon: 'delete',
        actionType: ACTIONS.TRIGGER_SUBJECT,
        metadata: {
          triggerSubject: periodsTableSubject$,
          action: ACTIONS.DELETE,
          deleteByField: 'name',
          altMessage: 'global.delete'
        }
      }
    ],
    metadata: {
      tableTitle: 'component.channel-range-upsert.periods',
      addNewButton: {
        actionType: ACTIONS.TRIGGER_SUBJECT,
        action: ACTIONS.NEW_ROW,
        triggerSubject: periodsTableSubject$
      }
    }
  };
};
