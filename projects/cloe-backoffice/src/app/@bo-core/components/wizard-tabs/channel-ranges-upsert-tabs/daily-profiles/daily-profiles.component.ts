import { ChangeDetectorRef, Component, Input, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { CloeWizardComponentInterface } from '@cloe-core/@core/interfaces/cloe-wizard-component.interface';
import { set, extend, cloneDeep, remove, difference, isNull, filter, range, map } from 'lodash';
import { dailyProfileSubject$, TABLE_CONFIG } from './daily-profiles-table.config';
import { CloeDynamicFormComponent } from '@cloe-core/@core/@ui/components';
import { ACTIONS } from '@cloe-core/@core/enums/ActionsEnum';
import { getPeriodsTableConfig, periodsTableSubject$ } from './periods-table.config';
import * as moment_ from 'moment';
import { ChannelRangeTypeEnum } from '@cloe-core/@core/enums/ChannelRangeTypeEnum';
import { getPeriodForm } from './daily-profiles-form-costant';

const moment = moment_;

@Component({
  selector: 'cloe-channel-range-daily-profiles',
  templateUrl: './daily-profiles.component.html',
  styleUrls: ['./daily-profiles.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class DailyProfilesComponent implements OnInit, OnDestroy, CloeWizardComponentInterface {
  @ViewChild(CloeDynamicFormComponent) dailyProfileForm: CloeDynamicFormComponent;
  @Input() data;
  dailyProfiles = [];
  currentProfile;
  tableConfig = TABLE_CONFIG;
  showPeriodsDynamicForm = false;
  periodQuestions = [];
  periodsTableConfig;
  dailyTableSubscription;
  periodsTableSubscription;

  constructor(
    private cdRef: ChangeDetectorRef
  ) {
    this.dailyTableSubscription = dailyProfileSubject$.asObservable().subscribe((event: any) => {
      switch (event.action) {
        case ACTIONS.NEW_ROW:
          const avaiableIDs = filter(
            difference(range(1, this.dailyProfiles.length + 2), map(this.dailyProfiles, (profile) => profile.id)),
            (idElement) => !isNull(idElement) && !isNaN(idElement)
          );
          this.dailyProfiles.push({ id: avaiableIDs[0], periods: [] });
          break;
        case ACTIONS.SHOW_ROW:
          this.currentProfile = event.data;
          break;
        case ACTIONS.DELETE:
          remove(this.dailyProfiles, event.data);
          this.currentProfile = undefined;
          break;
      }
    });
    this.periodsTableSubscription = periodsTableSubject$.asObservable().subscribe((event: any) => {
      switch (event.action) {
        case ACTIONS.NEW_ROW:
          this.showPeriodsDynamicForm = true;
          this.periodQuestions = getPeriodForm(this.data, {});
          this.cdRef.detectChanges();
          break;
        case ACTIONS.DELETE:
          remove(this.currentProfile.periods, event.data);
          break;
      }
    });
  }

  ngOnInit() {
    if (!this.data.jsonAlgorithm || !this.data.jsonAlgorithm.dailyProfiles) {
      set(this.data, 'jsonAlgorithm.dailyProfiles', []);
    }
    this.dailyProfiles = this.data.jsonAlgorithm.dailyProfiles;
    this.periodsTableConfig = getPeriodsTableConfig(this.data);
  }

  get isValid() {
    return !this.dailyProfileForm &&
      this.data.jsonAlgorithm &&
      (this.data.jsonAlgorithm.dailyProfiles && this.data.jsonAlgorithm.dailyProfiles.length > 0);
  }

  onPeriodSubmitted($event: any) {
    let period = {
      startPeriod: moment($event.hoursProfile.fromDate, 'HH:mm:ss').format('HH:mm'),
      endPeriod: moment($event.hoursProfile.toDate, 'HH:mm:ss').format('HH:mm')
    };
    if (this.data.rangeType === ChannelRangeTypeEnum.RATE) {
      period = extend(period, { rateId: $event.rateId });
    } else {
      period = extend(period, { profileValue: $event.profileValue });
    }
    this.currentProfile.periods.push(period);
    this.showPeriodsDynamicForm = false;
    this.cdRef.detectChanges();
  }

  onPeriodCancelled($event: any) {
    this.showPeriodsDynamicForm = false;
    this.cdRef.detectChanges();
  }

  ngOnDestroy() {
    this.dailyTableSubscription.unsubscribe();
    this.periodsTableSubscription.unsubscribe();
  }
}
