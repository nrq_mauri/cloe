import * as moment_ from 'moment';
import { map } from 'lodash';
import {
  DatepickerQuestion
} from '@cloe-core/@core/@ui/components/cloe-dynamic-form/models/DatepickerQuestion';
import {
  DATE_VALIDATOR,
  REQUIRED_VALIDATOR
} from '@cloe-core/@core/@ui/components/cloe-dynamic-form/models/validators/validators';
import {
  DateTimeTypeEnum
} from '@cloe-core/@core/@ui/components/cloe-date-time-picker/enums/DateTimeTypeEnum';
import { ChannelRangeTypeEnum } from '@cloe-core/@core/enums/ChannelRangeTypeEnum';
import { DropdownQuestion } from '@cloe-core/@core/@ui/components/cloe-dynamic-form/models/DropdownQuestion';
import { TextboxQuestion } from '@cloe-core/@core/@ui/components/cloe-dynamic-form/models/TextboxQuestion';

const moment = moment_;

export const getPeriodForm = (channelRange, period) => {
  const questions: any[] = [
    new DatepickerQuestion({
      key: 'hoursProfile',
      label: 'component.channel-range-upsert.hours-profile',
      value: [ moment(period.startPeriod, 'HH:mm:ss'), moment(period.endPeriod, 'HH:mm:ss') ],
      validators: [ REQUIRED_VALIDATOR, DATE_VALIDATOR ],
      order: 1,
      config: {
        isRange: true,
        placeholder: 'component.channel-range-upsert.hours-profile',
        inputType: DateTimeTypeEnum.TIMER
      }
    })
  ];
  if (channelRange.rangeType === ChannelRangeTypeEnum.RATE) {
    questions.push(
      new DropdownQuestion({
        key: 'rateId',
        label: 'component.channel-range-upsert.rate-id',
        value: period.rateId,
        validators: [ REQUIRED_VALIDATOR ],
        options: map(channelRange.jsonAlgorithm.rates, (rate) => ({key: rate.name, value: rate.id })),
        order: 2
      })
    );
  } else {
    questions.push(
      new TextboxQuestion({
        key: 'profileValue',
        label: 'component.channel-range-upsert.profile-value',
        value: period.profileValue,
        type: 'text',
        validators: [ REQUIRED_VALIDATOR ],
        order: 1
      })
    );
  }
  return questions;
};
