import {
  DATE_VALIDATOR,
  REQUIRED_VALIDATOR
} from '@cloe-core/@core/@ui/components/cloe-dynamic-form/models/validators/validators';
import {
  DatepickerQuestion
} from '@cloe-core/@core/@ui/components/cloe-dynamic-form/models/DatepickerQuestion';
import {
  DateTimeTypeEnum
} from '@cloe-core/@core/@ui/components/cloe-date-time-picker/enums/DateTimeTypeEnum';
import * as _moment from 'moment';
import { DropdownQuestion } from '@cloe-core/@core/@ui/components/cloe-dynamic-form/models/DropdownQuestion';
import { map } from 'lodash';

const moment = _moment;

export const getSpecialProfileForm = (channelRange, profile, translateService) => {
  return [
    new DatepickerQuestion({
      key: 'period',
      label: 'component.date-time-picker.select-date',
      value: moment(profile.startPeriod, 'YYYY-MM-DD'),
      validators: [REQUIRED_VALIDATOR, DATE_VALIDATOR],
      order: 1,
      config: {
        isRange: false,
        placeholder: 'component.date-time-picker.select-date',
        inputType: DateTimeTypeEnum.CALENDAR
      }
    }),
    new DropdownQuestion({
      key: 'idDailyProfile',
      label: 'component.channel-range-upsert.daily-profile',
      value: profile.idDailyProfile,
      validators: [REQUIRED_VALIDATOR],
      options: map(channelRange.jsonAlgorithm.dailyProfiles, (dailyProfile) => ({key: +dailyProfile.id, value: +dailyProfile.id})),
      order: 2
    }),
    new DropdownQuestion({
      key: 'recurrentPeriod',
      label: 'global.type',
      value: moment(profile.startPeriod, 'YYYY-MM-DD').get('y') === 0 ? 1 : -1,
      validators: [REQUIRED_VALIDATOR],
      options: [
        { key: translateService.instant('component.channel-range-upsert.recurrent-period'), value: 1 },
        { key: translateService.instant('component.channel-range-upsert.not-recurrent-period'), value: -1 }
      ],
      order: 2
    }),
  ];
};
