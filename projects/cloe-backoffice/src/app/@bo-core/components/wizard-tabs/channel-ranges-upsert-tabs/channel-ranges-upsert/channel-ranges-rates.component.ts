import { ChangeDetectorRef, Component, Input, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { CloeWizardComponentInterface } from '@cloe-core/@core/interfaces/cloe-wizard-component.interface';
import { range, map, difference, filter, isNull, isNaN, set, remove, cloneDeep, isNil, findIndex } from 'lodash';
import { ratesTableSubject$, TABLE_CONFIG } from './rates-table.config';
import { CloeDynamicFormComponent } from '@cloe-core/@core/@ui/components';
import { ACTIONS } from '@cloe-core/@core/enums/ActionsEnum';
import { getRatesForm } from './rates-form-costant';

@Component({
  selector: 'cloe-channel-range-rates',
  templateUrl: './channel-ranges-rates.component.html',
  styleUrls: ['./channel-ranges-rates.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ChannelRangesRatesComponent implements OnInit, OnDestroy, CloeWizardComponentInterface {
  @ViewChild(CloeDynamicFormComponent) ratesForm: CloeDynamicFormComponent;
  @Input() data;
  rates = [];
  currentRate;
  tableConfig = TABLE_CONFIG;
  ratesQuestions = [];

  ratesTableSubscription;

  constructor(
    private cdRef: ChangeDetectorRef,
  ) {
    this.ratesTableSubscription = ratesTableSubject$.asObservable().subscribe((event: any) => {
      switch (event.action) {
        case ACTIONS.NEW_ROW:
          this.ratesQuestions = getRatesForm({});
          this.currentRate = - 1;
          this.cdRef.detectChanges();
          break;
        case ACTIONS.EDIT_ROW:
          this.ratesQuestions = getRatesForm(event.data);
          this.currentRate = findIndex(this.rates, event.data);
          this.cdRef.detectChanges();
          break;
        case ACTIONS.DELETE:
          remove(this.rates, event.data);
          this.rates = cloneDeep(this.rates);
          this.currentRate = undefined;
          this.cdRef.detectChanges();
          break;
      }
    });
  }

  ngOnInit() {
    if (this.data.jsonAlgorithm) {
      this.rates = this.data.jsonAlgorithm.rates;
    } else {
      set(this.data, 'jsonAlgorithm.rates', []);
      this.rates = this.data.jsonAlgorithm.rates;
    }
  }

  get isValid() {
    return !this.ratesForm &&
      this.data.jsonAlgorithm &&
      (this.data.jsonAlgorithm.rates && this.data.jsonAlgorithm.rates.length > 0);
  }

  onRateSubmitted($event: any) {
    const rate = {
      id: 0,
      name: $event.name,
      color: $event.color
    };
    if (this.currentRate === - 1) {
      const avaiableIDs = filter(
        difference(range(1, this.rates.length + 2), map(this.rates, (r) => r.id)),
        (idElement) => !isNull(idElement) && !isNaN(idElement)
      );
      rate.id = +avaiableIDs[0];
      this.rates.push(rate);
    } else {
      rate.id = this.rates[this.currentRate].id;
      this.rates[this.currentRate] = rate;
    }
    this.rates = cloneDeep(this.rates);
    this.currentRate = undefined;
    this.cdRef.detectChanges();
  }

  onRateCancelled($event: any) {
    this.currentRate = undefined;
    this.cdRef.detectChanges();
  }

  getRateFormVisible() {
    return !isNil(this.currentRate);
  }

  ngOnDestroy(): void {
    this.ratesTableSubscription.unsubscribe();
  }
}
