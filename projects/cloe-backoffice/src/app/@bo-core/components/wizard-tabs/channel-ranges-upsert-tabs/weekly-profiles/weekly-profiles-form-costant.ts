import { map } from 'lodash';
import {
  REQUIRED_VALIDATOR
} from '@cloe-core/@core/@ui/components/cloe-dynamic-form/models/validators/validators';
import { DropdownQuestion } from '@cloe-core/@core/@ui/components/cloe-dynamic-form/models/DropdownQuestion';

export const getWeeklyProfileForm = (channelRange, profile) => {
  return [
    new DropdownQuestion({
      key: 'dailyProfile',
      label: 'component.channel-range-upsert.daily-profile',
      value: +profile,
      validators: [ REQUIRED_VALIDATOR ],
      options: map(channelRange.jsonAlgorithm.dailyProfiles, (dailyProfile) => ({key: dailyProfile.id, value: dailyProfile.id })),
      order: 1
    })
  ];
};
