import { ACTIONS } from '@cloe-core/@core/enums/ActionsEnum';
import { Subject } from 'rxjs';
import {ICloeTableStatic} from '@cloe-core/@core/@ui/components/cloe-table-static/cloe-table-static-interface/cloe-table-static-interface';

export const specialProfileSubject$ = new Subject();

export const TABLE_CONFIG: ICloeTableStatic = {
  header: [
    {
      fieldName: 'startPeriod',
      columnName: 'global.date',
      pipe: 'cloeDate'
    },
    {
      fieldName: 'idDailyProfile',
      columnName: 'component.channel-range-upsert.daily-profile'
    }
  ],
  actions: [
    {
      icon: 'edit',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: specialProfileSubject$,
        action: ACTIONS.EDIT_ROW
      }
    },
    {
      icon: 'delete',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: specialProfileSubject$,
        action: ACTIONS.DELETE,
        deleteByField: 'name',
        altMessage: 'global.delete'
      }
    }
  ],
  metadata: {
    tableTitle: 'component.channel-range-upsert.weekly-profiles',
    addNewButton: {
      actionType: ACTIONS.TRIGGER_SUBJECT,
      action: ACTIONS.NEW_ROW,
      triggerSubject: specialProfileSubject$
    },
    disableSort: true
  }
};
