import { ChangeDetectorRef, Component, Input, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { CloeWizardComponentInterface } from '@cloe-core/@core/interfaces/cloe-wizard-component.interface';
import { set, findIndex, isNil, cloneDeep, remove } from 'lodash';
import { CloeDynamicFormComponent } from '@cloe-core/@core/@ui/components';
import { ACTIONS } from '@cloe-core/@core/enums/ActionsEnum';
import { getSpecialProfileForm } from './special-profiles-form-costant';
import { specialProfileSubject$, TABLE_CONFIG } from './special-profiles-table.config';
import * as _moment from 'moment';
import { TranslateService } from '@ngx-translate/core';

const moment = _moment;

@Component({
  selector: 'cloe-channel-range-special-profiles',
  templateUrl: './special-profiles.component.html',
  styleUrls: ['./special-profiles.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class SpecialProfilesComponent implements OnInit, OnDestroy, CloeWizardComponentInterface {
  @ViewChild(CloeDynamicFormComponent) specialProfileForm: CloeDynamicFormComponent;
  @Input() data;
  specialProfiles = [];
  currentSpecialProfile;
  tableConfig = TABLE_CONFIG;
  specialQuestions = [];

  specialTableSubscription;

  constructor(
    private cdRef: ChangeDetectorRef,
    private translateService: TranslateService
  ) {
    this.specialTableSubscription = specialProfileSubject$.asObservable().subscribe((event: any) => {
      switch (event.action) {
        case ACTIONS.NEW_ROW:
          this.specialQuestions = getSpecialProfileForm(this.data, {}, this.translateService);
          this.currentSpecialProfile = - 1;
          this.cdRef.detectChanges();
          break;
        case ACTIONS.EDIT_ROW:
          this.specialQuestions = getSpecialProfileForm(this.data, event.data, this.translateService);
          this.currentSpecialProfile = findIndex(this.specialProfiles, event.data);
          this.cdRef.detectChanges();
          break;
        case ACTIONS.DELETE:
          remove(this.specialProfiles, event.data);
          this.specialProfiles = cloneDeep(this.specialProfiles);
          this.currentSpecialProfile = undefined;
          this.cdRef.detectChanges();
          break;
      }
    });
  }

  ngOnInit() {
    setTimeout(() => {
      if (!this.data.jsonAlgorithm.specialProfiles) {
        set(this.data, 'jsonAlgorithm.specialProfiles', []);
      }
      this.specialProfiles = this.data.jsonAlgorithm.specialProfiles;
    });
  }

  get isValid() {
    return !this.specialProfileForm && this.data.jsonAlgorithm.specialProfiles;
  }

  onSpecialProfileSubmitted($event: any) {
    const period = +$event.recurrentPeriod === 1 ?
      moment($event.period).set('y', 0).format('YYYY-MM-DD') : moment($event.period).format('YYYY-MM-DD');
    const specialProfile = {
      idDailyProfile: +$event.idDailyProfile,
      startPeriod: period,
      endPeriod: period
    };
    if (this.currentSpecialProfile === - 1) {
      this.specialProfiles.push(specialProfile);
    } else {
      this.specialProfiles[this.currentSpecialProfile] = specialProfile;
    }
    this.specialProfiles = cloneDeep(this.specialProfiles);
    this.currentSpecialProfile = undefined;
    this.cdRef.detectChanges();
  }

  onSpecialProfileCancelled($event: any) {
    this.currentSpecialProfile = undefined;
    this.cdRef.detectChanges();
  }

  getSpecialFormVisible() {
    return !isNil(this.currentSpecialProfile);
  }

  ngOnDestroy() {
    this.specialTableSubscription.unsubscribe();
  }
}
