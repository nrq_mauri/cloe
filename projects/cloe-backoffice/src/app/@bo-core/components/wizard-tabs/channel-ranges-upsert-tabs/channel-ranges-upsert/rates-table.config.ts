import { ACTIONS } from '@cloe-core/@core/enums/ActionsEnum';
import { Subject } from 'rxjs';
import {ICloeTableStatic} from '@cloe-core/@core/@ui/components/cloe-table-static/cloe-table-static-interface/cloe-table-static-interface';

export const ratesTableSubject$ = new Subject();

export const TABLE_CONFIG: ICloeTableStatic = {
  header: [
    {
      fieldName: 'id',
      columnName: 'Id'
    },
    {
      fieldName: 'name',
      columnName: 'global.name'
    },
    {
      fieldName: 'color',
      columnName: 'global.color'
    }
  ],
  actions: [
    {
      icon: 'edit',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: ratesTableSubject$,
        action: ACTIONS.EDIT_ROW
      }
    },
    {
      icon: 'delete',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: ratesTableSubject$,
        action: ACTIONS.DELETE,
        deleteByField: 'name',
        altMessage: 'global.delete'
      }
    }
  ],
  metadata: {
    tableTitle: 'component.channel-range-upsert.rates',
    addNewButton: {
      actionType: ACTIONS.TRIGGER_SUBJECT,
      action: ACTIONS.NEW_ROW,
      triggerSubject: ratesTableSubject$
    }
  }
};
