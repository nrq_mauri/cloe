import { ChangeDetectorRef, Component, Input, OnDestroy, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { CloeWizardComponentInterface } from '@cloe-core/@core/interfaces/cloe-wizard-component.interface';
import { set, findIndex, range, map, values, cloneDeep, isNil, dropRight, last } from 'lodash';
import { CloeDynamicFormComponent } from '@cloe-core/@core/@ui/components';
import { TABLE_CONFIG, weeklyProfileSubject$ } from './weekly-profiles-table.config';
import { TranslateService } from '@ngx-translate/core';
import { ACTIONS } from '@cloe-core/@core/enums/ActionsEnum';
import { getWeeklyProfileForm } from './weekly-profiles-form-costant';
import * as _moment from 'moment';

const moment = _moment;
@Component({
  selector: 'cloe-channel-range-weekly-profiles',
  templateUrl: './weekly-profiles.component.html',
  styleUrls: ['./weekly-profiles.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class WeeklyProfilesComponent implements OnInit, OnDestroy, CloeWizardComponentInterface {
  @ViewChild(CloeDynamicFormComponent) weeklyProfileForm: CloeDynamicFormComponent;
  @Input() data;
  weeklyProfiles = [];
  currentWeeklyProfile;
  tableConfig = TABLE_CONFIG;
  weeklyQuestions = [];
  weekdays;

  weeklyTableSubscription;

  constructor(
    private cdRef: ChangeDetectorRef,
    private translateService: TranslateService
  ) {
    this.weeklyTableSubscription = weeklyProfileSubject$.asObservable().subscribe((event: any) => {
      switch (event.action) {
        case ACTIONS.EDIT_ROW:
          this.weeklyQuestions = getWeeklyProfileForm(this.data, event.data.dailyProfile);
          this.currentWeeklyProfile = findIndex(this.weeklyProfiles, event.data);
          this.cdRef.detectChanges();
          break;
      }
    });
  }

  ngOnInit() {
    this.weekdays = values(this.translateService.instant('weekdays'));
    this.weekdays = [last(this.weekdays), ...dropRight(this.weekdays)];
    setTimeout(() => {
      if (!this.data.jsonAlgorithm.weeklyProfiles) {
        set(
          this.data,
          'jsonAlgorithm.weeklyProfiles',
          map(range(7), () => this.data.jsonAlgorithm.dailyProfiles[0].id)
        );
      }
      this.weeklyProfiles = map(this.data.jsonAlgorithm.weeklyProfiles, (weeklyProfile, index) => {
        const newWeeklyProfile: any = {};
        newWeeklyProfile.day = this.weekdays[+index];
        newWeeklyProfile.dailyProfile = weeklyProfile;
        return newWeeklyProfile;
      });
    });
  }

  get isValid() {
    return !this.weeklyProfileForm && (this.data.jsonAlgorithm.weeklyProfiles && this.data.jsonAlgorithm.weeklyProfiles.length > 0);
  }

  onWeeklyProfileSubmitted($event: any) {
    this.weeklyProfiles[this.currentWeeklyProfile].dailyProfile = +$event.dailyProfile;
    this.weeklyProfiles = cloneDeep(this.weeklyProfiles);
    this.data.jsonAlgorithm.weeklyProfiles = map(this.weeklyProfiles, (profile) => profile.dailyProfile);
    this.currentWeeklyProfile = undefined;
    this.cdRef.detectChanges();
  }

  onWeeklyProfileCancelled($event: any) {
    this.currentWeeklyProfile = undefined;
    this.cdRef.detectChanges();
  }

  getWeeklyProfileFormVisible() {
    return !isNil(this.currentWeeklyProfile);
  }

  ngOnDestroy() {
    this.weeklyTableSubscription.unsubscribe();
  }
}
