import { AfterViewInit, Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { CloeWizardComponentInterface } from '@cloe-core/@core/interfaces/cloe-wizard-component.interface';
import { JobUpsertDto } from '../../../modals/job-upsert/job-upsert-dto';

@Component({
  selector: 'cloe-job-cron-expression',
  templateUrl: './job-cron-expression-step.component.html',
  styleUrls: ['./job-cron-expression-step.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class JobCronExpressionStepComponent implements OnInit, AfterViewInit, CloeWizardComponentInterface {

  @ViewChild('jobForm') jobForm;
  @Input() data: JobUpsertDto;

  constructor() {}

  ngOnInit() {
  }

  ngAfterViewInit() {
  }

  get isValid() {
    return !!this.data.job.cron;
  }
}

