import { AfterViewInit, Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { CloeWizardComponentInterface } from '@cloe-core/@core/interfaces/cloe-wizard-component.interface';
import { JobUpsertDto } from '../../../modals/job-upsert/job-upsert-dto';
import { CommonService } from '@cloe-core/@core/services/common-service/common-service';

@Component({
  selector: 'cloe-job-config-selection',
  templateUrl: './job-config-selection-step.component.html',
  styleUrls: ['./job-config-selection-step.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class JobConfigSelectionStepComponent implements OnInit, AfterViewInit, CloeWizardComponentInterface {

  @ViewChild('jobForm') jobForm;
  @Input() data: JobUpsertDto;
  statuses;

  constructor(
    private commonService: CommonService,
  ) {}

  ngOnInit() {
    this.commonService.getCommonStatus().subscribe(resp => {
      this.statuses = resp;
    });
  }

  ngAfterViewInit() {
  }

  get isValid() {
    return this.jobForm ? this.jobForm.form.valid : false;
  }

  compareFn( s1, s2 ): boolean {
    return s1 && s2 ? s1.id === s2.id : s1 === s2;
  }
}

