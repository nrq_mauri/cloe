import { Component, Input, OnInit, ViewChild, ViewEncapsulation } from '@angular/core';
import { CloeWizardComponentInterface } from '@cloe-core/@core/interfaces/cloe-wizard-component.interface';
import { JobUpsertDto } from '../../../modals/job-upsert/job-upsert-dto';
import { QuestionBase } from '@cloe-core/@core/@ui/components/cloe-dynamic-form/models/QuestionBase';
import { TextboxQuestion } from '@cloe-core/@core/@ui/components/cloe-dynamic-form/models/TextboxQuestion';
import { CloeDynamicFormComponent } from '@cloe-core/@core/@ui/components';
import { JobConfigParamTypeEnum } from '@cloe-core/@core/enums/JobConfigParamTypeEnum';
import { forIn, isString } from 'lodash';
import {
  DatepickerQuestion
} from '@cloe-core/@core/@ui/components/cloe-dynamic-form/models/DatepickerQuestion';
import {
  DateTimeTypeEnum
} from '@cloe-core/@core/@ui/components/cloe-date-time-picker/enums/DateTimeTypeEnum';
import {
  DATE_RANGE_VALIDATOR,
  DATE_VALIDATOR,
  EMAIL_VALIDATOR,
  NUMBER_VALIDATOR,
  REQUIRED_VALIDATOR
} from '@cloe-core/@core/@ui/components/cloe-dynamic-form/models/validators/validators';
import * as moment_ from 'moment';
import {
  TreeNodeActiveIDsQuestion
} from '@cloe-core/@core/@ui/components/cloe-dynamic-form/models/TreeNodeActiveIDsQuestion';
import { StorageService } from '@cloe-core/@core/services/storage-service/storage.service';
import { map } from 'rxjs/operators';
import { TreeFiltersTypeEnum } from '@cloe-core/@core/services/cloe-tree-service/enums/TreeFiltersTypeEnum';
import { CloeTreeNodeService } from '@cloe-core/@core/services/cloe-treeNode-service/cloe-tree-node.service';
const moment = moment_;

@Component({
  selector: 'cloe-job-config-params',
  templateUrl: './job-config-params-step.component.html',
  styleUrls: ['./job-config-params-step.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class JobConfigParamsStepComponent implements OnInit, CloeWizardComponentInterface {

  @ViewChild(CloeDynamicFormComponent) dynamicForm;
  @Input() data: JobUpsertDto;
  questions = [];

  constructor(
    private treeNodeService: CloeTreeNodeService
  ) {
  }

  ngOnInit() {
    this.questions = this.buildQuestions();
  }

  get isValid() {
    return this.dynamicForm ? this.dynamicForm.isValid : false;
  }

  public onDynamicFormChanged(values) {
    const jsonParams = this.data.job.jsonParams ? JSON.parse(this.data.job.jsonParams) : {};
    forIn(values, (value, key) => {
      jsonParams[key] = value;
    });
    this.data.job.jsonParams = JSON.stringify(jsonParams);
  }

  private buildQuestions() {
    const questions: QuestionBase<any>[] = [];
    const jsonParams = this.data.job.jsonParams ? JSON.parse(this.data.job.jsonParams) : {};
    this.data.job.jobConfig.jobConfigParams.forEach( (param) => {
      const paramValue = jsonParams[param.paramKey];
      switch (param.type) {
        case JobConfigParamTypeEnum.NUMBER:
          questions.push(
            new TextboxQuestion({
              key: param.paramKey,
              label: 'page.job.' + param.paramLabel,
              value: paramValue ? paramValue : param.paramValue,
              type: 'number',
              validators: [ REQUIRED_VALIDATOR, NUMBER_VALIDATOR ],
              order: 1
            })
          );
          break;
        case JobConfigParamTypeEnum.STRING:
          questions.push(
            new TextboxQuestion({
              key: param.paramKey,
              label: 'page.job.' + param.paramLabel,
              value: paramValue ? paramValue : param.paramValue,
              type: 'text',
              validators: [ REQUIRED_VALIDATOR ],
              order: 1
            })
          );
          break;
        case JobConfigParamTypeEnum.EMAIL:
          questions.push(
            new TextboxQuestion({
              key: param.paramKey,
              label: 'page.job.' + param.paramLabel,
              value: paramValue ? paramValue : param.paramValue,
              validators: [ REQUIRED_VALIDATOR, EMAIL_VALIDATOR ],
              order: 1
            })
          );
          break;
        case JobConfigParamTypeEnum.DATE:
          questions.push(
            new DatepickerQuestion({
              key: param.paramKey,
              label: 'page.job.' + param.paramLabel,
              value: paramValue ? moment(paramValue) : moment(param.paramValue),
              validators: [ REQUIRED_VALIDATOR, DATE_VALIDATOR ],
              order: 2,
              config: {
                isRange: false,
                placeholder: 'component.date-time-picker.select-date',
                inputType: DateTimeTypeEnum.CALENDAR
              }
            })
          );
          break;
        case JobConfigParamTypeEnum.RANGE:
          const defaultValue: any = isString(param.paramValue) ?
            JSON.parse(param.paramValue) :
            param.paramValue;

          questions.push(
            new DatepickerQuestion({
              key: param.paramKey,
              label: 'page.job.' + param.paramLabel,
              value: paramValue ? paramValue : defaultValue,
              order: 2,
              validators: [ REQUIRED_VALIDATOR, DATE_RANGE_VALIDATOR ],
              config: {
                isRange: true,
                placeholder: 'component.date-time-picker.select-date-range',
                inputType: DateTimeTypeEnum.CALENDAR
              }
            })
          );
          break;
        case JobConfigParamTypeEnum.TIME:
          questions.push(
            new DatepickerQuestion({
              key: param.paramKey,
              label: 'page.job.' + param.paramLabel,
              value: paramValue ? moment(paramValue) : moment(param.paramValue),
              order: 2,
              validators: [ REQUIRED_VALIDATOR, DATE_VALIDATOR ],
              config: {
                isRange: false,
                placeholder: 'component.date-time-picker.select-timer',
                inputType: DateTimeTypeEnum.BOTH
              }
            })
          );
          break;
        case JobConfigParamTypeEnum.SITES_TREE:
          const sitesTreedefaultValue: any = isString(param.paramValue) ?
            JSON.parse(param.paramValue) :
            param.paramValue;

          questions.push(
            new TreeNodeActiveIDsQuestion({
              key: param.paramKey,
              label: 'page.job.' + param.paramLabel,
              value: paramValue ? paramValue : sitesTreedefaultValue,
              filtersType: TreeFiltersTypeEnum.SITE_FILTERS,
              order: 3,
              nodes$: this.treeNodeService.getTreeNodeByReferUuid(
                'SITE',
                StorageService.getCurrentUser(),
                true,
                'SITE',
                true
              ).pipe(
                map( result => {
                  return [result];
                })
              )
            })
          );
          break;
        case JobConfigParamTypeEnum.CHANNELS_TREE:
          // TODO endpoint to get all channels by user
          break;
      }
    });
    return questions.sort((a, b) => a.order - b.order);
  }
}

