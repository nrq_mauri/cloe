import { AfterViewInit, Component, Input, OnInit, ViewEncapsulation } from '@angular/core';
import { CloeWizardComponentInterface } from '@cloe-core/@core/interfaces/cloe-wizard-component.interface';
import { JobUpsertDto } from '../../../modals/job-upsert/job-upsert-dto';
import { remove, isString, cloneDeep, keys, keyBy } from 'lodash';
import {
  Destination,
  DestinationTypeEnum
} from '@cloe-core/@core/models/Job';
import { Validators } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { staticTableSubject$ } from '@cloe-core/@core/@ui/components/cloe-table-static';
import { ACTIONS } from '@cloe-core/@core/enums/ActionsEnum';
import { TABLE_CONFIG } from './destinations-table.config';

@Component({
  selector: 'cloe-job-destinations',
  templateUrl: './job-destinations-step.component.html',
  styleUrls: ['./job-destinations-step.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class JobDestinationsStepComponent implements OnInit, AfterViewInit, CloeWizardComponentInterface {

  staticTableSubject;
  tableConfig = TABLE_CONFIG;

  @Input() data: JobUpsertDto;

  newDestinationClosed = true;
  updateDestinationClosed = true;

  destinationTypesEnum = DestinationTypeEnum;
  destinationTypesValues = Object.keys(this.destinationTypesEnum);

  selectedDestinationType;
  selectedDestination: Destination;

  newDestination;

  domainPickerCC = {
    labelSearch: 'component.job-upsert.cc',
    separatorKeys: [';'],
    validators: [ Validators.email ],
    validationErrorMessages: {
      'email': this.translateService.instant('validation.generic.email')
    }
  };

  domainPickerCCN = {
    labelSearch: 'component.job-upsert.ccn',
    separatorKeys: [';'],
    validators: [ Validators.email ],
    validationErrorMessages: {
      'email': this.translateService.instant('validation.generic.email')
    }
  };

  constructor(
    private translateService: TranslateService
  ) {
    this.staticTableSubject = staticTableSubject$.asObservable().subscribe( (e: any) => {
      switch (e.action) {
        case ACTIONS.EDIT_ROW:
          this.selectedDestination = e.data;
          this.onUpdateDestination();
          break;
        case ACTIONS.DELETE:
          this.selectedDestination = e.data;
          this.onDeleteDestination();
          break;
      }
    });
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
  }

  get isValid() {
    return (this.updateDestinationClosed && this.newDestinationClosed);
  }

  onDeleteDestination() {
    if (this.selectedDestination && this.updateDestinationClosed)  {
      remove(this.data.job.destinations, this.selectedDestination);
      this.data.job.destinations = cloneDeep(this.data.job.destinations);
      this.selectedDestination = undefined;
    }
  }

  onSubmit(destination, update) {
    if (update) {
      this.updateDestinationClosed = true;
      this.selectedDestination.destinationJson = JSON.stringify(this.selectedDestination.destinationJson);
      this.selectedDestination = undefined;
    } else {
      this.newDestination.destinationType = this.selectedDestinationType;
      this.newDestination.destinationJson = JSON.stringify(this.newDestination.destinationJson);
      this.data.job.destinations ?
        this.data.job.destinations = [...this.data.job.destinations, this.newDestination] :
        this.data.job.destinations = [ this.newDestination ];
      this.newDestination = undefined;
      this.newDestinationClosed = true;
    }
  }

  onNewDestination() {
    if (!this.selectedDestinationType || !this.updateDestinationClosed) {
      return;
    }
    this.newDestination = new Destination(this.selectedDestinationType);
    this.newDestinationClosed = !this.newDestinationClosed;
  }

  onUpdateDestination() {
    if (!this.selectedDestination || !this.newDestinationClosed) {
      return;
    }
    if (isString(this.selectedDestination.destinationJson)) {
      this.selectedDestination.destinationJson = JSON.parse(this.selectedDestination.destinationJson);
    } else {
      if (!this.updateDestinationClosed) {
        this.selectedDestination.destinationJson = JSON.stringify(this.selectedDestination.destinationJson);
      }
    }
    this.updateDestinationClosed = !this.updateDestinationClosed;
  }
}
