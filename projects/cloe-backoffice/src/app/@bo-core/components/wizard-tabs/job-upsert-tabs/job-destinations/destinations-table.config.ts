import { ACTIONS } from '@cloe-core/@core/enums/ActionsEnum';
import { staticTableSubject$ } from '@cloe-core/@core/@ui/components/cloe-table-static';
import {ICloeTableStatic} from '@cloe-core/@core/@ui/components/cloe-table-static/cloe-table-static-interface/cloe-table-static-interface';

export const TABLE_CONFIG: ICloeTableStatic = {
  header: [
    {
      fieldName: 'name',
      columnName: 'global.name'
    }
  ],
  actions: [
    {
      icon: 'edit',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: staticTableSubject$,
        action: ACTIONS.EDIT_ROW,
        altMessage: 'global.edit'
      }
    },
    {
      icon: 'delete',
      actionType: ACTIONS.TRIGGER_SUBJECT,
      metadata: {
        triggerSubject: staticTableSubject$,
        action: ACTIONS.DELETE,
        deleteByField: 'name',
        altMessage: 'global.delete'
      }
    }
  ],
  metadata: {
    tableTitle: 'component.job-upsert.destinations'
  }
};
