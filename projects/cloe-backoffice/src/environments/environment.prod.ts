import {FeTypesEnum} from '@cloe-core/@core/enums/FeTypesEnum';

export const environment = {
  production: true,
  envName: 'prod',
  feType: FeTypesEnum.BO,
  feHostUrl: 'https://cloe.energyteam.it'
};
